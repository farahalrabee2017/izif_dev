function init() {
    window.addEventListener('scroll', function (e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 50,
            header = document.querySelector(".navbar");
        if (distanceY > shrinkOn) {
            classie.add(header, "smaller");
        } else {
            if (classie.has(header, "smaller")) {
                classie.remove(header, "smaller");
            }
        }
    });
}

try {
    init();
} catch (e) {
    console.log(e);
}


//Change Language Button
var $changeLang = $('button.lang-change');
$changeLang.click(function () {
    var lang = $(this).data('lang');
    var replacefrom = lang == 'ar' ? 'en' : 'ar';
    var url = window.location.href.replace(replacefrom, lang);

    if(url.indexOf(lang) < 0){
        url = url + '/' + lang;
    }

    url = url.split('/'+lang)[0] + "/" + lang;
    window.location.href = url;

});







// Smooth Scrolling
$('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
});



