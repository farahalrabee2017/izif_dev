<!doctype html>
<html>
<head>

    <title>@yield('title','Izif | Music School')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" type="image/x-icon" href='{{url("/static/img/i3zef-logo.ico")}}'/>
    <meta name="description" content="izif">
    <meta name="author" content="izif">


    <link href='{{url("static/bootstrap-3.3.6-dist/css/bootstrap.css")}}' rel="stylesheet">
    <link rel="stylesheet" href='{{url("static/font-awesome-4.5.0/css/font-awesome.min.css")}}'>
    <link rel="stylesheet" type="text/css" href='{{url("static/style/main.css")}}'>


    @yield('head')
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
@yield('video')

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container clearfix">
        <div class="groupbutton">
            <a href='{{url("/" . Lang::getLocale() . "/subscribe")}}'>
                <div class="signupbutton">

                <span>
                    {{ trans('course.subscribe_monthly') }}
                </span>

                    <strong> 19.99 $</strong>
                </div>
            </a>

            <a href='{{url("/" . Lang::getLocale() . "/intro")}}'>
                <div class="freebutton">

                <span>
                    جرب مجاناً !
                </span>
                </div>
            </a>
        </div>
        <div class="nav navbar-nav navbar-right">
            <ul class="top_menu">
                <!--<li>$USD <img src='{{url("static/img/small-dollar-sign.png")}}'></li>-->
                <li>
                    <div class="btn-group">
                        <button type="button" class=" currency btn btn-custom btn-custom-en btn-default dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"><img src='{{url("static/img/usd.png")}}'
                                                                                width="17px"/> USD<span
                                    class="caret caret-custom"></span></button>
                        <ul class="dropdown-menu dropdown-custom">

                            <li><a href="#"><img src='{{url("static/img/usd.png")}}' width="17px"/> USD</a></li>
                            <li><a href="#"><img src='{{url("static/img/sar.png")}}' width="17px"/> SAR</a></li>
                            <li><a href="#"><img src='{{url("static/img/egp.png")}}' width="17px"/> EGP</a></li>
                            <li><a href="#"><img src='{{url("static/img/jod.png")}}' width="17px"/> JOD</a></li>
                            <li><a href="#"><img src='{{url("static/img/aed.png")}}' width="17px"/> AED</a></li>
                            <li><a href="#"><img src='{{url("static/img/euro.png")}}' width="17px"/> EUR</a></li>

                        </ul>
                    </div>
                </li>
                <li>
                    <!--English <img src='{{url("static/img/english-icon.png")}}'>-->
                    <!--<div class="btn-group">
                        <button type="button" class="btn btn-custom btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            English
                            <span class="caret caret-custom"></span></button>
                        <ul class="dropdown-menu dropdown-custom">
                            <li><a href="#">عربي</a></li>
                            <li><a href="#">English</a></li>

                        </ul>
                    </div>-->

                    @if(Lang::getLocale() == 'ar')
                        <button type="button" class="btn btn-default lang-change" data-lang="en"><i
                                    class="fa fa-language"></i> English
                        </button>
                    @else
                        <button type="button" data-lang="ar" class="btn btn-default lang-change">عربي
                            <i class="fa fa-language"></i>
                        </button>
                    @endif

                </li>
            </ul>
            <ul class="register_and_profile">
                @if (Auth::check())
                    <li><a href='{{url("/" . Lang::getLocale() . "/profile")}}'> <i
                                    class="fa fa-user"></i> {{ trans('course.myprofile') }} </a></li>
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/logout")}}'> <i
                                    class="fa fa-sign-out"></i> {{ trans('course.logout') }} </a></li>
                @else
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/register")}}'> <i
                                    class="fa fa-user"></i> {{ trans('course.register') }} </a></li>
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/login")}}'> <i
                                    class="fa fa-sign-in"></i> {{ trans('course.login') }} </a></li>
                @endif
            </ul>
        </div>
        <div class="menu">
            <ul>
                <li><a href='{{url("/" . Lang::getLocale() . "/courses")}}'>الدورات الموسيقية <img
                                src='{{url("static/img/dots-icon.png")}}'></a>
                </li>

                <li><a href='{{url("/" . Lang::getLocale() . "/free/videos")}}'>
                        الدروس المجانية
                    </a></li>

                <li><a href='{{url("/" . Lang::getLocale() . "/subscribe")}}'>
                        الآشتراكات
                    </a></li>
            </ul>
        </div>
        <div class="toplogo "><a href='{{url("/" . Lang::getLocale() . "/")}}'><img
                        src='{{url("static/img/i3zef-logo.png")}}'></a></div>

    </div>
</div>


@yield('content')


<footer class="footer">
    <div class="inner-footer container">
        <div class="footer_logo"><a href='{{url("/" . Lang::getLocale() . "/")}}'><img
                        src='{{url("static/img/footer-logo.png")}}'></a></div>
        <ul>
            <li><a href="#"><img src='{{url("static/img/dots-icon.png")}}' width="8" hight="8">الدورات الموسيقية</a>
            </li>
            <li><a href="#"><i class="fa fa-circle"></i>من نحن</a></li>
            <li><a href="#"><i class="fa fa-circle"></i>إتصل بنا</a></li>
            <li><a href="#"><i class="fa fa-circle"></i>التسجيل</a></li>
            <li><a href="#"><i class="fa fa-circle"></i>حسابي</a></li>
            <li><a href="#"><i class="fa fa-circle"></i>الآشتراكات</a></li>
        </ul>
        <div class="social_icon">
            <ul>
                <li><a href="https://www.facebook.com/i3zif"> <i class="fa fa-facebook-square"></i></a></li>
                <li><a href="https://twitter.com/i3zif"> <i class="fa fa-twitter-square"></i></a></li>
                <li><a href="https://www.youtube.com/user/i3zif"> <i class="fa fa-youtube-square"></i></a></li>
                <li><a href="https://www.instagram.com/izifcom/"> <i class="fa fa-instagram"></i></a></li>
                <!--<li><a href=""> <i class="fa fa-google-plus-square"></i></a></li>-->
            </ul>
        </div>
        <div class="copy_right"><span>+0034 43244 44   |   Mon - Fri / 9.00AM - 06.00PM   |   Questions? questions@izif
                .com</span></div>
    </div>

</footer>
<script type="text/javascript" src='{{url("static/js/jquery-1.11.3.min.js")}}'></script>
<script type="text/javascript" src='{{url("static/bootstrap-3.3.6-dist/js/bootstrap.min.js")}}'></script>
<script type="text/javascript" src='{{url("static/js/classie.js")}}'></script>
<script type="text/javascript" src='{{url("static/js/main.js")}}'></script>
<script>

</script>
@yield('footer')
<div style="clear:both"></div>
<div style="display: none;position: absolute;top:0px;left: 0px;background: #ECF0F1;width:100%;height: 150%"
     class="preloader">
    <center><img src="http://www.averaraonline.com.br/estilos/loja/loja_front_averara/images/preload.gif"/></center>
    <div style="clear:both"></div>
</div>
</body>
</html>