@extends('master')

@section('head')
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }

        body {
            background: #333;
            font-family: 'i3zef';
            background: url('/static/img/bg_izif.png') #000;
            background-repeat: no-repeat;
            background-size: 110%;
            background-position: left 50px;
        }

        div {
            direction: rtl;
        }

        @font-face {
            font-family: 'i3zef';
            src: url('ara_fonts/i3zef-Reg.eot');
        }

        @font-face {
            font-family: 'i3zef';
            src: url('ara_fonts/i3zef-Reg.ttf');
        }

        @font-face {
            font-family: 'i3zef Bold';
            src: url('ara_fonts/i3zef-Bold.eot');
        }

        @font-face {
            font-family: 'i3zef Bold';
            src: url('ara_fonts/i3zef-Bold.ttf');
        }

        input[placeholder="اسم المستخدم"], input[placeholder="Username"] {
            width: 100%;
            border-radius: 3px;
            margin: 4px;
            border-style: none;
            height: 30px;
            line-height: 30px;
            padding-right: 25px;
        }

        input[placeholder="كلمة المرور"], input[placeholder="Password"] {
            width: 100%;
            border-radius: 3px;
            margin: 4px;
            border-style: none;
            height: 30px;
            line-height: 30px;
            padding-right: 25px;
        }

        .userName {
            position: relative;
            width: 100%;
            height: auto;
        }

        .userName:after {
            position: absolute !important;
            right: 10px !important;
            top: 7px !important;
            z-index: 9999 !important;
            content: "\f007";
            font-family: FontAwesome;
        }

        .password {
            position: relative;
            width: 100%;
            height: auto;
            margin-bottom: 10px;
        }

        .password:after {
            position: absolute !important;
            right: 10px !important;
            top: 7px !important;
            z-index: 9999 !important;
            content: "\f084";
            font-family: FontAwesome;
        }

        .forgot_pass {
            background: transparent;
            color: #fff !important;
            border-style: none;
            outline: none;
        }

        input[type="checkbox"] {
            float: left !important;
            display: block !important;
        }

        .remember_me {
            float: left;
        }

        .remember_me span {
            color: orange;
            font-size: 12px;
        }

        .titlelogin {
            color: orange;
            font-size: 30px;

        }

        .main-continer {
            height: 100%;
            padding-top: 17%;
        }

        .form-container {
            background: rgba(49, 49, 49, 0.6);
            /* opacity: 0.7; */
            padding: 35px;
            border-radius: 9px;
            position: relative;
        }

        .titleregister_en {
            position: absolute;
            top: 3.4em;
            right: 3em;
            color: #fff;
            font-size: 14px;
        }

        .titleregister_ar {
            position: absolute;
            top: 3em;
            left: 2em;
            color: #fff;
        }

        input[type="radio"], input[type="checkbox"] {
            margin: 0 4px 0px 4px;
        }

        .remember_me_en {
            float: right;
            padding-top: 3px;
        }

        .remember_me_ar {
            float: right;
        }

        .titlelogin_en {
            text-align: left;
        }
    </style>
@endsection


@section('content')
<div class="container main-continer">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            @include('errors.errors')
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 form-container">


            <h3 class="titlelogin titlelogin_{{Lang::getLocale()}}">
                {{trans('course.login')}}
            </h3>

            <h3 class="titleregister titleregister_{{Lang::getLocale()}}">
                <a href='{{url("/" . Lang::getLocale() . "/auth/register")}}'>
                    {{trans('course.or_register')}}
                </a>
            </h3>
            <form method="POST" action="/{{Lang::getLocale()}}/auth/login">
                {!! csrf_field() !!}
                <div class="userName">
                    <input id="email" type="email" name="email" value="{{ old('email') }}"
                           placeholder="{{trans('course.username')}}">
                </div>
                <div class="password">
                    <input type="password" name="password" id="password" placeholder="{{trans('course.password')}}">
                </div>
                <a href="/{{Lang::getLocale()}}/password/email" class="forgot_pass">
                    {{trans('course.forget_password')}}
                </a>
                <div class="remember_me">
                    <div class="remember_me_{{Lang::getLocale()}}">
                    <span>
                        {{trans('course.remember_me')}}
                    </span>
                    </div>
                    <input type="checkbox" name="remember">


                </div>


                <hr/>
                <div class="form-group">
                    <div class="row">

                        <div class="col-md-9" style="padding: 7px;">
                            <a href="#" style="width: 100%!important;padding-top: 8px!important;padding-bottom: 8px!important;padding-left: 20px!important;padding-right: 20px!important" class="loginWithFacebookIntro" onclick="loginWithFacebook();"><i
                                        class="fa fa-facebook-official"></i> {{trans('course.login_with_facebook')}}</a>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-default" type="submit">
                                {{trans('course.login')}}
                            </button>
                        </div>
                    </div>

                </div>


            </form>


            <div class="form-group" style="display: none">

                <form class="form-facebook" style="display: none" action="{{url("/" . Lang::getLocale() . "/")}}/facebook/login" method="POST">
                    <input type="email" name="email"  />
                </form>



            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '430058543740438',
                xfbml      : true,
                version    : 'v2.8'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function loginWithFacebook() {

            //checkLoginState(function (status) {
            FB.getLoginStatus(function(status) {

                if (status.status != "connected") {
                    FB.login(function (response) {
                                console.log(response);
                                FB.api('/me?fields=id,email,name', function (response) {

                                    if (response.email) {

                                        var $form = $('.form-facebook');
                                        $('.form-facebook input[name="email"]').val(response.email);
                                        $form.submit();
                                    }

                                });
                            },
                            {
                                scope: 'public_profile, user_friends, email',
                                return_scopes: true
                            }
                    );
                } else {

                    FB.api('/me?fields=id,email,name', function (response) {
                        var $form = $('.form-facebook');
                        $('.form-facebook input[name="email"]').val(response.email);
                        $form.submit();
                    });
                }

            });

        }
    </script>

@endsection




