<!-- resources/views/auth/register.blade.php -->
@extends('master')

@section('title', trans('course.create_account_page_title'))
@section('description', trans('course.create_account_page_description'))

@section('head')
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }

        body {
            background: #333;
            font-family: 'i3zef';
            background: url('/static/img/bg_izif.png') #000;
            background-repeat: no-repeat;
            background-size: 125%;
            background-position: left 50px;
        }

        div {
            direction: rtl;
        }

        @font-face {
            font-family: 'i3zef';
            src: url('ara_fonts/i3zef-Reg.eot');
        }

        @font-face {
            font-family: 'i3zef';
            src: url('ara_fonts/i3zef-Reg.ttf');
        }

        @font-face {
            font-family: 'i3zef Bold';
            src: url('ara_fonts/i3zef-Bold.eot');
        }

        @font-face {
            font-family: 'i3zef Bold';
            src: url('ara_fonts/i3zef-Bold.ttf');
        }

        .userName, .email {
            position: relative;
            width: 100%;
            height: auto;
        }

        .userName:after {
            position: absolute !important;
            right: 10px !important;
            top: 7px !important;
            z-index: 999 !important;
            content: "\f007";
            font-family: FontAwesome;
        }

        .password {
            position: relative;
            width: 100%;
            height: auto;
        }

        .password:after {
            position: absolute !important;
            right: 10px !important;
            top: 7px !important;
            z-index: 999 !important;
            content: "\f084";
            font-family: FontAwesome;
        }

        .forgot_pass {
            background: transparent;
            color: #fff !important;
            border-style: none;
            outline: none;
        }

        input[type="checkbox"] {
            float: left !important;
            display: block !important;
        }

        .remember_me {
            float: left;
        }

        .remember_me span {
            color: orange;
            font-size: 12px;
        }

        .titlelogin {
            color: orange;
            font-size: 30px;

        }

        .main-continer {
            height: 100%;
            padding-top: 17%;
        }

        .form-container {
            background: rgba(49, 49, 49, 0.6);
            /* opacity: 0.7; */
            padding: 35px;
            border-radius: 9px;
        }

        label {
            color: #fff;
            padding: 10px;
            font-family: 'i3zef';
        }

        .userName:after {
            position: absolute !important;
            right: 10px !important;
            top: 65% !important;
            z-index: 999 !important;
            content: "\f007";
            font-family: FontAwesome;
        }

        .email:after {
            position: absolute !important;
            right: 10px !important;
            top: 65% !important;
            z-index: 999 !important;
            content: "\f0e0";
            font-family: FontAwesome;
        }

        .password:after {
            position: absolute !important;
            right: 10px !important;
            top: 65% !important;
            z-index: 999 !important;
            content: "\f084";
            font-family: FontAwesome;
        }

        input[type="text"], input[type="email"], input[type="password"] {
            padding-right: 30px;
        }

        button.submit{
            height: 45px;
            background-color: #FCC624;
            border-style: none;
            font-size: 20px;
            /* color: #fff; */
            border-radius: 0px;
        }

    </style>
@endsection

<div class="container main-continer">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 form-container">

            @include('errors.errors')

            <h3 class="titlelogin">
                <i class="fa fa-users"></i> {{trans('course.create-free-account')}}
            </h3>
            <br/>

            <form method="POST" action="/{{Lang::getLocale()}}/auth/register">
                {!! csrf_field() !!}

                <div class="form-group userName">

                    <label for="name">{{trans('course.name')}}</label>
                    <input class="form-control" placeholder="{{trans('course.name')}}" type="text" name="name" value="{{ old('name') }}">
                </div>

                <div class="form-group email">

                    <label for="email">{{trans('course.email')}}</label>
                    <input class="form-control" placeholder="{{trans('course.email')}}" type="email" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group password">

                    <label for="password">{{trans('course.password')}}</label>
                    <input class="form-control" type="password" name="password">
                </div>

                <div class="form-group password">

                    <label for="password_confirmation">{{trans('course.confirm_password')}}</label>
                    <input class="form-control" type="password" name="password_confirmation">
                </div>
                <hr/>
                <div class="form-group">
                    <button class="form-control submit" type="submit"><i class="fa fa-check"></i> {{trans('course.submit')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>




