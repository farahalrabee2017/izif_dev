@extends('master')

@section('title', $course->seo_meta_title)

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>
        .level, .number_of_videos {
            font-weight: bold;
            font-size: 13px;

        }
        .dis_video_2 .level
        {

          text-align: center;
          float: left;
        }
        .types.filter.btn.btn-danger
        {

          margin-right: 10px;
        }

    </style>

    <!-- Add IntroJs styles -->
    <link href="{{url('/static/intro.js-2.0.0/introjs.css')}}" rel="stylesheet">
    <!-- Add IntroJs RTL styles -->
    <link href="{{url('/static/intro.js-2.0.0/introjs-rtl.css')}}" rel="stylesheet">

@endsection


@section('content')
    <div class="container" id="margin_top_only">
        <div class="row">

            <div class="col-md-6">
                <div class="row">
                    @include('partials.videogallary')
                </div>

            </div>
            <div class="col-md-6">
                <div class="boxs-right-top">
                    <div class="boxs-right-top-title">
                        <span>
                            @if(Lang::getLocale() == "ar")
                                {{$course->title_arabic}}
                            @else
                                {{$course->title_english}}
                            @endif
                        </span>
                    </div>
                    <div class="boxs-right-top-discrption">
                        <br/>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="level text-center">{{trans('course.level')}}
                                    : {{trans('course.'.$course->level.'-level')}}</div>
                            </div>
                            <div class="col-md-5">
                                <div class="number_of_videos text-center">{{trans('course.number_of_videos')}}
                                    : {{count($course->videos)}}</div>
                            </div>
                              <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <span>

                            @if(Lang::getLocale() == "ar")
                                {!! $course->overview_arabic !!}
                            @else
                                {!! $course->overview_english !!}
                            @endif
                        </span>
                        <br/>


                        @include('partials.coursePageSubscribeButton')

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box-left-bottom">
                    <div class="box-left-bottom-title">
                        <span>
                            @if(Lang::getLocale() == "ar")
                                {{$course->teacher->name}}
                            @else
                                {{$course->teacher->name_enu}}
                            @endif
                        </span>
                    </div>
                    <span>
                        @if(Lang::getLocale() == "ar")
                            {{$course->teacher->about_ara}}
                        @else
                            {{$course->teacher->about_enu}}
                        @endif
                    </span>
                    <img src="{{$course->teacher->profile_image}}">
                </div>

            </div>
            <div class="col-md-6">
              <div style="clear:both"></div>
              <div class="fhras" data-step="3" data-intro="{{trans('course.video_gallary_intro_video_list')}}">
                  <div class="title-fhras">
                      <span><i class="fa fa-bars"></i>

                          {{trans('course.lessons_list')}}
                          -
                          @if(Lang::getLocale() == "ar")
                              @if(isset($lessonsUrl))
                              <a  href='{{str_replace('http','https',url("/" . Lang::getLocale() . "$lessonsUrl"))}}'
                                  style="text-decoration: underline; color: white;">
                                     {{$course->title_arabic}}
                                 </a>
                              @else
                                   {{$course->title_arabic}}
                              @endif
                          @else
                               @if(isset($lessonsUrl))
                                   <a href='{{str_replace('http','https',url("/" . Lang::getLocale() . "$lessonsUrl"))}}'
                                      style="text-decoration: underline; color: white;">
                                       {{$course->title_english}}
                                   </a>
                               @else
                                  {{$course->title_english}}
                               @endif
                          @endif
                      </span>

                  </div>
                  <div class="fhras-img">

                      @if(!Auth::Guest())
                          <div>
                              {{trans('course.you_watched')}}
                              {{$completed_videos}}
                              {{trans('course.from')}}
                              {{count($course->videos)}}


                          </div>
                          <br/>
                          <!--<div class="progress">
                              <div class="progress-bar progress-bar-success progress-bar-striped active"
                                   role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                   style="width:{{$completed}}%;float:right">
                                  <span class="sr-only">{{$completed}}% Complete (success)</span>
                              </div>
                          </div>-->
                      @endif

                  </div>
              </div>




            </div>
        </div>
    </div>
    <!-- <form class="controls" id="Filters">

      <fieldset>
        <div class="col-md-12 col-xs-12" style="margin-top: 10px;margin-right: 329px;">

    @foreach($program_type  as $type)
    @if($type->programType)
    <!-- display all filters except (programs) beacause i want to display it seperatly as dropdown list. -->
    <!-- <a id = ".{{$type->song_type}}" href="#" class="types filter btn btn-danger" data-filter=".{{$type->song_type}}" style=" padding-left:20px;padding-right: 20px;  display: inline-block;   display: inline;   float: left;">{{$type->programType}}</a>
@endif
    @endforeach
    <a href = "#" id="Reset" class="hvr-icon-buzz-out filter show-all active btn btn-danger " data-filter="" >
  Reset
    </a>

      </div>
    </fieldset>
    </form>  -->
    <div class="container col-md-12 col-xs-12" style="padding-top:25px">
    <div id="Container" class="row">
@foreach ($course->videos as $video)
<div class="mix {{$video->song_type}} col-xs-12 col-md-4 Container11">
  <a  href='{{str_replace('https','http',url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$video->url_identifier"))}}' style="width :100%;">
<div class="videos-box_2">
  <div class="title_video_2" style="background:#bf80ff">

    <span>
        @if(Lang::getLocale() == "ar")
        {{$video->title_arabic}}
        @else
      {{$video->title_english}}
        @endif
    </span>

  </div>

  <div class="video-array_2" >
                           <button><img src="{{url('static/img/video_icon.png')}}"></button>

                           @if($video->cover_image)
                               <img class="img_test_2" src="{{url('/static/img/hex-loader.gif')}}"
                                    data-src="{{url('/uploads/'.$video->cover_image)}}">

                           @else
                               <img class="img_test_2" src="{{url('static/img/m-1.png')}}">
                           @endif
                       </div>

                       <div class="dis_video_2">
                       <span>
                           <!-- {{$course->overview_arabic}} -->
                       </span>
                           <div class="level">

                                 <span style="font-size: 11px">

                                     @if(strlen($video->estimated_time) == 4)


                                         {{substr($video->estimated_time,0,2)}}:{{substr($video->estimated_time,2,2)}}
                                     @else

                                         {{$video->estimated_time}}:00
                                     @endif

                                 </span>
                                </div>

                           <div class="teacher">

                               @if(!Auth::Guest())
                                   <div scope="row" style="width: 60px">
                                       @if(isset($status[$video->id]))
                                           @if($status[$video->id]['status'] == "Done")
                                               <i class="fa fa-check-circle"
                                                  style="font-size: 30px;color:green"></i>
                                           @else
                                               <i class="fa fa-adjust" style="font-size: 30px;color:goldenrod"></i>
                                           @endif
                                       @else
                                           <i class="fa fa-circle-o" style="font-size: 30px;color:#ccc"></i>
                                       @endif
                                   </div>
                               @endif
                           </div>
                       </div>
</div>

</a>

</div>

@endforeach
</div>
</div>


@endsection

@section('footer')
<style>
#container {
  width: 100%;
  margin: 0 auto;
}

a {
  display: inline-block; /* stupid simple flexibility */
  color: black;
}
#container a {
  margin: 0 15px 15px 0;
}
a.active {
  border-bottom: 2px solid #CCC
}
#container {
  margin-top: 3em;
}
.container .mix
{

    display: none;
  }
  fieldset{
    display: inline-block;
    float: right;
  }


    </style>


    <script type="text/javascript" src="{{url('/static/intro.js-2.0.0/intro.js')}}"></script>
    <script>
        //intro
        if (localStorage.getItem('intro') == null) {
            introJs().setOptions({
                'nextLabel': "{{trans('course.next')}}",
                'prevLabel': "{{trans('course.prevLabel')}}",
                'skipLabel': "{{trans('course.skipLabel')}}",
                'doneLabel': "{{trans('course.doneLabel')}}"
            }).start();
            localStorage.setItem('intro', 'done');
        }

        // Youtube API
        var url = "{{$course->course_intro_video_path}}".split('v=')[1];
        if (url.indexOf('&') >= 0) {
            url = url.split('&')[0];
        }
        var youtubesrc = "http://www.youtube.com/embed/" + url + "?enablejsapi=1&origin=http://dev.izif.com&rel=0";
        $('#player').attr('src', youtubesrc);

    </script>

    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="{{url('static/js/jquery.unveil.js')}}"></script>
    <script type="text/javascript" src="{{url('static/mixitup-master/build/jquery.mixitup.min.js')}}"></script>
    <script>
    // To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "buttonFilter".
    var buttonFilter = {
      // Declare any variables we will need as properties of the object
      $filters: null,
      $reset: null,
      groups: [],
      outputArray: [],
      outputString: '',
      // The "init" method will run on document ready and cache any jQuery objects we will need.
      init: function(){
        var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "buttonFilter" object so that we can share methods and properties between all parts of the object.

        var $lazyLoad = $("img[data-src]");
        $lazyLoad.unveil(200, function () {
            $(this).load(function () {
                this.style.opacity = 1;
            });
        });
       $('.mix img:lt(6)').trigger("unveil");
        self.$filters = $('#Filters');
        self.$reset = $('#Reset');
        self.$container = $('#Container');
        self.$filters.find('fieldset').each(function(){
          self.groups.push({
            $buttons: $(this).find('.filter'),
            active: ''
          });
        });
        self.bindHandlers();
      },
      // The "bindHandlers" method will listen for whenever a button is clicked.
      bindHandlers: function(){
        var self = this;
        // Handle filter clicks
        self.$filters.on('click', '.filter', function(e){
          //i used these to hide Container2 when page loads and display it again when we click on filters.

            val = 1;
          e.preventDefault();
          var $lazyLoad = $("img[data-src]");
          $lazyLoad.unveil(200, function () {
              $(this).load(function () {
                  this.style.opacity = 1;
              });
          });
         $('.mix img:lt(6)').trigger("unveil");
          var $button = $(this);
          //if you clicked on programms filter another tags will appear (programm type)
          if($button != '.2')
              {
                $(".drop").hide();
              }
              else {
                $(".drop").show();
                $(".drop").css({"display" : "inline-block","margin-top" : "20px"});
              }
          // If the button is active, remove the active class, else make active and deactivate others.
            $button.hasClass('active') ?
            $button.removeClass('active') :
            $button.addClass('active').siblings('.filter').removeClass('active');
            self.parseFilters();

        });
        // Handle reset click
        self.$reset.on('click', function(e){
          e.preventDefault();

          self.$filters.find('.filter').removeClass('active');
          self.$filters.find('.show-all').addClass('active');

          self.parseFilters();
        });
      },
      // The parseFilters method checks which filters are active in each group:

      parseFilters: function(){
        var self = this;
        // loop through each filter group and grap the active filter from each one.
        for(var i = 0, group; group = self.groups[i]; i++){
        var active = self.groups[i];
        group.active = group.$buttons.filter('.active').attr('data-filter') || '';

  }


        self.concatenate();
      },
      // The "concatenate" method will crawl through each group, concatenating filters as desired:
      concatenate: function(){
        var self = this;

        self.outputString = ''; // Reset output string

        for(var i = 0, group; group = self.groups[i]; i++){

          var first = self.outputString;

          self.outputString += group.active;

        }
        // If the output string is empty, show all rather than none:
        !self.outputString.length && (self.outputString = 'all');
        // ^ we can check the console here to take a look at the filter string that is produced
        // Send the output string to MixItUp via the 'filter' method:
    	  if(self.$container.mixItUp('isLoaded')){
        	self.$container.mixItUp('filter', self.outputString);

    	  }

      }

    };
    // On document ready, initialise our code.
    $(function(){

              var $lazyLoad = $("img[data-src]");

              $lazyLoad.unveil(200, function () {
                  $(this).load(function () {
                      this.style.opacity = 1;
                  });
              });
             $('.mix img:lt(6)').trigger("unveil");
      // Initialize buttonFilter code
      buttonFilter.init();

      // Instantiate MixItUp
      $('#Container').mixItUp({

        controls: {
          enable: false // we won't be needing these
        },
        'sort': 'order:asc',

        callbacks: {
          onMixFail: function(){
              self.$container = $('#Container');
          }
        }

      });



    });

    </script>
    <script>
    $(document).ready(function(e){
      //hide container 2 when page loads for the first time.
    $(".Container2").css({"height" : 0, "width" : 0, "opacity" : 0});
    });
    </script>


@endsection
