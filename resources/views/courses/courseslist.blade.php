@extends('master')


@section('title', trans('course.music_page_title'))
@section('description', trans('course.music_page_description'))


@section('head')
<link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">


@endsection
@section('content')

<form class="controls" id="Filters">
  <!-- We can add an unlimited number of "filter groups" using the following format: -->

  <fieldset>
<div class="icons_music" id="margin_top_only">
  <div class="row">

  @foreach($instruments as $instrument)
    <a id=".{{$instrument->english_name}}" class="hvr-icon-buzz-out filter" data-filter=".{{$instrument->english_name}}" style="width :75px;    margin-left: 50px;
 display: inline-block;   display: inline;   float: left;">@if(Lang::getLocale() == "ar")
      <span>
                {{$instrument->arabic_name}}
            @else
                {{$instrument->english_name}}
            @endif
          </span>
<img src="{{url($instrument->icon)}}"/>
          </a>
  @endforeach
<div class="fillter_ll col-md-1 col-sm-4 col-xs-4 col-xs-offset-4 icon-izif-9" id ="test">
    <a id="Reset" class="hvr-icon-buzz-out filter show-all" data-filter="" style="width :100%;">
      <span>{{trans('course.all')}}</span>
      <img src="/static/img/9.png"/>
    </a>
  </div>
  </fieldset>

    <fieldset>
      <div class="col-md-12 col-xs-12" style="margin-top: 10px;margin-right: 329px;">
        <a class="types filter all" id = "" data-filter="" style="padding-left:20px;padding-right: 20px; display: inline-block;   display: inline;   float: left;">الجميع</a>

@foreach($course_type as $type)
<!-- display all filters except (programs) beacause i want to display it seperatly as dropdown list. -->
  @if($type->id != "2")
<a id = ".{{$type->id}}" href="#" class="types filter" data-filter=".{{$type->id}}" style="padding-left:20px;padding-right: 20px;  display: inline-block;   display: inline;   float: left;">{{$type->arabic_name}}</a>
@endif
  @endforeach
  <a class="caret"></a>

    <a id = ".2" value="2" data-filter=".2" class="types maindrop filter btn dropdown-toggle" data-toggle="dropdown" style="margin-top: -9px;">
    برامج إعزف التعليمية
    </a>
      @foreach ($program_type as $progtype)
      @if($progtype->name)
      <?php $prog_type =  str_replace(' ', '_', $progtype->name); ?>
    <a style="display:none;" class="types drop filter"  id = ".{{$prog_type}}" value="{{$prog_type}}" data-filter=".{{$prog_type}}" href="#">&#10003; {{$progtype->name}}</a><br/>
    @endif
@endforeach
    </div>
  </fieldset>
</div>
</div>
</form>

<div class="container col-md-12 col-xs-12" style="padding-top:25px">
<div id="Container" class="row">
  @foreach ($courses as $course)
  <?php
  $program_type1 = str_replace(' ', '_', $course->programType);
  $course_type1 = explode(',' , $course->course_type);
$color[$course_type1[0]] = '#00FF00';
  ?>
  <div class="mix  <?php foreach($course_type1 as $type1){ echo $type1 ." "; }?> {{$course->instrument->english_name}} {{$program_type1}} col-xs-12 col-md-4 Container11" data-order="{{$course->course_order}}">
    <a  href='{{str_replace('https','http',url("/" . Lang::getLocale() . "/course/$course->url_identifier"))}}' style="width :100%;">
  <div class="videos-box_2">
    <div class="title_video_2" style="background-color:
    <?php
    // change the background color of the course title according to course type :
    foreach($course_type1 as $type1)
    {
       if($type1 == '1')
       {
         echo '#0086b3';
       }
       if($type1 == '2')
       {
         echo '#bf80ff';
       }
       if($type1 == '3')
       {
         echo '#400000';
       }
       if($type1 == '4')
       {
         echo '#5cb85c';
       }

     }?> !important;">
        <span>
            @if(Lang::getLocale() == "ar")
                {{$course->title_arabic}}
            @else
                {{$course->title_english}}
            @endif
        </span>
        </div>
        <div class="video-array_2">
                                 <button><img src="{{url('static/img/video_icon.png')}}"></button>

                                 @if($course->cover_image)
                                     <img class="img_test_2" src="{{url('/static/img/hex-loader.gif')}}"
                                          data-src="{{url('/uploads/'.$course->cover_image)}}">

                                 @else
                                     <img class="img_test_2" src="{{url('static/img/m-1.png')}}">
                                 @endif
                             </div>
          <div class="dis_video_2">
          <span>
              <!-- {{$course->overview_arabic}} -->
          </span>
              <div class="level">
                  <i class="fa fa-signal"></i> {{trans('course.'.$course->level.'-level')}}
              </div>

              <div class="teacher">
                  <i class="fa fa-graduation-cap"></i>
                  @if(Lang::getLocale() == "ar")
                      {{$course->teacher->name}}
                  @else
                      {{$course->teacher->name_enu}}
                  @endif
              </div>
          </div>
      </div>
    </a>
  </div>
         @endforeach
</div>

<!-- Container 2 is the div that will apear under (شاهد المزيد) when you click on any filter (course type filter) -->
<div id="Container2" class="row">
  <div class ="watch_more">
<h1 style="margin-right: 10px;padding-top: 5px;font-family: i3zif;">
    شاهد ايضاً ..
  </h1>
  <br/>
  </div>
  @foreach ($courses as $course)
  <?php
  // course type field is multiple so it's value is an array .
  $course_type1 = explode(',' , $course->course_type);
  ?>
  <div class="mix  <?php foreach($course_type1 as $type1){ echo $type1 ." "; }?>{{$course->instrument->english_name}} col-xs-12 col-md-4 Container22"  id="row2" data-order="{{$course->course_order}}">
    <a  href='{{str_replace('https','http',url("/" . Lang::getLocale() . "/course/$course->url_identifier"))}}' style="width :100%;">
  <div class="videos-box_2 Container2">
    <div class="title_video_2">
        <span>
            @if(Lang::getLocale() == "ar")
                {{$course->title_arabic}}
            @else
                {{$course->title_english}}
            @endif
        </span>
        </div>
        <div class="video-array_2" >
                                 <button><img src="{{url('static/img/video_icon.png')}}"></button>

                                 @if($course->cover_image)
                                     <img class="img_test_2" src="{{url('/static/img/hex-loader.gif')}}"
                                          data-src="{{url('/uploads/'.$course->cover_image)}}">

                                 @else
                                     <img class="img_test_2" src="{{url('static/img/m-1.png')}}">
                                 @endif
                             </div>
          <div class="dis_video_2">
          <!-- <span> {{$course->overview_arabic}}
          </span> -->
              <div class="level">
                  <i class="fa fa-signal"></i> {{trans('course.'.$course->level.'-level')}}
              </div>

              <div class="teacher">
                  <i class="fa fa-graduation-cap"></i>
                  @if(Lang::getLocale() == "ar")
                      {{$course->teacher->name}}
                  @else
                      {{$course->teacher->name_enu}}
                  @endif
              </div>
          </div>
      </div>
    </a>
  </div>
         @endforeach
</div>
<a class="placeholder"></a>
  <div class="gap"></div>
  <div class="gap"></div>
  <div class="gap"></div>
  <div class="gap"></div>
</div>
@endsection
@section('footer')
    <style>
    /* my own small default styles */
    #container {
      width: 100%;
      margin: 0 auto;
    }
    #Container2 {
      width: 100%;

    }
    ul {
      list-style: none;
      margin: 0;
      padding: 0
    }

    a {
      display: inline-block; /* stupid simple flexibility */
      color: black;
    }

    a:hover {
      cursor: pointer;
      color: black;
    }
    .filter-list {
      margin-bottom: 20px
    }
    #container a {
      margin: 0 15px 15px 0;
    }
    a.active {
      border-bottom: 2px solid #CCC
    }
    #container {
      margin-top: 3em;
    }
    .dropdown-menu
    {
      position: relative !important;
      min-width: 1px !important;
      margin-right: -93px;
      margin-top: 19px;
    }

    .types.drop.filter
    {
      float: right !important;
      margin-top: 20px !important;
    margin-right: -109px
      }
    .failnotice { display: none;}

            .fa-square {
                float: right;
                margin: 1px;
                font-size: 15px;
                color: #400000;
            }

            .dis {
                color: #cccccc;
            }

            .level {
                clear: both;
                float: left;
            }
            a.types.filter
            {

              float:right !important;
            }

            .teacher {
                float: right;
                color: #888787;
            }

            .teacher i, .level i {

                color: #888787;
                font-size: 16px;
            }

            .level {
                color: #888787;
            }

            img[data-src] {
                opacity: 0.4;
                transition: opacity .3s ease-in;
            }

            .mix {
                float: right;
            }
    /**
     * Form & Button Styles
     */
    h4{
      margin-bottom: .5em;
    }

    label{
      font-weight: 300;
    }

    a{
      display: inline-block;
      border: 0;
      border-radius: .25em;
      cursor: pointer;
    }

    a.active{
      background: rgba(252, 198, 36, .9);
    }

    a:focus{
      outline: 0 none;
    }

    a:first-of-type{
      margin-left: 0;
    }

    a:last-of-type{
      margin-right: 0;
    }

    a:focus{
      outline: 0 none;
    }

    .controls{
      padding: 2%;
    }

    fieldset{
      display: inline-block;
      float: right;
      margin-right: 61px;

    }
    .container .mix{

      display: none;
    }
    .Container2 .mix{

      display: none;
    }
    .caret
    {
          display: inline-block;
          width: 0 !important;
          height: 0 !important;
          vertical-align: middle !important;
          border-top: 9px dashed !important;
          float: right !important;
          border-right : 8px solid transparent !important;
          border-left : 8px solid transparent !important;
          margin-top: 3px;
    }


    div.Container22.remove {
      opacity: 0 ;
      display: none !important;
      width: 0px;
    }
    div.watch_more
    {
      margin-right: 24px;
      font-size: 23px;
      font-family: i3zif;
      margin-bottom: 10px;
      background: rgba(252, 198, 36, .9);
      height: 30px;
    }
    </style>
    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="{{url('static/js/jquery.unveil.js')}}"></script>
    <script type="text/javascript" src="{{url('static/mixitup-master/build/jquery.mixitup.min.js')}}"></script>
    <script>
    // To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "buttonFilter".
    var buttonFilter = {
      // Declare any variables we will need as properties of the object
      $filters: null,
      $reset: null,
      groups: [],
      outputArray: [],
      outputString: '',
      // The "init" method will run on document ready and cache any jQuery objects we will need.
      init: function(){
        var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "buttonFilter" object so that we can share methods and properties between all parts of the object.

        var $lazyLoad = $("img[data-src]");
        $lazyLoad.unveil(200, function () {
            $(this).load(function () {
                this.style.opacity = 1;
            });
        });
       $('.mix img:lt(6)').trigger("unveil");
        self.$filters = $('#Filters');
        self.$reset = $('#Reset');
        self.$container = $('#Container');
        self.$container2 = $('#Container2');
        self.$filters.find('fieldset').each(function(){
          self.groups.push({
            $buttons: $(this).find('.filter'),
            active: ''
          });
        });
        self.bindHandlers();
      },
      // The "bindHandlers" method will listen for whenever a button is clicked.
      bindHandlers: function(){
        var self = this;
        // Handle filter clicks
        self.$filters.on('click', '.filter', function(e){
          //i used these to hide Container2 when page loads and display it again when we click on filters.
          $(".Container2").css({"height" : 302, "width" : 398, "opacity" : 100});
          $(".Container22").css({"height" : 300, "float" : "right"});
            val = 1;
          e.preventDefault();
          var $lazyLoad = $("img[data-src]");
          $lazyLoad.unveil(200, function () {
              $(this).load(function () {
                  this.style.opacity = 1;
              });
          });
         $('.mix img:lt(6)').trigger("unveil");
          var $button = $(this);
          //if you clicked on programms filter another tags will appear (programm type)
          if($button != '.2')
              {
                $(".drop").hide();
              }
              else {
                $(".drop").show();
                $(".drop").css({"display" : "inline-block","margin-top" : "20px"});
              }
          // If the button is active, remove the active class, else make active and deactivate others.
            $button.hasClass('active') ?
            $button.removeClass('active') :
            $button.addClass('active').siblings('.filter').removeClass('active');
            self.parseFilters();

        });
        // Handle reset click
        self.$reset.on('click', function(e){
          e.preventDefault();

          self.$filters.find('.filter').removeClass('active');
          self.$filters.find('.show-all').addClass('active');

          self.parseFilters();
        });
      },
      // The parseFilters method checks which filters are active in each group:

      parseFilters: function(){
        var self = this;
        // loop through each filter group and grap the active filter from each one.
        for(var i = 0, group; group = self.groups[i]; i++){
        var active = self.groups[i];
        group.active = group.$buttons.filter('.active').attr('data-filter') || '';

        }
        if(self.groups[1].active != "")
        {
        $(".Container22").removeClass("remove");
        $(".Container22").addClass('mix');
        $("#Container2").show();
        var $f1 = ".hvr-icon-buzz-out";
        var $f2 = ".filter";
        var $f3 = ".active";
        var filter11 = $($f1  + $f3);
        //if someone clicked only on filter 2 . consider filter 2 as the first filter and hide watch more div.
        if(filter11.length != 0)
        {
            var first = filter11[0].id;
            $(".watch_more").css({"height" : 29, "opacity" : 100});

        }
        else
        {
          var first = self.groups[0].active;
            $(".watch_more").css({"height" : 0, "opacity" : 0});
        }
        //this is to return the first and the second filters .
        var $filter3 = ".Container22";
        var $filter4 = ".Container11";
        var $t1 = ".types";
        var filter22 = $($t1  + $f3);
        var second = filter22[0].id;
        var _divs1 = $(first +  second + $filter4);
        var _divs = $(first +  second + $filter3);
        if(second == '.shoman')
        {
            $(".maindrop").addClass('active');
        }
      if(_divs1.length > 0)
      {
      _divs.addClass('remove');
      _divs.removeAttr('style');
      _divs.removeClass('mix');
      }
      //filter the second div (Container 2) according to active instrument filter only !
        self.$container2.mixItUp('filter', first);
        }
        else {
          {
                    $("#Container2").hide();
          }
        }

        self.concatenate();
      },
      // The "concatenate" method will crawl through each group, concatenating filters as desired:
      concatenate: function(){
        var self = this;

        self.outputString = ''; // Reset output string

        for(var i = 0, group; group = self.groups[i]; i++){

          var first = self.outputString;

          self.outputString += group.active;

        }
        // If the output string is empty, show all rather than none:
        !self.outputString.length && (self.outputString = 'all');
        // ^ we can check the console here to take a look at the filter string that is produced
        // Send the output string to MixItUp via the 'filter' method:
    	  if(self.$container.mixItUp('isLoaded')){
        	self.$container.mixItUp('filter', self.outputString);

    	  }

      }

    };
    // On document ready, initialise our code.
    $(function(){

              var $lazyLoad = $("img[data-src]");

              $lazyLoad.unveil(200, function () {
                  $(this).load(function () {
                      this.style.opacity = 1;
                  });
              });
             $('.mix img:lt(6)').trigger("unveil");
      // Initialize buttonFilter code
      buttonFilter.init();

      // Instantiate MixItUp
      $('#Container').mixItUp({

        controls: {
          enable: false // we won't be needing these
        },
        'sort': 'order:asc',

        callbacks: {
          onMixFail: function(){
              self.$container = $('#Container');
          }
        }

      });

      $('#Container2').mixItUp({
        controls: {
          enable: false // we won't be needing these
        },
        callbacks: {
          onMixFail: function(){
              self.$container = $('#Container');

          }
        }
      });

    });

    </script>
    <script>
    $(document).ready(function(e){
      //hide container 2 when page loads for the first time.
    $(".Container2").css({"height" : 0, "width" : 0, "opacity" : 0});
    $(".Container22").css({"height" : 0, "opacity" : 100});
    $(".watch_more").css({"height" : 0, "opacity" : 0});
    });
    </script>
    <script>
        $('.caret').on('click',function(){
                $(".drop").show()
        });

    </script>
    @endsection
