@extends('master')

@section('title', $course->seo_meta_title)

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>
        .level, .number_of_videos {
            font-weight: bold;
            font-size: 13px;
            text-align: center;
        }

    </style>

    <!-- Add IntroJs styles -->
    <link href="{{url('/static/intro.js-2.0.0/introjs.css')}}" rel="stylesheet">
    <!-- Add IntroJs RTL styles -->
    <link href="{{url('/static/intro.js-2.0.0/introjs-rtl.css')}}" rel="stylesheet">

@endsection


@section('content')
    <div class="container" id="margin_top_only">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    @include('partials.videogallary')
                </div>

            </div>
            <div class="col-md-6">
                <div class="boxs-right-top">
                    <div class="boxs-right-top-title">
                        <span>

                            @if(Lang::getLocale() == "ar")
                                {{$course->title_arabic}}
                            @else
                                {{$course->title_english}}
                            @endif
                        </span>
                    </div>
                    <div class="boxs-right-top-discrption">
                        <br/>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="level text-center">{{trans('course.level')}}
                                    : {{trans('course.'.$course->level.'-level')}}</div>
                            </div>
                            <div class="col-md-5">
                                <div class="number_of_videos text-center">{{trans('course.number_of_videos')}}
                                    : {{count($course->videos)}}</div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <span>

                            @if(Lang::getLocale() == "ar")
                                {!! $course->overview_arabic !!}
                            @else
                                {!! $course->overview_english !!}
                            @endif
                        </span>
                        <br/>


                        @include('partials.coursePageSubscribeButton')

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box-left-bottom">
                    <div class="box-left-bottom-title">
                        <span>
                            @if(Lang::getLocale() == "ar")
                                {{$course->teacher->name}}
                            @else
                                {{$course->teacher->name_enu}}
                            @endif
                        </span>
                    </div>
                    <span>
                        @if(Lang::getLocale() == "ar")
                            {{$course->teacher->about_ara}}
                        @else
                            {{$course->teacher->about_enu}}
                        @endif
                    </span>
                    <img src="{{$course->teacher->profile_image}}">
                </div>

            </div>
            <div class="col-md-6">
                @include('partials.videoslist')
            </div>
        </div>
    </div>




@endsection

@section('footer')

    <script type="text/javascript" src="{{url('/static/intro.js-2.0.0/intro.js')}}"></script>
    <script>
        //intro
        if (localStorage.getItem('intro') == null) {
            introJs().setOptions({
                'nextLabel': "{{trans('course.next')}}",
                'prevLabel': "{{trans('course.prevLabel')}}",
                'skipLabel': "{{trans('course.skipLabel')}}",
                'doneLabel': "{{trans('course.doneLabel')}}"
            }).start();
            localStorage.setItem('intro', 'done');
        }

        // Youtube API
        var url = "{{$course->course_intro_video_path}}".split('v=')[1];
        if (url.indexOf('&') >= 0) {
            url = url.split('&')[0];
        }
        var youtubesrc = "http://www.youtube.com/embed/" + url + "?enablejsapi=1&origin=http://dev.izif.com&rel=0";
        $('#player').attr('src', youtubesrc);

    </script>

@endsection
