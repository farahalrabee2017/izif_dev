@extends('master')

@section('head')

    <link rel="stylesheet" href="{{ URL::asset('player/build/mediaelementplayer.css') }}" />

@endsection

@section('content')



    <video id="video" width="320" height="240" poster="poster.jpg" controls="controls" preload="none">
        <!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
        <source type="video/mp4" src="myvideo.mp4" />
        <!-- WebM/VP8 for Firefox4, Opera, and Chrome -->
        <source type="video/webm" src="myvideo.webm" />
        <!-- Ogg/Vorbis for older Firefox and Opera versions -->
        <source type="video/ogg" src="myvideo.ogv" />
        <!-- Optional: Add subtitles for each language -->

        <object width="320" height="240" type="application/x-shockwave-flash" data="flashmediaelement.swf">
            <param name="movie" value="flashmediaelement.swf" />
            <param name="flashvars" value="controls=true&file=myvideo.mp4" />
            <!-- Image as a last resort -->
            <img src="myvideo.jpg" width="320" height="240" title="No video playback capabilities" />
        </object>
    </video>

@endsection


@section('footer')

    <script src="{{ URL::asset('jquery.js') }}"></script>
    <script src="{{ URL::asset('player/build/mediaelement-and-player.js') }}"></script>
    <script>


        var player = new MediaElementPlayer('#video');

        player.setSrc('{{url('video/1')}}');
        player.play();
    </script>

@endsection


