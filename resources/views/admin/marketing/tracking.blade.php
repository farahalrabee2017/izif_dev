@extends('adminmaster')

@section('title')
    Monthly users tracking
@endsection


@section('breadcrumb')
    <li class="active">Monthly users tracking</li>
@endsection

@section('head')
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div style="width: 50%; margin: 0 auto;">

                <form action="/ar/admin/tracking" method="POST" id="form1">
                    <b>Supscription date : </b> <br/><br>
                    <select name="date"  class="form-control">
                        <optgroup label="2016">
                            <option value="2016-06-01">June</option>
                            <option value="2016-07-01">July</option>
                            <option value="2016-08-01">Aug</option>
                            <option value="2016-09-01">Sep</option>
                            <option value="2016-10-01">Oct</option>
                            <option value="2016-11-01">Nov</option>
                            <option value="2016-12-01">Dec</option>
                        </optgroup>
                        <optgroup label="2017">
                            <option value="2017-01-01">Jan</option>
                            <option value="2017-02-01">Feb</option>
                            <option value="2017-03-01">Mar</option>
                            <option value="2017-04-01">Apr</option>
                            <option value="2017-05-01">May</option>
                            <option value="2017-06-01">Jun</option>
                            <option value="2017-07-01">July</option>
                            <option value="2017-08-01">Aug</option>
                            <option value="2017-09-01">Sep</option>
                            <option value="2017-10-01">Oct</option>
                            <option value="2017-11-01">Nov</option>
                            <option value="2017-12-01">Dec</option>

                        </optgroup>
                    </select>
                 <br>
                    <label>Check month : </label>
                        <select name="month[]" id="month" multiple>
                            <optgroup label="2016">
                            <option value="2016-06-01">June</option>
                            <option value="2016-07-01">July</option>
                            <option value="2016-08-01">Aug</option>
                            <option value="2016-09-01">Sep</option>
                            <option value="2016-10-01">Oct</option>
                            <option value="2016-11-01">Nov</option>
                            <option value="2016-12-01">Dec</option>
                            </optgroup>
                            <optgroup label="2017">
                                <option value="2017-01-01">Jan</option>
                                <option value="2017-02-01">Feb</option>
                                <option value="2017-03-01">Mar</option>
                                <option value="2017-04-01">Apr</option>
                                <option value="2017-05-01">May</option>
                                <option value="2017-06-01">Jun</option>
                                <option value="2017-07-01">July</option>
                                <option value="2017-08-01">Aug</option>
                                <option value="2017-09-01">Sep</option>
                                <option value="2017-10-01">Oct</option>
                                <option value="2017-11-01">Nov</option>
                                <option value="2017-12-01">Dec</option>

                        </select>

                    <input type="submit" value="search">

                    <br/>
                </form>
                <br/>
                <div align="center">
                    @if(!empty($tracking))
                        @foreach($new as $num)
                            {{$num->new}} users <br>
                        @endforeach
                        <b>Result :</b>
                        <br/>
                    @endif
                </div>
                <div id="bars" style="min-width: 300px; height: 400px; margin: 0 auto">
                </div>
            </div>

        </div>

    </div>
    <?php
    $data = array();
            if ($values)
                {
                    foreach ($values as $date) {
                        $data[] = $date;
                    }
                }

    $data2 = array();
            if($tracking)
                {
                    foreach ($tracking as $key => $user) {
                        $value = $user[0]->num;
                        $data2[] = $value;
                    }
                }

    $data = array_combine($data, $data2);
    ?>
@endsection

@section('scripts')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript" src='{{url("static/bootstrap-3.3.6-dist/js/bootstrap.min.js")}}'></script>

    <script>
        $(function () {
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>

    <script>
        $(function () {
            $('#bars').highcharts({
                chart: {
                    type: 'areaspline'
                },
                title: {
                    text: 'Users tracking Counter By Month'
                },

                credits: {enabled: false},
                legend: {},
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }

                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of users '
                    },
                },
                series: [
                    {
                        name: 'Month',
                        data: [
                            @foreach($data as $key => $user)
                            {!!  "['" . $key ."'," . $user ."],"!!}
                            @endforeach
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            pointWidth: 40,
                            color: '#FFFFFF',
                            align: 'right',
                            //format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                ]
            });
        });
    </script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#month').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>
@endsection