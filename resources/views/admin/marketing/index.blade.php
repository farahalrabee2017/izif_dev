@extends('adminmaster')

@section('title')
    Marketing
@endsection


@section('breadcrumb')
    <li class="active">Instrument</li>
@endsection

@section('content')

    <body>
    @foreach($errors->all(':message') as $message)
        @if($message != "")
            <div id="form-messages" class="alert alert-success" role="alert">
                {{ $message }}
            </div>
        @endif
    @endforeach()
    <div style="width: 50%; margin: 0 305px;">
        <div class="form-group" style="width: 40%;">
        <form action="" method="POST">
            <div class="form-group from">
                <label>From</label><br>
                <input type="text" id="from" class="datepicker" name="from" ><br>
            </div>

            <div class="form-group to">
                <label>To</label><br>
                <input type="text" id="to" class="datepicker" name="to"><br>
            </div>
            <div class="form-group">
                <label>Amount in $</label>
                <input type="text" name="amount" id="amount"><br>
            </div>

            <div class="form-group">

            <input class="btn btn-danger" type="submit" value="save" name="save" style="margin: 0 30%; ">
            </div>
        </form>
            </div>
    </div>

<hr>

    <table id="marketing" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Start date </th>
            <th>End date </th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
                @foreach($marketing as $result)
            <tr>
                <td>
                    {{$result->start_date}}
                </td>
                <td>
                    {{$result->end_date}}
                </td>
                <td>
                    {{$result->amount}}$
                </td>
            </tr>
                    @endforeach


        </tbody>

    </table>

    </body>
@endsection
@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="   https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script>
        $(function () {

            $('#marketing').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });

        });

    </script>
    <script>
    $(function() {
    $( ".datepicker" ).datepicker();
    });
    </script>

@endsection

