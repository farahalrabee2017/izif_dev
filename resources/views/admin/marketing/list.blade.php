@extends('adminmaster')

@section('title')
    Marketing Dashboard
@endsection



@section('breadcrumb')
    <li class="active">Marketing Dashboard</li>
@endsection


@section('content')

    <hr/>
    <form method="get" action="/ar/admin/marketing_dashboard">
        <div style="width: 50%; margin: 0 auto;">
            <label>From</label>
            <input type="text" id="from" class="datepicker" name="from" value="{{$from}}">
            <label>To</label >
            <input type="text" id="to" class="datepicker" name="to" value="{{$to}}">
            <input id="search" type="submit" value="search">
        </div>
    </form>
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Total TRX</th>
            <th>New users</th>
            <th>Recurring TRX</th>
            <th>Recurring users</th>
            <th>canceled users</th>
            <th>Active students</th>
            <th>Revenue</th>
            <th>Avg spend per customer</th>
            <th> marketing expenditure</th>
            <th>CAC</th>
            <th>Free Trials</th>


        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                @foreach($total_payments as $result2)
                    <?php $t = $result2->total; ?>
                    {{$result2->total}} users
                @endforeach

            </td>

            <td>

                @foreach($unique as $user)
                    <?php   $u =  $user->unique1; ?>
                @endforeach
                <?php echo  $t - $u; ?>

            </td>
            <td>
                @foreach($renew as $result1)
                    {{$result1->Renew}} users
                @endforeach

            </td>

            <td>

                @foreach($unique as $user)
                    {{$user->unique1}}
                @endforeach


            </td>

            <td>
                @foreach($canceled as $cancel)
                    {{$cancel->canceled}} users
                @endforeach

            </td>
            <td>
                @foreach($totalUniqueActive as $active)
                    {{$active->uniqeCount}} students
                @endforeach

            </td>

            <td>

                <?php echo $new = round($total, 2); ?>$


            </td>
            <td>

                @foreach($total_subscriptions as $total)
                    <?php echo round($total->avg, 2); ?>$
                @endforeach

            </td>

            <td>
                @foreach($marketing as $mark)
                    {{$mark->mar}}$
                @endforeach
            </td>
            <td>
                @foreach($marketing as $mark)
                   <?php echo round($mark->mar/$new,3)."$";  ?>
                @endforeach
                
            </td>

            <td>
                @foreach($no_sub as $user)
                    {{$user->num}}
                @endforeach


            </td>


        </tr>
        <tr>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=total&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">Export</button></a>

            </td>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=new&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=renew&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">Export</button></a>
            </td>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=unique&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=canceled&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>

            <td>
                <a href="{{action('AdminController@getExport')}}?type=students&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>

            <td>
                <a href="{{action('AdminController@getExport')}}?type=income&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=income&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>
            <td>
                <a href="#"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>
            <td>
                <a href="#"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>
            <td>
                <a href="{{action('AdminController@getExport')}}?type=no_sub&@if(!empty($_GET['from']))from={{$_GET['from']}}@endif&@if(!empty($_GET['to']))to={{$_GET['to']}}@endif"><button type="button"  class="btn btn-danger" id="bt1">export</button></a>
            </td>


        </tr>

        </tbody>
    </table>



@endsection


@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>

    <script>
        $(function () {

            $('#users').DataTable({
                "order": [[5, "desc"]],
                "paging": true,
                "scrollX": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });


        $('#from').datepicker({
            format: 'YYYY-MM-DD hh:mm:ss',
            datepicker: true
        });
        $('#to').datepicker({
            'formatDate': 'Y-m-d H:i:s'
        });

    </script>
@endsection