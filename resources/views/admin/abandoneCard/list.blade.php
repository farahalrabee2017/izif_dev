@extends('adminmaster')

@section('title')
Abandone Cart
@endsection



@section('breadcrumb')
<li class="active">Abandone Cart</li>
@endsection


@section('content')


<table id="abandoneCard" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Email</th>
            <th>Subscription Model</th>
            <th>Course</th>
            <th>created_at</th>

        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Email</th>
            <th>Subscription Model</th>
            <th>Course</th>
            <th>created_at</th>
        </tr>
    </tfoot>
    <tbody>

        @foreach ($abandoneCardInfo as $card)
        <tr>

            <td>{{$card->user->email}}</td>
            <td>{{$card->subscriptions_model->title_enu}}</td>
            <td>@if($card->course) {{$card->course->title_english}} @else No Course @endif</td>
            <td>{{$card->created_at}}</td>

</tr>
@endforeach
</tbody>

</table>



@endsection


@section('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
$(function () {

    $('#abandoneCard').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
});

</script>
@endsection