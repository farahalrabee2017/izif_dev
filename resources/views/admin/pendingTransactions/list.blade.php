@extends('adminmaster')

@section('title')
Pending Transactions
@endsection


@section('breadcrumb')
<li class="active">Pending Transactions</li>
@endsection


@section('content')
<hr/>
<table id="users" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>User ID</th>
            <th>Amount</th>
            <th>Transfer Number</th>
            <th>Country</th>
            <th>Transaction type</th>
            <th>course</th>
            <th>Subscription model</th>
            <th>Activate</th>
            <th>Delete</th>
            <th>Date</th>
            <th>Status</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>User ID</th>
            <th>Amount</th>
            <th>Transfer Number</th>
            <th>Country</th>
            <th>Transaction type</th>
            <th>course</th>
            <th>Subscription model</th>
            <th>Activate</th>
            <th>Delete</th>
            <th>Date</th>
            <th>Status</th>
        </tr>
    </tfoot>
    <tbody>
        @foreach ($pendingTransactionData as $transaction)
        <tr>
            <td>{{$transaction->id}}</td>
            <td>{{$transaction->name}}</td>
            <td>{{$transaction->user_id}}</td>
            <td>{{$transaction->amount}}</td>
            <td>{{$transaction->transfer_number}}</td>
            <td>{{$transaction->country}}</td>
            <td>{{$transaction->transaction_type}}</td>
            <td>{{$transaction->course_id}}</td>
            <td>{{$transaction->SubscriptionModel->title_ara}}</td>
            <td><a href='{{url("/ar/admin/pendingTransactions/activate/$transaction->id")}}' class="btn btn-block btn-info btn-xs">Activate</a></td>
            <td><a href='{{url("/ar/admin/pendingTransactions/delete/$transaction->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>
            <td>{{$transaction->created_at}}</td>
            <td>@if($transaction->is_active==1) {{'active'}} @else {{'not active'}} @endif</td>

        </tr>
        @endforeach
    </tbody>
</table>



@endsection

@section('scripts')
<script>
    $(function () {

        $('#users').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
              "scrollX": true
        });
    });
</script>
@endsection