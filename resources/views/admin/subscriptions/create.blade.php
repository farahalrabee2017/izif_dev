@extends('adminmaster')

@section('title')
    Create New Subscriptions
@endsection


@section('breadcrumb')
    <li class="active">Create New Subscriptions </li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open( array('route' => array('subscriptions.create','ar'))) !!}
                <div class="box-body">



                    <div class="form-group">
                        {!! Form::label('user_id', 'User') !!}
                        {!! Form::select('user_id',$users,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('subscription_model_id', 'Subscription Model') !!}
                        {!! Form::select('subscription_model_id',$subscription_model,null,['class'=>'form-control']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label(' invoice_id', 'Invoice Title') !!}
                        {!! Form::text(' invoice_id', null , ['class' => 'form-control','placeholder'=>'Invoice Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('amount', 'Amount') !!}
                        {!! Form::input('number','amount', null , ['class' => 'form-control','placeholder'=>'Amount']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('payment_method_id', 'Payment Method') !!}
                        {!! Form::select('payment_method_id',$payment_method,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_id', 'Course') !!}
                        {!! Form::select('course_id',$courses,'0',['class'=>'form-control']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('start_date', 'Starts At') !!}
                        {!! Form::input('datetime','start_date', null , ['class' => 'form-control','placeholder'=>'Starts At']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('end_date', 'Ends At') !!}
                        {!! Form::input('datetime','end_date', null , ['class' => 'form-control','placeholder'=>'Ends At']) !!}
                    </div>










                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $('select').select2();
        $('[type="datetime"]').datetimepicker({
            format: 'YYYY-MM-DD hh:mm:ss a'
        });
    </script>
@endsection