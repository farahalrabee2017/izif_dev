@extends('adminmaster')

@section('title')
    Edit Group - {{$group->name}}
@endsection


@section('breadcrumb')
    <li class="active">Edit Group</li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($group, array('route' => array('group.edit','ar', $group->id))) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', $group->name , ['class' => 'form-control','placeholder'=>'Full Name']) !!}
                    </div>




                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection