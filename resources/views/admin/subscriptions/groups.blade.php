@extends('adminmaster')

@section('title')
    Users (Students Only)
@endsection


@section('breadcrumb')
    <li class="active">Users</li>
@endsection


@section('content')
    <hr/>
    <table id="groups" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </tfoot>
        <tbody>
        @foreach ($groups as $group)
            <tr>
                <td>{{$group->id}}</td>
                <td>{{$group->name}}</td>

                <td><a href='{{url("/ar/admin/group/edit/$group->id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/group/delete/$group->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#groups').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection