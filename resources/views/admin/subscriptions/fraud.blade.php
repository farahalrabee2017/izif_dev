@extends('adminmaster')

@section('title')
    Frauds Subscriptions
@endsection


@section('breadcrumb')
    <li class="active">Fraud Subscriptions</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>user</th>
            <th>subscription model</th>
            <th>starts</th>
            <th>ends</th>
            <th>Activate</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>user</th>
            <th>subscription model</th>
            <th>starts</th>
            <th>ends</th>
            <th>Activate</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach ($subscriptions as $subscription)
            <tr>
                <td>{{$subscription->id}}</td>
                <td>{{$subscription->user->email}}</td>
                <td>{{$subscription->subscriptions_model->title_ara}}</td>
                <td>{{$subscription->start_date}}</td>
                <td>{{$subscription->end_date}}</td>
                <td><a href='{{url("/ar/admin/subscriptions/activate/$subscription->id")}}' class="btn btn-block btn-info btn-xs">Activate</a></td>
                <td><a href='{{url("/ar/admin/subscriptions/delete/$subscription->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection