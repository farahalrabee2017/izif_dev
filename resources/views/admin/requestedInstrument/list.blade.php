@extends('adminmaster')

@section('title')
Reqested Instruments
@endsection



@section('breadcrumb')
<li class="active">Reqested Instruments</li>
@endsection


@section('content')


<table id="requestedInstruments" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Instrument</th>
            <th>Level</th>
            <th>Country</th>
            <th>City</th>
            <th>Status</th>
        </tr>
    </thead>
    <tfoot>
        <tr>

            <th>Name</th>
            <th>Email</th>
            <th>Instrument</th>
            <th>Level</th>
            <th>Country</th>
            <th>City</th>
            <th>Status</th>
        </tr>
    </tfoot>
    <tbody>
        @if(isset($requestedInstruments))
        @foreach ($requestedInstruments as $requestedInstrument)
        <tr>
    <input class="id" type="hidden" value="{{$requestedInstrument->id}}" >
    <td>{{$requestedInstrument->name}}</td>
    <td>{{$requestedInstrument->email}}</td>
    <td>{{$requestedInstrument->instrument}}</td>
    <td>{{$requestedInstrument->level}}</td>
    <td>{{$requestedInstrument->country}}</td>
    <td>{{$requestedInstrument->city}}</td>

    @if($requestedInstrument->is_active == 0)
    <td><button  type="button" class="btn btn-danger deliver">Not Delivered</button></td>
    @else
    <td><button  type="button" class="btn btn-success dontDeliver">Delivered</button></td>
    @endif

</tr>
@endforeach
</tbody>
@endif
</table>



@endsection


@section('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
$(function () {

    $('#requestedInstruments').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
});

</script>
<script>
    $('.deliver').click(function () {
        var id = $(this).parent().parent().children().eq(0).val();
        var $that = $(this);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{url('/' . Lang::getLocale() . '/deliver' )}}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {id: id},
            success: function (data) {
                setTimeout(function () {// wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 50);
            }
        });
    });
    $('.dontDeliver').click(function () {
        var $that = $(this);
        var id = $(this).parent().parent().children().eq(0).val();

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{url('/' . Lang::getLocale() . '/dontDeliver' )}}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {id: id},
            success: function (data) {
                setTimeout(function () {// wait for 5 secs(2)
                    location.reload(); // then reload the page.(3)
                }, 50);
            }
        });
    });
</script>
@endsection