@extends('adminmaster')

@section('title')
    Courses
@endsection


@section('breadcrumb')
    <li class="active">Courses</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>code</th>
            <th>payment methods</th>
            <th>subscription types</th>
            <th>starts</th>
            <th>ends</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>code</th>
            <th>payment methods</th>
            <th>subscription types</th>
            <th>starts</th>
            <th>ends</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach ($coupons as $coupon)
            <tr>
                <td>{{$coupon->id}}</td>
                <td>{{$coupon->code}}</td>
                <td>{{$coupon->payment_methods}}</td>
                <td>{{$coupon->subscription_types}}</td>
                <td>{{$coupon->starts_at}}</td>
                <td>{{$coupon->ends_at}}</td>
                <td><a href='{{url("/ar/admin/coupon/edit/$coupon->id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/coupon/delete/$coupon->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection