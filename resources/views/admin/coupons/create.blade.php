@extends('adminmaster')

@section('title')
    Create New Course
@endsection




@section('breadcrumb')
    <li class="active">Create New Course </li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(array('url' => "/" . Lang::getLocale() . '/admin/coupon/create')) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('code', 'Code') !!}
                        {!! Form::text('code', null , ['class' => 'form-control','placeholder'=>'Code']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Discount Type', 'Discount Type') !!}
                        {!! Form::select('discount_type',array('value'=>'value','percentage'=>'percentage'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('amount', 'Discount Amount') !!}
                        {!! Form::input('number','amount', null , ['class' => 'form-control','placeholder'=>'Amount']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('starts_at', 'Starts At') !!}
                        {!! Form::input('datetime','starts_at', null , ['class' => 'form-control','placeholder'=>'Code']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('ends_at', 'Ends At') !!}
                        {!! Form::input('datetime','ends_at', null , ['class' => 'form-control','placeholder'=>'Code']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('payment_methods', 'Payment Methods') !!}
                        {!! Form::select('payment_methods[]',$paymnet_methods,null,['class'=>'form-control','multiple'=>'multiple']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('subscription_types', 'Subscription Types') !!}
                        {!! Form::select('subscription_types[]',$subscriptions,null,['class'=>'form-control','multiple'=>'multiple']) !!}
                    </div>



                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $('select[multiple="multiple"]').select2();
        $('[type="datetime"]').datetimepicker({
            format: 'YYYY-MM-DD hh:mm:ss a'
        });
    </script>
@endsection