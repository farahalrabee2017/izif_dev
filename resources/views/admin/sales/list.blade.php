@extends('adminmaster')

@section('title')
    Sales
@endsection



@section('breadcrumb')
    <li class="active">Sales</li>
@endsection


@section('content')

    <hr/>
    <form method="get" action="/ar/admin/sales">
        <div style="width: 50%; margin: 0 auto;">
            <label>From</label>
            <input type="text" id="fromMonth" class="datepicker" name="from">
            <label>To</label>
            <input type="text" id="toMonth" class="datepicker" name="to">
            <input id="search" type="submit" value="search" >
        </div>
    </form>
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>


            <th>Date/Time</th>
            <!--<th>English / Arabic</th>-->
            <th>Is Reoccurring ? </th>
            <th>From Ads ? </th>
            <th>Source</th>
            <th>Medium</th>
            <th>Campaign</th>
            <th>Amount</th>
            <th>Subscription ID </th>
            <th>Subscription Type </th>
            <th>Course</th>
            <th>User ID</th>
            <th>Name </th>
            <th>Email </th>
            <th>Country </th>


        </tr>
        </thead>
        <tfoot>
        <tr>

            <th>Date / Time</th>
            <!--<th>English / Arabic</th>-->
            <th>Is Reoccurring ? </th>
            <th>From Ads ? </th>
            <th>Source</th>
            <th>Medium</th>
            <th>Campaign</th>
            <th>Amount</th>
            <th>Subscription ID </th>
            <th>Subscription Type </th>
            <th>Course</th>
            <th>User ID</th>
            <th>Name </th>
            <th>Email </th>
            <th>Country </th>


        </tr>
        </tfoot>
        <tbody>
        @foreach ($sales as $sale)
            <tr>

                <td>{{$sale->created_at}}</td>
                <!--<td>English / Arabic</td>-->
                <td>@if($sale->num_of_subscriptions > 1) Yes  @else No @endif </td>
                <td>@if($sale->real_user_id) Yes @else No @endif </td>
                <td>{{$sale->utm_source}}</td>
                <td>{{$sale->utm_medium}}</td>
                <td>{{$sale->utm_campaign}}</td>
                <td>{{$sale->amount}}</td>
                <td>{{$sale->id}} </td>
                <td>{{$sale->subtypename}} </td>
                @if(isset($sale->course_title))
                <td>{{$sale->course_title}} </td>
                @else 
                <td>No Course</td>
                @endif
                <td>{{$sale->user_id}}</td>
                <td>{{$sale->name}} </td>
                <td>{{$sale->email}} </td>
                <td>{{$sale->country_name}} </td>



            </tr>
        @endforeach
        </tbody>
    </table>



@endsection


@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script>
        $(function () {

            $('#users').DataTable({
                "order": [[ 5, "desc" ]],
                "paging": true,
                "scrollY": 600,
                "scrollX": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });

        $(function () {
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'

            });

        });
    </script>
@endsection