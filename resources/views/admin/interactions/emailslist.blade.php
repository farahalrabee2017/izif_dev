@extends('adminmaster')

@section('title')
    user interactions emails
@endsection


@section('breadcrumb')
    <li class="active">User emails</li>
@endsection

@section('content')
    <hr/>
    <form method="get" action="/ar/admin/interactions/emails">
        <div style="width: 50%; margin: 0 auto;">
            <label>From</label>
            <input type="text" id="fromMonth" class="datepicker" name="from">
            <label>To</label>
            <input type="text" id="toMonth" class="datepicker" name="to">
            <input id="search" type="submit" value="search" >
        </div>
    </form>
    <hr/>

    <table id="emails" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>

@foreach($emails as $email)
        <tr>
            <td>
                {{$email->user->email}}

            </td>
        </tr>

@endforeach
        </tbody>

    </table>
@endsection


@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="   https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script>
        $(function () {

            $('#emails').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });

        });
        $(function () {
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'

            });

        });
    </script>


@endsection
