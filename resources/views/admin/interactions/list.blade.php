@extends('adminmaster')

@section('title')
Interactions
@endsection


@section('breadcrumb')
<li class="active">Instrument</li>
@endsection

@section('content')
<body>

    <div style="width: 50%; margin: 0 auto;">
        <label>From</label>
        <input type="text" id="fromMonth" class="datepicker">
        <label>To</label>
        <input type="text" id="toMonth" class="datepicker">
        <input id="search" type="button" value="search" >
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <hr/>
        <div id="linechart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <div id="linechart2" style="min-width: 310px; height: 400px; margin-top: 30px;"></div>
    </div>

</body>
@endsection

@section('scripts')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<script type="text/javascript">
    $(function () {
        $('.datepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'

        });

    });

    var UserData;

    $("#search").click(function () {
        var fromMonth = $('#fromMonth').val();
        var toMonth = $('#toMonth').val();
        var year = $('#year').val();
        var rowss = new Array();
        var rowss2 = new Array();
        var date1 = new Array();
        var date2 = new Array();
        var allUsers1 = 0;
        var allUsers2 = 0;
        var count1=0;
        var count2=0;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{url('/' . Lang::getLocale() . '/admin/getUserInteractions' )}}",
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                }
            },
            data: {fromMonth: fromMonth, toMonth: toMonth, year: year},
            success: function (resData) {
                $(resData).each(function (index, el) {
                    $(el.users).each(function (ind, val) {
                        rowss.push(parseInt(val.count));
                        allUsers1 = allUsers1 + parseInt(val.count);
                        date1.push(val.daynumber + ' ' + val.crea_monthh);
                        count1++;
                    });
                });

                $(resData).each(function (index, el) {
                    $(el.subscripers).each(function (ind, val) {
                        rowss2.push(parseInt(val.countt));
                        allUsers2 = allUsers2 + parseInt(val.countt);
                        date2.push([val.daynumber + ' ' + val.crea_monthh]);
                        count2++;
                    });
                });
                console.log(resData.totalUniqueActive[0].uniqeCount);
                
                if (!$.isEmptyObject(resData.users) && !$.isEmptyObject(resData.subscripers)) {
                    $(function () {
                        $('#linechart').highcharts({
                            title: {
                                text: 'Active Trial Users',
                                x: -20 //center
                            },
                            subtitle: {
                                text: 'Total Number : ' + allUsers1 + '<br> Average Active Trial Users : '+ Math.round(allUsers1/count1),
                                x: -20
                            },
                            xAxis: {
                                categories: date1
                            },
                            yAxis: {
                                title: {
                                    text: 'Number Of Users'
                                },
                                plotLines: [{
                                        value: 0,
                                        width: 1,
                                        color: '#808080'
                                    }]
                            },
                            legend: {
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle',
                                borderWidth: 0
                            },
                            series: [{
                                    name: 'Active Trial Users',
                                    data: rowss
                                }]
                        });

                        $('#bars').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Courses Students'
                            },
                            subtitle: {
                                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                            },
                            xAxis: {
                                type: 'category',
                                labels: {
                                    rotation: -45,
                                    style: {
                                        fontSize: '13px',
                                        fontFamily: 'Verdana, sans-serif'
                                    }
                                }
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Population (millions)'
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            tooltip: {
                                pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
                            }

                        });

                    });


                    $(function () {
                        $('#linechart2').highcharts({
                            title: {
                                text: 'Active Students',
                                x: -20 //center
                            },
                            subtitle: {
                                text: 'Total Number : ' + allUsers2 + '<br> Average Active Students : '+ Math.round(allUsers2/count2)
                                +'<br> <span style="color:red;">Total Unique Active Students : '+ resData.totalUniqueActive[0].uniqeCount + '</span>',
                                x: -20
                            },
                            xAxis: {
                                categories: date2
                            },
                            yAxis: {
                                title: {
                                    text: 'Number Of Users'
                                },
                                plotLines: [{
                                        value: 0,
                                        width: 1,
                                        color: '#808080'
                                    }]
                            },
                            legend: {
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle',
                                borderWidth: 0
                            },
                            series: [{
                                    name: 'Active Students',
                                    data: rowss2
                                }]
                        });

                        $('#bars').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Courses Students'
                            },
                            subtitle: {
                                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
                            },
                            xAxis: {
                                type: 'category',
                                labels: {
                                    rotation: -45,
                                    style: {
                                        fontSize: '13px',
                                        fontFamily: 'Verdana, sans-serif'
                                    }
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            tooltip: {
                                pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
                            }
                        });

                    });
                }
            }
        });
    });

</script>
@endsection