@extends('adminmaster')

@section('title')
    Edit {{$note->title_arabic}}
@endsection


@section('breadcrumb')
    <li class="active">Edit {{$note->title_arabic}} </li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {!! Form::model($note, array('route' => array('note.edit','ar', $note->id),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('title_arabic', 'Arabic Title') !!}
                        {!! Form::text('title_arabic', null , ['class' => 'form-control','placeholder'=>'Arabic Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title_english', 'English Title') !!}
                        {!! Form::text('title_english', null , ['class' => 'form-control','placeholder'=>'English Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('note_path', 'Note Path') !!}
                        {!! Form::text('note_path', null , ['class' => 'form-control','placeholder'=>'Note Path']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('author', 'Author') !!}
                        {!! Form::text('author', null , ['class' => 'form-control','placeholder'=>'Author']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('type', 'Sheet Type') !!}
                        {!! Form::text('type', null , ['class' => 'form-control','placeholder'=>'Sheet Type']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('year', 'Year') !!}
                        {!! Form::text('year', null , ['class' => 'form-control','placeholder'=>'Year']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('description', 'Arabic Description') !!}
                        {!! Form::textarea('description', null , ['class' => 'form-control','placeholder'=>'Arabic Description']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('description_english', 'English Seo Description') !!}
                        {!! Form::textarea('description_english', null , ['class' => 'form-control','placeholder'=>'EnglishDescription']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('order_number', 'Order Number') !!}
                        {!! Form::textarea('order_number', null , ['class' => 'form-control','placeholder'=>'Order Number']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_id', 'Course') !!}
                        {!! Form::select('course_id',$courses,'0',['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_open', 'Is Open') !!}
                        {!! Form::select('is_open',array('0'=>'no','1'=>'yes'),null,['class'=>'form-control']) !!}
                    </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection

