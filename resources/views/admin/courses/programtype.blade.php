@extends('adminmaster')

@section('title')
     program Types
@endsection
@section('breadcrumb')
    <li class="active">program Types </li>
@endsection
<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
@section('content')
<!-- This page displays all course filters which used to filter courses in courses page. -->
    <div class="row">
      @foreach($errors->all(':message') as $message)
          @if($message != "")
              <div id="form-messages" class="alert alert-success" role="alert">
                  {{ $message }}
              </div>
          @endif
      @endforeach()
      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif
        <!-- left column -->
        <a href="createprogram" class="btn btn-danger" style="margin-left: 347px;"> Create Types</a>
        <div class="col-md-12">
        <h2>Program Types </h2>

        <table id="users" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Name</th>
                <th></th>


            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Name</th>
                <th></th>

            </tr>
            </tfoot>
            <tbody>
              @if(!empty($program_types))
                @foreach($program_types as $type)
                                <tr>
                    <td>{{$type->name}}</td>
                    <td><a href="program/delete/{{$type->id}}">Delete</a></td>

                </tr>
            @endforeach
            @endif

            </tbody>
        </table>



    @endsection

    @section('scripts')
        <script>
            $(function () {

                $('#users').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });
        </script>
    @endsection
    </div>
