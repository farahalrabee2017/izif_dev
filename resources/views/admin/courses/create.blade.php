@extends('adminmaster')

@section('title')
    Create New Course
@endsection


@section('breadcrumb')
    <li class="active">Create New Course </li>
@endsection
<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open( array('route' => array('course.create','ar'),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('title_arabic', 'Arabic Title') !!}
                        {!! Form::text('title_arabic', null , ['class' => 'form-control','placeholder'=>'Arabic Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title_english', 'English Title') !!}
                        {!! Form::text('title_english', null , ['class' => 'form-control','placeholder'=>'English Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('url_identifier', 'Course URL') !!}
                        {!! Form::text('url_identifier', null , ['class' => 'form-control','placeholder'=>'Ex. Instrument/Course Name (Should Be Two Nodes)']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_intro_video_path', 'Course intro video path') !!}
                        {!! Form::text('course_intro_video_path', null , ['class' => 'form-control','placeholder'=>'course_intro_video_path']) !!}
                    </div>





                    <div class="form-group">
                        {!! Form::label('overview_arabic', 'Arabic Overview') !!}
                        {!! Form::textarea('overview_arabic', null , ['class' => 'form-control','placeholder'=>'Arabic Overview']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('overview_english', 'English overview') !!}
                        {!! Form::textarea('overview_english', null , ['class' => 'form-control','placeholder'=>'English Overview']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_meta_title', 'Meta Title') !!}
                        {!! Form::text('seo_meta_title', null , ['class' => 'form-control','placeholder'=>'Meta Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_description_arabic', 'Arabic Seo Description') !!}
                        {!! Form::textarea('seo_description_arabic', null , ['class' => 'form-control','placeholder'=>'Arabic Seo Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_description_english', 'English Seo Description') !!}
                        {!! Form::textarea('seo_description_english', null , ['class' => 'form-control','placeholder'=>'English Seo Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('cover_image', 'Cover Image') !!}
                        {!! Form::file('cover_image', null , ['class' => 'form-control','placeholder'=>'Cover Image']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}
                        {!! Form::input('number','price', null , ['class' => 'form-control','placeholder'=>'Price']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('user_id', 'Teacher') !!}
                        {!! Form::select('user_id',$groups,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                      <label>Course type</label>
                        <select name="course_type" id="course_type" class="form-control">
                      @foreach ($all_types as $type)
                          <option value="{{$type->id}}">{{$type->arabic_name}}</option>
                      @endforeach
                    </select>
                        </div>

                        <div class="form-group">
                          <label> Program Type</label>
                            <select name="programType" id="programType" class="form-control" style="display :none ;">
                          @foreach ($program_types as $type)
                              <option value="{{$type->name}}">{{$type->name}}</option>
                          @endforeach
                        </select>
                      </div>

                    <!-- <div id="programType" style="display:none;" class="form-group">
                        <label for="specify">Program Type :</label>
                        <input type="text" name="specify" placeholder="Specify Program Type" class="form-control"/>
                    </div> -->


                    <div class="form-group">
                        {!! Form::label('course_category', 'Course Category') !!}
                        {!! Form::select('course_category',array('all'=>'All','pop'=>'Pop Songs','folklore' => 'Arabic Folklore Songs'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('instrument_id', 'Instrument') !!}
                        {!! Form::select('instrument_id',$instruments,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_estimated_time', 'Time To Complete Course In Hours') !!}
                        {!! Form::input('number','course_estimated_time', null , ['class' => 'form-control','placeholder'=>'Time To Complete Course In Hours']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_order', 'Course Order') !!}
                        {!! Form::input('number','course_order', null , ['class' => 'form-control','placeholder'=>'Course Order']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('language', 'Course language') !!}
                        {!! Form::select('language',array('1'=>'Arabic','2'=>'English'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('level', 'Course Level') !!}
                        {!! Form::select('level',array('1'=>'beginner','2'=>'intermediate','3'=>'advance','4'=>'comprehensive'),null,['class'=>'form-control']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('is_active', 'Is Active') !!}
                        {!! Form::select('is_active',array('1'=>'yes','0'=>'no'),null,['class'=>'form-control']) !!}
                    </div>



                    <div class="form-group">
                        {!! Form::label('old_website_prefix', 'Old Site Prefix') !!}
                        {!! Form::text('old_website_prefix', null , ['class' => 'form-control','placeholder'=>'Old Site Prefix']) !!}
                    </div>



                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $('#course_type').on('change',function(){
            if( $(this).val()==="2"){
                $("#programType").show()
            }
            else{
                $("#programType").hide()
            }
        });

    </script>


@endsection
