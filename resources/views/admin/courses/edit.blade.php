@extends('adminmaster')

@section('title')
    Edit Course
@endsection


@section('breadcrumb')
    <li class="active">Edit Course</li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="_token" value="{!!  csrf_token() !!}">
                {!! Form::model($course, array('route' => array('course.edit','ar', $course->id),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('title_arabic', 'Arabic Title') !!}
                        {!! Form::text('title_arabic', $course->title_arabic , ['class' => 'form-control','placeholder'=>'Arabic Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title_english', 'English Title') !!}
                        {!! Form::text('title_english', $course->title_english , ['class' => 'form-control','placeholder'=>'English Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('url_identifier', 'Course URL') !!}
                        {!! Form::text('url_identifier', $course->url_identifier , ['class' => 'form-control','placeholder'=>'Ex. Instrument/Course Name (Should Be Two Nodes)']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_intro_video_path', 'Course intro video path') !!}
                        {!! Form::text('course_intro_video_path', null , ['class' => 'form-control','placeholder'=>'course_intro_video_path']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('overview_arabic', 'Arabic Overview') !!}
                        {!! Form::textarea('overview_arabic', $course->overview_arabic , ['class' => 'form-control','placeholder'=>'Arabic Overview']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('overview_english', 'English overview') !!}
                        {!! Form::textarea('overview_english', $course->overview_english , ['class' => 'form-control','placeholder'=>'English Overview']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_meta_title', 'Meta Title') !!}
                        {!! Form::text('seo_meta_title', $course->seo_meta_title , ['class' => 'form-control','placeholder'=>'Meta Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_description_arabic', 'Arabic Seo Description') !!}
                        {!! Form::textarea('seo_description_arabic', $course->seo_description_arabic , ['class' => 'form-control','placeholder'=>'Arabic Seo Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_description_english', 'English Seo Description') !!}
                        {!! Form::textarea('seo_description_english', $course->seo_description_english , ['class' => 'form-control','placeholder'=>'English Seo Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('cover_image', 'Cover Image') !!}
                        {!! Form::file('cover_image', null , ['class' => 'form-control','placeholder'=>'Cover Image']) !!}

                        @if ($course->cover_image)
                            <br/>
                            <center><img src="{{url('uploads/'.$course->cover_image)}}" width="200px"/></center>
                        @endif

                    </div>

                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}
                        {!! Form::input('number','price', $course->price , ['class' => 'form-control','placeholder'=>'Price']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('user_id', 'Teacher') !!}
                        {!! Form::select('user_id',$groups,$course->user_id,['class'=>'form-control']) !!}
                    </div>
                    <!-- <div class="form-group">
                        {!! Form::label('course_type', 'Course type') !!}
                        {!! Form::select('course_type',array('3'=>'دورات تعليم الموسيقى مع مدرّسي فريق إعزف','1'=>' دورات تعليم عزف الأغاني مع مدرسي فريق إعزف','2' => 'برامج إعزف التعليمية','4' => 'دورات أصدقاء إعزف'),null,['class'=>'form-control']) !!}
                    </div> -->
                    <?php
                        $type = explode(',',$course->course_type);
                    ?>
                    <div class="form-group">
                      <label> Course Type</label>
                    <select name="course_type[]" id="course_type" class="form-control" multiple>
<!--
                      @foreach ($all_types as $type1)
                          <option value="{{$type1->id}}" <?php foreach ($type as $value) {
                            echo $value == "{{$type1->id}}" ? " selected" : "";
                          }
                        ?>>{{$type1->arabic_name}}</option>
                      @endforeach -->
                        <option value="1" <?php foreach ($type as $value) {
                          echo $value == "1" ? " selected" : "";
                        }
                      ?>> دورات تعليم عزف الأغاني مع مدرسي فريق إعزف</option>
                        <option value="2" <?php foreach ($type as $value) {
                          echo $value == "2" ? " selected" : "";
                        }
                       ?>>برامج إعزف التعليمية</option>
                        <option value="3" <?php foreach ($type as $value) {
                          echo $value == "3" ? " selected" : "";
                        }
                      ?>>دورات تعليم الموسيقى مع مدرّسي فريق إعزف</option>
                        <option value="4" <?php foreach ($type as $value) {
                          echo $value == "4" ? " selected" : "";
                        }
                       ?>>دورات أصدقاء إعزف</option>
                    </select>
                        </div>
                        <!-- if the course has type programs show the program type field.. -->
                        @foreach($type as $value)
                          @if($value == "2")
                          <div class="form-group">
                            <label> Program Type</label>
                              <select name="programType" id="programType" class="form-control">
                                <option>{{$course->programType}}</option>
                          </select>
                        </div>

                          @endif
                          @endforeach
                          <div class="form-group">
                            <label> Program Type</label>
                              <select name="programType" id="programType" class="form-control" style="display :none ;">
                            @foreach ($program_types as $type)
                                <option value="{{$type->name}}">{{$type->name}}</option>
                            @endforeach
                          </select>
                        </div>

                    <!-- <div id="programType" style="display:none;" class="form-group" style="display :none ;">
                        <label for="programType">Program Type :</label>
                        <input type="text" name="programType" placeholder="Specify Program Type" class="form-control"/>
                    </div> -->

                    <div class="form-group">
                        {!! Form::label('course_category', 'Course Category') !!}
                        {!! Form::select('course_category',array('all'=>'All','pop'=>'Pop Songs','folklore' => 'Arabic Folklore Songs'),$course->course_category,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('instrument_id', 'Instrument') !!}
                        {!! Form::select('instrument_id',$instruments,$course->instrument_id,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_estimated_time', 'Time To Complete Course In Hours') !!}
                        {!! Form::input('number','course_estimated_time', $course->course_estimated_time , ['class' => 'form-control','placeholder'=>'Time To Complete Course In Hours']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('course_order', 'Course Order') !!}
                        {!! Form::input('number','course_order', $course->course_order , ['class' => 'form-control','placeholder'=>'Course Order']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('language', 'Course language') !!}
                        {!! Form::select('language',array('1'=>'Arabic','2'=>'English'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('level', 'Course Level') !!}
                        {!! Form::select('level',array('1'=>'beginner','2'=>'intermediate','3'=>'advance','4'=>'comprehensive'),null,['class'=>'form-control']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('is_active', 'Is Active') !!}
                        {!! Form::select('is_active',array('1'=>'yes','0'=>'no'),null,['class'=>'form-control']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('old_website_prefix', 'Old Site Prefix') !!}
                        {!! Form::text('old_website_prefix', null , ['class' => 'form-control','placeholder'=>'Old Site Prefix']) !!}
                    </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')
<script>
    $('#course_type').on('change',function(){
      var value = $(this).val();
      var items = JSON.parse("[" + value + "]");
      for(var i=0; i < items.length; i++) {
     if( items[i] == 2){
          $("#programType").show()
        }

}
    });

</script>


@endsection
