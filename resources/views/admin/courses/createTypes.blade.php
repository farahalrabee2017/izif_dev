@extends('adminmaster')

@section('title')
    Create New Course Types
@endsection


@section('breadcrumb')
    <li class="active">Create New Course Types </li>
@endsection
<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- form start -->
                {!!Form::open(array('url'=>'ar/admin/courses/types/create','method'=>'post'))!!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('arabic_name', 'Arabic Title') !!}
                        {!! Form::text('arabic_name', null , ['class' => 'form-control','placeholder'=>'Arabic Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'English Title') !!}
                        {!! Form::text('name', null , ['class' => 'form-control','placeholder'=>'English Title']) !!}

                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection

@section('scripts')


@endsection
