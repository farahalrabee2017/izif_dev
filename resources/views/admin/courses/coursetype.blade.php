@extends('adminmaster')

@section('title')
    Create New Course Types
@endsection
@section('breadcrumb')
    <li class="active">Create New Course Types </li>
@endsection
<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
@section('content')
<!-- This page displays all course filters which used to filter courses in courses page. -->
    <div class="row">
        <!-- left column -->
        <a href="createTypes" class="btn btn-danger" style="    margin-left: 347px;">Create Types</a>
        <div class="col-md-12">
        <h2>Course Types </h2>
        <table id="users" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Arabic name</th>
                <th>English name</th>
                <th></th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>Arabic name</th>
              <th>English name</th>
              <th></th>


            </tr>
            </tfoot>
            <tbody>
              @if(!empty($all_types))
                @foreach($all_types as $type)
                 <tr>
                 <td>{{$type->name}}</td>
                 <td>{{$type->arabic_name}}</td>
                 <td><a href="types/delete/{{$type->id}}">Delete</a></td>
                 </tr>
               @endforeach
               @endif
            </tbody>
        </table>



    @endsection

    @section('scripts')
        <script>
            $(function () {

                $('#users').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            });
        </script>
    @endsection
