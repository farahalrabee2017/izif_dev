@extends('adminmaster')

@section('title')
    Courses Views
@endsection


@section('breadcrumb')
    <li class="active">Courses Views</li>
@endsection


@section('head')
    <style>
        td {
            text-align: center;
            padding: 5px;
        }

        tr {
            border: 1px solid #ccc;
        }

        table {
            width: 100%
        }
    </style>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Courses Views</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center>
                            <input type="text" id="fromDate" class="datepicker"> <span>To</span>
                            <input type="text" id="toDate" class="datepicker">
                            <input id="search" type="button" value="Search" disabled>
                            <div class="spinner search">
                                <div class="rect1"></div>
                                <div class="rect2"></div>
                                <div class="rect3"></div>
                                <div class="rect4"></div>
                                <div class="rect5"></div>
                            </div>
                        </center>
                    </div>
                    <hr/>
                    <table>
                        @foreach($courses as $course)
                            <tr>
                                <td><a href="javascript:$('tr[data-id=\'{{$course->id}}\']').toggle();" style="color:#000"><i class="fa fa-plus-circle" aria-hidden="true" style="font-size: 30px"></i></a></td>
                                <td>
                                    <h2>{{$course->title_arabic}}</h2>
                                </td>
                                <td data-id="{{$course->id}}">
                                    <div class="spinner">
                                        <div class="rect1"></div>
                                        <div class="rect2"></div>
                                        <div class="rect3"></div>
                                        <div class="rect4"></div>
                                        <div class="rect5"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr  style="display: none" data-id="{{$course->id}}">
                                <td colspan="3">

                                    <table>
                                        @foreach($course->videos as $video)
                                            <tr>
                                                <td>{{$video->title_arabic}} | {{$video->title_english}}</td>
                                                <td data-videoid="{{$video->id}}">
                                                    <div class="spinner">
                                                        <div class="rect1"></div>
                                                        <div class="rect2"></div>
                                                        <div class="rect3"></div>
                                                        <div class="rect4"></div>
                                                        <div class="rect5"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->


            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function getviews(id, callback,from,to) {
            $.ajax({
                url: "/ar/admin/views/course/" + id,
                method:'POST',
                data:{
                    from:from,
                    to:to
                }
            }).done(function (data) {
                var views = eval('(' + data + ')');
                var viewsCount = views.length;
                $($('td[data-id="'+id+'"]').parent().find('.spinner')).hide();
                $('td[data-id="'+id+'"]').prepend('<h3>'+viewsCount+'<h3>');
                callback();
            });
        }

        function getVideoviews(id,callback,from,to){
            $.ajax({
                url: "/ar/admin/views/video/" + id,
                method:'POST',
                data:{
                    from:from,
                    to:to
                }
            }).done(function (data) {
                var views = eval('(' + data + ')');
                var viewsCount = views.length;
                $($('td[data-videoid="'+id+'"]').parent().find('.spinner')).hide();
                $('td[data-videoid="'+id+'"]').prepend('<h3>'+viewsCount+'<h3>');
                callback();
            });
        }


        function getData(from,to){
            var $courses = $('td[data-id]');
            $courses.each(function () {
                getviews($(this).data('id'), function () {

                },from,to);
            });


            var $videos = $('td[data-videoid]');
            $videos.each(function(){
                getVideoviews($(this).data('videoid'), function () {

                },from,to);
            });

            $('#search').removeAttr('disabled');
            $('div.search').hide();
        }
        $(document).ready(function () {

            $('.datepicker').datetimepicker();


            $("#search").click(function () {
                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                $('td h3').remove();
                $('.spinner').show();
                getData(fromDate,toDate);
            });

            getData();

        });
    </script>
@endsection