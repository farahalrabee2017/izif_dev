@extends('adminmaster')

@section('title')
    Downloadable Files
@endsection


@section('breadcrumb')
    <li class="active">Downloadable Files</li>
@endsection


@section('content')

    <a href='{{url("/ar/admin/downloadables/create/$id")}}' class="btn btn-block btn-info" style="width:300px">Create a Downloadable File For this Video</a>
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>File Path</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Videos List</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>

        @foreach ($files as $file)
            <tr>
                <td>{{$file->id}}</td>
                <td>{{$file->title_ara}}</td>
                <td><a href='{{url("$file->file_path_ara")}}'>{{$file->file_path_ara}}</a></td>
                <td><a href='{{url("/ar/admin/downloadables/edit/$file->id/$file->video_id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/downloadables/delete/$file->id/$file->video_id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection