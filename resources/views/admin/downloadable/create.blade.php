@extends('adminmaster')

@section('title')
    Create New Downloadable File
@endsection


@section('breadcrumb')
    <li class="active">Create New Downloadable File</li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->


            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open( array('route' => array('downloadable.create','ar'),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('title_ara', 'Arabic Name') !!}
                        {!! Form::text('title_ara', null , ['class' => 'form-control','placeholder'=>'Arabic Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title_enu', 'English Name') !!}
                        {!! Form::text('title_enu', null , ['class' => 'form-control','placeholder'=>'English Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('file_path_ara', 'File In Arabic') !!}
                        {!! Form::file('file_path_ara', null , ['class' => 'form-control','placeholder'=>'File In Arabic']) !!}
                    </div>



                    <div class="form-group">
                        {!! Form::label('file_path_enu', 'File In English') !!}
                        {!! Form::file('file_path_enu', null , ['class' => 'form-control','placeholder'=>'File In English']) !!}
                    </div>



                    {!! Form::input('hidden','video_id',$id) !!}
                    {!! Form::input('hidden','course_id',$video->course_id) !!}


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->

        </div>
    </div>

@endsection

@section('scripts')

@endsection