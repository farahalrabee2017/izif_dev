@extends('adminmaster')

@section('title')
    Edit {{$testimonial->english_name}}
@endsection


@section('breadcrumb')
    <li class="active">Edit {{$testimonial->english_name}} </li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                {!! Form::model($testimonial, array('route' => array('testimonial.edit','ar', $testimonial->id),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null , ['class' => 'form-control','placeholder'=>'The Student Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('name_enu', 'Name English') !!}
                        {!! Form::text('name_enu', null , ['class' => 'form-control','placeholder'=>'The Student Name English']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('testimonial', 'Student Testimonial Arabic') !!}
                        {!! Form::textarea('testimonial', null , ['class' => 'form-control','placeholder'=>'Student Testimonial Arabic']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('testimonial_english', 'Student Testimonial English') !!}
                        {!! Form::textarea('testimonial_english', null , ['class' => 'form-control','placeholder'=>'Student Testimonial English']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('photo', 'Student Photo - Image Size (150px X 239px)') !!}
                        {!! Form::file('photo', null , ['class' => 'form-control','placeholder'=>'Student Photo']) !!}

                        <br/>
                        @if($testimonial->photo)
                            <img src="{{$testimonial->photo}}"/>
                        @endif

                    </div>




                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection

