@extends('adminmaster')

@section('title')
    Testimonials
@endsection


@section('breadcrumb')
    <li class="active">Testimonials</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach ($testimonials as $testimonial)
            <tr>
                <td>{{$testimonial->id}}</td>
                <td>{{$testimonial->name}}</td>
                <td><a href='{{url("/ar/admin/testimonial/edit/$testimonial->id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/testimonial/delete/$testimonial->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection