@extends('adminmaster')

@section('title')
    Videos
@endsection


@section('breadcrumb')
    <li class="active">Videos</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Course</th>
            <th>Teacher</th>
            <th>Order</th>
            <th>Downloadables</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Course</th>
            <th>Teacher</th>
            <th>Order</th>
            <th>Downloadables</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach ($videos as $video)
            <tr>
                <td>{{$video->id}}</td>
                <td>{{$video->title_arabic}}</td>
                <td>{{$video->course->title_arabic}}</td>
                <td>{{$video->teacher->name}}</td>
                <td>{{$video->video_order}}</td>
                <td><a href='{{url("/ar/admin/downloadables/list/$video->id")}}' class="btn btn-block btn-info btn-xs">Downloadable Files</a></td>
                <td><a href='{{url("/ar/admin/video/edit/$video->id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/video/delete/$video->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection