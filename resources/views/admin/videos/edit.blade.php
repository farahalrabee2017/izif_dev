@extends('adminmaster')

@section('title')
    Create New Video
@endsection


@section('breadcrumb')
    <li class="active">Create New Video </li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($video, array('route' => array('video.edit','ar', $video->id),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('title_arabic', 'Arabic Title') !!}
                        {!! Form::text('title_arabic', $video->title_arabic , ['class' => 'form-control','placeholder'=>'Arabic Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title_english', 'English Title') !!}
                        {!! Form::text('title_english', $video->title_english , ['class' => 'form-control','placeholder'=>'English Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('video_path', 'Video Path') !!}
                        {!! Form::text('video_path', $video->video_path , ['class' => 'form-control','placeholder'=>'Video Path On Media Server']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('url_identifier', 'Video URL') !!}
                        {!! Form::text('url_identifier', $video->url_identifier , ['class' => 'form-control','placeholder'=>'Video Name']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('description_arabic', 'Arabic Description') !!}
                        {!! Form::textarea('description_arabic', $video->description_arabic , ['class' => 'form-control','placeholder'=>'Arabic Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description_english', 'English description') !!}
                        {!! Form::textarea('description_english', $video->description_english , ['class' => 'form-control','placeholder'=>'English Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_meta_title', 'Meta Title') !!}
                        {!! Form::text('seo_meta_title', $video->seo_meta_title , ['class' => 'form-control','placeholder'=>'Meta Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_description_arabic', 'Arabic Seo Description') !!}
                        {!! Form::textarea('seo_description_arabic', $video->seo_description_arabic , ['class' => 'form-control','placeholder'=>'Arabic Seo Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('seo_description_english', 'English Seo Description') !!}
                        {!! Form::textarea('seo_description_english', $video->seo_description_english , ['class' => 'form-control','placeholder'=>'English Seo Description']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('cover_image', 'Cover Image') !!}
                        {!! Form::file('cover_image', null , ['class' => 'form-control','placeholder'=>'Cover Image']) !!}
                        <br/>
                        @if ($video->cover_image)
                            <br/>
                            <center><img src="{{url('uploads/'.$video->cover_image)}}" width="200px"/></center>
                        @endif

                    </div>

                    <div class="form-group">
                        {!! Form::label('course_id', 'Course') !!}
                        {!! Form::select('course_id',$courses,$video->course_id,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('user_id', 'Teacher') !!}
                        {!! Form::select('user_id',$groups,$video->user_id,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('instrument_id', 'Instrument') !!}
                        {!! Form::select('instrument_id',$instruments,$video->instrument_id,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('song_type', 'Song type') !!}
                        {!! Form::select('song_type',array('all'=>'All','pop'=>'Pop Songs','folklore' => 'Arabic Folklore Songs'),$video->course_type,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('estimated_time', 'Time To Complete Video In minutes') !!}
                        {!! Form::input('number','estimated_time', $video->estimated_time , ['class' => 'form-control','placeholder'=>'Time To Complete Video In minutes']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('video_order', 'Video Order') !!}
                        {!! Form::input('number','video_order', $video->video_order , ['class' => 'form-control','placeholder'=>'Video Order']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('free_order_number', 'Video Order On Free Videos Section') !!}
                        {!! Form::input('number','free_order_number', $video->free_order_number, ['class' => 'form-control','placeholder'=>'Free Order']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('video_number', 'Video Number') !!}
                        {!! Form::text('video_number', null , ['class' => 'form-control','placeholder'=>'Video Number']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_youtube', 'Is Youtube') !!}
                        {!! Form::select('is_youtube',array('0'=>'no','1'=>'yes'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_open', 'Is Open') !!}
                        {!! Form::select('is_open',array('0'=>'no','1'=>'yes'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_active', 'Is Active') !!}
                        {!! Form::select('is_active',array('0'=>'no','1'=>'yes'),null,['class'=>'form-control']) !!}
                    </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection