@extends('adminmaster')

@section('title')
    Courses
@endsection


@section('breadcrumb')
    <li class="active">Courses</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Videos List</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Videos List</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach ($courses as $course)
            <tr>
                <td>{{$course->id}}</td>
                <td>{{$course->title_arabic}}</td>
                <td><a href='{{url("/ar/admin/videos/$course->id")}}' class="btn btn-block btn-success btn-xs">Videos List</a></td>
                <td><a href='{{url("/ar/admin/course/edit/$course->id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/course/delete/$course->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection