@extends('adminmaster')

@section('title')
    Instrument
@endsection


@section('breadcrumb')
    <li class="active">Instrument</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach ($instruments as $instrument)
            <tr>
                <td>{{$instrument->id}}</td>
                <td>{{$instrument->english_name}}</td>
                <td><a href='{{url("/ar/admin/instrument/edit/$instrument->id")}}' class="btn btn-block btn-info btn-xs">Edit</a></td>
                <td><a href='{{url("/ar/admin/instrument/delete/$instrument->id")}}' class="btn btn-block btn-danger btn-xs">Delete</a></td>

            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection