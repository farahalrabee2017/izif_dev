@extends('adminmaster')

@section('title')
    Create New Instrument
@endsection


@section('breadcrumb')
    <li class="active">Create New Instrument </li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open( array('route' => array('instrument.create','ar'),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('arabic_name', 'Arabic Name') !!}
                        {!! Form::text('arabic_name', null , ['class' => 'form-control','placeholder'=>'Arabic Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('english_name', 'English Name') !!}
                        {!! Form::text('english_name', null , ['class' => 'form-control','placeholder'=>'English Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('icon', 'Icon') !!}
                        {!! Form::file('icon', null , ['class' => 'form-control','placeholder'=>'Icon']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('big_image', 'Big Image') !!}
                        {!! Form::file('big_image', null , ['class' => 'form-control','placeholder'=>'Big Image']) !!}
                    </div>




                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection