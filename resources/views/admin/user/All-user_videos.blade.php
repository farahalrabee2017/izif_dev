@extends('adminmaster')
@section('title')
    Name :{{$user->name}} <br>
@endsection
@section('breadcrumb')
    <li class="active">Users</li>
@endsection
@section('content')

    <div>
        <b>Total views : </b>{{$total}} Videos <br/>
        @foreach ($payment->payment_method as $payments)
            <b>Payment Method :</b> {{$payments->name}} <br/>
        @endforeach
        <b> Join Date : </b><?php echo date('Y/m/d', strtotime($user->created_at)); ?><br/>
    </div>
    <div>
        <b>Subscription :</b><br/>
        @foreach ($subscription as $subscription)
            {{$subscription->subscriptions_model->title_ara}}<br>

            <b>Subscription date :</b> {{$subscription->created_at}}<br/>
            <?php
            $days_between = $subscription->created_at->diff($user->created_at)->format("%a ");?>
            <b>Trial days : </b>{{$days_between}} Days <br>
        @endforeach

        <p/>

    </div>
    <?php
    function in_array_r($item, $array)
    {
        return preg_match('/"' . $item . '"/i', json_encode($array));
    }
    if (!empty($_GET['from']) && (!empty($_GET['to']))) {
        $from = $_GET['from'];
        $to = $_GET['to'];
    } else {
        $from = "2000/01/01";
        $to = "2030/01/01";
    }
    $count = 1;
    ?>
    <?php
    $arrayToSearch = [];
    $Video_count = 0;
    ?>

    @foreach($result as $course)
        <?php
        $course_date = date('Y/m/d', strtotime($course->ViewD));

        if ((date('Y/m/d', strtotime($from)) <= $course_date) && ($course_date <= date('Y/m/d', strtotime($to)))) {
            $Video_count++;
            if (in_array_r($course->course_id, $arrayToSearch)) {
                $arrayToSearch[$course->ctit][$course->course_id] = $arrayToSearch[$course->ctit] [$course->course_id] + 1;
            } else {
                $arrayToSearch[$course->ctit][$course->course_id] = 1;
            }
        }
        ?>
    @endforeach
    @if((count($result) > 0))
        <hr/>
        <form id="myform" name="myform" action='' method='get'>
            <div style="width: 60%; margin: 0 auto;">
                <label>From</label>
                <input class='datepicker' type='text' name='from' value="<?php if ($from != "2000/01/01") {
                    echo "$from";
                } else {
                    echo "YYYY/MM/DD";
                }  ?>">
                <label>To</label>
                <input class='datepicker' type='text' name='to' value="<?php if ($to != "2030/01/01") {
                    echo "$to";
                } else {
                    echo "YYYY/MM/DD";
                } ?>">
                <input type='submit' value='Filter'>
                <input align="middle" id="button" onClick="resetform()" type="button" value="Reset Form">
            </div>
        </form>
        <hr/>

        <table id="list" class="display dataTable" cellspacing="0" width="100%" border="1" align="center ">
            <thead>
            <tr>
                <th>ID</th>
                <th> Video title</th>
                <th>Status</th>
                <th> Course name</th>
                <th>Course percentage</th>
                <th>Time</th>
            </tr>
            </thead>
            @foreach ($result as $video)
                <?php
                $v_identifier = $video->Vid;
                $courseslug = $video->Cid;
                $date = date('Y/m/d', strtotime($video->ViewD));
                if ($video->videot) {
                    $lang = "ar";
                } elseif ($video->title_english) {
                    $lang = "en";
                }
                if ((date('Y/m/d', strtotime($from)) <= $date) && ($date <= date('Y/m/d', strtotime($to)))) {
                ?>
                <tr align="center">
                    <td>
                        {{$count}}

                    </td>
                    <td>
                        <a href="/{{$lang}}/videos/{{$courseslug}}/{{$v_identifier}}">  {{$video->videot}} {{$video->title_english}}</a> </li>
                    </td>
                    <td>
                        {{$video->status}}
                    </td>
                    <td>
                        <a href="/{{$lang}}/course/{{$courseslug}}"> {{$video->ctit}}</a>
                    </td>
                    <td>
                        <?php
                        foreach ($arrayToSearch as $key => $obj) {
                            foreach ($obj as $course_count) {
                                if ($key == $video->ctit) {

                                    echo "<br/>" . "percentage : " . substr(($course_count / $Video_count) * 100, 0, 3) . "% <br/>";
                                }
                            }
                        }
                        ?>
                    </td>
                    <td>{{$date}}</td>
                </tr>
                <?php
                $count++;
                }
                ?>
            @endforeach

        </table>
    @else
        <h4>This user didn't complete any video</h4>
    @endif
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#list').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
    <script>
        function resetform() {
            location.replace("/ar/admin/user/{{$user->id}}")
        }
    </script>
@endsection
