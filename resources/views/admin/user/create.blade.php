@extends('adminmaster')

@section('title')
    Create New User
@endsection


@section('breadcrumb')
    <li class="active">Create New User</li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open( array('route' => array('user.create','ar'), 'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null , ['class' => 'form-control','placeholder'=>'Full Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('name_enu', 'Name English') !!}
                        {!! Form::text('name_enu', null , ['class' => 'form-control','placeholder'=>'Full Name English']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('about_ara', 'Bio Arabic') !!}
                        {!! Form::textarea('about_ara', null , ['class' => 'form-control','placeholder'=>'Bio - Only For Teachers Arabic']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('about_enu', 'Bio English') !!}
                        {!! Form::textarea('about_enu', null , ['class' => 'form-control','placeholder'=>'Bio - Only For Teachers English']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('email', 'Email Address') !!}
                        {!! Form::email('email', null , ['class' => 'form-control','placeholder'=>'Email']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('telephone', 'Telephone') !!}
                        {!! Form::input('number','telephone', null , ['class' => 'form-control','placeholder'=>'Telephone']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('country_id', 'Country') !!}
                        {!! Form::select('country_id', $country,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('user_group_id', 'Group') !!}
                        {!! Form::select('user_group_id', $group ,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('gender', 'Gender') !!}
                        {!! Form::select('gender', ['male','female'] ,null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('age', 'Birth date') !!}
                        {!! Form::text('age', null, array('id' => 'datepicker','class'=>'form-control','placeholder'=>'Age'))   !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('password', 'Passowrd') !!}
                        {!! Form::input('password','password', null , ['class' => 'form-control','placeholder'=>'Password']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('profile_image', 'Profile Picture') !!}
                        {!! Form::file('profile_image', null , ['class' => 'form-control','placeholder'=>'Profile Picture']) !!}
                    </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection