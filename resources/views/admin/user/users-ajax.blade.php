@extends('adminmaster')

@section('title')
Users
@endsection


@section('breadcrumb')
<li class="active">Users</li>
@endsection


@section('content')
<hr/>
<table id="users" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>email</th>
            <!--<th>phone</th>
            <!--<th>Group</th>-->
            <th>Edit</th>
            <th>Delete</th>

        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>email</th>
            <!--<th>phone</th>
            <!--<th>Group</th>-->
            <th>Edit</th>
            <th>Delete</th>

        </tr>
    </tfoot>
</table>



@endsection

@section('scripts')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
    $(function () {



        $('#users').DataTable({
            "processing": true,
            "serverSide": true,
//                ajax: 'https://datatables.yajrabox.com/fluent/union-data',

            "ajax": "{{url('/' . Lang::getLocale() . '/admin/usersAjax' )}}",


            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name',
                    "render": function ( data, type, row) {
                        return '<a href="user/'+row['id']+'">'+data+'</a>';
                    }
                },
                {data: 'email', name: 'email'},
                {data: 'Edit', name: 'Edit' ,searchable: false},
                {data: 'Delete', name: 'Delete',searchable: false}
            ],
//                "paging": true,
//                "lengthChange": true,
//                "searching": true,
//                "ordering": true,
//                "info": true,
//                "autoWidth": true,
//                 dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
        });
    });
</script>
@endsection