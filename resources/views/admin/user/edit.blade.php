@extends('adminmaster')

@section('title')
    Edit User - {{$user->name}}
@endsection


@section('breadcrumb')
    <li class="active">Edit Users</li>
@endsection


@section('content')

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::model($user, array('route' => array('user.edit','ar', $user->id),'files' => true)) !!}
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', $user->name , ['class' => 'form-control','placeholder'=>'Full Name']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('name_enu', 'Name English') !!}
                        {!! Form::text('name_enu', null , ['class' => 'form-control','placeholder'=>'Full Name English']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('about_ara', 'Bio Arabic') !!}
                        {!! Form::textarea('about_ara', null , ['class' => 'form-control','placeholder'=>'Bio - Only For Teachers Arabic']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('about_enu', 'Bio English') !!}
                        {!! Form::textarea('about_enu', null , ['class' => 'form-control','placeholder'=>'Bio - Only For Teachers English']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email Address') !!}
                        {!! Form::email('email', $user->email , ['class' => 'form-control','placeholder'=>'Email']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('telephone', 'Telephone') !!}
                        {!! Form::input('number','telephone', $user->telephone , ['class' => 'form-control','placeholder'=>'Telephone']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('country_id', 'Country') !!}
                        {!! Form::select('country_id', $country,$user->country_id,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('user_group_id', 'Group') !!}
                        {!! Form::select('user_group_id', $group ,$user->user_group_id,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('gender', 'Gender') !!}
                        {!! Form::select('gender', ['male','female'] ,$user->gender,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('age', 'Birth date') !!}
                        {!! Form::text('age', $user->age, array('id' => 'datepicker','class'=>'form-control'))   !!}
                        </div>


                    <div class="form-group">
                        <hr/>
                        <b>Add ($2y$10$vLr/r3tQjCD4rcXtkxRZIuwUHcVNarVmcVyGPSFD6CIIUpp68P4Sq) to reset the password to (izif.com)</b>
                        <hr/>

                        {!! Form::label('password', 'Passowrd') !!}
                        {!! Form::input('password','password', null , ['class' => 'form-control','placeholder'=>'Password']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('profile_image', 'Profile Picture') !!}
                        {!! Form::file('profile_image', null , ['class' => 'form-control','placeholder'=>'Profile Picture']) !!}
                    </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->


        </div>
    </div>

@endsection

@section('scripts')

@endsection