@extends('adminmaster')

@section('title')
    User Tracking
@endsection


@section('breadcrumb')
    <li class="active">User Tracking</li>
@endsection


@section('content')
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Date</th>
            <th>IP</th>
            <th>Source</th>
            <th>Medium</th>
            <th>Campaign</th>
            <th>Is Subscribed</th>

            <th>User</th>

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>Date</th>
            <th>IP</th>
            <th>Source</th>
            <th>Medium</th>
            <th>Campaign</th>
            <th>Is Subscribed</th>

            <th>User</th>

        </tr>
        </tfoot>
        <tbody>
        @foreach ($tracking as $track)
            <tr>

                <td>{{$track->created_at}}</td>
                <td>{{$track->ip}}</td>
                <td>{{$track->utm_source}}</td>
                <td>{{$track->utm_medium}}</td>
                <td>{{$track->utm_campaign}}</td>
                <td>@if($track->subscription) @if(count($track->subscription) > 0) Yes @else No @endif @endif</td>
                <td>@if(count($track->user) > 0) {{$track->user->email}} @else Anonymous @endif</td>


            </tr>
        @endforeach
        </tbody>
    </table>



@endsection

@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection