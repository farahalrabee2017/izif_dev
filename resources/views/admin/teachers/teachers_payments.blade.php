@extends('adminmaster')

@section('title')
    Teachers Billing
@endsection


@section('breadcrumb')
    <li class="active">Teachers Billing</li>
@endsection


@section('head')

@endsection


@section('content')

    <hr/>
    <h3>number Of unique Views : {{$numberOfViews}}</h3>
    <h3>number Of Subscriptions For people watching this teachers Courses : {{count($subscriptions)}}</h3>

    <h3>Subscriptions On Teachers Courses that Ended And Due for payment</h3>
    <hr/>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>user</th>
            <th>course</th>
            <th>subscription model</th>
            <th>starts</th>
            <th>ends</th>


        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>user</th>
            <th>course</th>
            <th>subscription model</th>
            <th>starts</th>
            <th>ends</th>

        </tr>
        </tfoot>
        <tbody>
        @foreach ($subscriptions as $subscription)
            <tr>
                <td>{{$subscription->id}}</td>
                <td>{{$subscription->user->email}}</td>
                @if(isset($subscription->course->title_arabic))
                    <td>{{$subscription->course->title_arabic}}</td>
                @else
                    <td>No course</td>
                @endif
                <td>{{$subscription->subscriptions_model->title_ara}}</td>
                <td>{{$subscription->start_date}}</td>
                <td>{{$subscription->end_date}}</td>

            </tr>
        @endforeach
        </tbody>
    </table>


@endsection


@section('scripts')

    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
    </script>
@endsection