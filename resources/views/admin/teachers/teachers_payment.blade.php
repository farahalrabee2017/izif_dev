@extends('adminmaster')

@section('title')
    Teachers Billing
@endsection


@section('breadcrumb')
    <li class="active">Teachers Billing</li>
@endsection


@section('head')

@endsection


@section('content')

    <form method="post">
        <select name="teacher_id">
            @foreach($teachers as $teacherl)
                <option value="{{$teacherl->id}}">{{$teacherl->name}}</option>
            @endforeach
        </select>
        <input type="submit" value="Calculate Profit">
    </form>

    <hr/>
    @if($teacher_id)
        <h1>{{$teacher->name}}</h1>
    @endif
    <hr/>

    <h3>Profit Amount : {{$fullAmount}}</h3>
    <hr/>

    <h4>Profit Details List :</h4>
    <table id="users" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>

            <th>Amount</th>
            <th>Number Of Views</th>
            <th>Views Percentage</th>

            <th>Teacher</th>
            <th>User</th>
            <th>Subscription Model</th>
            <th>Date</th>


        </tr>
        </thead>
        <tfoot>
        <tr>

            <th>Amount</th>
            <th>Number Of Views</th>
            <th>Views Percentage</th>

            <th>Teacher</th>
            <th>User</th>
            <th>Subscription Model</th>
            <th>Date</th>

        </tr>
        </tfoot>
        <tbody>
        @foreach ($billings as $billing)
            <tr>

                <td>{{$billing->amount}}</td>
                <td>{{$billing->number_of_views}}</td>
                <td>{{$billing->views_percentage}}%</td>

                <td>{{$billing->teacher->name}}</td>
                <td>{{$billing->user->email}}</td>
                <td>{{$billing->subscription_model->title_enu}}</td>
                <td>{{$billing->subscription->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <hr/>
    <h3> Single Courses</h3>

    <table id="userss" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Title Arabic</th>
            <th>Title English</th>
            <th>Amount</th>
            <th>Amount After Payment Gateway Fees</th>
            <th>Profit Amount</th>

            <th>User</th>
            <th>Start Date</th>
            <th>End Date</th>


        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Title Arabic</th>
            <th>Title English</th>
            <th>Amount</th>
            <th>Amount After Payment Gateway Fees</th>
            <th>Profit Amount</th>
            <th>User</th>
            <th>Start Date</th>
            <th>End Date</th>


        </tr>
        </tfoot>
        <tbody>
        @foreach ($singleCourses as $singleCourse)
            @if ($singleCourse->payment->invoice_id != "old_website" AND $singleCourse->payment->invoice_id != "Olduser" AND $singleCourse->payment->amount != "0" AND $singleCourse->payment->amount != "")
                <tr>
                    <td>{{$singleCourse->id}}</td>

                    <td>{{$singleCourse->course->title_arabic}}</td>

                    <td>{{$singleCourse->course->title_english}}</td>

                    <td>{{$singleCourse->payment->amount}}</td>

                    <td>
                        <?php

                        $subscriptionFees = array(
                            //paypal
                                "1" => array(
                                        "4.9%",
                                        "$0.30"
                                ),

                            //2checkout
                                "2" => array(
                                        "5.5%",
                                        "$0.45"
                                ),

                            //cashu
                                "3" => array(
                                        "7%"
                                ),

                            //westren union
                                "4" => array(
                                        "1%"
                                ),

                            //Bank Trasfare
                                "5" => array(
                                        "0.5%",
                                        "$7.05"
                                )
                        )[$singleCourse->payment->payment_method_id];
                        $subscriptionAmount = $singleCourse->payment->amount;

                        foreach ($subscriptionFees as $subscriptionFee) {

                            // If is Percantage
                            if (strpos($subscriptionFee, '%') !== false) {
                                $fees = $subscriptionAmount * (floatval($subscriptionFee) / 100);
                                $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;

                                // IF $ Ammount
                            } elseif (strpos($subscriptionFee, '$') !== false) {
                                $fees = number_format(str_replace('$', '', $subscriptionFee), 2);
                                $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;
                            }

                        }

                        echo $subscriptionAmount;
                        ?>
                    </td>

                    <td>

                    <?php

                        $subscriptionFees = array(
                            //paypal
                                "1" => array(
                                        "4.9%",
                                        "$0.30"
                                ),

                            //2checkout
                                "2" => array(
                                        "5.5%",
                                        "$0.45"
                                ),

                            //cashu
                                "3" => array(
                                        "7%"
                                ),

                            //westren union
                                "4" => array(
                                        "1%"
                                ),

                            //Bank Trasfare
                                "5" => array(
                                        "0.5%",
                                        "$7.05"
                                )
                        )[$singleCourse->payment->payment_method_id];
                        $subscriptionAmount = $singleCourse->payment->amount;

                        foreach ($subscriptionFees as $subscriptionFee) {

                            // If is Percantage
                            if (strpos($subscriptionFee, '%') !== false) {
                                $fees = $subscriptionAmount * (floatval($subscriptionFee) / 100);
                                $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;

                                // IF $ Ammount
                            } elseif (strpos($subscriptionFee, '$') !== false) {
                                $fees = number_format(str_replace('$', '', $subscriptionFee), 2);
                                $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;
                            }

                        }

                        echo $subscriptionAmount * $teacher_percantage;

                            ?>

                    </td>


                    <td>{{$singleCourse->user->email}}</td>

                    <td>{{$singleCourse->start_date}}</td>

                    <td>{{$singleCourse->end_date}}</td>

                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    <hr/>


@endsection


@section('scripts')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script>
        $(function () {

            $('#users').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $('#userss').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });


        });
    </script>


@endsection