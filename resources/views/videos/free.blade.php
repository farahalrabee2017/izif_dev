@extends('master')

@section('title', 'free lessons')


@section('head')

    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>
        .fa-square {
            float: right;
            margin: 1px;
            font-size: 15px;
            color: #400000;
        }

        .dis {
            color: #cccccc;
        }

        .level {
            clear: both;
            float: left;
        }

        .teacher {
            float: right;
            color: #888787;
        }

        .teacher i, .level i {

            color: #888787;
            font-size: 16px;
        }

        .level {
            color: #888787;
        }

    </style>

@endsection


@section('content')

    <div class="icons_music" id="margin_top_only">


        @include('partials.instruments')


    </div>


    <div class="container" style="padding-top:25px">
        <div class="row" id="Container">

            @foreach ($videos as $video)

                @if($video->course->language == $lang)
                    <div class="col-md-4 mix" data-instrument="{{$video->instrument->id}}">
                        <a href='{{url("/" . Lang::getLocale() . "/videos/".$video->course->url_identifier . "/" . $video->url_identifier)}}'>
                            <div class="videos-box_2">
                                <div class="title_video_2" style="background: none">
                            <span style="font-size:11px;color:#400000">
                                @if(Lang::getLocale() == 'ar')
                                    {{$video->title_arabic}}
                                @else
                                    {{$video->title_english}}
                                @endif
                            </span>
                                </div>
                                <div class="video-array_2">
                                    <button><img src="{{url('static/img/video_icon.png')}}"></button>

                                    @if($video->course->cover_image)
                                        <img class="img_test_2" src="{{url('/static/img/buffer-loading.gif')}}"
                                             data-src="{{url('/uploads/'.$video->course->cover_image)}}"
                                             style="border-radius: 10px;">

                                    @else
                                        <img class="img_test_2" src="{{url('static/img/m-1.png')}}"
                                             style="border-radius: 10px;">
                                    @endif
                                </div>
                                <div class="dis_video_2">
                            <span>

                            </span>
                                    <div class="level">
                                        <i class="fa fa-signal"></i> {{trans('course.'.$video->course->level.'-level')}}
                                    </div>

                                    <div class="teacher">
                                        <i class="fa fa-graduation-cap"></i>

                                        @if(Lang::getLocale() == "ar")
                                            {{$video->teacher->name}}
                                        @else
                                            {{$video->teacher->name_enu}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
            <div style="clear: both"></div>
            <br/><br/>
        </div>

    </div>


@endsection


@section('footer')
    <script type="text/javascript" src="{{url('static/js/jquery.unveil.js')}}"></script>
    <script type="text/javascript" src="{{url('static/mixitup-master/build/jquery.mixitup.min.js')}}"></script>
    <script>

        $('.freeVideosMenu').addClass('activeMenu');

        var $lazyLoad = $("img[data-src]");

        $lazyLoad.unveil(200, function () {
            $(this).load(function () {
                this.style.opacity = 1;
            });
        });

        $('.mix img:lt(6)').trigger("unveil");


        $('#Container').mixItUp({
            animation: {
                duration: 400,
                effects: 'fade translateZ(-360px) stagger(34ms)',
                easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)'
            },
            'sort': 'order:asc'
        });
    </script>

@endsection