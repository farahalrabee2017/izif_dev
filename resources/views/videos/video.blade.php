@extends('master')

@section('title', $video->seo_meta_title)


@section('head')


    @include('partials.commentsfile')

    @if(Auth::check())
        @if(Auth::user()->user_group_id == 2)
            <link rel="stylesheet" href="//cdn.jsdelivr.net/medium-editor/latest/css/medium-editor.min.css"
                  type="text/css" media="screen" charset="utf-8">
        @endif
    @endif

    <script type="text/javascript" src="http://yandex.st/swfobject/2.2/swfobject.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <link href="{{url('node_modules/video.js/dist/video-js.css')}}" rel="stylesheet">
    <link href="{{url('node_modules/videojs-resolution-switcher/lib/videojs-resolution-switcher.css')}}"
          rel="stylesheet">


    <style>
        .vjs-control-bar {
            direction: ltr;
        }

        .video-js {
            width: 100%
        }

        .video-js .vjs-control-bar {
            height: 4em;
        }

        .vjs-menu-button-popup .vjs-menu .vjs-menu-content {
            width: 100% !important;
            overflow: hidden;
            z-index: 999;
        }

        .vjs-resolution-button .vjs-resolution-button-label {
            font-weight: bold;
            background: #333;
            color: #fff
        }

        #player {

            width: 100%;
            height: 100%;

        }
    </style>




@endsection


@section('content')
    <div class="container" id="margin_top_only2" xmlns="http://www.w3.org/1999/html">
        <div class="big_video_page">

            @include('partials.videoplayer')
            @if(!Auth::Guest())

                <div class="btn-group video_buttons_group" role="group">


                    <button type="button" class="btn btn-default ask_teacher">
                        <i class="fa fa-graduation-cap"></i>
                        {{trans('course.ask_teacher')}}
                        @if(Lang::getLocale() == "ar")

                            {{$course->teacher->name}}
                        @else

                            {{$course->teacher->name_enu}}
                        @endif
                    </button>


                    @if(count($downloadables))
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#{{$video->id}}">
                            <i class="fa fa-download"></i>
                            {{trans('course.download_notes_files')}}
                        </button>
                    @endif

                    <button type="button" class="btn btn-default comments">

                        <i class="fa fa-comments"></i>
                        {{trans('course.comment')}}
                    </button>

                    <button type="button" class="btn btn-default backToCourse">
                        <i class="fa fa-chevron-left"></i>
                         @if(Lang::getLocale() == "ar")

                            {{trans('course.backVideo')}}
                        @else

                            {{trans('course.backVideo')}}
                        @endif
                    </button>

                </div>

            @endif


            <div class="next_prev">
                <div class="btn-group video_next_prev_group" role="group">


                    <button type="button" class="btn btn-default prev_button">
                        {{trans('course.previous')}}
                        <i class="fa fa-chevron-left"></i>
                    </button>

                    <button type="button" class="btn btn-default next_button">
                        <i class="fa fa-chevron-right"></i>
                        {{trans('course.next')}}
                    </button>


                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 jquery-comments" id="comments-container">
                @include('partials.discuss')
            </div>
        </div>
        <div class="row m-t-25px">

            <div class="col-md-6">
                @include('partials.videoslist')

            </div>
            <div class="col-md-6">
                <div class="box-right-bottom_">
                    <div class="box-right-title">

                        @if(Lang::getLocale() == "ar")
                            <span>
                            @if(strpos($video->title_arabic, '–') === false and strpos($video->title_arabic, '-') === false)
                                    {{$video->title_arabic}}
                                @else
                                    @if(strpos($video->title_arabic, '–') !== false)
                                        {{explode('–',$video->title_arabic)[1]}}
                                    @else
                                        {{explode('-',$video->title_arabic)[1]}}
                                    @endif

                                @endif
                        </span>
                        @else

                            <span>
                            @if(strpos($video->title_english, '–') === false and strpos($video->title_english, '-') === false)
                                    {{$video->title_english}}
                                @else
                                    @if(strpos($video->title_english, '–') !== false)
                                        {{explode('–',$video->title_english)[1]}}
                                    @else
                                        {{explode('-',$video->title_english)[1]}}
                                    @endif

                                @endif
                        </span>

                        @endif
                    </div>
                    <div>
                    <span class="description">

                         @if(Lang::getLocale() == "ar")
                            {!! $video->description_arabic !!}
                        @else
                            {!! $video->description_english !!}
                        @endif

                    </span>
                        <div style="clear:both"></div>
                        <br/>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('footer')



    @include('partials.downloadables')





    <script>

        // Next Prev Buttons
        var $next_button = $('button.next_button');
        $next_button.click(function () {
            window.location.href = "{{$next}}";
        });

        var $prev_button = $('button.prev_button');
        $prev_button.click(function () {
            window.location.href = "{{$prev}}";
        });

        //video list highlighting
        var $activeVideo = $('tr[data-video="{{$video->id}}"]');
        $activeVideo.addClass('current');


        //show comments
        $showComments = $('.comments');
        $commentsContiner = $("#comments-container");
        $showComments.click(function () {
            $commentsContiner.toggle();
            return false;
        });
        
        $('.backToCourse').click(function(){
           window.history.back();
        });

    </script>

    @include('partials.videoPlayerStaticCheck')

    @include('partials.askTeacherJs')



    @include('partials.VideoRegistrationSuccess')
    @include('partials.registerWithFacebook')

    @if(Auth::check())
        @if(Auth::user()->user_group_id == 2)
            @include('partials.AdminDescriptionEditor')
        @endif
    @endif







@endsection