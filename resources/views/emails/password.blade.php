@extends('mailmaster')
@section('content')

                                                    <h1 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 26px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;">Change Your Password - رابط تغيير كلمة المرور</h1><p style="margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;">
                                                    <br/><hr/><br/>

                                                    <center>

                                                    <b>Click here to reset your password:</b> <a href="{{ url( 'en' . '/password/reset/'.$token) }}">Link</a> <br/><br/>

                                                    <b>اضغط على الرابط لتغيير كلمة المرور : </b> <a href="{{ url( 'ar' . '/password/reset/'.$token) }}"> الرابط</a> <br/><br/>
                                                    </center>
                                                    </p>




@endsection