@extends('mailmaster')
@section('content')

    <div style="direction:rtl;text-align:right">

        <center><h2 style="width: 100%;direction: rtl;text-align: right;">
                مرحباٌ {{$name}}</h2></center>
        <div style="clear:both"></div>
        <br/><br/>
        يمكنك الآن متابعة تعلّم الموسيقى على موقع إعزف الجديد الذي
        يحتوي على مزايا كثيرة
        <br/>
        الرجاء الضغط على الرابط هنا لتحديد كلمة سر جديدة ثم اضغط على
        "حسابي" في أعلى الصفحة لتجد مدة إشتراكك الجديد و نوعه على
        يمين الصفحة:
        <br/>
        <a href="http://izif.com/ar/password/email">Link</a>
    </div>

    <br/>
    <div style="direction:ltr;text-align:left">

        <br/><br/>
        <h2>Marhaba {{$name}}</h2>
        <br/>
        You can now continue learning music on the new izif.com with
        it's new features.
        <br/>
        Click on the following link To set a new password then click
        on "My Account" to to find your new subscription duration
        and type on the left hand side of the page:

        <a href="http://izif.com/en/password/email">Link</a>
    </div>

@endsection