@extends('mailmaster')
@section('content')

    <div style="direction:ltr;text-align:left">
        <h2>Instrument Request</h2>
        <br/><br/>

        <div>
            <br/>
            <b><i>Information : </i></b><br/>
            <b>Name : </b> {{$name}} <br/>
            <b>Email : </b> {{$email}} <br/>
            <b>Instrument : </b> {{$instrument_id}} <br/>
            <b>Level : </b> {{$level}} <br/>
            <b>Country : </b> {{$country_id}} <br/>
            <b>City : </b> {{$city}} <br/>
        </div>
        <br/><br/>
    </div>
    <br/>
    
@endsection