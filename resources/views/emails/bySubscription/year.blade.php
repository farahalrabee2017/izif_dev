@extends('mailmaster')
@section('content')

    <div style="text-align: center">
        <center>
            <h3 style="margin:0 auto">اشتراك السنوي في إعزف  Your Annual Subscription on izif</h3>
        </center>
    </div>

    <br/><br/>

    <div style="direction:rtl;text-align:right">
        <b>مرحبا {{$name}}</b><br/><br/>
        نأسف على الصعوبات التقنية التي نتجت عن تحديث الموقع في الأيام الأخيرة. لقد عالجنا جميع المشاكل
        و قمنا بإطلاق موقع إعزف الجديد izif.com  .<br/><br/> لديك الآن اشتراك بكامل الموقع لمدة سنة كاملة ابتداءً من اليوم ! يمكنك هذا الاشتراك من الدخول الى جميع الدورات و نوتات الأغاني<br/><br/>
        الرجاء الضغط على الرابط أدناه لتحديث كلمة المرور و متابعة التعلم على الموقع الجديد .<br/><br/>
        <b>فريق إعزف</b>

        <br/><br/>
        <center>
            <a href="{{ url( 'ar' . '/password/email') }}"><h4>الرابط</h4></a>
        </center>

    </div>
    <br/><br/>

    <div style="direction:ltr;text-align:left">
        <b>Marhaba {{$name}}</b><br/><br/>
        We apologize for the technical difficulties that came with upgrading the website in the last few days. It is all good now!<br/><br/>
        We have launched the new izif.com. You now have a full access annual subscription that starts today. This subscription allows you to access all courses and music sheets.<br/><br/>
        Please click on the link below to update your password to continue learning on the new izif.<br/><br/>
        <b>izif team</b>


        <br/><br/>
        <center>
            <a href="{{ url( 'en' . '/password/email') }}"><h4>Link</h4></a>
        </center>


    </div>



@endsection