@extends('mailmaster')
@section('content')

    <div style="text-align: center">
        <center>
            <h3 style="margin:0 auto">اشتراك الشهري في إعزف  Your Monthly Subscription on izif</h3>
        </center>
    </div>

    <br/><br/>

    <div style="direction:rtl;text-align:right">
        <b>مرحبا {{$name}}</b><br/><br/>
        نأسف على الصعوبات التقنية التي نتجت عن تحديث الموقع في الأيام الأخيرة. لقد عالجنا جميع المشاكل.<br/><br/>
        يرجى العلم بأنه قد انتهى اشتراكك في موقع إعزف بعد مضي أكثر من 12 شهر من تاريخ الاشتراك و لكننا قمنا بإطلاق موقع إعزف الجديد izif.com   و نريد أن نشكرك كونك من مشتركي إعزف الأوائل .<br/><br/> لديك الآن اشتراك بكامل الموقع لمدة شهر واحد ابتداءً من اليوم ! يمكنك هذا الاشتراك من الدخول الى جميع الدورات و نوتات الأغاني<br/>
        الرجاء الضغط على الرابط أدناه لتحديث كلمة المرور و متابعة التعلم على الموقع الجديد .<br/><br/>
        <b>فريق إعزف</b>

        <br/><br/>
        <center>
            <a href="{{ url( 'ar' . '/password/email') }}"><h4>الرابط</h4></a>
        </center>

    </div>
    <br/><br/>

    <div style="direction:ltr;text-align:left">
        <b>Marhaba {{$name}}</b><br/><br/>

        We apologize for the technical difficulties that came with upgrading the website in the last few days. It is all good now!<br/><br/>
        Please note that your subscription on izif has expired after more than 12 months since subscription date, but we have just launched the new izif.com and we want to thank you for being one of our early subscribers .You now have a one month full access subscription that starts today. This subscription allows you to access all courses and sheet music titles.<br/><br/>
        Please click on the link below to update your password to continue learning on the new izif.com<br/><br/>
        <b>izif team</b>


        <br/><br/>
        <center>
            <a href="{{ url( 'en' . '/password/email') }}"><h4>Link</h4></a>
        </center>
    </div>

@endsection