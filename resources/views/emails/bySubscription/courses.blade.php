@extends('mailmaster')
@section('content')
    <div style="text-align: center">
    <center>
        <h3 style="margin:0 auto">اشتراكك في إعزف Your Subscription on izif</h3>
    </center>
    </div>

    <br/><br/>

    <div style="direction:rtl;text-align:right">
        <b>مرحبا {{$name}}</b><br/><br/>
        نأسف على الصعوبات التقنية التي نتجت عن تحديث الموقع في الأيام الأخيرة. لقد عالجنا جميع المشاكل
        و قمنا بإطلاق موقع إعزف الجديد <b>izif.com</b>  <br/><br/> الرجاء الضغط على الرابط أدناه لتحديث كلمة المرور و متابعة التعلم  على الموقع الجديد .<br/><br/>
        ستجد دوراتك في صفحة الحساب الخاصة بك.<br/><br/>
        <b>فريق إعزف</b>

        <br/><br/>
        <center>
            <a href="{{ url( 'ar' . '/password/email') }}"><h4>الرابط</h4></a>
        </center>
    </div>
    <br/><br/>

    <div style="direction:ltr;text-align:left">
        <b>Marhaba {{$name}}</b><br/><br/>
        We apologize for the technical difficulties that came with upgrading the website in the last few days. It is all good now!<br/><br/>
        We have launched the new <b>izif.com</b> Please click on the link below to update your password and continue learning on the new izif.<br/><br/>
        You will find your courses in your profile page.  <br/><br/>
        <b>izif Team</b>

        <br/><br/>
        <center>
            <a href="{{ url( 'en' . '/password/email') }}"><h4>Link</h4></a>
        </center>
    </div>

@endsection