@extends('mailmaster')
@section('content')

 <div style="direction:rtl;text-align:right">
        <h2 style="width: 100%;direction: rtl;text-align: right;">مرحباً {{$name}} </h2>
        <hr/>
        <b style="color:darkseagreen">  لتفعيل حسابك في إعزف ! <a href="{{url(Lang::getLocale() . '/' . 'verify/' . $token)}}"> اضغط هنا </a></b><br/>
        <hr/>
        <br/>
        <br/><br/>
        شكراً لانضمامك لإعزف . أنت الآن تملك حساباً مجانياً مدى الحياة !        <br/>
        استخدم معلومات الدخول في الأسفل للدخول الى العديد من الفيديوهات و نوتات الأغاني لتبدأ بتعلم الآلات الموسيقية التي تحبها اليوم .<br/>
    </div>

    <br/><br/><br/>

    <div style="direction:ltr;text-align:left">
        <h2>Marhaba {{$name}}</h2>
        <br/><br/>
        Thank you for joining izif.
        <br/>
        You now have a lifetime free account!
        <br/>
        Use the log in credentials below to access many free videos and sheet music titles and start learning your favorite instrument right now.
        <br/><br/>
        <div>
            <hr/>
                <b style="color:darkseagreen">Please Activate Your Account  <a href="{{url(Lang::getLocale() . '/' . 'verify/' . $token)}}">Click Here</a></b><br/>
            <hr/>
            <br/>
            <b><i>You Login Credentials : </i></b><br/>
            <b>Username : </b> {{$email}} <br/>
            <b>Password : </b> {{$password}} <br/>
        </div>
        <br/><br/>
        You may receive useful updates from us on this email. You can always unsubscribe from our promotional emails by clicking the "unsubscribe from this list" link on the bottom of any promotional email you receive.
    </div>

   
@endsection