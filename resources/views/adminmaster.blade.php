<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>izif | Dashboard</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("/bower_components/AdminLTE/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />




    <link rel="stylesheet" href="{{ URL::asset("/bower_components/bootstrap/dist/css/bootstrap.min.css")}}" />
    <link rel="stylesheet" href="{{ URL::asset("/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css")}}" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ URL::asset("/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("/static/select2-4.0.2-rc.1/dist/css/select2.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


    @yield('head')

    <style>
        .form-group{
            position: relative;
        }
        .spinner {

            width: 50px;
            height: 40px;
            text-align: center;
            font-size: 10px;
        }

        .spinner > div {
            background-color: #333;
            height: 100%;
            width: 6px;
            display: inline-block;

            -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
            animation: sk-stretchdelay 1.2s infinite ease-in-out;
        }

        .spinner .rect2 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .spinner .rect3 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .spinner .rect4 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .spinner .rect5 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        @-webkit-keyframes sk-stretchdelay {
            0%, 40%, 100% { -webkit-transform: scaleY(0.4) }
            20% { -webkit-transform: scaleY(1.0) }
        }

        @keyframes sk-stretchdelay {
            0%, 40%, 100% {
                transform: scaleY(0.4);
                -webkit-transform: scaleY(0.4);
            }  20% {
                   transform: scaleY(1.0);
                   -webkit-transform: scaleY(1.0);
               }
        }
    </style>
</head>
<body class="skin-blue">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{{url("/" . Lang::getLocale() .'/admin')}}" class="logo"><b>Izif</b> Backend</a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li>
                                <!-- inner menu: contains the messages -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <!-- User Image -->
                                                <img src="{{ URL::asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image"/>
                                            </div>
                                            <!-- Message title and timestamp -->
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <!-- The message -->
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li><!-- end message -->
                                </ul><!-- /.menu -->
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li><!-- /.messages-menu -->

                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <li><!-- start notification -->
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li><!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks Menu -->
                    <li class="dropdown tasks-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 9 tasks</li>
                            <li>
                                <!-- Inner menu: contains the tasks -->
                                <ul class="menu">
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <!-- Task title and progress text -->
                                            <h3>
                                                Design some buttons
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <!-- The progress bar -->
                                            <div class="progress xs">
                                                <!-- Change the css width attribute to simulate progress -->
                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li><!-- end task item -->
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->

                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">Izif Administrator</span>
                        </a>
                        <ul class="dropdown-menu">

                            <!-- Menu Footer-->
                            <li class="user-footer">

                                <div class="pull-right">
                                    <a href="{{url("/" . Lang::getLocale() .'/auth/logout')}}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ URL::asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>Izif Administrator</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
          <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">Menu</li>
                <li class="active"><a href="{{url("/" . Lang::getLocale() .'/admin')}}"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>


                <li class="treeview">
                    <a href="#"><span>Views</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/views/courses')}}">Views</a></li>

                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Teachers Billing</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/views/teachers/TeachersBilling')}}">Teachers Billing</a></li>

                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        {{--<li><a href="{{url("/" . Lang::getLocale() .'/admin/users')}}">Manage Users</a></li>--}}
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/usersAjaxView')}}">Manage Users New</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/user/create')}}">Create New User</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/groups')}}">Manage Groups</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/group/create')}}">Create New User Group</a></li>

                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/tracking')}}">Advertising URL Tracking</a></li>


                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Courses And Videos</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/courses')}}">Manage Courses</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/course/create')}}">Create New Course</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/courses/types')}}">Create New Course Type</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/courses/program_types')}}">Create New programs </a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/videos')}}">Manage Videos</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/video/create')}}">Create New Video</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Music Instruments</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/instruments')}}">Manage Instruments</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/instrument/create')}}">Create New Instrument</a></li>

                    </ul>
                </li>


                <li class="treeview">
                    <a href="#"><span>Testimonials</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/testimonials')}}">Manage Testimonials</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/testimonial/create')}}">Create New testimonial</a></li>

                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Subscriptions Models</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/subscriptions_models')}}">Manage Subscriptions Models</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/subscriptions_model/create')}}">Create New Subscription Model</a></li>

                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Subscriptions</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/subscriptions')}}">Manage Subscriptions</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/subscriptions/create')}}">Create New Subscription</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/subscriptions/fraud')}}">Fraud Subscriptions</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/subscriptions/pendingTransactions')}}">Pending Transactions</a></li>


                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><span>Music Sheets</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/notes')}}">Manage Music Sheets</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/notes/create')}}">Create New Music Sheet</a></li>

                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><span>Coupons</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/coupons')}}">Manage Coupons</a></li>
                        <li><a href="{{url("/" . Lang::getLocale() .'/admin/coupon/create')}}">Create New Coupons</a></li>

                    </ul>
                </li>
                <li class="header">Reports</li>
                <li class="treeview">
                    <a href="#"><span>Marketing Dashboard</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        {{--<li><a href="{{url("/".Lang::getLocale().'/admin/marketing_dashboard')}}"><span> marketing analytics </span></a></li>--}}
                        <li><a href="{{url("/".Lang::getLocale().'/admin/marketing')}}"><span> marketing expenditure </span></a></li>
                        <li><a href="{{url("/".Lang::getLocale().'/admin/marketing_tracking')}}"><span> users tracking </span></a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><span>Users Information</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{url("/".Lang::getLocale().'/admin/interactions')}}"><span>Users Interactions</span></a></li>
                        <li><a href="{{url("/".Lang::getLocale().'/admin/interactions/emails')}}"><span>Users Interactions emails</span></a></li>
                    </ul>
                </li>
                <li><a href="{{url("/".Lang::getLocale().'/admin/sales')}}"><span>Sales Dashboard</span></a></li>
                {{--<li><a href="#"><span>Users Information</span></a></li>--}}

                <li><a href="{{url("/".Lang::getLocale().'/admin/blog/upublished')}}"><span>Blog</span></a></li>
                <li><a href="{{url("/".Lang::getLocale().'/admin/abandonedCard')}}"><span>Abandoned Card</span></a></li>
                <li><a href="#"><span>Course Subscription Summary</span></a></li>
                <li><a href="#"><span>Teacher Subscriptions Summary</span></a></li>

            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('title')
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Backend</a></li>
                @yield('breadcrumb')


            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">

        </div>
        <!-- Default to the left -->
        <strong>Copyright © 2015 <a href="#">Izif</a>.</strong> All rights reserved.
    </footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->


<!-- jQuery 2.1.3 -->


<script src="{{ URL::asset("bower_components/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ URL::asset("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}" ></script>

<script type="text/javascript" src="{{ URL::asset("/bower_components/moment/min/moment.min.js")}}"></script>
<script type="text/javascript" src="{{ URL::asset("/bower_components/bootstrap/dist/js/bootstrap.min.js")}}"></script>
<script type="text/javascript" src="{{ URL::asset("/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js")}}"></script>

<!-- AdminLTE App -->
<script src="{{ URL::asset("/bower_components/AdminLTE/dist/js/app.min.js") }}" ></script>
<script src="{{ URL::asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js") }}" ></script>

<script src="{{ URL::asset("/static/select2-4.0.2-rc.1/dist/js/select2.full.min.js") }}"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="{{ URL::asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js") }}" ></script>
{{--<script>--}}
    {{--$(function() {--}}
        {{--$( "#datepicker" ).datepicker();--}}
    {{--});--}}
{{--</script>--}}
{{--<script>--}}
    {{--$(function() {--}}
        {{--$( ".datepicker" ).datepicker();--}}
    {{--});--}}
{{--</script>--}}
<!--<script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
<script>
    try {
        CKEDITOR.replace('overview_arabic');
    }catch(e){}
</script>-->
@yield('scripts')
</body>

</html>
