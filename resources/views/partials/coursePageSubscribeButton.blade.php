@if(Auth::Guest())

    <button class="subscribeForm" onclick="window.location.href='{{url("/" . Lang::getLocale() . '/subscribe')}}?c={{$course->id}}&t={{ csrf_token() }}';"
            class="course_subscribe" data-id="{{$course->id}}">
        {{ trans('course.subscribe_with_this_course_now') }}
    </button>
@else

    @if(count($subscriptions) > 0 )

        @if(!$course_status)
            <form class="subscribeForm" action="{{url("/" . Lang::getLocale() . '/course/start')}}"
                  method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="course_id" value="{{$course->id}}"/>
                <input type="hidden" name="return" value="{{$course->url_identifier}}"/>
                <input type="submit" class="course_button" value="{{trans('course.start_course')}}"/>
            </form>
        @endif

    @elseif(count($course_bought) > 0)
        @if(!$course_status)
            <form class="subscribeForm" action="{{url("/" . Lang::getLocale() . '/course/start')}}"
                  method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="course_id" value="{{$course->id}}"/>
                <input type="hidden" name="return" value="{{$course->url_identifier}}"/>
                <input type="submit" class="course_button" value="{{trans('course.start_course')}}"/>
            </form>
        @endif

    @else
        <button class="subscribeForm" onclick="window.location.href='{{url("/" . Lang::getLocale() . '/subscribe')}}?c={{$course->id}}&t={{ csrf_token() }}';"
                class="course_subscribe" data-id="{{$course->id}}">
            {{ trans('course.subscribe_with_this_course_now') }}
        </button>
    @endif


@endif