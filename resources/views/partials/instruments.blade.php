<div class="row">

    @foreach($instruments as $instrument)
        <div class="col-md-1 col-sm-4 col-xs-3 icon-izif-9">
            <a class="hvr-icon-buzz-out filter" href="#" data-filter="[data-instrument='{{$instrument->id}}']">
                        <span>

                            @if(Lang::getLocale() == "ar")
                                {{$instrument->arabic_name}}
                            @else
                                {{$instrument->english_name}}
                            @endif
                        </span>
                <img src="{{url($instrument->icon)}}"/>
            </a>
        </div>
    @endforeach

    <div class="fillter_all col-md-1 col-sm-4 col-xs-4 col-xs-offset-4 icon-izif-9">
        <a class="hvr-icon-buzz-out filter" href="#" data-filter="all">
                        <span>
                            {{trans('course.all')}}
                        </span>
            <img src="/static/img/9.png"/>
        </a>
    </div>

</div>