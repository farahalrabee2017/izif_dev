<div class="groupbutton subscription_buttons_container">

    @if(!Auth::check() or Auth::check() and count( \Request::get('subscriptions')) == 0)
        <a href='{{url("/" . Lang::getLocale() . "/" . trans("course.subscriptions_link"))}}'>
            <div class="signupbutton with_price">

                <span>
                    {{ trans('course.subscribe_monthly') }}
                </span>

                <strong>
                    {{trans('course.with')}} 19$
                </strong>
            </div>
        </a>

    @elseif(Auth::check() and count( \Request::get('subscriptions')) == 0 and count( \Request::get('course_subscriptions')) > 0)

        <a href='{{url("/" . Lang::getLocale() . "/" . trans("course.subscriptions_link"))}}' onclick="ga('send', 'event', '{{Lang::getLocale()}}-header', 'click', '{{Lang::getLocale()}}-monthly19-header');">
            <div class="signupbutton with_price">

                <span>
                    {{ trans('course.subscribe_monthly') }}
                </span>

                <strong>
                    {{trans('course.with')}} 19$
                </strong>
            </div>
        </a>

        <a href='{{url("/" . Lang::getLocale() . "/" . trans("course.my_profile_link"))}}'>
            <div class="signupbutton subscribed"
                 style="padding:2px;padding-top:15px;color: #fff;background-color: #5cb85c;border-color: #4cae4c;">

                <span style="font-size: 19px;color:#fff">
                    <i class="fa fa-user"></i> {{ trans('course.subscribed') }}
                </span>


            </div>
        </a>

    @else

        <a href='{{url("/" . Lang::getLocale() . "/" . trans("course.my_profile_link"))}}'>
            <div class="signupbutton subscribed"
                 style="padding:2px;padding-top:15px;color: #fff;background-color: #5cb85c;border-color: #4cae4c;">

                <span style="font-size: 19px;color:#fff">
                    <i class="fa fa-user"></i> {{ trans('course.subscribed') }}
                </span>


            </div>
        </a>

    @endif

    @if(!Auth::check())
        <a onclick="ga('send', 'event', '{{Lang::getLocale()}}-header', 'click', '{{Lang::getLocale()}}-signupfree-header');" href='{{strpos(url("/"), 'https') !== false ? url("/" . Lang::getLocale() . "/" . trans("course.free_account_link")) : str_replace('http','https',url("/" . Lang::getLocale() . "/" . trans("course.free_account_link")))}}'>
            <div class="freebutton">

                <span>
                    {{trans('course.register_for_free')}}
                </span>
            </div>
        </a>
    @endif

</div>