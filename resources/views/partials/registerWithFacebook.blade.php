<script>

    $('input[name="return_path"]').val(window.location.pathname);
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    var $introForm = $('input[type="submit"].i3zef_button');
    var alerted = false;

    $introForm.click(function (event) {

        event.preventDefault();
        if(validateEmail($('#email').val())) {
            try {
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'free-account-pg',
                    eventAction: 'click',
                    eventLabel: '{{Lang::getLocale()}}-izif-free-account'
                });

            } catch (e) {
                console.log(e);
            }




            swal({
                title: "{{trans('course.thank_you')}}",
                text: "{{trans('course.thank_you_msg')}}",
                type: "success",
                showCancelButton: false,
                showConfirmButton: false
            });

            setTimeout(function () {

                $('form.form_your_emial').submit();
            }, 6000);
        }else{
            event.preventDefault();
            $('.emailerror').show();
        }
    });
</script>

<script>

    // Load the SDK asynchronously
    /*window.fbAsyncInit = function() {
     FB.init({
     appId      : '359819794074863',
     xfbml      : true,
     version    : 'v2.6'
     });
     };

     (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));*/


    window.fbAsyncInit = function() {
        FB.init({
            appId      : '430058543740438',
            xfbml      : true,
            version    : 'v2.8'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    function loginWithFacebook() {

        //checkLoginState(function (status) {
        FB.getLoginStatus(function(status) {

            if (status.status != "connected") {
                FB.login(function (response) {
                            console.log(response);
                            FB.api('/me?fields=id,email,name', function (response) {

                                if (response.email) {

                                    var $form = $('.form_your_emial');
                                    $('.form_your_emial input[name="email"]').val(response.email);
                                    $('.form_your_emial input[name="type"]').val('facebook');
                                    $form.submit();
                                }

                            });
                        },
                        {
                            scope: 'public_profile, user_friends, email',
                            return_scopes: true
                        }
                );
            } else {

                FB.api('/me?fields=id,email,name', function (response) {
                    var $form = $('.form_your_emial');
                    $('.form_your_emial input[name="email"]').val(response.email);
                    $form.submit();
                });
            }

        });

    }
</script>


<script>
/*
    $('input[name="return_path"]').val(window.location.pathname);

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function checkLoginState(callback) {
        FB.getLoginStatus(function (response) {
            callback(response);
        });
    }

    window.fbAsyncInit = function () {
        FB.init({
            appId: '359819794074863',
            cookie: true,  // enable cookies to allow the server to access
                           // the session
            xfbml: true,  // parse social plugins on this page
            version: 'v2.5' // use graph api version 2.5
        });
    };


    function loginWithFacebook() {

        checkLoginState(function (status) {

            if (status.status != "connected") {
                FB.login(function (response) {
                            console.log(response);
                            FB.api('/me?fields=id,email,name', function (response) {


                                var $form = $('.form_your_emial');
                                $('.form_your_emial input[name="email"]').val(response.email);
                                $form.submit();

                            });
                        },
                        {
                            scope: 'public_profile, user_friends, email',
                            return_scopes: true
                        }
                );
            } else {

                FB.api('/me?fields=id,email,name', function (response) {
                    var $form = $('.form_your_emial');
                    $('.form_your_emial input[name="email"]').val(response.email);
                    $form.submit();
                });
            }

        });

    }
    */
</script>