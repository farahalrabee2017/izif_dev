<div class="video_gallary col-md-12">

    <div class="row">
        <div class="main-video col-md-12" data-step="1" data-intro="{{trans('course.video_gallary_intro_video')}}">

            <iframe id="player" type="text/html" width="100%" height="300"
                    src="#"
                    frameborder="0"></iframe>
        </div>
    </div>

    <div class="row free-videos-row" data-step="2" data-intro="{{trans('course.video_gallary_intro_free_videos')}}">
        @foreach($gallery_videos as $gvideos)
            <div class="col-md-4 free-video-block"><a
                        href='{{url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$gvideos->url_identifier")}}'>
                    @if($gvideos->cover_image)
                        <img src="{{url('/uploads/'.$gvideos->cover_image )}}" width="163px" height="80px"/>
                    @else
                        <img src="{{url('/uploads/'.$course->cover_image )}}" width="163px" height="80px"/>
                    @endif
                    <p>
                        @if(Lang::getLocale() == "ar")
                            {{$gvideos->title_arabic}}
                        @else
                            {{$gvideos->title_english}}
                        @endif
                    </p>
                </a>
            </div>
        @endforeach
    </div>

</div>