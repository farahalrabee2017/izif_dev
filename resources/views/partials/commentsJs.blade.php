<script>
    //Comments
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getFullYear() + " @ "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

    $addComment = $('.add-comment');
    $showComments = $('.comments');
    $commentsContiner = $("#comments-container");
    $close = $('span.close');


    $close.click(function () {
        $('div.textarea').text('');
        return false;
    });

    $showComments.click(function () {
        $commentsContiner.toggle();
        return false;
    });


    $addComment.click(function () {
        $.ajax({
                    method: "POST",
                    url: "{{url("/" . Lang::getLocale() . '/video/comment')}}",
                    data: {
                        comment: $('div.textarea').text(),
                        _token: "{!! csrf_token() !!}",
                        video_id: "{{$video->id}}",
                        user_id: "@if(!Auth::Guest()){{Auth::user()->id}}@endif"
                    }
                })
                .done(function (msg) {
                    $('div.textarea').text('');
                    window.location.href = '#comment-list';
                    var comment_template = '<li id="new" class="comment by-current-user"> <div class="comment-wrapper"> <time style="float:left">' + datetime + '</time> <div class="name highlight-font-bold" style="float:right">You</div><div style="clear: both"></div><br/> <div class="wrapper"> <div style="float:right "> <img style="float:right" src="https://app.viima.com/static/media/user_profiles/user-icon.png" class="profile-picture round"> </div><div class="content">' + $('div.textarea').text() + '</div></div></div></li>';
                    $('#comment-list').prepend(comment_template);

                });

        return false;
    });
</script>