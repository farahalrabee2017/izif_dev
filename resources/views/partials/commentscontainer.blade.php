<div class="jquery-comments">

    <div class="commenting-field main"><img src="https://viima-app.s3.amazonaws.com/media/user_profiles/user-icon.png"
                                            class="profile-picture round by-current-user">
        <div class="textarea-wrapper"><span class="close" style="display: block;"><span class="left"></span><span
                        class="right"></span></span>
            <div class="textarea" data-placeholder="اضف تعليقك" contenteditable="true" style="height: 3.65em;"></div>

        </div>
    </div>
    <a class="btn btn-default add-comment" href="#" role="button">
        <i class="fa fa-commenting-o"></i>
        اضف تعليقك
    </a>
    <div style="clear: both"></div>
    <br/>
    <ul class="navigation">

    </ul>
    <div class="data-container" data-container="comments">
        <ul id="comment-list" class="main">

            @foreach($comments as $comment)

                <li data-id="{{$comment->id}}" class="comment by-current-user">
                    <div class="comment-wrapper">
                        <time data-original="2015-01-03" style="float:left">{{$comment->created_at}}</time>
                        <div class="name highlight-font-bold" style="float:right">You</div>
                        <div style="clear: both"></div>
                        <br/>
                        <div class="wrapper">
                            <div style="float:right ">
                                <img style="float:right"
                                     src="https://app.viima.com/static/media/user_profiles/user-icon.png"
                                     class="profile-picture round">

                            </div>
                            <div class="content">{{$comment->comment}}
                            </div>

                        </div>

                    </div>
                </li>
            @endforeach

        </ul>

        <div class="no-comments no-data"><i class="fa fa-comments fa-2x"></i><br>No comments</div>
    </div>
</div>