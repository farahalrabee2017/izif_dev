<form action="https://www.paypal.com/cgi-bin/webscr" method="post">

    <!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="business" value="i3zif.com@gmail.com">

    <!-- Specify a Subscribe button. -->
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <!-- Identify the subscription. -->
    <input type="hidden" name="item_name" value="{{$subscription_model->title_enu}}">
    <input type="hidden" name="item_number" value="{{Auth::user()->id}},{{$subscription_model_id}},{{$course_id}},{{ csrf_token() }},{{$abanded_record->id}}">
    <input type="hidden" name="code" value="{{$code}}">
    <!-- Set the terms of the regular subscription. -->
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="a3" value="{{$price}}">
    <input type="hidden" name="p3" value="{{$subscription_model_period}}">
    <input type="hidden" name="t3" value="M">


    <!-- No shipping is required -->
    <input type="hidden" name="no_shipping" value="1">

    <!-- Set recurring payments until canceled. -->
    <input type="hidden" name="src" value="1">
    <input type="hidden" name="return" value="{{url("/" . Lang::getLocale() . "/")}}/thank-you">
    <input type="hidden" name="cancel_return" value="http://izif.com/ar/cancel/paypal">
    <input type="hidden" name="notify_url" value="http://izif.com/ar/paypal_ipn">

    <!-- Custom Params -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
    <input type="hidden" name="subscription_model_id" value="{{$subscription_model_id}}">
    <input type="hidden" name="course_id" value="{{$course_id}}">


    <!-- Display the payment button. -->
    <input type="image" class="ckeck_shadow_right" name="submit" border="0"
           src="{{url('/static/img/payPal.png')}}"
           alt="PayPal - The safer, easier way to pay online">
    <img alt="" border="0" width="1" height="1"
         src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
</form>
