<form action='https://2checkout.com/checkout/purchase' method='post'>

    <input type='hidden' name='sid' value='202356302' />
    <input type='hidden' name='mode' value='2CO' />
    <input type='hidden' name='li_0_type' value='product' />
    <input type='hidden' name='li_0_name' value='{{$subscription_model->title_enu}}' />
    <input type='hidden' name='li_0_price' value='{{$price}}' />
    <input type='hidden' name='li_0_recurrence' value='{{$subscription_model_period}} Month' />
    <input type='hidden' name='li_0_product_id' value='{{Auth::user()->id}},{{$subscription_model_id}},{{$course_id}},{{ csrf_token() }},{{$abanded_record->id}}' />

    <input type='hidden' name='li_0_tangible' value='N' />

    <input type='hidden' name='currency_code' value='USD' />

    <!-- Custom Params -->
    <input type="hidden" name="custom_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="custom_userid" value="{{Auth::user()->id}}" />
    <input type="hidden" name="subscription_id" value="{{$subscription_model_id}}" />
    <input type="hidden" name="custom_courseid" value="{{$course_id}}" />

    <input type="image" class="ckeck_shadow_right" name="submit" border="0"
           src="{{url('/static/img/cards.png')}}"
           alt="2checkout izif">
</form>
<!--202356302-->
<!-- https://www.2checkout.com/checkout/purchase -->