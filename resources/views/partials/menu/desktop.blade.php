<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container clearfix">

        @include('partials.mainTopSubscribeButtons')

        <div class="nav navbar-nav navbar-right">
            <ul class="top_menu language_buttons_contanier">
                <li>
                    @if(Lang::getLocale() == 'ar')
                        <button type="button" class="btn btn-default lang-change active" data-lang="en"><i
                                    class="fa fa-language"></i> English
                        </button>
                    @else
                        <button type="button" data-lang="ar" class="btn btn-default lang-change active">
                            {{trans('course.arabic')}}
                            <i class="fa fa-language"></i>
                        </button>
                    @endif
                </li>
            </ul>

            <ul class="register_and_profile">
                @if (Auth::check())
                    <li><a href='{{url("/" . Lang::getLocale() . "/" . trans("course.my_profile_link"))}}'> <i
                                    class="fa fa-user"></i> {{ trans('course.myprofile') }} </a></li>
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/logout")}}'> <i
                                    class="fa fa-sign-out"></i> {{ trans('course.logout') }} </a></li>
                @else
                    <li><a href='{{url("/" . Lang::getLocale() . "/" . trans("course.create_account_link"))}}'> <i
                                    class="fa fa-user"></i> {{ trans('course.register') }} </a></li>
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/login")}}'> <i
                                    class="fa fa-sign-in"></i> {{ trans('course.login') }} </a></li>
                @endif

                <li><i class="fa fa-whatsapp" aria-hidden="true" style="color:#43d854;
    font-size: 16px;
    font-weight: bold;float: right;cursor: initial"></i>
                    <span style="float: right;
    direction: ltr;
    color: #000;
    font-size: 11px;
    font-weight: bolder;
    padding: 0.4em;
    cursor: initial">+962797636886</span>
                </li>
            </ul>
        </div>

        <div class="menu">
            <ul>
                <li class="coursesMenu"><a
                            href='{{url("/" . Lang::getLocale() . "/" . trans("course.courses_list_link"))}}'>
                        {{trans('course.music_courses_menu')}}
                        <img
                                src='{{url("static/img/dots-icon.png")}}'></a>
                </li>


                <li class="freeVideosMenu"><a href='{{url("/" . Lang::getLocale() . "/free/videos")}}'>
                        {{trans('course.free_videos')}}
                    </a></li>


                <li class="musicSheets"><a href='{{url("/" . Lang::getLocale() . "/music/sheets")}}'>
                        {{trans('course.music_sheets')}}
                    </a></li>

                <li class="subscribeMenu"><a
                            href='{{url("/" . Lang::getLocale() . "/" . trans("course.subscriptions_link"))}}'>
                        {{trans('course.subscriptions')}}
                    </a></li>
                <li class="blog"><a
                            href='{{url("/" . Lang::getLocale() . "/" . trans("blog"))}}'>
                        {{trans('course.blog')}}
                    </a></li>
                    
                    <li class="instrument" style="color: #81070c;margin-right: 200px"><a href='{{url("/" . Lang::getLocale() . "/" . trans("course.dont_have_link"))}}'>
                        <b> {{trans('course.dont_have_header')}} </b>
                     </a></li>

                

            </ul>
        </div>

        <div class="toplogo "><a href='{{url("/" . Lang::getLocale() . "/")}}'><img
                        src='{{url("static/img/izif-logo.png")}}'></a></div>

    </div>
</div>