<nav class="navbar navbar-default" style="height: 60px;">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="true">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="height: 60px;" href="{{url("/" . Lang::getLocale() . "/")}}">
                <img src='{{url("static/img/izif-logo.png")}}' style="height: 2em;"></a>

        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background: #f8f8f8;">
            <ul class="nav navbar-nav">
                <li class="coursesMenu"><a
                            href='{{url("/" . Lang::getLocale() . "/" . trans("course.courses_list_link"))}}'>
                        {{trans('course.music_courses_menu')}}
                        <img src='{{url("static/img/dots-icon.png")}}'></a>
                </li>


                <li class="freeVideosMenu"><a href='{{url("/" . Lang::getLocale() . "/free/videos")}}'>
                        {{trans('course.free_videos')}}
                    </a></li>


                <li class="musicSheets"><a href='{{url("/" . Lang::getLocale() . "/music/sheets")}}'>
                        {{trans('course.music_sheets')}}
                    </a></li>

                <li class="subscribeMenu"><a
                            href='{{url("/" . Lang::getLocale() . "/" . trans("course.subscriptions_link"))}}'>
                        {{trans('course.subscriptions')}}
                    </a></li>
                    
                    <li class="instrument" ><a  style="color: #81070c !important;" href='{{url("/" . Lang::getLocale() . "/" . trans("course.dont_have_link"))}}'>
                         {{trans('course.dont_have_header')}} 
                    </a></li>
                    
                    

                @if (Auth::check())
                    <li><a href='{{url("/" . Lang::getLocale() . "/" . trans("course.my_profile_link"))}}'> <i
                                    class="fa fa-user"></i> {{ trans('course.myprofile') }} </a></li>
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/logout")}}'> <i
                                    class="fa fa-sign-out"></i> {{ trans('course.logout') }} </a></li>
                @else
                    <li><a href='{{url("/" . Lang::getLocale() . "/" . trans("course.create_account_link"))}}'> <i
                                    class="fa fa-user"></i> {{ trans('course.register') }} </a></li>
                    <li><a href='{{url("/" . Lang::getLocale() . "/auth/login")}}'> <i
                                    class="fa fa-sign-in"></i> {{ trans('course.login') }} </a></li>
                @endif

                <li>
                    @if(Lang::getLocale() == 'ar')
                        <button type="button" class="btn btn-default lang-change active" data-lang="en"><i
                                    class="fa fa-language"></i> English
                        </button>
                    @else
                        <button type="button" data-lang="ar" class="btn btn-default lang-change active">
                            {{trans('course.arabic')}}
                            <i class="fa fa-language"></i>
                        </button>
                    @endif
                </li>

            </ul>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>