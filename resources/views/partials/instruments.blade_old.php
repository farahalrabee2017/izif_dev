<div class="row">

    @foreach($instruments as $instrument)
        <div class="col-md-1">
            <a class="hvr-icon-buzz-out filter" href="#" data-filter="[data-instrument='{{$instrument->id}}']">
                        <span>
                            {{$instrument->arabic_name}}
                        </span>
                <img src="{{url($instrument->icon)}}"/>
            </a>
        </div>
    @endforeach

    <div class="col-md-1">
        <a class="hvr-icon-buzz-out filter" href="#" data-filter="all">
                        <span>
                            الكل
                        </span>
            <img src=""/>
        </a>
    </div>

</div>