@if(!Auth::Guest())
    @if(Auth::user()->user_group_id == 2)
        <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
              rel="stylesheet"/>
        <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
        <script>
            /**
             * Created by halnator on 1/29/16.
             */


            function Subscriptions() {
            }

            Subscriptions.prototype.deleteSubscription = function (id) {
                $.ajax({
                            method: "POST",
                            url: "{{url('/subscription/delete')}}",
                            data: {
                                _token: "{{csrf_token()}}",
                                id: id
                            }
                        })
                        .done(function (msg) {
                            window.location.reload();
                        });
            };


            Subscriptions.prototype.deleteSubscriptionFeature = function (id) {
                $.ajax({
                            method: "POST",
                            url: "{{url('/feature/delete')}}",
                            data: {
                                _token: "{{csrf_token()}}",
                                id: id
                            }
                        })
                        .done(function (msg) {
                            window.location.reload();
                        });
            };

            var $container = $('.main-container');


            var buttons_container = [

                '<center>',
                '<button type="button" data-toggle="modal" data-target="#addSubscriptionModal" class="add_subscription btn btn-default"><i class="fa fa-plus-circle"></i> Add New Subscription Model</a></button>',
                '</center>'
            ];

            $container.prepend(buttons_container.join(''));


            // Enable Editables
            var $enableEdit = $('.features li span');
            $enableEdit.editable({
                params: function(params) {
                    // add custom param _token
                    params._token = "{{csrf_token()}}";
                    return params;
                }
            });


            // Delete Subscription
            var $deleteSubscription = $('.delete-subscription');
            $deleteSubscription.click(function () {
                var subscriptions = new Subscriptions();
                var id = $(this).data('id');
                subscriptions.deleteSubscription(id);
            });


            // Delete Feature
            var $deleteSubscriptionFeature = $('.delete-subscription-feature');
            $deleteSubscriptionFeature.click(function () {
                var subscriptions = new Subscriptions();
                var id = $(this).data('id');
                subscriptions.deleteSubscriptionFeature(id);
            });

            // show preloader
            $('.withloader').click(function(){
                $('.preloader').fadeIn();
            })


        </script>
    @endif
@endif