<script>
    // Ask the teacher
    var $ask_teacher = $('.ask_teacher');
    $ask_teacher.click(function () {

        var url = window.location.pathname;
        var fullurl = window.location.href;
        var useremail = "@if(!Auth::Guest()){{Auth::user()->email}}@endif";
        var teacher = "{{$course->teacher->name}}";
        var action = "{{url('/' . Lang::getLocale() . '/ask/teacher')}}";

        @if(count( \Request::get('subscriptions')) == 0)

            swal({
            title: "{{trans('course.this_service_for_subscribed_users_only')}}",
            text: "",
            //type: "info",
            showCancelButton: true,
            confirmButtonColor: "#5cb85c",
            confirmButtonText: "{{trans('course.subscribe_now')}}",
            cancelButtonText: "{{trans('course.cancel')}}",
            closeOnConfirm: false
        }, function () {
            window.location.href = "{{url("/" . Lang::getLocale() . '/subscribe')}}";
        });

        @else

            swal({
                    title: "{{trans('course.ask_teacher')}} @if(Lang::getLocale() == 'ar') {{$course->teacher->name}} @else {{$course->teacher->name_enu}} @endif",
                    text: `<br/><form class='ask_teacher_form' method='post' action='${action}'><textarea required name='question'></textarea><input type='hidden' name='url' value='${url}'/><input type='hidden' name='fullurl' value='${fullurl}'/><input type='hidden' name='teacher' value='${teacher}'/><input type='hidden' name='useremail' value='${useremail}'/></form>`,
                    html: true,
                    closeOnConfirm: false,
                    confirmButtonText: "{{trans('course.submit')}}",
                    showCancelButton: true,
                    cancelButtonText: "{{trans('course.cancel')}}"

                },
                function (isConfirm) {
                    if (isConfirm) {

                        if ($('form.ask_teacher_form textarea').val() == "") {

                            $('.error').remove();
                            $('form.ask_teacher_form').prepend('<div class="alert alert-danger error" role="alert">{{trans("course.ask_teacher_form_reqiured")}}</div>');
                        } else {
                            $('form.ask_teacher_form').submit();
                        }
                    }
                });

        @endif

    });
</script>