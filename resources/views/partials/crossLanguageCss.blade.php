@if(Lang::getLocale() == "ar")
    <style>
        .nav-tabs > li {
            float: right;
        }

        .videos-box, .free-video-block {
            float: right
        }

        .videos-profile{
            float: right!important;
        }
    </style>
@else
    <style>

        .freebutton span {
            color: #000!important;
            font-size: 15px!important;
            padding-right: 19px!important;
            display: block!important;
            font-weight: bold!important;
            line-height: 20px!important;
            margin-top: 13px!important;
        }


        .signupbutton span {
            font-size: 14px!important;
            display: block;
            font-weight: bold;
            line-height: 18px!important;
            margin-top: 6px!important;
        }

        .signupbutton strong {
            font-size: 20px;
            display: block;
            font-weight: bold;
            line-height: 21px;
            font-weight: bold;
        }

        .register_and_profile {
            height: 18px;
            float: right;
            font-size: 12px;
            margin-top: 10px;
        }

        .navbar.smaller .freebutton span {
            font-size: 14px;
            padding-right: 19px;
            display: block;
            font-weight: bold;
            line-height: 20px;
        }

        .navbar.smaller .signupbutton span{
            line-height: 13px!important;
        }


        .banner_i3zef,.text_array,.block-one{
            direction: ltr!important;
        }

        .block-one{
            padding-left: 1em;
        }

        .boxs-right-top-discrption span,.box-left-bottom span{
            direction: ltr!important;
        }


        .boxs-right-top-title,.box-left-bottom-title,.title-fhras,.box-right-title{

            text-align: left!important;
            padding-left: 0.7em!important;
            direction: ltr!important;
        }

        .box-left-bottom span{
            width:100%!important;
        }

        .fhras .table,.fhras td{
            direction: ltr!important;
            text-align: left!important;
        }

        .button_test_here{
            float: right!important;
        }

        .enter_your_email{
            direction: ltr!important;
        }

        .description{
            direction: ltr;
        }

        .copy_right_text{
            margin-right:0px!important;
            margin-left: 3.1em!important;
            font-size:10px!important;
        }
        .titlelogin{
            font-size:25px
        }

        .title_free_account span{
            font-size: 40px!important;
        }
        .enter_your_email span{
            font-size:15px!important;
        }

        .enter_your_email .copy_right_text{
            margin-left: 5.1em!important;
        }

        .enter_your_email .copy_right_text span{
            font-size:10px!important;
        }


    </style>
@endif