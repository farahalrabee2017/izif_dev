<div class="container">
    <div class="row">
        @foreach($testimonials as $testimonial)
            <div class="col-md-6">
                <div class="block-one">
		<span style="font-size: 13px;font-style: italic;line-height: 20px;">
            <b style="font-size: 20px;font-style: normal;">
                @if(Lang::getLocale() == "ar")
                    {{$testimonial->name}}
                @else
                    {{$testimonial->name_enu}}
                @endif

            </b><br/><br/>
            <i class="fa fa-quote-right" style="font-size: 12px;"></i>

            @if(Lang::getLocale() == "ar")
                {{$testimonial->testimonial}}
            @else
                {{$testimonial->testimonial_english}}
            @endif
            <i class="fa fa-quote-left" style="font-size: 11px;"></i>
		</span>
                    <img src="{{url($testimonial->photo)}}" style="width:150px;height: 220px">
                </div>
            </div>
        @endforeach

    </div>
</div>