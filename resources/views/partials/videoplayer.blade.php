

@if(Auth::check() and $video->is_open or Auth::check() and  count($subscriptions) > 0 or Auth::check() and count($course_bought) > 0)

    <div class="big_video_array_">
        @if($video->is_youtube == "1")
     <?php
           // $video_id = explode("&video_id=", $video->video_path);
            $video_id = explode("v=", $video->video_path);
            $youtubesrc = "http://www.youtube.com/embed/" .$video_id[1]."?rel=0";
     ?>
        <iframe id="player" type="text/html" width="100%" height="450"
                src="{{$youtubesrc}}"
                frameborder="0" allowfullscreen="allowfullscreen">

        </iframe>

        @else


        <div id="player">
            <!-- this paragraph will be shown if FlashPlayer unavailable -->
            <p>
                <a href="http://www.adobe.com/go/getflashplayer">
                    <img src="http://wwwimages.adobe.com/www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                </a>
            </p>
        </div>



        @endif
    </div>
@elseif(Auth::check() and  count($subscriptions) == 0)
    <div class="big_video_array_" style="padding-top: 11%;">
        <div class="no-subscription">
            <h1 style="font-size: 22px;color: #fff;padding: 25px;"> {{trans('course.for_subscribed_only')}} <i
                        class="fa fa-exclamation"
                        aria-hidden="true"></i>
            </h1>
            <div class="btn-group" role="group" aria-label="...">

                <button style="width: 140px;font-size: 18px;" onclick="window.location.href='{{url("/" . Lang::getLocale() . '/free/videos')}}';"
                        class="btn btn-default btn-info">
                    {{ trans('course.free_lessons') }}
                </button>

                <button style="width: 140px;font-size: 18px;" onclick="window.location.href='{{url("/" . Lang::getLocale() . '/subscribe')}}?c={{$course->id}}&t={{ csrf_token() }}';"
                        class="btn btn-default btn-success" data-id="{{$course->id}}">
                    {{ trans('course.subscribe_now') }}
                </button>
            </div>
        </div>
    </div>

@else
    <div class="big_video_array_" style="padding-top: 7%;">

        <div class="learning_music_block center-block  col-md-offset-5"
             style="margin-top: 0px;margin-bottom: 0px;height: auto">

            <div class="enter_your_email" style="background: #504C4C;height: 300px">
                <span>
                    {{trans('course.intro_paragraph_email')}}
                    <strong>
                        {{trans('course.now')}}
                    </strong>
                </span>

                <form onsubmit="ga('send', 'event', 'locked-video-player', 'click', '{{Lang::getLocale()}}-izif-videos');" action="{{url("/" . Lang::getLocale() . "/")}}/start" method="POST"
                      class="form_your_emial center-block">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="email" name="email" class="your_email " id="email" required/>
                    <input type="hidden" name="return_path"/>
                    <input type="hidden" name="lang" value="{{ Lang::getLocale() }}">
                    <input type="hidden" name="type" value="mail">
                    <input class="i3zef_button" type="submit" value="{{trans('course.izif')}}"/>
                </form>

                <h3 style="color: #fff;padding: 10px;font-weight: bold;">
                    {{trans('course.or')}}
                </h3>

                <div style="clear:both"></div>
                <button class="loginWithFacebookIntro" onclick="ga('send', 'event', 'locked-video-player', 'click', '{{Lang::getLocale()}}-fb-onvideos');loginWithFacebook();"><i
                            class="fa fa-facebook-official"></i> {{trans('course.start_with_facebook')}}</button>

                <div style="clear:both"></div>
                <div class="copy_right_text">
                    <span>
                        {{trans('course.intro_bottom_paragraph')}}
                        <a href="">
                            {{trans('course.conditions')}}

                        </a>
                        {{trans('course.izif_website')}}
                    </span>
                </div>

                <div id="status">
                </div>
            </div>
        </div>

    </div>
@endif
