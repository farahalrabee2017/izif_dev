<!-- Modal -->
<div class="modal fade" id="{{$video->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> &nbsp;
                    @if(Lang::getLocale() == 'ar')
                        {{$video->title_arabic}}
                    @else
                        {{$video->title_english}}
                    @endif
                    &nbsp;</h4>
            </div>
            <div class="modal-body">
                <ul class="filelist">
                    @foreach($downloadables as $downloadable)
                        <li><a target="_blank" href="@if(Lang::getLocale() == 'ar'){{ $downloadable->file_path_ara}} @else {{ $downloadable->file_path_enu}} @endif" class="file"><i
                                        class="fa fa-file-text"></i>
                                @if(Lang::getLocale() == 'ar')
                                    {{$downloadable->title_ara}}
                                @else
                                    {{$downloadable->title_enu}}
                                @endif

                            </a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
