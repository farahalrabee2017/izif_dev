
@if($video->is_youtube == "1")
    <script>
        var url = "{{$video->video_path}}".split('v=')[1].replace(/ /g, '');
        if (url.indexOf('&') >= 0) {
            url = url.split('&')[0];
        }
        var youtubesrc = "http://www.youtube.com/embed/" + url + "?enablejsapi=1&origin=http://dev.izif.com";
        $('#player').attr('src', youtubesrc);
    </script>
@else
    @include('partials.videoplayerfiles')
@endif