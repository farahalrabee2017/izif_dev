<div style="clear:both"></div>
<div class="fhras" data-step="3" data-intro="{{trans('course.video_gallary_intro_video_list')}}">
    <div class="title-fhras">
        <span><i class="fa fa-bars"></i>
            
            {{trans('course.lessons_list')}}
            -
            @if(Lang::getLocale() == "ar")
                @if(isset($lessonsUrl))
                <a  href='{{str_replace('http','https',url("/" . Lang::getLocale() . "$lessonsUrl"))}}'
                    style="text-decoration: underline; color: white;"> 
                       {{$course->title_arabic}}  
                   </a>
                @else
                     {{$course->title_arabic}}
                @endif     
            @else
                 @if(isset($lessonsUrl))
                     <a href='{{str_replace('http','https',url("/" . Lang::getLocale() . "$lessonsUrl"))}}'
                        style="text-decoration: underline; color: white;"> 
                         {{$course->title_english}}
                     </a>
                 @else
                    {{$course->title_english}}
                 @endif 
            @endif
        </span>

    </div>
    <div class="fhras-img">

        @if(!Auth::Guest())
            <div>
                {{trans('course.you_watched')}}
                {{$completed_videos}}
                {{trans('course.from')}}
                {{count($course->videos)}}


            </div>
            <br/>
            <!--<div class="progress">
                <div class="progress-bar progress-bar-success progress-bar-striped active"
                     role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                     style="width:{{$completed}}%;float:right">
                    <span class="sr-only">{{$completed}}% Complete (success)</span>
                </div>
            </div>-->
        @endif
        <table class="table table-hover">
            <tbody>
            @foreach ($course->videos as $video)
                <tr class="active" data-video="{{$video->id}}" data-order="{{$video->video_order}}">

                    @if(!Auth::Guest())
                        <th scope="row" style="width: 60px">
                            @if(isset($status[$video->id]))
                                @if($status[$video->id]['status'] == "Done")
                                    <i class="fa fa-check-circle"
                                       style="font-size: 30px;color:green"></i>
                                @else
                                    <i class="fa fa-adjust" style="font-size: 30px;color:goldenrod"></i>
                                @endif
                            @else
                                <i class="fa fa-circle-o" style="font-size: 30px;color:#ccc"></i>
                            @endif
                        </th>
                    @endif
                    <td class="VideoNumber">

                        @if($video->is_open OR count($subscriptions) > 0 OR count($course_bought) > 0)
                            <a href='{{str_replace('http','https',url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$video->url_identifier"))}}'>
                                <span class="videoNumber">{{$video->video_number}}</span>
                            </a>
                        @else
                            <span class="videoNumber">{{$video->video_number}}</span>
                        @endif
                    </td>
                    <td style="vertical-align: middle;font-size:15px;direction: ltr;text-align: right;">
                        @if($video->is_open OR count($subscriptions) > 0 OR count($course_bought) > 0)

                            <a href='{{url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$video->url_identifier")}}'>

                                @if(Lang::getLocale() == "en")
                                    <i class="fa fa-unlock-alt" style="color:#20804d"></i>
                                @endif

                                @if(Lang::getLocale() == "ar")
                                    @if(strpos($video->title_arabic, '–') === false and strpos($video->title_arabic, '-') === false)
                                        {{$video->title_arabic}}
                                    @else
                                        @if(strpos($video->title_arabic, '–') !== false)
                                            {{explode('–',$video->title_arabic)[1]}}
                                        @else
                                            {{explode('-',$video->title_arabic)[1]}}
                                        @endif

                                    @endif
                                @else
                                    @if(strpos($video->title_english, '–') === false and strpos($video->title_english, '-') === false)
                                        {{$video->title_english}}
                                    @else
                                        @if(strpos($video->title_english, '–') !== false)
                                            {{explode('–',$video->title_english)[1]}}
                                        @else
                                            {{explode('-',$video->title_english)[1]}}
                                        @endif

                                    @endif
                                @endif

                                @if(Lang::getLocale() == "ar")
                                    <i class="fa fa-unlock-alt" style="color:#20804d"></i>
                                @endif

                            </a>

                        @else

                            <a href='{{url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$video->url_identifier")}}'
                               class="locked">

                                @if(Lang::getLocale() == "en")
                                    <i class="fa fa-lock" style="color:#f2dede"></i>
                                @endif

                                @if(Lang::getLocale() == "ar")
                                    @if(strpos($video->title_arabic, '–') === false and strpos($video->title_arabic, '-') === false)
                                        {{$video->title_arabic}}
                                    @else
                                        @if(strpos($video->title_arabic, '–') !== false)
                                            {{explode('–',$video->title_arabic)[1]}}
                                        @else
                                            {{explode('-',$video->title_arabic)[1]}}
                                        @endif

                                    @endif
                                @else
                                    @if(strpos($video->title_english, '–') === false and strpos($video->title_english, '-') === false)
                                        {{$video->title_english}}
                                    @else
                                        @if(strpos($video->title_english, '–') !== false)
                                            {{explode('–',$video->title_english)[1]}}
                                        @else
                                            {{explode('-',$video->title_english)[1]}}
                                        @endif

                                    @endif

                                @endif
                                @if(Lang::getLocale() == "ar")
                                    <i class="fa fa-lock" style="color:#f2dede"></i>
                                @endif

                            </a>
                        @endif

                    </td>

                    <td style="width:70px;color:#333;vertical-align: middle;">
                        <span style="font-size: 11px">

                            @if(strlen($video->estimated_time) == 4)


                                {{substr($video->estimated_time,0,2)}}:{{substr($video->estimated_time,2,2)}}
                            @else

                                {{$video->estimated_time}}:00
                            @endif

                        </span> <i
                                class="fa fa-clock-o"></i>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>