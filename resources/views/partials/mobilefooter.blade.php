
<div style="padding: 20px;background: #fff;    text-align: center;">
    <div><a href='{{url("/" . Lang::getLocale() . "/")}}'><img
                    src='{{url("static/img/footer-logo.png")}}'></a></div>
    <div style="clear: both"></div><hr/><br/>
    <ul>
        <li><a href="{{url("/" . Lang::getLocale() . "/courses")}}"><img src='{{url("static/img/dots-icon.png")}}'
                                                                         width="8" hight="8">
                {{trans('course.music_courses_menu')}}                </a>
        </li>
        <li><a href="{{url("/" . Lang::getLocale() . "/about")}}#20"><i
                        class="fa fa-circle"></i>{{trans('course.who_we_are')}}</a></li>
        <li><a href="{{url("/" . Lang::getLocale() . "/about")}}#23"><i
                        class="fa fa-circle"></i>{{trans('course.contact_us')}}</a></li>
        <li><a href="{{url("/" . Lang::getLocale() . "/auth/register")}}"><i
                        class="fa fa-circle"></i>{{trans('course.register')}}</a></li>

        @if (Auth::check())
            <li><a href="{{url("/" . Lang::getLocale() . "/profile")}}"><i
                            class="fa fa-circle"></i>{{trans('course.myprofile')}}</a></li>

        @endif
        <li><a href="{{url("/" . Lang::getLocale() . "/subscribe")}}"><i
                        class="fa fa-circle"></i>{{trans('course.subscriptions')}}</a></li>

        <li><a href="{{url("/" . Lang::getLocale() . "/about")}}#24"><i
                        class="fa fa-circle"></i>{{trans('course.faq')}}</a></li>


        <li><a href="{{url("/" . Lang::getLocale() . "/madrasaty")}}"><i
                        class="fa fa-circle"></i>{{trans('course.madrasaty_program')}}</a></li>


    </ul>

    <div style="clear: both"></div><hr/><br/>
    <div class="social_icon" style="margin-right: 25%;">
        <ul>
            <li><a target="_blank" href="https://www.facebook.com/i3zif"> <i class="fa fa-facebook-square"></i></a>
            </li>
            <li><a target="_blank" href="https://twitter.com/i3zif"> <i class="fa fa-twitter-square"></i></a></li>
            <li><a target="_blank" href="https://www.youtube.com/user/i3zif"> <i
                            class="fa fa-youtube-square"></i></a></li>
            <li><a target="_blank" href="https://www.instagram.com/izifcom/"> <i class="fa fa-instagram"></i></a>
            </li>
            <!--<li><a href=""> <i class="fa fa-google-plus-square"></i></a></li>-->
        </ul>
    </div>
    <div style="clear: both"></div><hr/><br/>
    <div class="copy_right"><span>
                2016 Play for Music Limited. All Rights Reserved
            </span></div>
    <br/><br/>
</div>

