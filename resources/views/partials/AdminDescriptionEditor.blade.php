<script src="//cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>

<script>

    var editor = new MediumEditor('.description', {
        toolbar: {
            /* These are the default options for the toolbar,
             if nothing is passed this is what is used */
            allowMultiParagraphSelection: true,
            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
            diffLeft: 0,
            diffTop: -10,
            firstButtonClass: 'medium-editor-button-first',
            lastButtonClass: 'medium-editor-button-last',
            standardizeSelectionStart: false,
            static: false,
            relativeContainer: null,
            /* options which only apply when static is true */
            align: 'center',
            sticky: false,
            updateOnEmptySelection: false
        }
    });

    editor.subscribe('editableInput', function (event, editable) {
        $.ajax({
                    method: "POST",
                    url: "{{url("/" . Lang::getLocale() . '/edit/video/description')}}",
                    data: {
                        description: $('span.description').html(),
                        _token: "{!! csrf_token() !!}",
                        video_id: "{{$video->id}}"

                    }
                })
                .done(function (msg) {


                });

        return false;
    });
</script>