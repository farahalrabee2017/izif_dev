<script type="text/javascript" src="http://flashls.org/flashls-0.4.4.21/examples/libs/swfobject.js"></script>

<script type="text/javascript" src="http://flashls.org/flashls-0.4.4.21/examples/libs/ParsedQueryString.js"></script>

<script>

    var v = "{{$video->video_path}}".replace(/ /g, '').replace(/-/g, '').replace(/_/g, '');
    var videoname = v.split('/')[1].replace('.mp4', '');
    var foldername = v.split('/')[0].replace(/ /g, '').replace(/-/g, '').replace(/_/g, '');
    var src = `http://d36ofzrq5ymb6f.cloudfront.net/output/playlists/${foldername}/${videoname}${foldername}.m3u8`;

    // Replace the & charecter with the origenal to be readed by AWS
    if(src.indexOf('&amp;') >= 0){
        src = src.replace('&amp;','%26');
    }


    var pqs = new ParsedQueryString();
    var parameterNames = pqs.params(false);
    var parameters = {
        src: src,
        autoPlay: "true",
        verbose: true,
        controlBarAutoHide: "true",
        controlBarPosition: "bottom",
        poster: "images/poster.png",
        javascriptCallbackFunction: "jsbridge",
        plugin_hls: "{{url('static/flashls-0.4.4.21/bin/debug/flashlsOSMF.swf')}}",
        hls_minbufferlength: -1,
        hls_maxbufferlength: 30,
        hls_lowbufferlength: 3,
        hls_seekmode: "KEYFRAME",
        hls_startfromlevel: 1,
        hls_seekfromlevel: -1,
        hls_live_flushurlcache: false,
        hls_info: false,
        hls_debug: false,
        hls_debug2: false,
        hls_warn: true,
        hls_error: true,
        hls_fragmentloadmaxretry: -1,
        hls_manifestloadmaxretry: -1,
        hls_capleveltostage: true,
        hls_maxlevelcappingmode: "downscale"
    };

    for (var i = 0; i < parameterNames.length; i++) {
        var parameterName = parameterNames[i];
        parameters[parameterName] = pqs.param(parameterName) ||
                parameters[parameterName];
    }

    var wmodeValue = "direct";
    var wmodeOptions = ["direct", "opaque", "transparent", "window"];
    if (parameters.hasOwnProperty("wmode")) {
        if (wmodeOptions.indexOf(parameters.wmode) >= 0) {
            wmodeValue = parameters.wmode;
        }
        delete parameters.wmode;
    }

    //console.log('=>',parameters,wmodeValue);
    // Embed the player SWF:

    var is_iPad = navigator.userAgent.match(/iPad/i) != null;
    var is_iPhone = navigator.userAgent.match(/iPhone/i) != null;
    if(is_iPad || is_iPhone){
        var player = '<video controls autoplay ><source src="'+src+'"></video>';
        $('#player').html(player);
    } else {
        swfobject.embedSWF(
                "{{url('static/GrindPlayer/GrindPlayer.swf')}}"
                , "player"
                , 640
                , 480
                , "10.2"
                , "expressInstall.swf"
                , parameters
                , {
                    allowFullScreen: "true",
                    wmode: wmodeValue
                }
                , {
                    name: "GrindPlayer"
                }
        );
    }

    function onComplete() {

        $.post(window.location.href, function (data) {
            console.log(data);
        });
    }

    function jsbridge(playerId, event, data) {

        switch (event) {
            case "onJavaScriptBridgeCreated":
                // reference to player
                player = document.getElementById(playerId);
                player.addEventListener("complete", "onComplete");
                break;

            // player state change
            case "ready":
            case "loading":
            case "playing":
            case "paused":
            case "buffering":

            // other events
            case "mediaSize":
            case "seeking":
            case "seeked":
            case "volumeChange":
            case "durationChange":
            case "timeChange":
            case "progress": // for progressive download only
            case "complete":
            //console.log('completed');
            case "advertisement":

            default:
                // console.log(event, data);
                break;
        }
    }
</script>


