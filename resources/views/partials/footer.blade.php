
<div class="inner-footer container">
    <div class="footer_logo"><a href='{{url("/" . Lang::getLocale() . "/")}}'><img
                    src='{{url("static/img/footer-logo.png")}}'></a></div>
    <ul>
        <li><a href="{{url("/" . Lang::getLocale() . "/courses")}}"><img src='{{url("static/img/dots-icon.png")}}'
                                                                         width="8" hight="8">
                {{trans('course.music_courses_menu')}}                </a>
        </li>
        <li><a href="{{url("/" . Lang::getLocale() . "/about")}}#20"><i
                        class="fa fa-circle"></i>{{trans('course.who_we_are')}}</a></li>
        <li><a href="{{url("/" . Lang::getLocale() . "/about")}}#23"><i
                        class="fa fa-circle"></i>{{trans('course.contact_us')}}</a></li>
        <li><a href="{{url("/" . Lang::getLocale() . "/auth/register")}}"><i
                        class="fa fa-circle"></i>{{trans('course.register')}}</a></li>

        @if (Auth::check())
            <li><a href="{{url("/" . Lang::getLocale() . "/profile")}}"><i
                            class="fa fa-circle"></i>{{trans('course.myprofile')}}</a></li>

        @endif
        <li><a href="{{url("/" . Lang::getLocale() . "/subscribe")}}"><i
                        class="fa fa-circle"></i>{{trans('course.subscriptions')}}</a></li>
                          <li><a href="{{url("/" . Lang::getLocale() . "/terms")}}"><i
                        class="fa fa-circle"></i>{{trans('course.terms')}}</a></li>

        <li><a href="{{url("/" . Lang::getLocale() . "/about")}}#24"><i
                        class="fa fa-circle"></i>{{trans('course.faq')}}</a></li>
        
                      

        <li><a href="{{url("/" . Lang::getLocale() . "/madrasaty")}}"><i
                        class="fa fa-circle"></i>{{trans('course.madrasaty_program')}}</a></li>

        <li>
            <a href="//www.iubenda.com/privacy-policy/7910526" class="iubenda-nostyle iubenda-embed" title="Privacy Policy">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src = "//cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
        </li>
        
        


    </ul>

    <br/>
    <div class="social_icon">
        <ul>
            <li><a target="_blank" href="https://www.facebook.com/i3zif"> <i class="fa fa-facebook-square"></i></a>
            </li>
            <li><a target="_blank" href="https://twitter.com/i3zif"> <i class="fa fa-twitter-square"></i></a></li>
            <li><a target="_blank" href="https://www.youtube.com/user/i3zif"> <i
                            class="fa fa-youtube-square"></i></a></li>
            <li><a target="_blank" href="https://www.instagram.com/izifcom/"> <i class="fa fa-instagram"></i></a>
            </li>
            <!--<li><a href=""> <i class="fa fa-google-plus-square"></i></a></li>-->
        </ul>
    </div>
    <div class="copy_right"><span>
                2016 Play for Music Limited. All Rights Reserved
            </span></div>
</div>

