@extends('master')

@section('title', "404")

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>

        .thank_you_container {
            padding-top: 120px;
            height: 30em;
            text-align: center;
        }

        h1 {
            font-size: 40px;
            margin: 30px
        }

        .thank_you_container button {
            width: 150px;
        }


    </style>
@endsection
@section('content')
    <div class="container thank_you_container">

        <h1>
            <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: darkred;font-size:50px"></i>
            <br/>
            {{trans('course.404')}}
        </h1>
        <p>{{trans('course.404_description')}}</p>

        <br/>


    </div>
@endsection

@section('footer')

@endsection