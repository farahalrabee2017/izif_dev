@extends('master')
<head>
    <meta charset="utf-8">
    <title>Tales - Blog</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/bootstrap/dist/css/bootstrap.min.css")}}'>

    <!-- Font-Awesome -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/font-awesome/css/font-awesome.min.css")}}'>

    <!-- Google Webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CPT+Serif:400,400italic' rel='stylesheet'
          type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/css/styles-bluegreen.css")}}' id="theme-styles">


    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="/static/style/blog/node_modules/jquery/dist/jquery.min.js"><\/script>')</script>

    <title> Create blog</title>
    <script type="text/javascript">

        function slugtitle(val) {
            var x = document.getElementById("target").value;
            x = x.replace(/\s+/g, '_');
            document.getElementById("slug").value = x;
            document.getElementById("page_title").value = x;

        }
    </script>

    <title> edit blog</title>
    <script type="text/javascript">

        function slugtitle(val) {
            var x = document.getElementById("target").value;
            x = x.replace(/\s+/g, '_');
            document.getElementById("slug").value = x;

        }


    </script>
</head>

<body>


<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
        menubar: "false",
        height: "200"
    });

</script>


<!-- end icons music-->
<div class="container" style="padding-top:143px ; width: 60%">
    <div class="row" id="Container" align="center">
        @foreach($errors->all(':message') as $message)
            @if($message != "")
                <div id="form-messages" class="alert alert-success" role="alert">
                    {{ $message }}
                </div>
            @endif
        @endforeach()
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="/ar/update/{{ $blog->slug}}">
            <input type="hidden" name="id" value="{{$blog->id}}">
            <div class="form-group title">
                <b style="font-size: medium; color: indianred">Title : </b><input required="required"
                                                                                  onchange="slugtitle(this.value)"
                                                                                  placeholder="Enter title here"
                                                                                  type="text" name="title" id="target"
                                                                                  class="form-control"
                                                                                  value="@if(!old('title')){{$blog->title}}@endif{{ old('title') }}"/>
            </div>

            <div class="form-group">
    <textarea name='content' class="form-control">
      @if(!old('content'))
            {!! $blog->content !!}
        @endif
        {!! old('content') !!}
    </textarea>
            </div>
            <div class="form-group meta">
                <b style="font-size: medium; color: indianred"> Meta description : </b><input
                        value="@if(!old('meta')){{$blog->meta}}@endif{{ old('meta') }}" type="text" name="meta"
                        class="form-control"/>
            </div>

            <div class="form-group slug">
                <b style="font-size: medium; color: indianred">Slug :</b> <input required="required" type="text"
                                                                                 id="slug" name="slug"
                                                                                 class="form-control"
                                                                                 value="@if(!old('slug')){{$blog->slug}}@endif{{ old('slug') }}"/>
            </div>
            <div class="form-group category">
                <b style="font-size: medium; color: indianred">Category :</b> <input required="required" type="text"
                                                                                     id="category" name="category"
                                                                                     class="form-control"
                                                                                     value="@if(!old('category')){{$blog->category}}@endif{{ old('category') }}"/>
            </div>
            <div class="form-group date">
                <b style="font-size: medium; color: indianred">Created at :</b> <input required="required" type="text"
                                                                                     id="date" name="date"
                                                                                     class="form-control"
                                                                                     value="@if(!old('date')){{$blog->created_at}}@endif{{ old('date') }}"/>
            </div>
            <div class="form-group Pagetitle">
                <b style="font-size: medium; color: indianred"> Page title : </b><input id="page_title"
                                                                                        value="@if(!old('slug')){{$blog->page_title}}@endif{{ old('page_title') }}"
                                                                                        type="text" name="page_title"
                                                                                        class="form-control"/>
            </div>
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" name='save' class="btn btn-default" value="Save"/>
            <a href="{{  url('ar/delete/'.$blog->slug.'?_token='.csrf_token()) }}" class="btn btn-danger">Delete</a>
        </form>
        <br/>
        <form action="?active">
            <input class="btn btn-danger" type="submit" value="<?php if ($blog->active == '1') {
                echo "Unpublish";
            } else {
                echo "Publish";
            }  ?>" name="active">
        </form>
        <br/>
        <form action="/ar/blog">
            <input type="submit" class="btn btn-success" value="Main page">

        </form>

    </div>
</div>
</body>

<style>
    .form-group.slug,
    .form-group.title,
    .form-group.Pagetitle,
    .form-group.meta,
    .form-group.category,
    .form-group.date {

        width: 300px;
    }


</style>