@extends('master')

<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Izif Blog</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap styles -->
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">

    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/bootstrap/dist/css/bootstrap.min.css")}}'>

    <!-- Font-Awesome -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/font-awesome/css/font-awesome.min.css")}}'>

    <!-- Google Webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CPT+Serif:400,400italic' rel='stylesheet'
          type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/css/styles-bluegreen.css")}}' id="theme-styles">


    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="/static/style/blog/node_modules/jquery/dist/jquery.min.js"><\/script>')</script>
</head>
@section('content')
    <div>

        @if((\Auth::check() && Auth::user()->user_group_id == '2'))
            <a href="/ar/new-blog" style="font-size: large; color: white"> <b>Create Post </b></a>
        @endif

    </div>

    <div class="widewrapper main">

        <div class="container">

            <div class="row">
                <div class="container col-md-12 col-xs-12" style="padding-top:118px">

                    <div class="row" id="Container" align="center">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                    @endif
                        <div class="blog_logo">
                            <a class="logo" href="/ar/blog"><h3 style="font-size: 40px;">مدونة إعزف</h3></a>

                        </div>
                            <div class="tales-searchbox">
                                <form action="/ar/blog " method="get" accept-charset="utf-8" name="query">
                                    <input class="searchfield" name="query" id="query" type="text" placeholder="إبحث">

                                    <button class="searchbutton" type="submit">

                                        <i class="fa fa-search"></i>
                                    </button>


                                </form>
                            </div>

                    <!-- Blog Filters-->
                        <div class="btn-group filters" style="margin: 0 586px 0 167px; ;border-radius: 10px;">
                            <form method="get" name="filters" id="filters">

                                <select name="filter" class="form-control filter" onchange="$('#filters').submit();">
                                    <option value="0">ترتيب</option>
                                    <option value="latest" <?php if (!empty($filter)) {
                                        if ($filter == 'latest') {
                                            echo 'selected';
                                        }
                                    }  ?>>اﻷحدث
                                    </option>
                                    <option value="oldest" <?php if (!empty($filter)) {
                                        if ($filter == 'oldest') {
                                            echo 'selected';
                                        }
                                    } ?>>اﻷقدم
                                    </option>
                                    <option value="most_viewed" <?php if (!empty($filter)) {
                                        if ($filter == 'most_viewed') {
                                            echo 'selected';
                                        }
                                    }  ?>>اﻷكثر مشاهدة
                                    </option>
                                </select>
                            </form>
                            <br/>
                            <h4>صفحة {{ $blogs->currentPage() }} من {{ $blogs->lastPage() }}</h4>

                            <br/>
                        </div>
                    </div>
                </div>
                <div>

                    <div class="col-md-8 blog-main">

                        <div class="row">
                            @foreach ($blogs as $post)
                                <div class="col-md-6 col-sm-6">

                                    <article class="blog-teaser">
                                        <header>
                                            @if(preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', html_entity_decode($post->content), $m))
                                                <img height="300px" src="{{$m[1]}}">
                                            @endif
                                            <h3><a href="blog/{{ $post->slug }}">{{ $post->title }}</a></h3>
                                            <span class="meta">{{ $post->created_at->format('M jS Y g:ia') }}</span>
                                            <hr>
                                        </header>
                                        <div class="body">
                                            <?php $content = preg_replace("/<img[^>]+\>/i", " ", $post->content);   ?>
                                            {!! str_limit(html_entity_decode($content) , 350)!!}

                                        </div>
                                        <div class="clearfix">
                                            <a href="/ar/blog/{{$post->slug}}" class="btn btn-tales-one">المزيد</a>
                                        </div>
                                    </article>
                                </div>
                            @endforeach
                        </div>
                        <div class="paging">
                            {!! $blogs->render() !!}
                        </div>
                    </div>
                    <aside class="col-md-4 blog-aside">

                        <div class="aside-widget">

                            <header>
                                <h3> إقرأ أيضاً ..</h3>
                            </header>
                            <div class="body">
                                <ol class="tales-list">
                                    @foreach($random as $post)
                                        <li><a href="/ar/blog/{{$post->slug}}">{{$post->title}}</a></li>

                                    @endforeach
                                </ol>
                            </div>
                        </div>

                        {{--<div class="aside-widget">--}}
                        {{--<header>--}}
                        {{--<h3>Authors Favorites</h3>--}}
                        {{--</header>--}}
                        {{--<div class="body">--}}
                        {{--<ul class="tales-list">--}}
                        {{--<li><a href="index.html">Email Encryption Explained</a></li>--}}
                        {{--<li><a href="#">Selling is a Function of Design.</a></li>--}}
                        {{--<li><a href="#">It’s Hard To Come Up With Dummy Titles</a></li>--}}
                        {{--<li><a href="#">Why the Internet is Full of Cats</a></li>--}}
                        {{--<li><a href="#">Last Made-Up Headline, I Swear!</a></li>--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="aside-widget">
                            <header>
                                <h3>التصنيفات</h3>
                            </header>
                            <div class="body clearfix">
                                <ul class="tags">
                                    @foreach($tags as $tag)
                                        @if($tag->category != "")
                                            <li><a href="/ar/blog/category/{{$tag->category}}">{{$tag->category}}</a>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>

                            </div>
                        </div>
                        <hr style="border-width: 1px">
                    </aside>
                </div>
            </div>
        </div>
    </div>

@endsection
<script>
    document.getElementById("filters").onchange = function () {
        localStorage.setItem('filters', document.getElementById("filters").value);
    }

    if (localStorage.getItem('filters')) {
        document.getElementById("filters").options[localStorage.getItem('filters')].selected = true;
    }
    ​
</script>

<script src='{{url("static/style/blog/node_modules/bootstrap/dist/js/bootstrap.min.js")}}'></script>
<script src='{{url("static/style/blog/modules/modernizr/modernizr-custom.js")}}'></script>
</body>
</html>