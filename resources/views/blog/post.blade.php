@extends('master')
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>    {{ $blog->page_title }}</title>
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="{{$blog->meta}}" rel="stylesheet">

    <script>
        (function () { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = '//izif.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments
            powered by Disqus.</a></noscript>

    </div>

    <!-- Bootstrap styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/bootstrap/dist/css/bootstrap.min.css")}}'>

    <!-- Font-Awesome -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/font-awesome/css/font-awesome.min.css")}}'>

    <!-- Google Webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CPT+Serif:400,400italic' rel='stylesheet'
          type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/css/styles-bluegreen.css")}}' id="theme-styles">

    <!--[if lt IE 9]>
    <script src='{{url("static/style/blog/modules/respond/html5-3.6-respond-1.1.0.min.js")}}'></script>
    <![endif]-->

    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    <script>window.jQuery || document.write('<script src=\'{{url("static/style/blog/node_modules/jquery/dist/jquery.min.js")}}\'><\/script>')</script>
</head>
@section('content')
<div class="widewrapper main">
    <div class="container">
        <div class="row post" style="margin-top: 95px;">
            <div class="tales-breadcrumb">
                <a href="/ar/blog">المدونة</a>
                <span class="separator">&#x2F;</span>
                <a href="#">{{ $blog->title }}</a>
            </div>
            <div class="col-md-8 blog-main">
                <article class="blog-post">
                    <header>

                        <h1>&#171;{{ $blog->title }}&#187;</h1>
                        <div class="lead-image">
                            <div class="meta clearfix">

                                <div class="author">
                                    {{--<i class="fa fa-user"></i>--}}
                                    {{--<span class="data">Alexander Rechsteiner</span>--}}
                                </div>
                                <div class="date">
                                    <i class="fa fa-calendar"></i>
                                    <span class="data">{{ $blog->created_at->format('M jS Y g:ia') }}</span>
                                </div>
                                <div class="comments">
                                    <i class="fa fa-comments"></i>
                                    <div class='data'><a href="/ar/blog/{{$blog->slug}}#disqus_thread">0 Comments</a>
                                    </div>
                                </div>
                                <div class="category">
                                    <div class='data'><a href="/ar/blog/category/{{$blog->category}}">{{$blog->category}}</a>
                                    </div>
                                </div>
                                @if((\Auth::check() && Auth::user()->user_group_id == '2'))
                                <div class="edit">
                                    <div class='data'><a href="/ar/edit-blog/<?php echo $blog->slug?>">تحرير</a></div>
                                </div>
                                @endif

                            </div>
                        </div>
                    </header>
                    <div class="body">

                        {!! nl2br((html_entity_decode($blog->content))) !!}
                    </div>
                    <div align="center">
                        <a class="btn btn-tales-one" href="/ar/blog">Home</a></div>

                </article>
                <div id="disqus_thread"></div>

            </div>
            <aside class="col-md-4 blog-aside post">

                <div class="aside-widget post">
                    <header>
                        <h3>إقرأ أيضاً...</h3>
                    </header>
                    <div class="body">
                        <ul class="tales-list">
                            @foreach($random as $post)
                                <li><a href="/ar/blog/{{$post->slug}}">{{$post->title}}</a></li>

                            @endforeach
                        </ul>
                    </div>
                </div>

                    {{--<div class="aside-widget post">--}}
                        {{--<header>--}}
                            {{--<h3>Authors Favorites</h3>--}}
                        {{--</header>--}}
                        {{--<div class="body">--}}
                            {{--<ul class="tales-list">--}}
                                {{--<li><a href="index.html">Email Encryption Explained</a></li>--}}
                                {{--<li><a href="#">Selling is a Function of Design.</a></li>--}}
                                {{--<li><a href="#">It’s Hard To Come Up With Dummy Titles</a></li>--}}
                                {{--<li><a href="#">Why the Internet is Full of Cats</a></li>--}}
                                {{--<li><a href="#">Last Made-Up Headline, I Swear!</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                <div class="aside-widget post">
                    <header>
                        <h3>التصنيفات</h3>
                    </header>
                    <div class="body clearfix">
                        <ul class="tags">
                            @foreach($tags as $tag)
                                @if($tag->category != "")
                                    <li><a href="/ar/blog/category/{{$tag->category}}">{{$tag->category}}</a></li>
                                @endif
                            @endforeach

                        </ul>
                    </div>
                </div>
                <hr style="border-width: 1px">
            </aside>
        </div>

    </div>

</div>
@endsection
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'izif'; // required: replace example with your forum shortname
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script');
        s.async = true;
        s.type = 'text/javascript';
        s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
</body>

<script src='{{url("static/style/blog/node_modules/bootstrap/dist/js/bootstrap.min.js")}}'></script>
<script src='{{url("modules/modernizr/modernizr-custom.js")}}'></script>
</body>
</html>