@extends('adminmaster')

@section('title')
  Izif Blog
@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
@endsection

@section('breadcrumb')
    <li class="active">Izif Blog</li>
@endsection


@section('content')

    <!-- Music Icons -->

    <!-- end icons music-->
    <div class="container col-md-12 col-xs-12" style="padding-top:25px">
        <div class="row" id="Container" align="center">
            @foreach($errors->all(':message') as $message)
                @if($message != "")
                    <div id="form-messages" class="alert alert-success" role="alert">
                        {{ $message }}
                    </div>
                @endif
            @endforeach()
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        <!-- Blog Filters-->
            <div>

                <a class="btn btn-primary" href="/ar/blog">Blog</a>
                <a class="btn btn-primary" href="/ar/admin/blog/new-blog">Create post</a>
                <br/>
            </div>
            <br>
            <h5>Page {{ $blogs->currentPage() }} of {{ $blogs->lastPage() }}</h5>
            <hr>
            <?php $count = 1; ?>

            <table id="blog" class="display dataTable" cellspacing="0" width="100%" border="1" align="center ">
                <thead>
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th> post title</th>
                    <th>Created date</th>

                </tr>
                </thead>
                @foreach ($blogs as $post)

                    <tr align="center">
                        <td>
                            <form method="get" action="?{{$post->id}}">

                                <input type="hidden" name="checkbox[]" data-id="checkbox" class="cb"
                                       value="{{$post->id}}"/>

                                <input class="btn btn-primary" type="submit" value="publish">
                            </form>
                        </td>
                        <td>
                            {{$count}}
                        </td>

                        <td>

                            <a href="/ar/blog/{{ $post->slug }}">  {{ $post->title }}</a>
                        </td>
                        <td>
                            {{ $post->created_at->format('M jS Y g:ia') }}
                        </td>

                    </tr>
                    <?php
                    $count++;

                    ?>
                @endforeach

            </table>
        </div>
    </div>
    <hr>
@endsection
@section('scripts')
    <script>
        $(function () {
            $('#blog').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });

    </script>

@endsection