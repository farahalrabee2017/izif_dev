@extends('adminmaster')
<head>
    <meta charset="utf-8">
    <title>Tales - Blog</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <!-- Bootstrap styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/bootstrap/dist/css/bootstrap.min.css")}}'>
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/bootstrap/dist/css/bootstrap-theme.min.css")}}'>

    <!-- Font-Awesome -->
    <link rel="stylesheet" href='{{url("static/style/blog/node_modules/font-awesome/css/font-awesome.min.css")}}'>

    <!-- Google Webfonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CPT+Serif:400,400italic' rel='stylesheet'
          type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href='{{url("static/style/blog/css/styles-bluegreen.css")}}' id="theme-styles">


    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="/static/style/blog/node_modules/jquery/dist/jquery.min.js"><\/script>')</script>

    <title> Create blog</title>
    <script type="text/javascript">

        function slugtitle(val) {
            var x = document.getElementById("target").value;
            x = x.replace(/\s+/g, '_');
            document.getElementById("slug").value = x;
            document.getElementById("page_title").value = x;

        }
    </script>
</head>
<body>


<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste jbimages"],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    });
</script>


<!-- end icons music-->
@section('content')
    <h3 align="center">Create post</h3>
    <div class="container" style="padding-top:25px; width: 60%">

        <div class="row" id="Container" align="center">

            <form action="/ar/new-blog" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group title">
                    <b style="font-size: medium; color: indianred"> Title : </b><input required="required" id="target"
                                                                                       value="{{ old('title') }}"
                                                                                       placeholder="أدخل العنوان هنا"
                                                                                       type="text" name="title"
                                                                                       class="form-control"
                                                                                       onchange="slugtitle(this.value)"/>
                </div>
                <div class="form-group">
                    <b style="font-size: medium; color: indianred"> Content :
                    </b>
                    <textarea name='content' class="form-control">{{ old('content') }}</textarea>
                </div>
                <div class="form-group meta">

                    <b style="font-size: medium; color: indianred"> Meta description : </b><input
                            value="{{ old('meta') }}" type="text" name="meta" class="form-control"/>
                </div>
                <div class="form-group title">

                    <b style="font-size: medium; color: indianred"> Page title : </b><input id="page_title"
                                                                                            value="{{ old('page_title') }}"
                                                                                            type="text"
                                                                                            name="page_title"
                                                                                            class="form-control"/>
                </div>
                <div class="form-group slug">
                    <b style="font-size: medium; color: indianred"> Slug : </b>
                    <input required="required" id="slug" type="text" name="slug" class="form-control" value=""/>
                </div>
                <div class="form-group category">
                    <b style="font-size: medium; color: indianred"> Category : </b>
                    <input required="required" id="category" type="text" name="category" class="form-control"
                           value="{{ old('category') }}"/>
                </div>
                <div class="form-group date">
                    <?php  $dt = new DateTime();?>
                    <b style="font-size: medium; color: indianred"> Created at : </b>
                    <input required="required" id="date" type="text" name="date" class="form-control" value="{{ $dt->format('Y-m-d H:i:s')}}"/>
                </div>
                <input type="submit" name='publish' class="btn btn-success" value="Publish"/>
            </form>
            <br/>
            <form action="/ar/blog">
                <input type="submit" class="btn btn-success" value="Main page">
            </form>
        </div>
    </div>

@endsection

<style>
    .form-group.slug,
    .form-group.title,
    .form-group.Pagetitle,
    .form-group.meta,
    .form-group.category,
    .form-group.date {

        width: 300px;
        direction: rtl;
    }


</style>