@extends('master')

@section('title', "About Izif")

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>
        h1{
            font-size: 30px;
            margin-top: 25px;
            margin-bottom: 25px;
        }


        p {
            font-size: 12px;
            line-height: 1.5em;
        }
        h2 {
            font-size: 20px;
            margin-top: 10px;
            margin-bottom: 10px;
        }


    </style>
@endsection
@section('content')


    <div class="container" style="margin-top: 8em;">
        <div class="row">
            <div class="col-md-12" style="direction: ltr;">

                <h1>Terms of Service</h1>

                <h2>Ownership and Acceptance of Terms</h2>

                <p>Welcome to izif.com, owned and operated by Play for Music Limited. Subject to the following Terms of Service
                    (TOS) izif.com provides users of the Site with access to a collection of music lessons (the “Service”). Izif.com reserves the right to amend these TOS from time to time and at any time. Use of the Services by a user shall constitute that user’s acceptance of these Terms of Service. 
                   The Site is owned and operated by Play for Music Limited, in which will be considered the Owner of the site. 
                </p>
                <br/>


                <h2>Services</h2>

                <p>
                 
                    Izif.com provides music lessons for various levels for various musical instruments,
                    and instructing chosen musical songs. Any visitor to the Site will be able to access only basic 
                    information about the Site and the Services of izif.com. Site visitors may register to use the free Services of
                    izif.com by providing basic information to izif.com, and registered users receive limited access to Site Services.
                    Registered users may subscribe to a certain course, subscribe to multiple courses, or subscribe to izif.com’s
                    full access to all video lessons. Subscriptions require periodic payments to gain and maintain access to izif.com’s content.
                    Subscribers agree to pay for the subscription period selected at the time of purchase. Subscribers 
                    will receive a confirmation notice upon their original effective date. Subscription amounts are billed automatically
                    through electronic payments providers. It is the subscriber’s responsibility to review the notice to ensure that it
                    accurately reflects the requested subscription period. Subscribers agree that by subscribing and settling the first
                    payment for the first subscription interval, the next payment will be charged automatically as soon as the subscription 
                    interval ends, it is the subscriber’s responsibility to check the date of the next due payment that is available on 
                    the subscription email notification, and on the subscriber’s profile page on izif.com. Subscribers can cancel 
                    subscription and automatic billing by sending an email to izif@izif.com or directly through their selected payment provider. 
                    No granting of any license to dispose, use, publish, or alter any of the materials mentioned herein. 

               <br/><br/>

                </p>

                <h2>Reservation of Rights</h2>
                <p>
                    
                    Usage of any/all of the materials in any manner is not allowed; displaying any of the Materials must be accompanied
                    with a written approval from the Owner.
                    Izif.com reserves the right to terminate and/or cancel subscribers’ subscriptions or access to the Site at any 
                    time for any breach of subscribers to the terms as mentioned herein. 
                    All videos displayed and subscribed for within are the sole property of izif.com, and no usage outside the Site
                    will be allowed by the user and/or subscriber. Any commercial publication or exploitation of the Services or any 
                    content provided in connection therewith is specifically prohibited such as copying, electronic transfer, uploading,
                    posting, or submission at any other site, under legal liability for any loss and damage. 
                    <br/><br/>

                </p>
                <h2>Username/Password Protection</h2>

                <p>
                    Upon registration, registered users and subscribers will choose a username and password that will permit 
                    them to access the appropriate level of Services for the Site. Registered users and subscribers to the Site 
                    agree to take all reasonable steps to protect and ensure the accuracy of any login, password, or payment information
                    provided in connection with the Services. Subscribers agree to be the sole users of their username/password and the 
                    Services, and to promptly notify izif.com if this is not the case. Izif.com may terminate access and deactivate or
                    delete a user’s account without prior notice and without obligation. Izif.com reserves the right to restrict access
                    to the Services to any user, and may at any time without notice immediately deny access to any person. 
                    <br/>
                </p>

                <h2>Links</h2>

                <p>
                   Linking to the website requires a prior permission from the webmaster.
                   Owner owns the following URLs:<br/><br/>

                    www.izif.com<br/>
                    www.i3zif.com<br/>
                    www.i3zif.net<br/>
                    www.i3zif.org<br/>
                    www.i3zef.com<br/>
                    www.e3zif.com<br/>
                    www.e3zef.com<br/><br/>

                  Once permission is granted consider the following points:

                    <br/>
                    The title is the official website.<br/>
                    Hyperlink must be established to www.izif.com <br/>
                    Link must be done through any of the icons provided by the Owner. <br/>
                   Owner will by no way be held responsible for the content of any of the websites used for linking as 
                   they are not considered part of the Owner’s website. <br/> <br/>

                </p>

                <h2>Owner’s Liability Restrictions</h2>
                <p>
                    Owner will not be held liable to any cost incurring from interruption of the site due to error or incur any repair
                    cost or correction due to your use of the site.
                    Owner hereby grants the use of reasonable efforts to ensure the accuracy, correctness and reliability of the 
                    content with no representations or warranties or responsibility as to the content’s accuracy, correctness or reliability. 
                    Owner will not be held responsible or liable for technical, hardware or software failures of any kind, the use of 
                    the site will be at the user’s own risk, as Owner will not be held liable to any direct or indirect or consequential
                    profit loss caused by use of the Owner’s site. 
                    The Owner will not be held responsible for the use of any linked sites to this site, as the user waives any and 
                    all claims against the Owner regarding the inclusion of links to outside websites or the user’s use of those websites. 
                    The Owner will in no way be held liable or responsible for the quality or merchantability of any of the services
                    mentioned (if mentioned).
                </p>

            </div>
        </div>
    </div>

@endsection

@section('footer')

@endsection