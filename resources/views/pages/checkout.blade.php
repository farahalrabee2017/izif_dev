@extends('master')

@section('title', trans('course.payment_method_title'))
@section('description', trans('payment_method_description_ar'))


@section('head')
<style type="text/css">


    .ckeck_shadow_right {
        box-shadow: 3px 3px 7px rgba(0, 0, 0, .3);
        cursor: pointer;
        margin-top: 10px;
        border-radius: 7px;

    }

    .ckeck_left {
        background-color: #EDEDEE !important;
        box-shadow: 4px 4px 7px rgba(0, 0, 0, .3);
        height: 495px;
        text-align: center !important;
        border-radius: 10px;
        padding-bottom: 30px;
        padding-top: 20px;
        margin: 10px;

    }

    .ckeck_left img {
        cursor: pointer;
        margin-top: 10px;
    }

    .check_med {
        margin-top: 10px;
        background-color: white;
        border-radius: 10px;
        /* width: 100%; */
        margin-left: 10px;
        margin-right: 10px;

    }

    .ckeck_right {
        background-color: #EDEDEE !important;
        box-shadow: 4px 4px 7px rgba(0, 0, 0, .3);
        border-radius: 10px;
        text-align: center;
        padding-top: 30px;
        padding-left: 0 !important;
        padding-right: 0 !important;
        height: 495px;
        margin: 10px;
    }

    .ckeck_right span strong {
        width: 20px !important;
        height: 20px !important;
        border-radius: 10px !important;
        background-color: #666 !important;
        color: #fff !important;
        display: inline-block !important;

    }

    .ckeck_right span {
        margin-top: 50px;
    }

    .black_msg {
        width: 100%;
        height: 40px;
        background-color: black;
        margin-top: 20px;
        padding-left: 0 !important;
        padding-right: 0 !important;
        line-height: 40px;
        color: #fff;
    }

    .orange_msg {
        width: 100%;
        height: 70px;
        background-color: #FAC424;
        padding-left: 0 !important;
        padding-right: 0 !important;
        line-height: 80px;
        color: #000;
        font-size: 50px;
        font-weight: bold;
    }

    .choice_msg {
        margin-top: 50px;
    }

    .check_small_logo {
        margin-top: 100px;
    }

    .checkout {
        padding-top: 120px;
        padding-bottom: 50px;
    }

    .sweet-alert p {
        font-size: 13px;

        @if(Lang::getLocale() == "en")
        direction: ltr;
        text-align: left;

        @else
        direction: rtl;
        text-align: right;
        @endif




    }

    .sweet-alert form input {
        display: block;
    }
</style>
@endsection


@section('content')

<div class="container checkout">
    <div class="row">
        <div class="col-md-9 col-md-offset-2">
            <div class="col-md-8 ckeck_left">
                <div class="row">
                    <div class="col-md-6">
                        <!--<img class="ckeck_shadow_right" src="{{url('/static/img/cash-u.png')}}">-->
                        @include('partials.payment_gateway.cashu')

                    </div>
                    <div class="col-md-6">
                        @include('partials.payment_gateway.paypal')
                                <!--<img class="ckeck_shadow_right" src="{{url('/static/img/payPal.png')}}">-->
                    </div>
                </div>
                <div class="row check_med" style="padding: 10px;">
                    @include('partials.payment_gateway.2checkout')
                            <!--<div class="col-md-4 col-xs-12"><img src="{{url('/static/img/visa.png')}}"></div>
                    <div class="col-md-4 col-xs-12"><img src="{{url('/static/img/masterCard.png')}}"></div>
                    <div class="col-md-4 col-xs-12"><img src="{{url('/static/img/americanExpress.png')}}"></div>
                    <div class="col-md-4 col-xs-12"><img src="{{url('/static/img/JCB.png')}}"></div>
                    <div class="col-md-4 col-xs-12"><img src="{{url('/static/img/discover.png')}}"></div>
                    <div class="col-md-4 col-xs-12"><img src="{{url('/static/img/dinersClub.png')}}"></div>-->
                </div>
                <div class="row">
                    <div class="col-md-6"><img class="banktransfer ckeck_shadow_right"
                                               src="{{url('/static/img/bankTransfer.png')}}"></div>

                    <div class="col-md-6"><img class="westernUnion ckeck_shadow_right"
                                               src="{{url('/static/img/westernUnion.png')}}"></div>
                </div>
            </div>
            <div class="col-md-3 ckeck_right">
                
                <div class="col-md-12 black_msg"><span>
                        @if(Lang::getLocale() == "ar")
                        {{$subscription_model->title_ara}}
                        @else
                        {{$subscription_model->title_enu}}
                        @endif
                    </span></div>
                <div class="col-md-12 orange_msg"><span> ${{$price}}</span></div>
                <div class="col-md-12 choice_msg">
                    <span>{{trans('course.select_payment_method')}}</span>
                </div>
                <div class="col-md-12 check_small_logo"><img src="{{url('/static/img/check_small_logo.png')}}">
                </div>
            </div>
            <input type="hidden" id="subscription_model_id" value="{{$subscription_model_id}}">
            <input type="hidden" id="display_price" value="{{$price}}">
            
            

        </div>
    </div>
</div>

@endsection


@section('footer')

<script>
    try {
        ga('require', 'ecommerce');

        ga('ecommerce:addTransaction', {
            'id': '{{$subscription_model->id}}',
            'affiliation': '@if(Lang::getLocale() == "ar"){{$subscription_model->title_ara}} @else {{$subscription_model->title_enu}} @endif',
            'revenue': '{{$price}}',
            'shipping': '0',
            'tax': '0'
        });

        ga('ecommerce:addItem', {
            'id': '{{$subscription_model->id}}',
            'name': '@if(Lang::getLocale() == "ar"){{$subscription_model->title_ara}} @else {{$subscription_model->title_enu}} @endif',
            'sku': 'subscription-{{$subscription_model->id}}',
            'category': 'subscription-{{$subscription_model_period}}-months',
            'price': '{{$price}}',
            'quantity': '1'
        });

        ga('ecommerce:send');

    } catch (e) {
        console.log(e);
    }
</script>


<script>


    var $cashu = $('form.cashu');
    var $banktransfer = $('.banktransfer');
    var $westernUnion = $('.westernUnion');

    $cashu.submit(function (event) {
        var that = this;


        event.preventDefault();


        swal({
            title: "شكراً لك !",
            text: "سيتم التفعيل قريبا",
            type: "info",
            showCancelButton: true,
            showConfirmButton: false
        });

    });


    $banktransfer.click(function (event) {
        var that = this;


        event.preventDefault();

        /*
         swal({
         title: "شكراً لك !",
         text: "يمكنك الدفع عن طريق حوالة بنكية إلى :\nبنك الإتحاد  الفرع الرئيسي – الشميساني , رقم حساب \nInternational: Bank transfer to :  BANK AL ETIHAD\nmain branch - Shemsani\nBank account number:\nUSD:   0010204561215101\nIBAN: JO78UBSI1010000010204561215101\nSwift Code : UBSIJOAXXXX\nCorporate branch\nPLAY FOR MUSIC LIMITED\n- الشركات الكبرى – شركة :\nبلاي فور ميوزك ليميتد\nPLAY FOR MUSIC LIMITED\n(غير متوفر للإشتراك الشهري\n",
         type: "info",
         showCancelButton: true,
         showConfirmButton: false
         });*/

        swal({
            title: "",
            text: '{!! $banktrasfare  !!}',
            html: true
        }, function () {
            var name = $('.transaction_user_name').val();
            var transactionNumber = $('.transaction_number').val();
            var country = $('.transaction_country').val();
            var type = $('.transaction_type').val();
            var subscription_model_id = $('#subscription_model_id').val();
            var display_price = $('#display_price').val();
            if (name != '' && transactionNumber != '' && country != '' && type != '') {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{url('/' . Lang::getLocale() . '/addPendingTrans' )}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {name: name, transactionNumber: transactionNumber, country: country, type: type, subscription_model_id: subscription_model_id, display_price:display_price},

                    success: function (resData) {

                    }
                });
            }
        });

    });


    $westernUnion.click(function (event) {
        var that = this;
        event.preventDefault();

        swal({
            title: "",
            text: '{!! $westren_union !!}',
            html: true
        }, function () {
            var name = $('.transaction_user_name').val();
            var transactionNumber = $('.transaction_number').val();
            var country = $('.transaction_country').val();
            var type = $('.transaction_type').val();
            var subscription_model_id = $('#subscription_model_id').val();
            var display_price = $('#display_price').val();
            if (name != '' && transactionNumber != '' && country != '' && type != '') {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{url('/' . Lang::getLocale() . '/addPendingTrans' )}}",
                    beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');

                        if (token) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                    },
                    data: {name: name, transactionNumber: transactionNumber, country: country, type: type, subscription_model_id: subscription_model_id, display_price:display_price},
                    success: function (resData) {

                    }
                });
            }
        });

    });
</script>


@endsection