@extends('master')

@section('title', trans('course.music_sheets'))

@section('head')

    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">

    <style>
        .sheet {
            background: #eee;
            border: 1px solid #E2DFDF;
            border-radius: 10px;
            padding: 10px;
            margin-bottom: 5px;
        }

        .sheet-title {
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .sheet-footer {
            padding: 10px;

            margin-top: 10px;
            margin-bottom: 10px;
        }

        .sheet-footer ul {
            display: inline;
            width: 100%;
        }

        .sheet-footer ul li {
            float: left;
            font-size: 12px;
            width: 50%;
            text-align: center;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        h1 {
            font-size: 22px;
            padding: 12px;
        }
    </style>



@endsection
@section('content')


    <div class="container" style="margin-top: 10%;padding:10px">
        <div class="filters"></div>


        <div class="col-md-12 mix" style="margin:0 auto" data-author="{{$note->author}}" data-type="{{$note->type}}"
             data-year="{{$note->year}}">
            <center>
                <h1>
                    @if(Lang::getLocale() == "ar")
                        {{$note->title_arabic}}
                    @else
                        {{$note->title_english}}
                    @endif
                    -
                    {{$note->author}}</h1>
                <iframe src='{{url("static/ViewerJS/")}}#https://dt5x436ix2iss.cloudfront.net/{{$note->note_path}}'
                        width='800' height='800' allowfullscreen webkitallowfullscreen></iframe>
            </center>


        </div>


    </div>

@endsection

@section('footer')


@endsection