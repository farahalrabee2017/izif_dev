@extends('master')

@section('title', "About Izif")

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>
        .section {
            padding: 10px;
        }

        .section h3 {
            font-size: 28px;
            background: #6D060A;
            color: #fff;
            padding: 0.3em;
        }

        .section .section-content {
            font-size: 15px;
            padding: 1.5em;
        }

        p {
            padding: 0.3em;
        }

        @if(Lang::getLocale() == "en")
            .containers {
            direction: ltr !important;
        }

        .containers * {
            direction: ltr !important;
        }

        @endif


    </style>
@endsection
@section('content')


    <div class="container" style="margin-top: 8em; direction: rtl;">
        <div class="row">
            @if(!Auth::Guest())
                @if(Auth::user()->user_group_id == 2)
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add New Page Section</h3>
                            </div>
                            <div class="panel-body">
                                <form id="addPageSection">
                                    <div class="form-group">
                                        <label for="title_arabic">Title Arabic</label>
                                        <input type="text" class="form-control" name="title_arabic" id="title_arabic"
                                               placeholder="Title Arabic">
                                    </div>
                                    <div class="form-group">
                                        <label for="title_english">Title English</label>
                                        <input type="text" class="form-control" name="title_english" id="title_english"
                                               placeholder="Title English">
                                    </div>
                                    <div class="form-group">
                                        <label for="description_arabic">Description Arabic</label>
                                <textarea id="description_arabic" class="form-control" name="description_arabic"
                                          placeholder="Description Arabic"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="description_english">Description English</label>
                                <textarea id="description_english" class="form-control" name="description_english"
                                          placeholder="Description English"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="section_order">Section Order</label>
                                        <input type="number" class="form-control" name="section_order"
                                               id="section_order"
                                               placeholder="Section Order">
                                    </div>

                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                        </div>

                    </div>
                @endif
            @endif
        </div>
        <div class="row containers">
            @foreach($containers as $container)
                <div class="col-md-12 section" id="{{$container->id}}">
                    @if(!Auth::Guest())
                        @if(Auth::user()->user_group_id == 2)
                            <a href="#" class="deleteSection" data-id="{{$container->id}}">X</a>
                        @endif
                    @endif
                    <h3>
                        @if(Lang::getLocale() == "ar")
                            {{$container->title_arabic}}
                        @else
                            {{$container->title_english}}
                        @endif
                    </h3>
                    <div class="section-content">
                        @if(Lang::getLocale() == "ar")
                            {!! $container->description_arabic !!}
                        @else
                            {!! $container->description_english !!}
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection

@section('footer')

    @if(!Auth::Guest())
        @if(Auth::user()->user_group_id == 2)
            <script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
            <script>

                CKEDITOR.replace('description_arabic');
                CKEDITOR.replace('description_english');


                var Pages = {};

                Pages.add = function (data, callback) {
                    var request = $.ajax({
                        url: "{{url('/' . Lang::getLocale() . '/pages/add' )}}",
                        method: "POST",
                        data: data,
                    });

                    request.done(function (msg) {
                        callback(msg);
                    });

                    request.fail(function (jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);
                    });
                };


                Pages.remove = function (id, callback) {
                    var request = $.ajax({
                        url: `{{url('/' . Lang::getLocale() . '/pages/delete' )}}/${id}`,
                        method: "POST",

                    });

                    request.done(function (msg) {
                        callback(msg);
                    });

                    request.fail(function (jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);
                    });
                };


                function toObject(array) {
                    var o = {};
                    var a = array;
                    $.each(a, function () {
                        if (o[this.name] !== undefined) {
                            if (!o[this.name].push) {
                                o[this.name] = [o[this.name]];
                            }
                            o[this.name].push(this.value || '');
                        } else {
                            o[this.name] = this.value || '';
                        }
                    });
                    return o;
                }

                $('form#addPageSection').submit(function (event) {
                    event.preventDefault();
                    var formData = toObject($(this).serializeArray());
                    formData['description_arabic'] = CKEDITOR.instances.description_arabic.getData();
                    formData['description_english'] = CKEDITOR.instances.description_english.getData();

                    var that = this;
                    Pages.add(formData, function (msg) {
                        $('div.containers').append(`<div class="col-md-12"><h3>${formData['title_arabic']}</h3><div class="section-content">${formData['description_arabic']}</div></div>`);
                        $(that).trigger('reset');
                    });

                });


                $('a.deleteSection').click(function () {
                    var id = $(this).attr('data-id');
                    Pages.remove(id, function () {
                        $(`#${id}`).remove();
                    });
                    return false;
                });
            </script>

        @endif
    @endif
@endsection