@extends('master')

@section('title', "Thank You")

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <style>

        .thank_you_container{
            padding-top:120px;
            height: 30em;
            text-align: center;
        }
        h1{
            font-size: 40px;
            margin: 30px
        }

        .thank_you_container button{
            width:150px;
        }


    </style>
@endsection
@section('content')
   <div class="container thank_you_container">

       <h1>
           <i class="fa fa-check-circle" aria-hidden="true" style="color: green;font-size:50px"></i>
           <br/>
           {{trans('course.congrats')}}
       </h1>
       <p>{{trans('course.congrats_msg')}}</p>

       <br/>

       <div class="btn-group" role="group">
           <button type="button" class="btn btn-default"><a href='{{url("/" . Lang::getLocale() . "/profile")}}'> <i
                           class="fa fa-user"></i> {{ trans('course.myprofile') }} </a></button>
           <button type="button" class="btn btn-default"><a href='{{url("/" . Lang::getLocale() . "/courses")}}'>
               {{trans('course.music_courses_menu')}}
               <img
                       src='{{url("static/img/dots-icon.png")}}'></a></button>
       </div>
   </div>
@endsection

@section('footer')

@endsection