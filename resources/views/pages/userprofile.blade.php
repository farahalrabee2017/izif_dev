@extends('master')

@section('title', trans('course.profile_page_title'))
@section('description', trans('course.profile_page_description'))

@section('head')
    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
          rel="stylesheet"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .spinner {

            width: 50px;
            height: 40px;
            text-align: center;
            font-size: 10px;
            position: absolute;
            top: -30px;
            z-index: 99;
            left: 60px;
            display: none;
        }

        .spinner > div {
            background-color: #fff;
            height: 100%;
            width: 6px;
            display: inline-block;

            -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
            animation: sk-stretchdelay 1.2s infinite ease-in-out;
        }

        .spinner .rect2 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .spinner .rect3 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .spinner .rect4 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .spinner .rect5 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        @-webkit-keyframes sk-stretchdelay {
            0%, 40%, 100% {
                -webkit-transform: scaleY(0.4)
            }
            20% {
                -webkit-transform: scaleY(1.0)
            }
        }

        @keyframes sk-stretchdelay {
            0%, 40%, 100% {
                transform: scaleY(0.4);
                -webkit-transform: scaleY(0.4);
            }
            20% {
                transform: scaleY(1.0);
                -webkit-transform: scaleY(1.0);
            }
        }

        .level, .number_of_videos {
            font-weight: bold;
            font-size: 13px;
            text-align: center;
        }

        th {
            font-size: 11px !important;
        }


    </style>
@endsection


@section('content')
    <div class="container" id="margin_top_only2">
        <div class="profile-header-img">

        </div>
        <div class="col-md-2 height_100 col-md-push-10" style="background: #fff">
            <div class="profile-col" id="profile-col">
                <div class="profile-pic">

                    <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                    </div>
                    @if(Auth::user()->profile_image)
                        <img src="{{url('/uploads/'.Auth::user()->profile_image)}}" class="myprofilepic">
                    @else
                        <img src="{{url('static/img/profile-img.png')}}" class="myprofilepic">
                    @endif
                </div>
                <button class="camera_" id="myprofilecamera"><i class="fa fa-camera"></i></button>
            </div>
            <form class="profile-form">
                <ul>
                    <li>
                        <label>الاسم :</label>
                        <span class="editable" id="name" data-type="text"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/user/edit"
                              data-title="Name">

                                @if(Auth::user()->name == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else
                                {{Auth::user()->name}}
                            @endif
                        </span>
                    </li>

                    <li>
                        <label>العمر :</label>
                        <span class="editable-country-select" id="age" data-type="number"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/user/edit"
                              data-title="Age">

                                @if(Auth::user()->age == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else
                                {{Auth::user()->age}}
                            @endif
                        </span>
                    </li>
                    <li>
                        <label>البلد:</label>
                        <span class="editable-country-select" id="country_id" data-type="select"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/user/edit"
                              data-title="Country">

                        </span>

                    </li>
                    <li>
                        <label>الجنس :</label>
                          <span class="editable-gender-select" id="gender" data-type="select"
                                data-pk="{{Auth::user()->id}}"
                                data-url="/user/edit"
                                data-title="Gender">

                              @if(Auth::user()->gender == "")
                                  Edit <i class="fa fa-pencil-square"></i>
                              @else
                                  {{Auth::user()->gender}}
                              @endif

                          </span>
                    </li>
                    <li>
                        <label>الهاتف :</label>
                        <span class="editable" id="telephone" data-type="tel"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/user/edit"
                              data-title="Telephone">

                              @if(Auth::user()->telephone == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else
                                {{Auth::user()->telephone}}
                            @endif

                        </span>
                    </li>
                    <li>

                        <span class="editable" id="email" data-type="text"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/user/edit"
                              data-title="email" style="width:100%;text-align: center">

                              @if(Auth::user()->email == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else
                                {{Auth::user()->email}}
                            @endif

                        </span>
                    </li>
                </ul>
            </form>

            <div style="clear:both"></div>
            <hr/>

            <center><h2>{{trans('course.subscriptions')}}</h2></center>


            <br/>


            <table class="table table-striped subscriptions-table">
                <thead>
                <tr>


                    <th>{{trans('course.courses')}}</th>
                    <th>{{trans('course.end_date')}}</th>
                </tr>
                </thead>
                <tbody>


                @foreach($subscriptions as $subscription)

                    <tr>


                        <td>
                            @if($subscription->course_id == 0)
                                {{trans('course.allcourses')}}
                            @else

                                @if(Lang::getLocale() == "ar")
                                    {{$subscription->course->title_arabic}}
                                @else
                                    {{$subscription->course->title_english}}
                                @endif

                            @endif
                        </td>

                        <td class="subscription-date">{{$subscription->end_date}}</td>
                    </tr>
                @endforeach





                </tbody>
            </table>


        </div>
        <div class="col-md-10 col-md-pull-2">
            <div class="col-md-12">
                <div class="pof-menu">
                    <ul>
                        <!--
                         <li>حسابي</li>
                         <li>دوراتي</li>
                         <li>معلومات الدفع</li>
                         -->
                    </ul>
                </div>
            </div>
            <div class="col-md-12 profile-videos">
                <div class="row">

                    <div>

                        <!-- Nav tabs -->
                        <br/>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#courses" aria-controls="courses" role="tab"
                                                                      data-toggle="tab"><i
                                            class="fa fa-leanpub"></i> {{ trans('course.courses_am_learning') }}</a>
                            </li>


                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active row" style="padding:1em" id="courses">

                                @if(count($subscriptions))


                                    <div class="bs-callout bs-callout-info" id="callout-navs-tabs-plugin"
                                         style="background: #fff">
                                        <h4>{{trans('course.you_are_subscribed')}}</h4>
                                        <p>{{ trans('course.you_have_subscriptions') }}</p><br/>
                                        <a class="btn btn-primary btn-lg"
                                           href="{{url('/' . Lang::getLocale() . '/courses')}}">{{trans('course.music_courses_menu')}}</a>
                                    </div>
                                    <hr/>
                                    @endif

                                            <!-- List Of Started Courses -->
                                    @foreach ($courses as $course)


                                        <div class="videos-box_2 col-md-6 videos-profile">
                                            <a href="{{url('/' . Lang::getLocale() . "/course/$course->url_identifier")}}">
                                                <div class="title_video_2">
                            <span>
                                @if(Lang::getLocale() == "ar")
                                    {{$course->title_arabic}}
                                @else
                                    {{$course->title_english}}
                                @endif                            </span>
                                                </div>
                                                <div class="video-array_2">
                                                    <button><img src="{{url('static/img/video_icon.png')}}"></button>
                                                    @if($course->cover_image)
                                                        <img class="img_test_2"
                                                             src="{{url('/uploads/'.$course->cover_image)}}">
                                                    @else
                                                        <img class="img_test_2" src="{{url('static/img/m-1.png')}}">
                                                    @endif
                                                </div>
                                                <div class="dis_video_2">

                                                </div>
                                            </a>
                                        </div>


                                        @endforeach
                                                <!-- End List Of Started Courses -->


                            </div>


                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>







@endsection


@section('footer')




    <script type="text/javascript" src="{{url('/static/plupload-2.1.8/js/plupload.full.min.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script>

        $('.subscription-date').each(function () {

            moment.locale('ar');
            $(this).text(moment($(this).text()).fromNow());
        });

        // Enable Editables
        var $enableEdit = $('.editable');
        $enableEdit.editable({
            params: function (params) {
                // add custom param _token
                params._token = "{{csrf_token()}}";
                return params;
            }
        });

        var $editableCountrySelect = $('.editable-country-select');

        @if(Auth::user()->country_id)
            $editableCountrySelect.val('{{$country[Auth::user()->country_id]}}');
        @endif

        $editableCountrySelect.editable({
            params: function (params) {
                // add custom param _token
                params._token = "{{csrf_token()}}";
                return params;
            },
            value: 1,
            source: [
                <?php

                foreach ($country as $key => $val) {
                    $val = str_replace("'", "", $val);
                    print "{value: $key, text: '$val'},";
                }
                ?>


            ]
        });

        var $editableGenderSelect = $('.editable-gender-select');
        $editableGenderSelect.editable({
            params: function (params) {
                // add custom param _token
                params._token = "{{csrf_token()}}";
                return params;
            },
            value: 1,
            source: [
                {value: '', text: ''},
                {value: 'male', text: 'ذكر'},
                {value: 'female', text: 'انثى'}

            ]
        });


        // Upload Profile Picture
        var uploader = new plupload.Uploader({
            runtimes: 'html5,html4',

            browse_button: 'myprofilecamera', // you can pass in id...
            container: document.getElementById('profile-col'), // ... or DOM Element itself

            url: "{{url('/' . Lang::getLocale() . '/plupload')}}",
            unique_names: true,
            rename: true,
            dragdrop: true,
            // add X-CSRF-TOKEN in headers attribute to fix this issue
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            filters: {
                max_file_size: '10mb',
                mime_types: [
                    {title: "Image files", extensions: "jpg,gif,png"},

                ]
            },


            init: {
                PostInit: function () {


                },

                FilesAdded: function (up, files) {
                    /* plupload.each(files, function(file) {
                     document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                     });*/
                    $('.spinner').fadeIn();
                    uploader.start();

                },

                UploadProgress: function (up, file) {
                    console.log(file.percent);
                    //document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
                },

                Error: function (up, err) {
                    alert(err.message);
                },
                UploadComplete: function (up, file) {

                    $('.myprofilepic').attr('src', "/uploads/{{Auth::user()->id}}_" + file[0]['name']);
                    $('.spinner').fadeOut();
                }
            }
        });

        uploader.init();

    </script>

@endsection