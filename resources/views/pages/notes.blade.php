@extends('master')

@section('title', trans('course.music_sheets'))

@section('head')

    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">

    <style>
        .sheet {
            background: #eee;
            border: 1px solid #E2DFDF;
            border-radius: 10px;
            padding: 10px;
            margin-bottom: 5px;
        }

        .sheet-title {
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .sheet-footer {
            padding: 10px;
            border-top: 1px dotted #ccc;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .sheet-footer ul {
            display: inline;
            width: 100%;
        }

        .sheet-footer ul li {
            float: left;
            font-size: 12px;
            width: 50%;
            text-align: center;
            padding-top: 3px;
            padding-bottom: 3px;
        }

        .sheet-content {
            margin-top: 10px;
            border-top: 1px dotted #ccc;

        }
    </style>

@endsection
@section('content')


    <div class="container" style="margin-top: 10%;padding:10px">
        <div class="filters"></div>
        @foreach($notes as $note)
            <div class="col-md-3 mix" data-author="{{$note->author}}" data-type="{{$note->type}}"
                 data-year="{{$note->year}}">
                <div class="sheet">
                    @if($access)
                        <a href="{{url("/" . Lang::getLocale() . "/music/sheets/$note->id")}}">
                            @else
                                <a href="#" class="notes_not_subscribed">
                                    @endif
                                    <h3 class="sheet-title"><i class="fa fa-file-text"
                                                               aria-hidden="true"></i>
                                        @if(Lang::getLocale() == "ar")
                                            {{$note->title_arabic}}
                                        @else
                                            {{$note->title_english}}

                                        @endif
                                    </h3>
                                    <div class="sheet-content">
                                        <?php
                                        $path = explode('/', $note->note_path);
                                        ?>

                                        @if($path[0] == 'Sheet Music')
                                        <img width='100%' height='300'
                                             data-src="{{url("/sheets/" . str_replace('pdf','jpg',str_replace(' ','+',str_replace('Sheet Music/','',$note->note_path))))}}"/>
                                       @endif
                                            @if($path[0] == 'Humam Eid Notes')
                                        <img width='100%' height='300'
                                             data-src="{{url("/sheets/" . str_replace('pdf','jpg',str_replace(' ','+',str_replace('Humam Eid Notes/','',$note->note_path))))}}"/>
                                            @endif

                                            @if($path[0] == 'Courses PDF')
                                                <img width='100%' height='300'
                                                     data-src="{{url("/sheets/" . str_replace('pdf','jpg',str_replace(' ','+',str_replace('Courses PDF/','',$note->note_path))))}}"/>
                                            @endif

                                    </div>
                                    <div class="sheet-footer">
                                        <ul>
                                            <li><i class="fa fa-pencil" aria-hidden="true"></i> {{$note->author}}</li>
                                            <li><i class="fa fa-music" aria-hidden="true"></i> {{$note->type}}</li>
                                        </ul>
                                    </div>
                                </a>
                </div>
            </div>

        @endforeach
    </div>

@endsection

@section('footer')

    <script type="text/javascript" src="{{url('static/js/jquery.unveil.js')}}"></script>

    <script>
        $('.musicSheets').addClass('activeMenu');
        var $lazyLoad = $("img[data-src]");

        $lazyLoad.unveil(500, function () {
            $(this).load(function () {
                this.style.opacity = 1;
            });
        });


        $('img:lt(6)').trigger("unveil");


        $('.notes_not_subscribed').click(function () {

            swal({
                title: "{{trans('course.this_service_for_subscribed_users_only')}}",

                type: "warning",
                showCancelButton: true,
                showConfirmButton: true,
                cancelButtonText: "{{trans('course.cancel')}}",
                confirmButtonText: "{{trans('course.subscribe_now')}}"

            }, function () {
                window.location.href = $('li.subscribeMenu a').attr('href');
            });

        });
    </script>


@endsection