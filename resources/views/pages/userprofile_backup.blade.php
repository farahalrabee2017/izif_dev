@extends('master')

@section('head')
    <script src="//code.jquery.com/jquery-2.2.3.min.js"></script>

    <link rel="stylesheet" type="text/css" href="{{url('static/style/inner.css')}}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-formhelpers.css')}}">
    <script src="{{url('js/bootstrap-formhelpers.js')}}"></script>

    <style>
        .spinner {

            width: 50px;
            height: 40px;
            text-align: center;
            font-size: 10px;
            position: absolute;
            top: -30px;
            z-index: 99;
            left: 60px;
            display: none;
        }

        .spinner > div {
            background-color: #fff;
            height: 100%;
            width: 6px;
            display: inline-block;

            -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
            animation: sk-stretchdelay 1.2s infinite ease-in-out;
        }

        .spinner .rect2 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .spinner .rect3 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .spinner .rect4 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .spinner .rect5 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        @-webkit-keyframes sk-stretchdelay {
            0%, 40%, 100% {
                -webkit-transform: scaleY(0.4)
            }
            20% {
                -webkit-transform: scaleY(1.0)
            }
        }

        @keyframes sk-stretchdelay {
            0%, 40%, 100% {
                transform: scaleY(0.4);
                -webkit-transform: scaleY(0.4);
            }
            20% {
                transform: scaleY(1.0);
                -webkit-transform: scaleY(1.0);
            }
        }

        .level, .number_of_videos {
            font-weight: bold;
            font-size: 13px;
            text-align: center;
        }

        th {
            font-size: 11px !important;
        }
        div.editable-buttons
        {
            display: inherit!important;
        }
    </style>
@endsection


@section('content')
    <div class="container" id="margin_top_only2">
        {{--<div class="profile-header-img"--}}
             {{--style="background-image: url({{url('img/music-wallpaper16.jpg')}} ); z-index: -1 ;    opacity: 0.5;width: 97%;margin-right: 17px;float: right">--}}
        {{--</div>--}}
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href=""
                                                          aria-controls="courses" role="tab"
                                                          data-toggle="tab"><i
                                class="fa fa-leanpub"></i> {{trans('course.subscriptions')}}</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active row" style="padding:1em"
                     id="courses">

                    <table class="table table-striped subscriptions-table">
                        <thead>
                        <tr>


                            <th>{{trans('course.subscriptions')}}</th>
                            <th>{{trans('course.next_renew')}}</th>
                            <th>{{trans('course.courses')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subscriptions as $subscription)
                            <tr>
                                @if($subscription->course_id == 0)
                                    <td>
                                        <h4><b>
                                                {{trans('course.you_have')}}
                                                @if(Lang::getLocale() == "ar")
                                                    {{$subscription->subscriptions_model->title_ara}}
                                                    {{trans('course.in')}} {{trans('course.izif_website')}}
                                                @else
                                                    {{$subscription->subscriptions_model->title_enu}}
                                                @endif

                                            </b>
                                        </h4>
                                        <p style="font-size: 11px">
                                            {{trans('course.stuff_with_full_subscription')}}
                                        </p>

                                    </td>
                                    <td class="subscription-date">{{Carbon\Carbon::parse($subscription->end_date)->format('d/m/Y')}}</td>
                                    <td>
                                        <a href="{{url("/" . Lang::getLocale() . "/courses")}}">{{trans('course.allcourses')}}</a>
                                    </td>
                                @else
                                    <td>
                                        <h4><b>
                                                {{trans('course.you_bought_single_course')}} -

                                            </b></h4>

                                        <p style="font-size: 11px">
                                            {{trans('course.stuff_with_single_course')}}
                                        </p>

                                    </td>
                                    <td class="subscription-date">{{$subscription->end_date}}</td>
                                    <td>
                                        <a href='{{url("/" . Lang::getLocale() . "/course/" . $subscription->course->url_identifier)}}'>
                                            @if(Lang::getLocale() == "ar")
                                                {{$subscription->course->title_arabic}}
                                            @else
                                                {{$subscription->course->title_english}}
                                            @endif
                                        </a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        <br>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row" style="direction: rtl;">
                                <div class="col-md-12 lead">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <div class="profile-col" id="profile-col">
                                        <div class="profile-pic">
                                            <div class="spinner">
                                                <div class="rect1"></div>
                                                <div class="rect2"></div>
                                                <div class="rect3"></div>
                                                <div class="rect4"></div>
                                                <div class="rect5"></div>
                                            </div>
                                            @if(Auth::user()->profile_image)
                                                <img src="{{url('/uploads/'.Auth::user()->profile_image)}}" class="myprofilepic">
                                            @else
                                                <img src="{{url('static/img/profile-img.jpg')}}" class="myprofilepic">
                                            @endif
                                            <button class="camera_" id="myprofilecamera"><i class="fa fa-camera"></i></button>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    <label>الاسم :</label>
                        <span class="" id="name" data-type="text"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/{{Lang::getLocale()}}/user/edit"
                              data-title="Name">
                                @if(Auth::user()->name == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else
                                {{Auth::user()->name}}
                            @endif
                        </span>
                                                </li>
                                                <li>
                                                    <label>تاريخ الميلاد :</label>
                        <span class="editable" id="age" data-type="date"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/{{Lang::getLocale()}}/user/edit"
                              data-title="">
                                @if(Auth::user()->age == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else

                                <?php
                                if (Auth::user()->age)
                                {
                                    $age = Auth::user()->age;
                                }
                                ?>
                                <i class="fa fa-pencil-square"></i>
                                {{$age}}
                            @endif
                        </span>
                                                </li>
                                                <li>
                                                    <label>بلد الإقامة :</label>
                                                    <span class="fa fa-pencil-square"   id="click"></span>
                        <span class="editable-country-select" id="country_id" data-type="select" data-pk="{{Auth::user()->id}}" data-url="/{{Lang::getLocale()}}/user/edit" data-title="Country">

                        </span>

                                                </li>
                                                <li>
                                                    <label>الجنس :</label>
                          <span class="editable-gender-select" id="gender" data-type="select"
                                data-pk="{{Auth::user()->id}}"
                                data-url="/{{Lang::getLocale()}}/user/edit"
                                data-title="Gender">

                              @if(Auth::user()->gender == "")
                                  Edit <i class="fa fa-pencil-square"></i>
                              @else
                                  <i class="fa fa-pencil-square"></i>

                                  {{Auth::user()->gender}}
                              @endif

                          </span>
                                                </li>
                                                <li>


                                                                                                   <label>الهاتف :</label>

                        <span class="editable" id="telephone" data-type="tel"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/{{Lang::getLocale()}}/user/edit"
                              data-title="Telephone">
                              @if(Auth::user()->telephone == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else
                                <i class="fa fa-pencil-square"></i>
                                {{--<div class="bfh-selectbox bfh-countries" data-country="US" data-flags="true">--}}
                                {{--</div>--}}
                                {{Auth::user()->telephone}}
                            @endif

                        </span>
                                                </li>
                                                <li>
                                                    <label>الايميل :</label>
                        <span class="" id="email" data-type="text"
                              data-pk="{{Auth::user()->id}}"
                              data-url="/{{Lang::getLocale()}}/user/edit"
                              data-title="email" style="width:100%;text-align: center">

                              @if(Auth::user()->email == "")
                                Edit <i class="fa fa-pencil-square"></i>
                            @else

                                {{Auth::user()->email}}
                            @endif

                        </span>
                                                </li>
                                                {{--<li>--}}
                                                    {{--<label>آخر ظهور :</label>--}}
                                                    {{--{{$last_seen->created_at}}--}}
                                                {{--</li>--}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="">
            <div class="col-md-12">
                <div class="pof-menu">
                    <ul>
                        <!--
                         <li>حسابي</li>
                         <li>دوراتي</li>
                         <li>معلومات الدفع</li>
                         -->
                    </ul>
                </div>
            </div>
            <div class="col-md-12 profile-videos">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
@endsection
<style>

    body {
        background-color: #eaeaea;

    }

    img.myprofilepic {

        top: 2px !important;
        left: 20% !important;
        border-radius: 200px;
        width: 68% !important;
    }

    img.avatar {
        border: 1px solid #eee;

    }

    .col-md-4.text-center {
        float: right;
    }

    .only-bottom-margin {
        margin-top: 0px;
    }

    .activity-mini {
        padding-right: 15px;
        float: left;
    }

    div.col-md-6 {
        float: right;
        direction: rtl;
        top: 13px !important;

    }

    i.fa.fa-camera
    {
        margin-top: 87px;
        margin-left: 57px;

    }


    div.col-md-6 li {

        margin-bottom: 28px;
    }

    div.col-md-9 {

        width: 93%;
        float: right;
        margin-right: 32px;
    }

    ul.nav.nav-tabs {

        margin-top: 54px;
        margin-right: 46px;
    }

    th.prev
    {

        -webkit-transform: rotate(135deg)!important;
        border: solid black !important;
        border-width: 0px 3px 3px 0 !important;
        display: inline-block !important;
        padding: 6px !important;
        height:10px!important;
        width: 10px !important;
        height: 12px!important;
        margin-left: 6px!important;
    }
    th.next
    {

        -webkit-transform: rotate(-47deg)!important;
        border: solid black !important;
        border-width: 0px 3px 3px 0 !important;
        display: inline-block !important;
        padding: 6px !important;
        height:10px!important;
        width: 10px !important;
        height: 12px!important;
    }

</style>

@section('footer')

    <script type="text/javascript" src="{{url('/static/plupload-2.1.8/js/plupload.full.min.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script>

        /*$('.subscription-date').each(function () {

         moment.locale('ar');
         $(this).text(moment($(this).text()).fromNow());
         });*/

        // Enable Editables
        var $enableEdit = $('.editable');
        $enableEdit.editable({
            params: function (params) {
                // add custom param _token
                params._token = "{{csrf_token()}}";
                return params;
            }
        });

        var $editableCountrySelect = $('.editable-country-select');

        @if(Auth::user()->country_id)
            $editableCountrySelect.text('{{$country[Auth::user()->country_id]}}');
        @endif

        $editableCountrySelect.editable({
            params: function (params) {
                // add custom param _token
                params._token = "{{csrf_token()}}";
                return params;
            },
            value: 0,
            source: [
                <?php

                foreach ($country as $key => $val) {
                    $val = str_replace("'", "", $val);
                    print "{value: $key, text: '$val'},";
                }
                ?>
            ]
        });

        var $editableGenderSelect = $('.editable-gender-select');
        $editableGenderSelect.editable({
            params: function (params) {
                // add custom param _token
                params._token = "{{csrf_token()}}";
                return params;
            },
            value: 1,
            source: [
                {value: '', text: 'لا أفضل الإجابة'},
                {value: 'male', text: 'ذكر'},
                {value: 'female', text: 'انثى'}

            ]
        });
        
        // Upload Profile Picture
        var uploader = new plupload.Uploader({
            runtimes: 'html5,html4',

            browse_button: 'myprofilecamera', // you can pass in id...
            container: document.getElementById('profile-col'), // ... or DOM Element itself

            url: "{{url('/' . Lang::getLocale() . '/plupload')}}",
            unique_names: true,
            rename: true,
            dragdrop: true,
            // add X-CSRF-TOKEN in headers attribute to fix this issue
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            filters: {
                max_file_size: '10mb',
                mime_types: [
                    {title: "Image files", extensions: "jpg,gif,png"},

                ]
            },


            init: {
                PostInit: function () {


                },

                FilesAdded: function (up, files) {
                    /* plupload.each(files, function(file) {
                     document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                     });*/
                    $('.spinner').fadeIn();
                    uploader.start();

                },

                UploadProgress: function (up, file) {
                    console.log(file.percent);
                    //document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
                },

                Error: function (up, err) {
                    alert(err.message);
                },
                UploadComplete: function (up, file) {

                    $('.myprofilepic').attr('src', "/uploads/{{Auth::user()->id}}_" + file[0]['name']);
                    $('.spinner').fadeOut();
                }
            }
        });

        uploader.init();

        //this function to click the country field automaticly when click the pencil near it.
        $(document).on('click', '#click', function (e) {
            e.preventDefault();
            $('#country_id').trigger('click');
            return false;

        });


    </script>

@endsection