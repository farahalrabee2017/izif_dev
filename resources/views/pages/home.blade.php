@extends('master')

@section('title', trans('course.home_page_title'))
@section('description', trans('course.home_page_description'))

@section('head')
    <style>
        body {
            background: #000 !important;
        }
        .sa-custom{
            width:100% !important;
        }
    </style>
@endsection

@section('video')
    <video id="video_background" preload="auto" autoplay="" loop="">
        <source src="{{url('static/videos/Log-in-page-Video-1.mp4')}}" type="video/mp4">
    </video>
@endsection

@section('content')
    <div class="container">


        <div class="learning_music_block center-block  col-md-offset-5">
            <div class="title_learning_music">
                <span>
                    {{trans('course.learn_music_now')}}
                </span>
                <span>
                    {{trans('course.lessons_online')}}
                </span>
            </div>
            <div class="get_free_courses">
                <span>

                {{trans('course.start_learning_today')}}</span>
                <div class="group_button_get_free center-block">
                    <ul>
                        <li><a onclick="ga('send', 'event', '{{Lang::getLocale()}}-home-page', 'click', '{{Lang::getLocale()}}-start1');" style="display: block;width:100%;height: 100%;color:#000"
                               href="{{url("/" . Lang::getLocale() . "/" . trans("course.free_account_link"))}}">
                                {{trans('course.try_now')}}
                            </a></li>
                        <li><a onclick="ga('send', 'event', '{{Lang::getLocale()}}-home-page', 'click', '{{Lang::getLocale()}}-learn-more');" style="display: block;width:100%;height: 100%;color:#000" class="know_more"
                               href="#video_with_text">
                                {{trans('course.know_more')}}
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="banner_i3zef col-xs-12">
        <span>
            {{trans('course.easiest_way_to_learn_izif')}}
        </span>
    </div>
    <div id="video_with_text" class="container">

        <div class="video_with_text center-block col-xs-push-0 col-xs-pull-0">
            <div class="video_array">
                <video id="intro_video" controls width="100%" height="100%">

                    @if(Lang::getLocale() == "ar")
                        <source src="{{url('static/videos/I3zif.mp4')}}" type="video/mp4">
                    @else
                        <source src="{{url('static/videos/izifpromoEnglish.mp4')}}" type="video/mp4">
                    @endif


                </video>
            </div>
            <div class="text_array">

                <span>

                {{trans('course.under_video_text')}}
                </span>

            </div>
            <a onclick="ga('send', 'event', '{{Lang::getLocale()}}-home-page', 'click', '{{Lang::getLocale()}}-start2');" href="{{url("/" . Lang::getLocale() . '/intro')}}">
                <div class="button_test_here">
                    <span>
                        {{trans('course.try_now')}}
                    </span>
                </div>
            </a>
        </div>
    </div>


<!--<img src='http://localhost:5555/img/pop-up.jpg'>-->
    @if(!Agent::isMobile() AND !Agent::isTablet())
        @include('partials.testimonials')
    @elseif(Agent::isTablet())
        @include('partials.testimonials')
    @elseif(Agent::isMobile())
        @include('partials.mobiletestimonials')
    @endif

@endsection

@section('footer')
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script>
        // Know More button to play intro video
        var $knowMore = $('.know_more');

        $knowMore.click(function(){
            document.getElementById("intro_video").play();
            ga('send', 'event', '{{Lang::getLocale()}}-home-page', 'click', '{{Lang::getLocale()}}-homepagevideo-playbutton');

        });
    </script>
@endsection
