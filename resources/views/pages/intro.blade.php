@extends('master')

@section('title', trans('course.free_account_page_link'))
@section('description', trans('course.free_account_page_description'))

@section('head')
    <style type="text/css">
        body {
            background: url('/static/img/bg_izif.png') #000;
            background-repeat: no-repeat;
            background-size: 110%;
            background-position: left 50px;
        }
    </style>


@endsection
@section('content')


    <div class="container">
        <div class="learning_music_block center-block  col-md-offset-5">
            <div class="title_free_account">
                <span>
                    {{trans('course.create-free-account')}}
                </span>
            </div>
            <div class="enter_your_email">
                <span>
                    {{trans('course.intro_paragraph_email')}}
                    <strong>
                        {{trans('course.now')}}
                    </strong>
                </span>
                <div class="alert alert-danger emailerror" role="alert" style="display: none;width: 80%;margin: 0 auto;margin-top: 10px;">{{trans('course.invalid_email')}}</div>
                <form action="{{url("/" . Lang::getLocale() . "/")}}/start" method="POST"
                      class="form_your_emial center-block">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="lang" value="{{ Lang::getLocale() }}">
                    <input type="hidden" name="type" value="mail">
                    <input type="email" name="email" class="your_email " id="email" required/>
                    <input class="i3zef_button" type="submit" value="{{trans('course.izif')}}"/>
                </form>


                <h3 style="color: #fff;padding: 10px;font-weight: bold;display: none">
                    {{trans('course.or')}}
                </h3>

                <button  class="loginWithFacebookIntro" onclick="ga('send', 'event', 'free-account-pg', 'click', '{{Lang::getLocale()}}-fb-free-account');loginWithFacebook();"><i
                            class="fa fa-facebook-official"></i> {{trans('course.start_with_facebook')}}</button>


                <div class="copy_right_text">
                    <span>
                        {{trans('course.intro_bottom_paragraph')}}
                        <a href="{{url("/" . Lang::getLocale() . "/")}}/terms">
                            {{trans('course.conditions')}}

                        </a>
                        {{trans('course.izif_website')}}
                    </span>
                </div>

                <div id="status">
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    <script>


        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        var $introForm = $('input[type="submit"].i3zef_button');
        var alerted = false;

        $introForm.click(function (event) {

            event.preventDefault();
            if(validateEmail($('#email').val())) {
                try {
                    ga('send', {
                        hitType: 'event',
                        eventCategory: 'free-account-pg',
                        eventAction: 'click',
                        eventLabel: '{{Lang::getLocale()}}-izif-free-account'
                    });

                } catch (e) {
                    console.log(e);
                }




                swal({
                    title: "{{trans('course.thank_you')}}",
                    text: "{{trans('course.thank_you_msg')}}",
                    type: "success",
                    showCancelButton: false,
                    showConfirmButton: false
                });

                setTimeout(function () {

                    $('form.form_your_emial').submit();
                }, 6000);
            }else{
                event.preventDefault();
                $('.emailerror').show();
            }
        });
    </script>

    <script>

        // Load the SDK asynchronously
        /*window.fbAsyncInit = function() {
            FB.init({
                appId      : '359819794074863',
                xfbml      : true,
                version    : 'v2.6'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));*/


        window.fbAsyncInit = function() {
            FB.init({
                appId      : '430058543740438',
                xfbml      : true,
                version    : 'v2.8'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function loginWithFacebook() {

            //checkLoginState(function (status) {
                FB.getLoginStatus(function(status) {

                if (status.status != "connected") {
                    FB.login(function (response) {
                                console.log(response);
                                FB.api('/me?fields=id,email,name', function (response) {

                                    if (response.email) {

                                        var $form = $('.form_your_emial');
                                        $('.form_your_emial input[name="email"]').val(response.email);
                                        $('.form_your_emial input[name="type"]').val('facebook');
                                        $form.submit();
                                    }

                                });
                            },
                            {
                                scope: 'public_profile, user_friends, email',
                                return_scopes: true
                            }
                    );
                } else {

                    FB.api('/me?fields=id,email,name', function (response) {
                        var $form = $('.form_your_emial');
                        $('.form_your_emial input[name="email"]').val(response.email);
                        $form.submit();
                    });
                }

            });

        }
    </script>


@endsection