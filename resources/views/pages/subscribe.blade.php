@extends('master')

@section('title', trans('course.subscription_page_title'))
@section('description', trans('course.subscription_page_description'))

@section('head')
    <style type="text/css">

        * {
            margin: 0;
            padding: 0;
            font-family: 'i3zef';
        }

        body {
            background: #D6D6D6;
        }

        div {
            direction: rtl;
        }

        /*----------
        Blocks
        ----------*/
        /*Pricing table and price blocks*/
        .pricing_table {
            line-height: 150%;
            font-size: 12px;
            margin: 0 auto;
            padding-top: 10px;
            margin-top: 20px;
        }

        .price_block {
            text-align: center;
            width: 100%;
            color: #fff;
            float: left;
            list-style-type: none;
            transition: all 0.25s;
            position: relative;
            box-sizing: border-box;

            margin-bottom: 10px;
            border-bottom: 1px solid transparent;
        }

        /*Price heads*/
        .pricing_table_h3 {
            text-transform: uppercase;
            padding: 5px 0;
            background: #000;
            margin: -10px 0 1px 0;
            line-height: normal;
            font-size: 16px;
        }

        /*Price tags*/
        .price {
            display: table;
            background: #444;
            width: 100%;
            height: 70px;
        }

        .price_figure {
            font-size: 24px;
            text-transform: uppercase;
            vertical-align: middle;
            display: table-cell;
        }

        .price_number {
            font-weight: bold;
            display: block;
        }

        .price_tenure {
            font-size: 11px;
        }

        /*Features*/
        .features {
            background: #fff;
            color: #000;
            min-height: 196px;
        }

        .features li {
            padding: 15px 15px;
            border-bottom: 1px solid #ccc;
            font-size: 13px;
            list-style-type: none;
        }

        .footer {

            background: #fff;
        }

        .action_button {
            text-decoration: none;
            color: #444444;
            font-weight: bold;
            /* border-radius: 5px; */
            background: linear-gradient(#FCC624, #FCC624);
            /* padding: 15px 103px; */
            font-size: 11px;
            text-transform: uppercase;
            display: block;
            width: 100%;
            height: 46px;
            line-height: 44px;
            font-size: 15px
        }

        .price_block:hover {
            box-shadow: 0 0 10px 5px rgba(0, 0, 0, 0.5);
            /*transform: scale(1.04) translateY(-5px);*/
            /*z-index: 1;*/
            border-bottom: 0 none;
        }

        .price_block:hover .price {
            background-color: #FCC624;
        }

        .pricing_table_h3:hover {
            background: #222;
            font-size: 16px;
        }

        .price_block:hover .action_button {
            background: linear-gradient(#FCC624, #FCC624);
            text-decoration: none;

        }

        @media only screen and (min-width: 480px) and (max-width: 768px) {
            .price_block {
                width: 50%;
            }

            .price_block:nth-child(odd) {
                border-right: 1px solid transparent;
            }

            .price_block:nth-child(3) {
                clear: both;
            }

            .price_block:nth-child(odd):hover {
                border: 0 none;
            }
        }

        @media only screen and (min-width: 768px) {
            .price_block {
                width: 25%;
            }

            .price_block {
                border-right: 1px solid transparent;
                border-bottom: 0 none;
            }

            .price_block:last-child {
                border-right: 0 none;
            }

            .price_block:hover {
                border: 0 none;
            }

        }

        .skeleton, .skeleton ul, .skeleton li, .skeleton div, .skeleton h3, .skeleton span, .skeleton p {
            border: 5px solid rgba(255, 255, 255, 0.9);
            border-radius: 5px;
            margin: 7px !important;
            background: rgba(0, 0, 0, 0.05) !important;
            padding: 0 !important;
            text-align: left !important;
            display: block !important;

            width: auto !important;
            height: auto !important;

            font-size: 10px !important;
            font-style: italic !important;
            text-transform: none !important;
            font-weight: normal !important;
            color: black !important;
        }

        .skeleton .label {
            font-size: 11px !important;
            font-style: italic !important;
            text-transform: none !important;
            font-weight: normal !important;
            color: white !important;
            border: 0 none !important;
            padding: 5px !important;
            margin: 0 !important;
            float: none !important;
            text-align: left !important;
            text-shadow: 0 0 1px white;
            background: none !important;
        }

        .skeleton {
            display: none !important;
            margin: 100px !important;
            clear: both;
        }

        .leader_board {
            display: none !important;
        }

        .formcode {
            width: 100%;
            margin: 0 auto;
            font-size: 10px;
        }

        .formcode label {
            float: right;
            width: 70px;
            background-color: #efefef;
            text-align: center;
            line-height: normal;
            height: 30px;
            font-size: 13px;
            padding-top: 3px;
            color: #000;
            width: 42%;

        }

        .formcode input[type="text"] {
            height: 30px;
            padding: 0;
            margin: 0;
            border-style: none;
            width: 40%;
        }

        .formcode input[type="submit"] {
            width: 17%;
            height: 30px;
            border-style: none;
            text-align: center;
            line-height: normal;
            background-color: #FCC624;
            color: #000;
            font-size: 13px;
        }

        .main-container {
            margin-top: 150px;
        }

        .add_subscription {
            direction: ltr !important;
            margin-bottom: 20px;

        }

        .delete-subscription {
            font-size: 12px;
            margin: 13px;
            color: #000;
        }

        .editarea {
            border: 1px dotted #ccc;
            width: 80%;
            margin: 0 auto;
            padding: 10px;
            background: #ccc;
        }

        .addnewfeaturemsg {
            font-size: 9px;
        }

        .delete-subscription-feature {
            float: left;
        }

        .activeMenu {
            color: maroon;
        }

        .dropdown-custom {
            min-width: 21em;
            font-size: 14px;
        }

        button.currency {
            width: 32em;
            border-radius: 0px;
            height: 30px
        }

        span.currencyspan {
            float: left;
            /* width: 70px; */
            background-color: #efefef;
            text-align: center;
            line-height: normal;
            height: 30px;
            font-size: 13px;
            padding-top: 2px;
            color: #000;
            width: 94%;
        }

        span.green {
            color: #4AA24C !important
        }

        .introjs-helperLayer{
            z-index:9999!important;
        }

    </style>

    <!-- Add IntroJs styles -->
    <link href="{{url('/static/intro.js-2.0.0/introjs.css')}}" rel="stylesheet">
    <!-- Add IntroJs RTL styles -->
    <link href="{{url('/static/intro.js-2.0.0/introjs-rtl.css')}}" rel="stylesheet">
@endsection


@section('content')

    <div class="container main-container">
        <div class="row">
            @if($code)
                @if($discount)
                    <div class="col-md-12">
                        <div class="alert alert-success" role="alert"><i class="fa fa-check-circle"
                                                                         aria-hidden="true"></i> {{trans('course.discount_activated')}}
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert"><i class="fa fa-times-circle"
                                                                        aria-hidden="true"></i> {{trans('course.discount_expired')}}
                        </div>
                    </div>
                @endif
            @else
                @if($wrong_code)

                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert"><i class="fa fa-times-circle"
                                                                        aria-hidden="true"></i> {{trans('course.discount_code_wrong')}}
                        </div>
                    </div>
                @endif
            @endif


            <div class="col-md-6">
                <div class="formcode">
                    <form id="codes" method="GET" action="{{url('/' . Lang::getLocale() . '/subscribe' )}}">
                        {!! csrf_field() !!}
                        <label>
                            {{trans('course.discount_code')}}
                        </label>

                        <input name="code" type="text"/>
                        <input type="submit" value="{{trans('course.enter')}}"/>
                    </form>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="row">

                    <div class="col-md-6" style="padding: 0px;">
                        <div class="btn-group">
                            <button type="button"
                                    class=" currency btn btn-custom btn-custom-en btn-default dropdown-toggle"
                                    data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false"><img src='{{url("static/img/usd.png")}}'
                                                                                    width="17px"/> USD<span
                                        class="caret caret-custom"></span></button>
                            <ul class="dropdown-menu dropdown-custom">

                                <li><a href="#"><img src='{{url("static/img/usd.png")}}' width="17px"/> USD</a></li>
                                <li><a href="#"><img src='{{url("static/img/sar.png")}}' width="17px"/> SAR</a></li>
                                <li><a href="#"><img src='{{url("static/img/egp.png")}}' width="17px"/> EGP</a></li>
                                <li><a href="#"><img src='{{url("static/img/jod.png")}}' width="17px"/> JOD</a></li>
                                <li><a href="#"><img src='{{url("static/img/aed.png")}}' width="17px"/> AED</a></li>
                                <li><a href="#"><img src='{{url("static/img/euro.png")}}' width="17px"/> EUR</a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding: 0px;"><span
                                class="currencyspan">{{trans('course.currency')}}</span></div>
                </div>

            </div>
            <div class="col-md-12">

                <ul class="pricing_table">


                    @foreach($subscription_models as $subscription_model)


                        <li class="price_block">

                            @if(!Auth::Guest())
                                @if(Auth::user()->user_group_id == 2)
                                    <button data-id="{{$subscription_model->id}}"
                                            class="btn btn-default delete-subscription withloader"><i
                                                class="fa fa-trash"></i>
                                    </button>
                                @endif
                            @endif

                            <h3 class="pricing_table_h3">

                                @if(count($course) > 0 && $subscription_model->display_price == "")

                                    @if(Lang::getLocale() == "ar")
                                        {{$course->title_arabic}}
                                    @else
                                        {{$course->title_english}}
                                    @endif
                                @else
                                    @if(Lang::getLocale() == "ar")
                                        {{$subscription_model->title_ara}}
                                    @else
                                        {{$subscription_model->title_enu}}
                                    @endif
                                @endif

                            </h3>
                            <div class="price">
                                <div class="price_figure">

                                    <span class="price_number" data-currency="USD">
                                        @if($subscription_model->display_price == "")

                                            @if(count($course) > 0)

                                                @if($code and $discount)


                                                    @if(!strrpos($coupon->subscription_types, $subscription_model->id) === false || $coupon->subscription_types == "14" AND  $subscription_model->id == "14")

                                                        @if($coupon->discount_type == "percentage")
                                                            <span class="green">   ${{$course->price - ceil(floatval(($course->price * $coupon->amount) / 100))}} </span>
                                                        @else
                                                            <span class="green">  ${{ceil(floatval($course->price - $coupon->amount))}} </span>
                                                        @endif
                                                    @else
                                                        ${{$course->price}}
                                                    @endif

                                                @else
                                                    ${{$course->price}}
                                                @endif
                                            @else
                                                <a style="color: #fff;"
                                                   href='{{url("/" . Lang::getLocale() . "/courses")}}'>
                                                    {{trans('course.music_courses')}}
                                                </a>
                                            @endif

                                        @else

                                            @if($code and $discount)

                                                @if(!(strrpos(strval($coupon->subscription_types), strval($subscription_model->id)) === false))

                                                    @if($coupon->discount_type == "percentage")

                                                        <span class="green"> ${{$subscription_model->display_price - ceil(floatval(($subscription_model->display_price * $coupon->amount) / 100))}} </span>
                                                    @else
                                                        <span class="green">  ${{ceil(floatval($subscription_model->display_price - $coupon->amount))}} </span>
                                                    @endif
                                                @else

                                                    ${{$subscription_model->display_price}}
                                                @endif

                                            @else

                                                ${{$subscription_model->display_price}}
                                            @endif


                                        @endif
                                    </span>

                                </div>
                            </div>
                            <ul class="features">
                                @if(!Auth::Guest())
                                    @if(Auth::user()->user_group_id == 2)
                                        <br/>
                                        <div class="editarea">

                                            <span class="addnewfeaturemsg">Add a new feature to this model</span>
                                            {!! Form::open( array('route' => array('subscriptionFeature.create'))) !!}

                                            <div class="form-group">
                                                {!! Form::text('title_ara', null , ['class' => 'form-control','placeholder'=>'Arabic Feature']) !!}
                                            </div>

                                            <div class="form-group">
                                                {!! Form::text('title_enu', null , ['class' => 'form-control','placeholder'=>'English Feature']) !!}
                                            </div>

                                            <input type="hidden" name="subscription_model_id"
                                                   value="{{$subscription_model->id}}"/>

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-default withloader"><i
                                                            class="fa fa-plus-circle "></i>
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    @endif
                                @endif
                                @foreach($subscription_model->SubscriptionModelFeatures as $feature)
                                    <li>
                                        <span id="{{$feature->id}}" data-type="text"
                                              data-pk="{{$subscription_model->id}}"
                                              data-url="/feature/edit"
                                              data-_token="test"
                                              data-title="{{$feature->title_ara}}">

                                            @if(Lang::getLocale() == "ar")
                                                @if($subscription_model->display_price == "")
                                                    {{str_replace($tags,$coursetagvalues,$feature->title_ara)}}
                                                @else
                                                    {{str_replace($tags,$tagvalues,$feature->title_ara)}}
                                                @endif
                                            @else

                                                @if($feature->title_enu == "")
                                                    no text
                                                @else
                                                    @if($subscription_model->display_price == "")
                                                        {{str_replace($tags,$coursetagvalues,$feature->title_enu)}}
                                                    @else
                                                        {{str_replace($tags,$tagvalues,$feature->title_enu)}}
                                                    @endif

                                                @endif

                                            @endif

                                        </span>
                                        @if(!Auth::Guest())
                                            @if(Auth::user()->user_group_id == 2)
                                                <button data-id="{{$feature->id}}"
                                                        class="btn btn-default btn-sm delete-subscription-feature withloader">
                                                    <i
                                                            class="fa fa-times-circle-o"></i></button>
                                            @endif
                                        @endif
                                    </li>
                                @endforeach

                            </ul>
                            <div class="footer">
                                @if($code and $discount and $coupon->amount == "100" and $coupon->discount_type == "percentage")
                                    <form method="POST" action="{{url('/' . Lang::getLocale() . '/fulldiscount' )}}">
                                        <input type="hidden" name="code" value="{{$code}}"/>

                                        @if(count($course) > 0)
                                            <input type="hidden" name="course" value="{{$course->id}}"/>
                                        @else
                                            <input type="hidden" name="course" value="0"/>
                                        @endif
                                        <input type="hidden" name="subscription_model"
                                               value="{{$subscription_model->id}}"/>

                                        <input type="submit" value="{{ trans('course.subscribe_now') }}"
                                               class="action_button withloader">
                                        </input>
                                    </form>
                                @else
                                    @if($subscription_model->display_price == "")

                                        @if(count($course) > 0)

                                            <form method="POST"
                                                  action="{{url('/' . Lang::getLocale() . '/checkout' )}}">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="subscription_model"
                                                       value="{{$subscription_model->id}}"/>
                                                <input type="hidden" name="course" value="{{$course->id}}"/>
                                                <input type="hidden" name="code"
                                                       value="@if($code) @if(!(strrpos($coupon->subscription_types, $subscription_model->id) === false)){{$code}} @endif @endif"/>
                                                <input type="submit" value="{{ trans('course.subscribe_now') }}"
                                                       class="action_button withloader">
                                                </input>
                                            </form>

                                        @else
                                            <a href="{{url('/' . Lang::getLocale() . '/courses' )}}"
                                               class="select_course action_button withloader">{{ trans('course.subscribe_now') }}</a>
                                        @endif

                                    @else
                                        <form method="POST" action="{{url('/' . Lang::getLocale() . '/' . trans('course.payment_methods_link') )}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="subscription_model"
                                                   value="{{$subscription_model->id}}"/>
                                            <input type="hidden" name="course" value="0"/>
                                            <input type="hidden" name="code"
                                                   value="@if($code) @if(!(strrpos($coupon->subscription_types, $subscription_model->id) === false)){{$code}} @endif @endif"/>
                                            <input type="submit" value="{{ trans('course.subscribe_now') }}"
                                                   class="action_button withloader">
                                            </input>
                                        </form>

                                    @endif
                                @endif
                            </div>
                        </li>



                    @endforeach

                </ul>


                <ul class="skeleton pricing_table" style="margin-top: 100px; overflow: hidden;">
                    <li class="label" style="margin:0px;">ul.pricing_table</li>
                    <li class="price_block">
                        <span class="label">li.price_block</span>
                        <h3><span class="label">h3</span></h3>
                        <div class="price">
                            <span class="label">div.price</span>
                            <div class="price_figure">
                                <span class="label">div.price_figure</span>
<span class="price_number">
<span class="label">span.price_number</span>
</span>
<span class="price_tenure">
<span class="label">span.price_tenure</span>
</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li class="label">ul.features</li>
                            <br><br><br>
                        </ul>
                        <div class="footer">
                            <span class="label">div.footer</span>
                        </div>
                    </li>


                    <li class="price_block" style="opacity: 0.5;">
                        <span class="label">li.price_block</span>
                        <h3><span class="label">h3</span></h3>
                        <div class="price">
                            <span class="label">div.price</span>
                            <div class="price_figure">
                                <span class="label">div.price_figure</span>
<span class="price_number">
<span class="label">span.price_number</span>
</span>
<span class="price_tenure">
<span class="label">span.price_tenure</span>
</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li class="label">ul.features</li>
                            <br><br><br>
                        </ul>
                        <div class="footer">
                            <span class="label">div.footer</span>
                        </div>
                    </li>
                    <li class="price_block" style="opacity: 0.25;">
                        <span class="label">li.price_block</span>
                        <h3><span class="label">h3</span></h3>
                        <div class="price">
                            <span class="label">div.price</span>
                            <div class="price_figure">
                                <span class="label">div.price_figure</span>
<span class="price_number">
<span class="label">span.price_number</span>
</span>
<span class="price_tenure">
<span class="label">span.price_tenure</span>
</span>
                            </div>
                        </div>
                        <ul class="features">
                            <li class="label">ul.features</li>
                            <br><br><br>
                        </ul>
                        <div class="footer">
                            <span class="label">div.footer</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <br/>
    <br/>

    <!-- Modal -->
    <div class="modal fade" id="addSubscriptionModal" tabindex="-1" role="dialog"
         aria-labelledby="addSubscriptionModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Subsctiption Model</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( array('route' => array('subscription.create'))) !!}

                    <div class="form-group">
                        {!! Form::label('title_ara', 'Arabic Title') !!}
                        {!! Form::text('title_ara', null , ['class' => 'form-control','placeholder'=>'Arabic Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title_enu', 'English Title') !!}
                        {!! Form::text('title_enu', null , ['class' => 'form-control','placeholder'=>'English Title']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('display_price', 'Display Price') !!}
                        {!! Form::input('number','display_price', null , ['class' => 'form-control','placeholder'=>'Display Price']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}
                        {!! Form::input('number','price', null , ['class' => 'form-control','placeholder'=>'Price']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('time_period_in_days', 'Time Period In Days') !!}
                        {!! Form::input('number','time_period_in_days', null , ['class' => 'form-control','placeholder'=>'Time Period In Days']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_full_access', 'Full Site Access') !!}
                        {!! Form::select('is_full_access',array('1'=>'yes','0'=>'no'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_open_time_period', 'Open Time Period') !!}
                        {!! Form::select('is_open_time_period',array('1'=>'yes','0'=>'no'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('is_premium', 'Premium Subscription') !!}
                        {!! Form::select('is_premium',array('1'=>'yes','0'=>'no'),null,['class'=>'form-control']) !!}
                    </div>

                    <div class="box-footer">
                        <center>
                            <button type="submit" class="btn btn-primary withloader">Save</button>
                        </center>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')


    @include('partials.editable')
    <script src="{{url('/static/js/prefixfree.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('/static/intro.js-2.0.0/intro.js')}}"></script>

    <script>

        var course_id = "{{$_GET['c'] or ''}}";
        var coupon_types = "@if($coupon){{$coupon->subscription_types}}@endif";

        if(coupon_types == '14' && course_id == ''){

            $($('.select_course').parent().parent().find('.price_figure')).attr("data-intro","{{trans('course.select_course')}}");
            introJs().setOptions({
                'nextLabel': "{{trans('course.next')}}",
                'prevLabel': "{{trans('course.prevLabel')}}",
                'skipLabel': "{{trans('course.skipLabel')}}",
                'doneLabel': "{{trans('course.doneLabel')}}"
            }).start();
        }
        if (course_id) {
            $('form#codes').append('<input type="hidden" name="c" value="' + course_id + '"/>');
        }



        $('.subscribeMenu').addClass('activeMenu');

        var $currencyLinks = $('ul.dropdown-custom a');
        $currencyLinks.click(function () {
            $('button.currency').html($(this).html() + '<span class="caret caret-custom"></span>');
            $to = $('button.currency').text().replace(' ', '');
            $url = "{{url('/' . Lang::getLocale() . '/changerate/' )}}/" + $('li.price_block .price_number[data-currency]').first().attr('data-currency') + "/" + $('button.currency').text().replace(' ', '');
            console.log($url);

            var request = $.ajax({
                url: $url,
                method: "POST"

            });

            request.done(function (rate) {

                $('li.price_block .price_number[data-currency]').each(function () {
                    var price = $(this).text().replace(/[^\w\s]/gi, '').replace(/[^0-9]/g, '').replace(/ /g, '');
                    if(parseInt(price)>0){
                    console.log(price);
                    newprice = parseInt(price) * rate;
                    $(this).text($to + " " + Math.ceil(newprice));
                    $(this).attr('data-currency', $to);
                }
                });

            });

            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });


        });
    </script>

@endsection
