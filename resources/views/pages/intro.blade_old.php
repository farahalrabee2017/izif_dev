
@extends('master')

@section('video')
    <video id="video_background" preload="auto" autoplay="" loop="" >
        <source src="{{url('static/videos/Log-in-page-Video-1.mp4')}}" type="video/mp4">
    </video>
@endsection

@section('content')


    <div class="container">
        <div class="learning_music_block center-block  col-md-offset-5">
            <div class="title_free_account">
                <span>إنشاء حساب مجاني</span>
            </div>
            <div class="enter_your_email">
                <span>ادخل بريدك الإلكتروني لتبداء مجاناً <strong>الآن</strong></span>

                    <form action="/ar/start" method="POST" class="form_your_emial center-block">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="email" name="email" class="your_email " id="email" required/>
                        <input class="i3zef_button" type="submit" value="إعزف"/>
                    </form>


                <div class="copy_right_text">
                    <span>بالضغط على الزر اعلاه اوافق على <a href="">شروط واحكام </a>موقع إعزف</span>
                </div>
            </div>
        </div>
    </div>

@endsection