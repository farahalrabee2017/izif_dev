<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
        <!--<script src="https://www.2checkout.com/static/checkout/javascript/direct.min.js"></script>-->
    </head>
    <body>
        <div class="container">
            <div class="content">

                <form action='https://www.2checkout.com/checkout/purchase' method='post'>
                    <input type='hidden' name='sid' value='202356302' />
                    <input type='hidden' name='mode' value='2CO' />
                    <input type='hidden' name='li_0_type' value='product' />
                    <input type='hidden' name='li_0_name' value='Monthly Subscription' />
                    <input type='hidden' name='li_0_price' value='1.00' />
                    <input type='hidden' name='li_0_recurrence' value='1 Month' />
                    <input type='hidden' name='currency_code' value='USD' />

                    <input name='submit' type='submit' value='Checkout' />
                </form>

                <div class="title">Laravel 5</div>
            </div>
        </div>

    </body>
</html>
