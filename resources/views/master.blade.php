<!doctype html>
<html>
<head>

    <title>@yield('title','Izif | Music School')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" type="image/x-icon" href='{{url("/static/img/izif-logo.ico")}}'/>

    <meta property="og:url" content="@yield('FacebookUrl','http://izif.com')"/>
    <meta property="og:title" content="@yield('facebookTitle','izif | The Online Music School')"/>
    <meta property="og:description" content="@yield('FacebookDescription','http://izif.com')"/>
    <meta property="og:image" content="@yield('FacebookImage',url("static/img/izif-logo.png"))"/>

    <meta name="description" content="@yield('description','Izif | Music School')">
    <meta name="author" content="izif.com">


    <link href='{{url("static/bootstrap-3.3.6-dist/css/bootstrap.css")}}' rel="stylesheet">
    <link rel="stylesheet" href='{{url("static/font-awesome-4.5.0/css/font-awesome.min.css")}}'>
    <link rel="stylesheet" type="text/css" href='{{url("static/style/main.css")}}'>
    <link rel="stylesheet" type="text/css" href="{{url('static/sweetalert-master/dist/sweetalert.css')}}">

    @include('partials.crossLanguageCss')

    <script>

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {

                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),

                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)

        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');


        ga('create', 'UA-77916487-1', 'auto');

        ga('send', 'pageview');


    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KPQV2JV');</script>
    <!-- End Google Tag Manager -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '596503287100689');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=596503287100689&ev=PageView&noscript=1"
        /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    @yield('head')
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->



</head>

<body style="max-width: 100%;">

@if(!Agent::isMobile() AND !Agent::isTablet())
    @yield('video')
    @include('partials.menu.desktop')

@elseif(Agent::isTablet())
    @include('partials.menu.desktop')
@elseif(Agent::isMobile())

    @include('partials.menu.mobile')

@endif

@yield('content')


<footer class="footer">


    @if(!Agent::isMobile() AND !Agent::isTablet())
        @include('partials.footer')

    @elseif(Agent::isTablet())
        @include('partials.footer')
    @elseif(Agent::isMobile())

        @include('partials.mobilefooter')

    @endif

<center>
    <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=3oIycycLAL7DuJTqHEQiY0sMONbVM5ox29WHBJhWaLCZI87LZDsZRb6a7IrR"></script></span>
</center>
</footer>
<script type="text/javascript" src='{{url("static/js/jquery-1.11.3.min.js")}}'></script>
<script type="text/javascript" src='{{url("static/bootstrap-3.3.6-dist/js/bootstrap.min.js")}}'></script>
<script type="text/javascript" src='{{url("static/js/classie.js")}}'></script>
<script type="text/javascript" src='{{url("static/js/main.js")}}'></script>
<script type="text/javascript" src="{{url('static/sweetalert-master/dist/sweetalert.min.js')}}"></script>
@yield('footer')

<div style="clear:both"></div>
<div style="display: none;position: absolute;top:0px;left: 0px;background: #ECF0F1;width:100%;height: 150%"
     class="preloader">
    <center><img src="#http://www.averaraonline.com.br/estilos/loja/loja_front_averara/images/preload.gif"/></center>
    <div style="clear:both"></div>
</div>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KPQV2JV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body>
</html>