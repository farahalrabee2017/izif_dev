/**
 * Created by halnator on 1/29/16.
 */


var Subscriptions = new Object();

Subscriptions.prototype.deleteSubscription = function (id) {
    $.ajax({
            method: "POST",
            url: "{{url('/delete')}}",
            data: {
                _token: "{{csrf_token()}}",
                id: id
            }
        })
        .done(function( msg ) {
            window.location.reload();
        });
};

var $container = $('.main-container');


var buttons_container = [

    '<center>',
    '<button type="button" data-toggle="modal" data-target="#addSubscriptionModal" class="add_subscription btn btn-default"><i class="fa fa-plus-circle"></i> Add New Subscription Model</a></button>',
    '</center>'
];

$container.prepend(buttons_container.join(''));


// Enable Editables
var $enableEdit = $('.features li');
$enableEdit.editable();


// Delete Subscription

var $deleteSubscription = $('.delete-subscription');
var subscriptions = new Subscriptions();

$deleteSubscription.click(function(){
    subscriptions.deleteSubscription();
});

