<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{

    public $timestamps = true;

    protected $fillable = [
        'arabic_name',
        'english_name',
        'icon',
        'big_image'
    ];
}
