<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class email_temp extends Model {
    protected $table='email_temp';
    protected $fillable=['html_code','arabic_title','english_title','category_id'];
}
