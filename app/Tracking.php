<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table = "traking";

    protected $fillable = [
        "utm_source",
        "utm_medium",
        "utm_campaign",
        "url",
        "ip",
        "user_id"
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function subscription(){
        return $this->hasMany('App\Subscription', 'user_id', 'user_id');
    }
}
