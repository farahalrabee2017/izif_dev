<?php

namespace App\Http\Middleware;

use Closure;


class LocalizationMiddleware
{
    /**
     * Handle an incoming request with localization and course prefix detection from old website links.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        // Get Language Segment From URL
        $locale = $request->segment(1);

        // Check if this segment matches a Course Prefix From the old website
        $course = \App\Course:: where('old_website_prefix',$request->segment(2))
            ->count();


        // Check Localization to be ar or en
        if ($locale == "") {
            $locale = "ar";
        } else if ($locale != "ar" && $locale != "en") {
            $locale = "ar";
        }

        // Reset Localization
        app()->setLocale($locale);


        // redirect if course exists else move to next request
        if ($course == 0) {

            return $next($request);

        }else{

            return redirect('/'.$locale . '/course/'.$request->segment(2));

        }
    }
}
