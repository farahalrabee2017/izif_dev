<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\subscription;
use App\SubscriptionModel;
use Carbon\Carbon;
use Cache;

class IsSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::Check()){

            // Get subscriptions

            $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;
            $courses_subscriptions = subscription::where('user_id', Auth::user()->id)->where('subscription_model_id', '==', $limitedAccessSubscriptionId)->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();


            $subscriptions = subscription::where('user_id',Auth::user()->id)->with('subscriptions_model')->get();
            $request->attributes->add(['subscriptions' => $subscriptions,'course_subscriptions'=>$courses_subscriptions]);
        }
        return $next($request);
    }
}
