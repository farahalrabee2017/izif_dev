<?php

namespace App\Http\Middleware;

use Closure;
use App\Tracking;
use Auth;
use Carbon\Carbon;

class TrackingMiddleware
{
    /**
     * Handle an incoming tracking request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function get_client_ip_server() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }


    public function handle($request, Closure $next)
    {

        //var_dump(session('track_id'));

        $tracking_keyword = $request->input("track");


        if ($tracking_keyword == "1") {

            $utm_source = $request->input("utm_source");
            $utm_medium = $request->input("utm_medium");
            $utm_campaign = $request->input("utm_campaign");
            $url = $request->url();
            $ip = $this->get_client_ip_server();
            $now = Carbon::now();
            $now =  $now->toDateTimeString();

            $tracking = Tracking::firstOrCreate([
                "utm_source" => $utm_source,
                "utm_medium" => $utm_medium,
                "utm_campaign" => $utm_campaign,
                "url" => $url,
                "ip" => $ip,
                "user_id" => Auth::guest() == false ? Auth::user()->id : "anonymous",
                "real_user_id" => Auth::guest() == false ? Auth::user()->id : null,
                "created_at" => $now
            ]);

            session(['track_id' => $tracking->id]);
            /*

            $tracking = new Tracking;

            $tracking->utm_source = $utm_source;
            $tracking->utm_medium = $utm_medium;
            $tracking->utm_campaign = $utm_campaign;
            $tracking->url = $url;
            $tracking->ip = $ip;
            $tracking->user_id = Auth::guest() == false ? Auth::user()->id : "anonymous";

            $tracking->save();

            */

        }

        return $next($request);
    }
}
