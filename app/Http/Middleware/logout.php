<?php

namespace App\Http\Middleware;

use App\Session;
use Closure;
use Auth;
use DB;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
class logout
{
    protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    } /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $this->auth->user()->id;
        $user = Session::where('user_id', '=', $id)->first();
        if (!empty($user))
        {
            $now = new \DateTime();
            DB::table('session')
                ->where('user_id', $id)
                ->update(['logged_in' => "0",'updated_at' => $now]);
        }

        return $next($request);
    }
}
