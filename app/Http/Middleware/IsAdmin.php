<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Only Allow Admin To Access the request or unauthorized
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::Guest()){

            abort(403, 'Unauthorized action.');

        }else{

            // Check if Admin
            if(Auth::user()->user_group_id != 2){

                abort(403, 'Unauthorized action.');
            }
        }
        return $next($request);
    }
}
