<?php

namespace App\Http\Middleware;
use App\Session;
use Closure;
use DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
class addSession
{
    protected $redirectPath = '/ar/profile';
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
        $id = $this->auth->user()->id;
        $user = Session::where('user_id', '=', $id)->first();
            if (!empty($user))
            {
                $now = new \DateTime();
                DB::table('session')
                    ->where('user_id', $id)
                    ->update(['logged_in' => "1",'created_at' => $now]);
            }
            else
            {
            $user = Session::create([
                'user_id' => "$id",
                'logged_in' => "1",

            ]);
        }
        return $next($request);
    }
}
