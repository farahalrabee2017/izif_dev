<?php

namespace App\Http\Controllers;

use App\Downloadable;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;
use App\Course;
use App\CourseStatus;
use App\VideoStatus;
use Auth;
use \Aws\S3\S3Client;
use \Aws\CloudFront\CloudFrontClient;
use App\Comments;
use App\Instrument;
use App\subscription;
use App\SubscriptionModel;
use Lang;
use Mail;
use Carbon\Carbon;
use Cache;
use App;


class VideoController extends Controller
{

    public function index($lang, $instrument, $courseslug, $videoslug)
    {

        $lessonsUrl = "/course/$instrument/$courseslug";
        $course = Course::where('url_identifier', $instrument . '/' . $courseslug)
            ->with('videos', 'teacher', 'instrument', 'completedVideos')
            ->with(['videoStatus' => function ($query) {
                if (Auth::check()) {
                    $query->where('user_id', Auth::user()->id);
                } else {
                    $query->where('user_id', 0);
                }
            }])
            ->first();


            $course_lang = $course->language == '1' ? 'ar' : 'en';

            if($course_lang != $lang){

                 return App::abort(404, 'Page not found');
            }


        $video = Video::where('course_id', $course->id)
            ->where('url_identifier', $videoslug)
            ->first();

        $downloadables = Downloadable::where('video_id', $video->id)->get();

        $comments = Comments::where('video_id', $video->id)->get();

        // values for videolist.partials
        $user_id = Auth::Guest() ? 0 : Auth::user()->id;
        $course_status = CourseStatus::where(['course_id' => $course->id, 'user_id' => $user_id])->get();
        $course_status = count($course_status) > 0 ? true : false;
        $completed_videos = count($course->completedVideos);
        $not_completed_videos = abs($completed_videos - count($course->videoStatus));
        $not_completed_precent = 0.5 * $not_completed_videos;
        $completed = floor(($completed_videos + $not_completed_precent) / count($course->videos) * 100);

        // Next And Previous Videos
        $current_order = intval($video->video_order);

        $next = $current_order + 1;

        $prv = $current_order - 1;

        $next_video = Video::where([
            'video_order' => $next,
            'course_id' => $course->id
        ])->first();


        if ($current_order == 1) {
            $prv_video = 0;
        } else {
            $prv_video = Video::where([
                'video_order' => $prv,
                'course_id' => $course->id
            ])->first();
        }


        // Generate next and prev urls
        $next = $next_video ? url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$next_video->url_identifier") : '#';
        $prev = $prv_video ? url("/" . Lang::getLocale() . "/videos/$course->url_identifier/$prv_video->url_identifier") : '#';


        // Get subscriptions
        $subscriptions = [];
        $course_bought = [];
        if (Auth::Check()) {
            $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;
            $course_bought = subscription::where(['user_id' => Auth::user()->id, 'course_id' => $course->id])->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();
            $subscriptions = subscription::where('user_id', Auth::user()->id)->where('subscription_model_id', '!=', $limitedAccessSubscriptionId)->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();
        }


        // Check Course Status and videos status
        $video_status = VideoStatus::where(['course_id' => $course->id, 'user_id' => $user_id, 'video_id' => $video->id])->count();
        if (!Auth::Guest() and count($subscriptions) > 0) {
            if ($video_status == 0) {
                VideoStatus::create([
                    'video_id' => $video->id,
                    'course_id' => $course->id,
                    'user_id' => $user_id,
                    'status' => 'started'
                ]);
            }
        }

        $status = array();

        foreach ($course->videoStatus as $stat) {
            $status[$stat->video_id] = array(
                'status' => $stat->status
            );
        }


        // Check subscription



        return view('videos.video', compact('video', 'course', 'status', 'course_status', 'completed_videos', 'not_completed_videos', 'completed', 'prv_video', 'next_video', 'downloadables', 'comments', 'subscriptions', 'course_bought', 'next', 'prev', 'lessonsUrl'));


    }


    public function videoCompleted($lang, $instrument, $courseslug, $videoslug)
    {

        $course = Course::where('url_identifier', $instrument . '/' . $courseslug)->first();


        $video = Video::where('course_id', $course->id)
            ->where('url_identifier', $videoslug)
            ->first();

        VideoStatus::create([
            'video_id' => $video->id,
            'course_id' => $course->id,
            'user_id' => Auth::user()->id,
            'status' => 'Done'
        ]);

        return '{"status":"updated"}';
    }


    public function Editdescription(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->user_group_id == 2) {

                $video_id = $request->input('video_id');
                $description = $request->input('description');
                Video::where('id', $video_id)->update(['description_arabic' => $description]);
            }
        }
    }

    public function geturl($id)
    {


        $video = Video::where('id', $id)
            ->get()
            ->first();

        $s3 = S3Client::factory([
            'key' => "AKIAICDJUJ6NBUCRQ6PA",
            'secret' => "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",
            'region' => 'us-west-2',
            'version' => 'latest',

            // You can override settings for specific Services
            'Ses' => [
                'region' => 'us-east-1',
            ]
        ]);

        $cloudfrom = CloudFrontClient::factory([
            'private_key' => dirname(__FILE__) . "/pk-APKAIAQOMIX23WJ7F5YQ.pem",
            'key_pair_id' => "APKAIAQOMIX23WJ7F5YQ",
            'region' => 'us-west-2',
            'version' => 'latest',

            // You can override settings for specific Services
            'Ses' => [
                'region' => 'us-east-1',
            ],
        ]);

        $object = $video->video_path;

        $expire = new \DateTime("+" . $video->estimated_time . "minutes");

        $url = $cloudfrom->getSignedUrl([
            'url' => "https://d1vmqa1344wrt8.cloudfront.net/" . $object,
            'expires' => $expire->getTimestamp(),
            'private_key' => dirname(__FILE__) . "/pk-APKAIAQOMIX23WJ7F5YQ.pem",
            'key_pair_id' => "APKAIAQOMIX23WJ7F5YQ",
        ]);


        return redirect($url);

    }


    public function freevideos()
    {

        $instruments = Instrument::all();


        $lang = Lang::getLocale() == 'ar' ? 1 : 2;
        //$course = Course::where('language',$lang)->get();
        //$videos = Video::where('is_open', '1')->orderBy('free_order_number')->with('teacher', 'course', 'instrument')->get();


        $courses = Cache::remember('FreeCourse', 60, function() use ($lang) {
            return Course::where('language',$lang)->get();
        });

        $videos = Cache::remember('FreeVideos', 60, function() use ($lang) {
            return Video::where('is_open', '1')->orderBy('free_order_number')->with('teacher', 'course', 'instrument')->get();
        });
        //in_array("Irix", $os)
        //if(Lang::getLocale() == "ar"){
            return view('videos.free', compact('videos', 'instruments', 'lang'));
        //}else{
            //return view('videos.free_english', compact('videos', 'instruments', 'lang'));
        //}

    }

    public function ask(Request $request)
    {

        $question = $request->input('question');
        $url = $request->input('url');
        $fullurl = $request->input('fullurl');
        $useremail = $request->input('useremail');
        $teacher = $request->input('teacher');

        $data = array(
            "url" => $url,
            "question" => $question,
            "fullurl" => $fullurl,
            "useremail" => $useremail,
            "teacher" => $teacher
        );

        Mail::send('emails.quesion', $data, function ($message) {
            $message->from('info@izif.com', 'Question For Izif');

            $message->to('i3zif.com@gmail.com');


        });

        return redirect($url);
    }
}
