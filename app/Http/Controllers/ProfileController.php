<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\CourseStatus;
use App\Course;
use App\subscription;
use Lang;
use App\SubscriptionModel;
use Carbon\Carbon;


class ProfileController extends Controller
{
    public function index(Request $request){

         if(Auth::check()){
            $user_id = Auth::user()->id;

            $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;
            $coursesubscriptions = subscription::where('user_id', Auth::user()->id)->where('subscription_model_id', '==', $limitedAccessSubscriptionId)->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();

            $CourseStatus = CourseStatus::where('user_id',$user_id)->get();

            $subscriptions = subscription::where('user_id',$user_id)->where('end_date','>',Carbon::now())->with('subscriptions_model','course')->get();

            $course_ids = array();
            foreach( $CourseStatus as $status ){
                $course_ids[] = $status->course_id;
            }

            foreach($coursesubscriptions as $subsc){
                $course_ids[] = $subsc->course_id;
            }

            $courses = Course::whereIn('id',$course_ids)->get();
            $user = User::where('id',Auth::user()->id)->get();
            $country = Country::lists('country_name', 'id')->prepend('Select', '0');
//            echo '<pre>';
//            var_dump($country); die();
            $last_seen = Session::where('user_id', '=', $user_id)->first();
             return view('pages.userprofile_backup',compact('user','last_seen','country','courses','subscriptions'));

        }else{
            return redirect("/" . Lang::getLocale() . "/auth/login");
        }
    }



    public function editUserInfo(Request $request){

        $field_name = $request->input('name');
        $field_value = $request->input('value');
        $field_value = ltrim( $field_value);
        $user_id = $request->input('pk');

        $new_values = array("$field_name"=>"$field_value");
        User::where('id',$user_id)->update($new_values);

        return $field_value;
    }
}
