<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SubscriptionModel;
use App\Course;
use Lang;
use Auth;
use App\Coupon;
use App\pendingTransactions;
use App\abandoned_carts;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\Session;

class CheckoutController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        if (!Auth::guest()) {


            try {

                $abanded_record = new abandoned_carts;
                $abanded_record->user_id = Auth::user()->id;
                $abanded_record->subscription_model_id = $request->input('subscription_model');
                $abanded_record->course_id = $request->input('course');
                $abanded_record->save();
            } catch (\Exception $e) {
                
            }

            $subscription_model_id = $request->input('subscription_model');
            $course_id = $request->input('course');
            $code = str_replace(' ', '', $request->input('code'));


            $subscription_model = SubscriptionModel::where('id', $subscription_model_id)->first();



            $subscription_model_period = floor($subscription_model->time_period_in_days / 30);

            $price = 0;
            if ($course_id == "0") {
                $price = $subscription_model->display_price;
            } else {
                $price = Course::where('id', $course_id)->first()->price;
            }


            // Check if there is a discount
            if ($code) {
                $coupon = Coupon::where('code', $code)->first();
                if (count($coupon) > 0) {
                    if ($coupon->discount_type == "percentage") {
                        $price = ceil(floatval(($price * $coupon->amount) / 100));
                    } else {
                        $price = ceil(floatval($price - $coupon->amount));
                    }
                }
            }


            $cashuData = $this->cashu($subscription_model, $price, $course_id, $abanded_record);
            $westren_union = $this->ModalContent('partials.payment_gateway.payment_method_description_' . Lang::getLocale() . '.westrenunion');
            $banktrasfare = $this->ModalContent('partials.payment_gateway.payment_method_description_' . Lang::getLocale() . '.banktrasfare');


            return view('pages.checkout', compact('subscription_model_id', 'course_id', 'subscription_model', 'subscription_model_period', 'price', 'code', 'westren_union', 'banktrasfare', 'abanded_record', 'cashuData'));
        } else {
            return redirect('/' . Lang::getLocale() . '/auth/login');
        }
    }

    public function ModalContent($path) {

        $content = preg_replace("/\n/", "", view($path));

        return trim($content);
    }

    public function paypal(Request $request) {


        $url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

        $item_name = $request->input('item_name');
        $item_number = $request->input('item_number');
        $a3 = $request->input('a3');
        $p3 = $request->input('p3');
        $_token = $request->input('_token');
        $user_id = $request->input('user_id');
        $subscription_model_id = $request->input('subscription_model_id');
        $course_id = $request->input('course_id');

        $code = str_replace(' ', '', $request->input('code'));

        $subscription_model = SubscriptionModel::where('id', $subscription_model_id)->first();

        $subscription_model_period = floor($subscription_model->time_period_in_days / 30);

        $price = 0;
        if ($course_id == "0") {
            $price = $subscription_model->display_price;
        } else {
            $price = Course::where('id', $course_id)->first()->price;
        }


        // Check if there is a discount
        if ($code) {
            $coupon = Coupon::where('code', $code)->first();
            if (count($coupon) > 0) {
                if ($coupon->discount_type == "percentage") {
                    $price = ceil(floatval(($price * $coupon->amount) / 100));
                } else {
                    $price = ceil(floatval($price - $coupon->amount));
                }
            }
        }

        // logged in user
        $active_user_id = Auth::user()->id;

        $page = <<<EOD
        <html>
        <head></head>
        <body>
            <form id="$_token" action="$url" method="POST">
            <input type="hidden" name="item_name" value="$item_name">
            <input type="hidden" name="item_number" value="$item_number">

            <input type="hidden" name="a3" value="$price">
            <input type="hidden" name="p3" value="$subscription_model_period">


            <!-- Custom Params -->
            <input type="hidden" name="_token" value="$_token">
            <input type="hidden" name="user_id" value="$active_user_id">
            <input type="hidden" name="subscription_model_id" value="$subscription_model_id">
            <input type="hidden" name="course_id" value="$course_id">
            <input type="hidden" name="business" value="i3zif.com-facilitator-1@gmail.com"><input type="hidden" name="cmd" value="_xclick-subscriptions"><input type="hidden" name="t3" value="M"><input type="hidden" name="currency_code" value="USD"><input type="hidden" name="no_shipping" value="1"><input type="hidden" name="src" value="1"><input type="hidden" name="return" value="http://dev.izif.com/ar/success/paypal"><input type="hidden" name="cancel_return" value="http://dev.izif.com/ar/cancel/paypal"><input type="hidden" name="notify_url" value="http://dev.izif.com/ar/paypal_ipn">
            </form>
            <script>
                document.getElementById("$_token").submit();
            </script>
        </body>
        <html>
EOD;

        echo $page;
    }

    public function twocheckout() {
        
    }

    public function cashu($subscription_model, $price, $course_id, $abanded_record) {
        $merchant_id = 'i3zifmusic';
        $encryption_key = 'whensailsizeperhapssix';
        $display_text = $subscription_model->title_enu;
        $currency = 'usd';
        $amount = $price;
        $language = 'en';
        $session_id = Session::getId();
        $txt1 = $subscription_model->title_enu;
        $standingOrder = 'yes';
        $soTxt1 = $subscription_model->title_enu;
        $soTxt2 = Auth::user()->id;
        $soDisplayText = $subscription_model->title_enu;
        $soPeriod = 30;
//        $token = md5($merchant_id . ':' . $amount . ':' . $currency . ':' . $session_id . ':' . $encryption_key);
        $token = md5($merchant_id . ':' . $amount . ':' . $currency . ':' . $encryption_key);
        $txt2 = Auth::user()->id . "," . $subscription_model->id . "," . $course_id . "," . $token . "," . $abanded_record->id;

        $cashuData = array(
            'amount' => $amount, 'merchant_id' => $merchant_id, 'encryption_key' => $encryption_key,
            'display_text' => $display_text, 'currency' => $currency, 'language' => $language,
            'session_id' => $session_id, 'txt1' => $txt1, 'standingOrder' => $standingOrder, 'soTxt1' => $soTxt1,
            'soTxt2' => $soTxt2, 'soDisplayText' => $soDisplayText, 'soPeriod' => $soPeriod,
            'token' => $token, 'txt2' => $txt2);

        return $cashuData;
    }

    public function pendingTransactionSave(Request $request) {
        pendingTransactions::create([
            'name' => $request->input('name'),
            'transfer_number' => $request->input('transactionNumber'),
            'country' => $request->input('country'),
            'transaction_type' => $request->input('type'),
            'cource_id' => $request->input(''),
            'subscription_model_id' => $request->input('subscription_model_id'),
            'amount' => $request->input('display_price'),
            'user_id' => Auth::user()->id
        ]);
    }

}
