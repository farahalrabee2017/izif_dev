<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Lang;
use App\Tracking;


class AutoStart extends Controller
{

    protected $mailchimp;
    protected $listId = '0cac252b03';

    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function addEmailToList($email)
    {
        try {
            $this->mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $email]
                );
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            // do something
            //dd($e);
        } catch (\Mailchimp_Error $e) {
            // do something
            //dd($e);
        }
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {

        // Auto Registration
        $email = $request->input('email');
        $path = $request->input('return_path');
        $user_lang =  $request->input('lang');
        $type = $request->input('type');

        $isRegistered = User::where('email', $email)->count();

        if ($isRegistered < 1) {
            $this->addEmailToList($email);
            $name = explode('@', $email)[0];
            $password = bcrypt("$name-izif");

            // Create User
            $user = User::create([
                'name' => "$name",
                'email' => $email,
                'password' => $password,
                'lang'=>$user_lang,
                'referal_account'=> $this->get_client_ip_server()

            ]);


            // Update Tracking if exists
            $track_id = session('track_id');
            if($track_id){
                $tracking  = Tracking::find($track_id);
                $tracking->user_id = $user->id;
                $tracking->real_user_id = $user->id;
                $tracking->save();

                //session()->forget('track_id');
            }



            // Auto Login
            $authDate = ['email' => $email, 'password' => "$name-izif",'name'=>$name,"token"=>$user->token];

            if($type == "facebook"){
                if(Auth::attempt($authDate)){

                    return redirect()->intended(Lang::getLocale() . '/courses');
                }
            }else {

                //return redirect('http://izif.com/' . Lang::getLocale() . '/');
                return redirect()->intended(Lang::getLocale() . '/auth/login')->withErrors([trans('course.please_check_ur_email_for_activation')]);

            }



        }else{
            return redirect()->intended(Lang::getLocale() . '/auth/login')->withErrors([trans('course.you_aleady_have_an_account')]);
        }



    }


    public function get_client_ip_server() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }


}
