<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use App\User;
use Auth;
use App\CourseStatus;
use App\Video;
use App\subscription;
use App\SubscriptionModel;
use Lang;
use Carbon\Carbon;
use App;
use App\Songs_type;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang, $instrument, $slug)
    {

        $course = Course:: where('url_identifier', $instrument . '/' . $slug)
            ->with('videos', 'teacher', 'instrument')
            ->with(['videoStatus' => function ($query) {
                if (Auth::check()) {
                    $query->where('user_id', Auth::user()->id);
                } else {
                    $query->where('user_id', 0);
                }
            }])
            ->with(['completedVideos'=> function ($query) {
                if (Auth::check()) {
                    $query->where('user_id', Auth::user()->id)->where('status','Done');
                } else {
                    $query->where('user_id', 0);
                }
            }])
            ->first();

        $course_lang = $course->language == '1' ? 'ar' : 'en';

        if($course_lang != $lang){

            return App::abort(404, 'Page not found');
        }


        $gallery_videos = Video::where(['course_id' => $course->id, 'is_open' => '1'])->orderBy('video_order')->take(3)->get();

        // values for videolist.partials
        $user_id = Auth::Guest() ? 0 : Auth::user()->id;
        $course_status = CourseStatus::where(['course_id' => $course->id, 'user_id' => $user_id])->get();
        $course_status = count($course_status) > 0 ? true : false;
        $completed_videos = count($course->completedVideos);
        $not_completed_videos = abs($completed_videos - count($course->videoStatus));
        $not_completed_precent = 0.5 * $not_completed_videos;
        $completed = floor(($completed_videos + $not_completed_precent) / count($course->videos) * 100);


        $status = array();
        foreach ($course->videoStatus as $stat) {
            $status[$stat->video_id] = array(
                'status' => $stat->status
            );
        }

        // Check subscription
        $subscriptions = [];
        $course_bought = [];
        if (Auth::Check()) {
            $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;
            $course_bought = subscription::where(['user_id' => Auth::user()->id, 'course_id' => $course->id])->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();
            $subscriptions = subscription::where('user_id', Auth::user()->id)->where('subscription_model_id', '!=', $limitedAccessSubscriptionId)->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();
        }
        //these if statments used because we want to display specific layout for programs and song courses.
if($course->course_type == '2')
{
$program_type = Course::distinct()->get(['programType']);
  return view('courses.programsDetails', compact('course','program_type','song_type', 'status', 'course_status', 'gallery_videos', 'completed_videos', 'not_completed_videos', 'completed', 'subscriptions', 'course_bought'));
}
if($course->course_type == '1')
{
  //$song_type = Video::distinct()->get(['song_type']);
  $song_type = Songs_type::all();
  return view('courses.SongsDetails', compact('course','song_type', 'status', 'course_status', 'gallery_videos', 'completed_videos', 'not_completed_videos', 'completed', 'subscriptions', 'course_bought'));
}
else {
  return view('courses.CourseDetails', compact('course', 'status', 'course_status', 'gallery_videos', 'completed_videos', 'not_completed_videos', 'completed', 'subscriptions', 'course_bought'));

}
    }


    public function find($lang, $context)
    {


        $course = Course:: where('old_website_prefix',$context)
            ->with('videos', 'teacher', 'instrument', 'completedVideos')
            ->with(['videoStatus' => function ($query) {
                if (Auth::check()) {
                    $query->where('user_id', Auth::user()->id);
                } else {
                    $query->where('user_id', 0);
                }
            }])
            ->first();


        if (count($course) > 0) {
            $gallery_videos = Video::where(['course_id' => $course->id, 'is_open' => '1'])->orderBy('video_order')->take(3)->get();

            // values for videolist.partials
            $user_id = Auth::Guest() ? 0 : Auth::user()->id;
            $course_status = CourseStatus::where(['course_id' => $course->id, 'user_id' => $user_id])->get();
            $course_status = count($course_status) > 0 ? true : false;
            $completed_videos = count($course->completedVideos);
            $not_completed_videos = abs($completed_videos - count($course->videoStatus));
            $not_completed_precent = 0.5 * $not_completed_videos;
            $completed = floor(($completed_videos + $not_completed_precent) / count($course->videos) * 100);


            $status = array();
            foreach ($course->videoStatus as $stat) {
                $status[$stat->video_id] = array(
                    'status' => $stat->status
                );
            }

            // Check subscription
            $subscriptions = [];
            $course_bought = [];
            if (Auth::Check()) {
                $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;
                $course_bought = subscription::where(['user_id' => Auth::user()->id, 'course_id' => $course->id])->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();
                $subscriptions = subscription::where('user_id', Auth::user()->id)->where('subscription_model_id', '!=', $limitedAccessSubscriptionId)->where('end_date','>',Carbon::now())->with('subscriptions_model')->get();
            }


            return view('courses.CourseDetails', compact('course', 'status', 'course_status', 'gallery_videos', 'completed_videos', 'not_completed_videos', 'completed', 'subscriptions', 'course_bought'));


        } else {
            return redirect()->action('HomeController@index');
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function start(Request $request)
    {

        $course_id = $request->input('course_id');
        $user_id = Auth::user()->id;
        $return = $request->input('return');
        CourseStatus::create([
            'user_id' => $user_id,
            'course_id' => $course_id,
            'status' => 'start'
        ]);
        $redirect = "/" . Lang::getLocale() . "/course/$return";
        return redirect($redirect);

    }


}
