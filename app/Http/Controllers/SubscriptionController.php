<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\subscription;
use App\SubscriptionModel;
use Carbon\Carbon;
use App\Video;
use App\Course;
use App\Coupon;
use App\Note;
use Lang;
use Cache;


class SubscriptionController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        // Initial Variables Declaration
        $discount = false;
        $coupon = null;
        $wrong_code = false;
        $course_id = null;
        $course = [];
        $code = null;

        // Fetch request params
        $course_id = $request->input('c');
        $code = $request->input('code');

        if ($course_id) {
            $course = Course::find($course_id);
        }


        //Check if discound code applied
        if ($code) {

            // fetch discount code record
            $coupon = Coupon::where('code', $code)->first();

            // Check if discount code is in the database
            if (count($coupon) > 0) {

                // Collect dates & now
                $starts_at = strtotime($coupon->starts_at);
                $ends_at = strtotime($coupon->ends_at);
                $now = strtotime(Carbon::now()->toDateTimeString());

                // If its starts less or now and end more or now
                if ($starts_at <= $now and $ends_at >= $now) {
                    $discount = true;
                }

            } else {

                // wrong discount code
                $code = false;
                $wrong_code = true;
            }

        }
        //$subscription_models = SubscriptionModel::with('SubscriptionModelFeatures')->get();

        $subscription_models = Cache::remember("SubscriptionModel:SubscriptionModelFeatures", 60, function(){
            return SubscriptionModel::with('SubscriptionModelFeatures')->get();
        });

        $number_of_videos = Video::all()->count();
        $number_of_courses = Course::all()->count();
        $number_of_notes = Note::all()->count();


        $number_of_course_videos = Video::where('course_id', $course_id)->count();

        $number_of_course_notes = Note::where('course_id', $course_id)->count();
        $lang = Lang::getLocale() == 'ar' ? 1 : 2;
        $engcourses = Course::where('language', $lang)->get();

        $englishvideos = 0;

        foreach ($engcourses as $key => $value) {
            $number_off_videos = Video::where('course_id', $value->id)->count();
            $englishvideos = $englishvideos + $number_off_videos;
        }




        $tags = array(
            '{{number_of_videos}}',
            '{{number_of_courses}}',
            '{{number_of_sheetmusic}}',
            '{{number_of_articles}}',
            '{{number_of_months}}',
            '{{number_of_course_videos}}',
            '{{number_of_english_courses}}',
            '{{number_of_english_videos}}'
        );




        $tagvalues = array(
            $number_of_videos,
            $number_of_courses,
            $number_of_notes,
            $number_of_notes,
            '12',
            $number_of_course_videos,
            count($engcourses),
            $englishvideos
        );


        $coursetagvalues = array(
            $number_of_course_videos,
            '1',
            $number_of_notes,
            $number_of_notes,
            '12',
            $number_of_course_videos
        );






        return view('pages.subscribe', compact('subscription_models', 'tags', 'tagvalues', 'course', 'discount', 'code', 'coupon', 'number_of_course_notes', 'number_of_course_videos', 'coursetagvalues', 'wrong_code'));

    }


    public function subscribe(Request $request)
    {
        if (Auth::check()) {
            $user_id = Auth::user()->id;
            $subscription_model = SubscriptionModel::find($request->subscription_model_id);
            $subscription = new subscription;
            $subscription->user_id = $user_id;
            $subscription->subscription_model_id = $request->subscription_model_id;
            $subscription->start_date = Carbon::now();
            $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);
            $subscription->course_id = $request->course_id;
            $subscription->save();
        }
    }
}
