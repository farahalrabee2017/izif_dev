<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Payments;
use Log;
use App\subscription;
use App\User;
use App\SubscriptionModel;
use Carbon\Carbon;
use App\abandoned_carts;

class CashuController extends Controller {

    /**
     * 
     * @param Requests $request
     * 
     * cashU process notofication
     */
    public function returnSucess() {

        // check amount , courseId , session and userId
        Log::info('notification start here');
        Log::info("CashU POST || " . $_POST["amount"]);
        $merchant_id = 'i3zifmusic';
        $encryptionKey = 'whensailsizeperhapssix';
        $amount = $_POST["amount"];
        $currency = strtolower($_POST["currency"]);
        $language = $_POST["language"];
        $txt1 = $_POST["txt1"];
        $txt2 = $_POST["txt2"];
        $token = $_POST["token"];
        $trn_id = $_POST["trn_id"];
        $session_id = $_POST["session_id"];
        $verificationString = $_POST["verificationString"];
        $netAmount = $_POST["netAmount"];
        $standingOrder = $_POST["standingOrder"];
        $soTxt1 = $_POST["soTxt1"];
        $soTxt2 = $_POST["soTxt2"];
//        $soTxt3 = $_POST["soTxt3"];
        $soDisplayText = $_POST["soDisplayText"];
        $soPeriod = $_POST["soPeriod"];
        $soStatus = $_POST["soStatus"];
        Log::info('before calculation');
        $calculatedVerificationString = sha1(strtolower($merchant_id) . ':' . $trn_id . ':' . $encryptionKey);
        if ($calculatedVerificationString != $verificationString) {
            Log::info('calculatedVerificationString not equal to verificationString');

            // if they didn't match, then don't release the service or the product as this request is not from CASHU side
            // in such case please refund the transaction 
        } else {
            // if they matched, then calculate the token and make sure that it match token parameter value as below to verify the amount
            $calculatedtoken = md5($merchant_id . ':' . $amount . ':' . $currency  . ':' . $encryptionKey); // use this if you are using Enhanced encryption            
            
            Log::info("marchent_id : $merchant_id || amount : || $amount || currency : $currency || sesion: $session_id || enc_key : $encryptionKey");
            if ($calculatedtoken != $token) {
                Log::info('calculatedtoken not equal to calculated token');
                // if they didn't match, then don't release the service
                // in such case please refund the transaction
            } else {

                // if they matched, then release the service
                $userEmail = User::find($soTxt2)->email;

                $item_id = explode(',', $txt2);
                $user_id = $item_id[0];
                $subscription_id = $item_id[1];
                $course_id = $item_id[2];
                $token = $item_id[3];

                try {
                    $abandedId = $item_id[4];
                } catch (\Exception $e) {
                    
                }


                $invoice_id = "CashU: $trn_id";
                $payment = new Payments;
                $payment->invoice_id = $invoice_id;
                $payment->user_id = $soTxt2;
                $payment->amount = $amount;
                $payment->payment_method_id = 3;
                $payment->save();

                $payment_id = Payments::where('invoice_id', $invoice_id)->first()->id;

                // Get Subscription Model
                $subscription_model = SubscriptionModel::find($subscription_id);

                // Create A new Subscription
                $subscription = new subscription;
                $subscription->user_id = $user_id;
                $subscription->subscription_model_id = $subscription_id;
                $subscription->payment_id = $payment_id;
                $subscription->start_date = Carbon::now();
                $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);

                if ($course_id != '0') {
                    $subscription->course_id = $course_id;
                }

                $subscription->save();

                // Log
                Log::info(date('[Y-m-d H:i e] ') . "Done | Verified IPN: $invoice_id | Payer Email : $userEmail");

                try {
                    if (isset($abandedId)) {
                        abandoned_carts::where('id', $abandedId)->delete();
                    }
                } catch (\Exception $e) {
                    
                }

                Log::info('success cashu , good job');
                return view('pages.thankyou');
            }
        }
    }

    public function notificationResponse() {
        Log::info('new notification');
        $encryption_key = 'izifEncryptionKeyword';
        $sRequest = $_POST["sRequest"];
        $successTransaction = new SimpleXMLElement($sRequest);
        $merchant_id = $successTransaction->cashUTransaction[0]->merchant_id;
        $amount = $successTransaction->cashUTransaction[0]->amount;
        $currency = $successTransaction->cashUTransaction[0]->currency;
        $language = $successTransaction->cashUTransaction[0]->language;
        $txt1 = $successTransaction->cashUTransaction[0]->txt1;
        $token = $successTransaction->cashUTransaction[0]->token;
        $cashU_trnID = $successTransaction->cashUTransaction[0]->cashU_trnID;
        $session_id = $successTransaction->cashUTransaction[0]->session_id;
        $cashUToken = $successTransaction->cashUTransaction[0]->cashUToken;
        $display_text = $successTransaction->cashUTransaction[0]->display_text;
        $responseCode = $successTransaction->cashUTransaction[0]->responseCode;
        $servicesName = $successTransaction->cashUTransaction[0]->servicesName;
        $trnDate = $successTransaction->cashUTransaction[0]->trnDate;

        $calculatedCashuToken = sha1(strtolower($merchant_id) . ':' . $trn_id . ':' . strtolower($encryptionKey));
        if ($calculatedCashuToken != $cashUToken) {
            //if they didn't match, then don't release the service or the product as this request is
            //not from CASHU side
            //in such case please refund the transaction
        } else {
            // if they matched, then calculate the token and make sure that it match token parameter value as below
            $calculatedtoken = md5($merchant_id.':'.$amount.':'.$currency.':'.$encryption_key);
//            $calculatedtoken = md5($merchant_id . ':' . $amount . ':' . $currency . ':' . $session_id . ':' . $encryption_key); //use this if you are using Enhanced encryption
            if ($calculatedtoken != $token) {
                // if they didn't match, then don't release the service
            } else {
                // if they matched, then release the service
            }
        }
        // the below part is the merchant response on the xml notification.
        if ($responseCode == 'OK') {
            $sRequest = "sRequest=<cashUTransaction><merchant_id>" . $merchant_id . "</merchant_id><cas hU_trnID>" . $cashU_trnID . "</cashU_trnID><cashUToken>" . $cashUToken . "</cashUT oken><responseCode>" . $responseCode . "</responseCode><responseDate>" . date("Y-m-d H:i:s") . "</responseDate></cashUTransaction>";
            $ch = curl_init();
            //change the below URL to - https://www.cashu.com/cgi- bin/notification/MerchantFeedBack.cgi for live area
            $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0";
            curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
            curl_setopt($ch, CURLOPT_URL, 'https://sandbox.cashu.com/cgi-bin/notification/MerchantFeedBack.cgi');
            curl_setopt($ch, CURLOPT_VERBOSE, 0);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $sRequest);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: close'));
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }

    public function cashuSorry() {
        $cashUerrors = array(
            2 => 'Inactive Merchant ID.',
            4 => 'Inactive Payment Account.',
            6 => 'Insufficient funds.',
            7 => 'Incorrect Payment account details.',
            8 => 'Invalid account.',
            15 => 'The password of the Payment Account has expired.',
            17 => 'The transaction has not been completed.',
            20 => 'The Merchant has limited his sales to some countries; the purchase attempt is coming from a country that is not listed in the profile.',
            21 => 'The transaction value is more than the limit. This limitation is applied to Payment Accounts that do not comply with KYC rules.',
            22 => 'The Merchant has limited his sales to only KYC- compliant Payment accounts; the purchase attempt is coming from a Payment account that is NOT KYC- compliant.',
            23 => 'The transaction has been cancelled by the customer. If the user clicks on the ―Cancel‖ button.',
            27 => 'The user trying to buy standing order item twice.'
        );
        $errorCode = $_POST["errorCode"];
        $txt1 = $_POST["txt1"];
        $session_id = $_POST["session_id"];

        echo $cashUerrors[$errorCode];
    }

}
