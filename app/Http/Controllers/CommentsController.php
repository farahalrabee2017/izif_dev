<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comments;

class CommentsController extends Controller
{

    public function video(Request $request){
        $inputs = $request->all();
        Comments::create($inputs);
    }
}
