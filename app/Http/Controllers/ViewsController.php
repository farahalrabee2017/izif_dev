<?php

namespace App\Http\Controllers;

use App\Course;
use App\VideoStatus;
use Illuminate\Http\Request;
use Cache;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use App\User;
use App\subscription;
use App\SubscriptionModel;
use App\Instrument;
use App\Payments;
use App\TeacherBilling;


class ViewsController extends Controller
{
    public function __construct()
    {
        //$this->middleware('isadmin');
    }

    public function courses($lang)
    {

        $courses = Cache::remember("admin-Course-list", 60, function () use ($lang) {
            return Course::with('videos')->get();
        });


        //return $response->send();
        return view('admin.views.courses', compact('courses'));
    }

    public function getCourseViews(Request $request, $lang, $id)
    {
        //dd($id);
        $from = $request->input('from');
        $to = $request->input('to');
        //var_dump($from);
        //var_dump($to);
        if ($from) {
            $views = VideoStatus::where('course_id', $id)->where('created_at', '>=', Carbon::parse($from))->where('created_at', '<=', Carbon::parse($to))->get();
        } else {
            $views = VideoStatus::where('course_id', $id)->get();
        }

        echo json_encode($views);
    }

    public function getVideoViews(Request $request, $lang, $id)
    {
        //dd($id);
        $from = $request->input('from');
        $to = $request->input('to');

        if ($from) {
            $views = VideoStatus::where('video_id', $id)->where('created_at', '>=', Carbon::parse($from))->where('created_at', '<=', Carbon::parse($to))->get();
        } else {
            $views = VideoStatus::where('video_id', $id)->get();
        }
        echo json_encode($views);
    }


    public function getTeachers()
    {

        $teachers = Cache::remember("admin-Teachers-list", 60, function () {
            return User::where('user_group_id', '3')->get();
        });

        return $teachers;
    }


    public function getTeachersPercantage($id)
    {
        return array(
            "3" => 15,
            "11" => 15,
            "13" => 15,
            "4" => 0,
            "23" => 0,
            "21" => 20,
            "16" => 15,
            "15" => 5,
            "22" => 10,
            "19" => 10,
            "20" => 10,
            "18" => 10,
            "17" => 10,
            "23760" => 15,
            "24" => 15,
            "14" => 15
        )[$id];
    }

    public function paymentGatewaysFees($id)
    {
        return array(
            //paypal
            "1" => array(
                "4.9%",
                "$0.30"
            ),

            //2checkout
            "2" => array(
                "5.5%",
                "$0.45"
            ),

            //cashu
            "3" => array(
                "7%"
            ),

            //westren union
            "4" => array(
                "1%"
            ),

            //Bank Trasfare
            "5" => array(
                "0.5%",
                "$7.05"
            )
        )[$id];
    }

    public function deductPaymentGatwayFees($payment,$payment_method_id){
        // Calculate Payment Gateway Fees // TODO : Saparate Function
        $subscriptionFees = $this->paymentGatewaysFees($payment_method_id);
        $subscriptionAmount = $payment->amount;

        foreach ($subscriptionFees as $subscriptionFee) {

            // If is Percantage
            if (strpos($subscriptionFee, '%') !== false) {
                $fees = $subscriptionAmount * (floatval($subscriptionFee) / 100);
                $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;

                // IF $ Ammount
            } elseif (strpos($subscriptionFee, '$') !== false) {
                $fees = number_format(str_replace('$', '', $subscriptionFee), 2);
                $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;
            }

        }

        return $subscriptionAmount;
    }

    public function calculateTeachersAmounts($rates, $subscription, $totalViews, $sortByTeacher, $userId)
    {

        $amounts = array();

        $getSubscriptionPayment = Payments::where('id', $subscription->payment_id)->first();
        if ($getSubscriptionPayment->invoice_id != "old_website" AND $getSubscriptionPayment->invoice_id != "Olduser" AND $getSubscriptionPayment->amount != "0" AND $getSubscriptionPayment->amount != "") {

            foreach ($rates as $teacherId => $percantage) {

                $teacherPercantage = $this->getTeachersPercantage($teacherId);

                $getSubscriptonModel = Cache::remember("admin-Teachers-SubscriptionModels-$subscription->subscription_model_id", 60, function () use ($subscription) {
                    return SubscriptionModel::where('id', $subscription->subscription_model_id)->first();
                });

                // Calculate Payment Gateway Fees // TODO : Saparate Function
                $subscriptionFees = $this->paymentGatewaysFees($getSubscriptionPayment->payment_method_id);
                $subscriptionAmount = $getSubscriptionPayment->amount;

                foreach ($subscriptionFees as $subscriptionFee) {

                    // If is Percantage
                    if (strpos($subscriptionFee, '%') !== false) {
                        $fees = $subscriptionAmount * (floatval($subscriptionFee) / 100);
                        $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;

                        // IF $ Ammount
                    } elseif (strpos($subscriptionFee, '$') !== false) {
                        $fees = number_format(str_replace('$', '', $subscriptionFee), 2);
                        $subscriptionAmount = number_format($subscriptionAmount, 2) - $fees;
                    }

                }


                // Calculate Teacher Percantage

                $teacherViewPercantageRate = $subscriptionAmount * (floatval($percantage) / 100);
                $teacherProfitRate = $teacherViewPercantageRate * (floatval($teacherPercantage) / 100);

                $amounts[$teacherId] = $teacherProfitRate;

                $teacherBilling = new TeacherBilling();
                $teacherBilling->amount = $teacherProfitRate;
                $teacherBilling->number_of_views = $sortByTeacher[$teacherId];
                $teacherBilling->views_percentage = $percantage;
                $teacherBilling->views_list = json_encode($rates);
                $teacherBilling->teacher_id = $teacherId;
                $teacherBilling->user_id = $userId;
                $teacherBilling->subscription_id = $subscription->id;
                $teacherBilling->subscription_model_id = $subscription->subscription_model_id;
                $teacherBilling->save();
            }

            return $amounts;
        }

        return false;
    }

    public function calculateWatchingPercantages($watchRates)
    {
        $totalNumberOfViews = 0;
        $watchPercantages = array();


        foreach ($watchRates as $teacherId => $watchRate) {
            $totalNumberOfViews = $totalNumberOfViews + $watchRate;
        }

        foreach ($watchRates as $teacherId => $watchRate) {
            $watchPercantages[$teacherId] = round((number_format($watchRate, 2) / $totalNumberOfViews) * 100);
        }


        return array(
            "total" => $totalNumberOfViews,
            "rates" => $watchPercantages
        );
        //dd($totalNumberOfViews);
    }


    public function sortCoursesByTeacher($coursesList)
    {

        $sortedTeachersList = array();

        foreach ($coursesList as $courseId => $views) {

            $course = Cache::remember("admin-Teachers-Course-$courseId", 60, function () use ($courseId) {
                return Course::where('id', $courseId)->first();
            });


            $teachersId = $course->user_id;

            if (isset($sortedTeachersList[$teachersId])) {
                $sortedTeachersList[$teachersId] = $sortedTeachersList[$teachersId] + intval($views);
            } else {
                $sortedTeachersList[$teachersId] = intval($views);
            }
        }

        return $sortedTeachersList;

    }

    public function getCoursesFromViews($views)
    {

        $coursesList = array();

        foreach ($views as $view) {

            if (isset($coursesList[$view->course_id])) {

                $coursesList[$view->course_id] = $coursesList[$view->course_id] + 1;
            } else {
                $coursesList[$view->course_id] = 1;
            }

        }

        return $coursesList;
    }

    public function getViewsByUser($userId)
    {

        return VideoStatus::where('user_id', $userId)
            ->get();
    }


    public function getInstrumentsList()
    {
        return Instrument::all();
    }

    public function getEndedSubscriptions($lang)
    {

        $watchedCoursesByUserId = array();

        // Query Ended Subscriptions // TODO : Add Date Filters

        $endedSubscriptions = subscription::where('end_date', '<=', Carbon::today())
            ->where('user_id', '!=', '1')
            ->where("subscription_model_id", "!=", "14")
            ->groupBy('user_id')
            ->get();

        // Get Instruments List
        //$instrumentsList = $this->getInstrumentsList();



        // Calculate Users Intersts Based On instruments
        foreach ($endedSubscriptions as $subscription) {

            $userId = $subscription->user_id;
            $userViews = $this->getViewsByUser($userId);
            $watchedCourses = $this->getCoursesFromViews($userViews);
            $sortByTeacher = $this->sortCoursesByTeacher($watchedCourses);
            $rates = $this->calculateWatchingPercantages($sortByTeacher);

            // Calculate each teacher amount
            $amounts = $this->calculateTeachersAmounts($rates["rates"], $subscription, $rates["total"], $sortByTeacher, $userId);
            if ($amounts) {
                $watchedCoursesByUserId[$userId] = array(
                    '$watchedCourses' => $watchedCourses,
                    '$sortByTeacher' => $sortByTeacher,
                    '$rates' => $rates
                );

            }


        }


        dd($watchedCoursesByUserId);


    }

    public function calculateSingleCoursesByTeacher($teacherId){

        // Get Teacher Courses Ids
        $coursesIds = Course::where('user_id' ,$teacherId)->lists('id')->toArray();

        // Get Single Subscriptions For the Selected Teacher And His Courses
        $singleSubscriptions = subscription::with('payment','course','user')
            ->where('user_id', '!=', '1')
            ->where("subscription_model_id", "14")
            ->whereIn('course_id', $coursesIds)
            ->get();



        return $singleSubscriptions;
    }

    public function viewBillings(Request $request){


        // Teachers List
        $teachers = $this->getTeachers();

        $teacher_id = $request->input('teacher_id');

        $billings = [];
        $singleCourses = [];

        $singleCoursesAmounts = 0;
        $amount = 0;
        $fullAmount = 0;
        $teacher = false;

        if($teacher_id){

            $teacher = User::where('id',$teacher_id)->first();

            // Handle Normal Subscriptions
            $billings = TeacherBilling::with('teacher','user','subscription_model')->where('teacher_id',$teacher_id)->get();

            foreach ($billings as $billing){

                $amount = $amount + $billing->amount;
            }

            // Handle Single Subscriptions
            $singleCourses = $this->calculateSingleCoursesByTeacher($teacher_id);

            foreach ($singleCourses as $singleCourse){
                if ($singleCourse->payment->invoice_id != "old_website" AND $singleCourse->payment->invoice_id != "Olduser" AND $singleCourse->payment->amount != "0" AND $singleCourse->payment->amount != "") {
                    $amountAfterPaymentGatewayFeesDeduction = $this->deductPaymentGatwayFees($singleCourse->payment,$singleCourse->payment->payment_method_id);

                    $singleCoursesAmounts = $singleCoursesAmounts + $amountAfterPaymentGatewayFeesDeduction;


                }
            }

            $teacher_percantage = (floatval($this->getTeachersPercantage($teacher_id)) / 100);
            $singleCoursesAmounts = ($singleCoursesAmounts * (floatval($this->getTeachersPercantage($teacher_id)) / 100));
            $fullAmount = $amount + $singleCoursesAmounts;

        }




        return View('admin.teachers.teachers_payment',compact('teacher_id','teachers','teacher','billings','singleCourses','singleCoursesAmounts','amount','fullAmount','teacher_percantage'));



    }


    public function getViewsForTeacher($id)
    {

        // Inital Counters
        $numberOfViews = 0;
        $viewsList = array();
        $userIds = array();
        $viewsByUsers = array();


        $subscriptions_models = Cache::remember("admin-Subscription-models-list", 60, function () {
            return SubscriptionModel::all();
        });

        // Get and cahce teachsers Courses
        $teacherCourses = Cache::remember('admin-teacher-' . $id . '-views', 60, function () use ($id) {
            return Course::where('user_id', $id)->get();
        });

        // Get unique Views Of teacher Courses
        foreach ($teacherCourses as $course) {

            $views = VideoStatus::where('course_id', $course->id)
                ->where('user_id', '!=', '1')
                ->groupBy('user_id')
                ->get();

            $viewsList[$course->id] = $views;

            foreach ($views as $view) {

                $user_id = $view->user_id;
                if (isset($viewsByUsers[$user_id])) {
                    $viewsByUsers[$user_id][] = $view;
                } else {
                    $viewsByUsers[$user_id] = array();
                    $viewsByUsers[$user_id][] = $view;
                }

                $userIds[] = $user_id;
            }

            $numberOfViews = $numberOfViews + count($views);
        }


        // calculate user views for each course
        foreach ($viewsByUsers as $viewsByUser) {

        }

        // Get subscriptions
        $subscriptions = subscription::where('end_date', '<=', Carbon::today())->whereIn('user_id', $userIds)->get();


        return (array(
            'numberOfViews' => $numberOfViews,
            'views' => $viewsList,
            'user_ids' => $userIds,
            'subscriptions' => $subscriptions,
            'subscriptions_models' => $subscriptions_models
        ));
    }

    public function getSubscriptionForTeacher($lang, $id)
    {

        $teacher_data = $this->getViewsForTeacher($id);

        $subscriptions_models = $teacher_data['subscriptions_models'];
        $numberOfViews = $teacher_data['numberOfViews'];
        $subscriptions = $teacher_data['subscriptions'];

        return View('admin.teachers.teachers_payments', compact('teacher_data', 'subscriptions', 'subscriptions_models', 'numberOfViews'));


    }

    public function course($lang, $id)
    {

    }

    public function video($lang, $id)
    {

    }
}
