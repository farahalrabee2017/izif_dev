<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Lang;
use Mail;

class FacebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $email = $request->input('email');
        $user_lang =  Lang::getLocale();
        $user = User::where('email',$email)->first();

        if(count($user) > 0) {

            Auth::login($user);
            return redirect()->intended(Lang::getLocale() . '/courses');

        }else{

            $name = explode('@', $email)[0];
            $password = bcrypt("$name-izif");

            // Create User
            $user = User::create([
                'name' => "$name",
                'email' => $email,
                'password' => $password,
                'lang'=>$user_lang,
                'referal_account'=> 'facebook'

            ]);

            $authDate = ['email' => $email, 'password' => "$name-izif",'name'=>$name];

            if(Auth::attempt($authDate)){

                Mail::send('emails.welcome', $authDate, function ($message) use ($user) {
                    $message->from('info@izif.com', 'Welcome To Izif');

                    $message->to($user->email)->subject('Welcome to izif! أهلاُ و سهلاُ بك في إعزف');
                });

                return redirect()->intended(Lang::getLocale() . '/courses');
            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
