<?php

namespace App\Http\Controllers;

use Faker\Provider\Payment;
use Illuminate\Http\Request;
use App\Country;
use App\Payments;
use App\PaymentMethod;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Testimonial;
use App\SubscriptionModel;
use App\SubscriptionModelFeatures;
use App\User;
use App\subscription;
use Carbon\Carbon;
use Mail;
use Hash;

class HomeController extends Controller
{

    public function index()
    {

        //dd(Hash::make('AdminIzifTeam2016'));
        $testimonials = Testimonial::all();
        return view('pages.home', compact('testimonials'));
    }


    public function conv()
    {
        return bcrypt("izif.com");
    }


    public function notify()
    {

        $payments = Payments::where('invoice_id', 'old_website')->get();
        $array = array();
        foreach ($payments as $payment) {
            $subscription = subscription::where('payment_id', $payment->id)->first();
            $value = $subscription->user_id;
            $subscription_type = $subscription->subscription_model_id;

            if (!in_array($value, $array)) {


                $user = User::where('id', $value)->first();

                if (count($user) > 0) {

                    $email = $user->email;
                    $name = $user->name == "" ? $user->name : explode('@', $email)[0];

                    $authDate = ['email' => $email, 'password' => "izif", 'name' => $name];

                    if ($subscription_type == "14") {

                        Mail::send('emails.bySubscription.courses', $authDate, function ($message) use ($user) {
                            $message->from('info@izif.com', 'Welcome To Izif');
                            $message->to($user->email)->subject('  اشتراكك في إعزف Your Subscription on izif');
                        });

                    } elseif ($subscription_type == "13") {

                        Mail::send('emails.bySubscription.monthly', $authDate, function ($message) use ($user) {
                            $message->from('info@izif.com', 'Welcome To Izif');
                            $message->to($user->email)->subject('اشتراك الشهري في إعزف  Your Monthly Subscription on izif');
                        });

                    } elseif ($subscription_type == "12") {

                        Mail::send('emails.bySubscription.sixmonths', $authDate, function ($message) use ($user) {
                            $message->from('info@izif.com', 'Welcome To Izif');
                            $message->to($user->email)->subject('اشتراك النصف سنوي في إعزف  Your Half Annual Subscription on izif');
                        });

                    } else {

                        Mail::send('emails.bySubscription.year', $authDate, function ($message) use ($user) {
                            $message->from('info@izif.com', 'Welcome To Izif');
                            $message->to($user->email)->subject('اشتراك السنوي في إعزف  Your Annual Subscription on izif');
                        });
                    }

                }


                $array[] = $value;
            }

            var_dump($array);
        }


        /*
        $email = "tareq@i3zif.com";
        $user = User::where('email',$email)->first();

        $authDate = ['email' => $email, 'password' => "izif",'name'=>"Hassan"];

        Mail::send('emails.bySubscription.courses', $authDate, function ($message) use ($user) {
            $message->from('info@izif.com', 'Welcome To Izif');
            $message->to($user->email)->subject('  اشتراكك في إعزف Your Subscription on izif');
        });

        Mail::send('emails.bySubscription.monthly', $authDate, function ($message) use ($user) {
            $message->from('info@izif.com', 'Welcome To Izif');
            $message->to($user->email)->subject('اشتراك الشهري في إعزف  Your Monthly Subscription on izif');
        });


        Mail::send('emails.bySubscription.sixmonths', $authDate, function ($message) use ($user) {
            $message->from('info@izif.com', 'Welcome To Izif');
            $message->to($user->email)->subject('اشتراك النصف سنوي في إعزف  Your Half Annual Subscription on izif');
        });


        Mail::send('emails.bySubscription.year', $authDate, function ($message) use ($user) {
            $message->from('info@izif.com', 'Welcome To Izif');
            $message->to($user->email)->subject('اشتراك السنوي في إعزف  Your Annual Subscription on izif');
        });*/


    }


    public function import()
    {

        $json = '[{"Name":"Kamal I. Almaghrabi","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"kalmaghrabi@Hikma.com"},{"Name":"Ayman S. Alhasanat","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"AAlhasanat@Hikma.com"},{"Name":"Jamil M. Samara","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"JSamara@Hikma.com"},{"Name":"Nael A. Abu Eideh","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"NAbuEideh@Hikma.com"},{"Name":"Tarek M. Rimawi","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"tRimawi@Hikma.com"},{"Name":"Mohammad M. Alshakshir","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Malshakshir@Hikma.com"},{"Name":"Hamza N. Tahboub","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"htahboub@Hikma.com"},{"Name":"Jalal K. Atieh","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"jatieh@Hikma.com"},{"Name":"Ibrahim I. Muqbel","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"imuqbel@Hikma.com"},{"Name":"Hazem H. Hussein","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Hhussein@Hikma.com"},{"Name":"Islam Abbas","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"iabbas@hikma.com "},{"Name":"Dareen M. Al-Abki","Subscription Model ID":"14","Course ID":"4","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"DAlAbki@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Raya A. Al-Manasir","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"RAlManasir@Hikma.com"},{"Name":"Nada N. Rawashdah","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Nrawashdah@Hikma.com"},{"Name":"Ahmad S. AlHourani","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"ASAlHourani@Hikma.com"},{"Name":"Yazan H. Hammad","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"yHammad@Hikma.com"},{"Name":"Lama N. AlAdham","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"lnaladham@Hikma.com"},{"Name":"Hanan A. Hassan","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"hhassan@Hikma.com"},{"Name":"Feras F. Suliman","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Fsuliman@Hikma.com"},{"Name":"Amani J. Alqawasmi","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"aalqawasmi@Hikma.com"},{"Name":"Omar N. Omairi","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"oomairi@Hikma.com"},{"Name":"Diana S. Nuseir","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"DNuseir@Hikma.com"},{"Name":"Heba Q. Qasrawi","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"hqasrawi@Hikma.com"},{"Name":"Abeer M. Alammouri","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"aalammouri@Hikma.com"},{"Name":"Rana M. Saadeh","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Rsaadeh@Hikma.com"},{"Name":"Katia M. Jadoun","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"kjadoun@Hikma.com"},{"Name":"Nancy H. AlQaq","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"nalqaq@Hikma.com"},{"Name":"Aseel T. Alkhatib","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"aalkhatib@Hikma.com"},{"Name":"Haya H. AbuTaha","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"HAbutaha@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Moayed K. Amiera","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"MAmiera@Hikma.com"},{"Name":"Layal B. Anshasi","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"LAnshasi@Hikma.com"},{"Name":"Ibrahim M. Aldibis","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"IAldibis@Hikma.com"},{"Name":"Safa H. Al Nimer","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Salnimer@Hikma.com"},{"Name":"Asma I. Al-Maayha","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"AAlMaayha@Hikma.com"},{"Name":"Shadi H. Hammdan","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"SHammdan@Hikma.com"},{"Name":"Alhanoof J. Alghamdi","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"aalghamdi@Hikma.com"},{"Name":"Leen G. Farah","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"lfarah@Hikma.com"},{"Name":"Leen M. Rimawi","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"LRimawi@Hikma.com"},{"Name":"Rayan E. Qasem","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"RQasem@Hikma.com"},{"Name":"Raeda M. Alghazo","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"RAlghazo@Hikma.com"},{"Name":"Alaa\' I. Al Momani","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"AAlMomani@Hikma.com"},{"Name":"Reem S. Abu Suleiman","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"Rabusuleiman@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Moh\u2019d H. Abu-Hamdah","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"MAbuHamdah@Hikma.com"},{"Name":"Murad S. Atallah","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"MAtallah@Hikma.com"},{"Name":"Lama Rabadi","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"lrabadi@Hikma.com"},{"Name":"Ruba F. Shaheen","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"RShaheen@Hikma.com"},{"Name":"Hiba A. Zarour","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"HZarour@Hikma.com"},{"Name":"Jihan Hakooz","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"JHakooz@Hikma.com"},{"Name":"Osama H. Huzayen","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"ohuzayen@Hikma.com"},{"Name":"Bisher T. Alkhatib","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"BAlkhatib@Hikma.com"},{"Name":"Amani F. AbuZayed","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"AAbuZayed@Hikma.com"},{"Name":"Omar N. Omairi","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"oomairi@Hikma.com"},{"Name":"Diana S. Nuseir","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"DNuseir@Hikma.com"},{"Name":"Heba Q. Qasrawi","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"Jordan","Date":"6\/14\/16","Email":"hqasrawi@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Basima M. Gharaibeh","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"BGharaibeh@Hikma.com"},{"Name":"Laith G. Najjar","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"lNajjar@Hikma.com"},{"Name":"Abdallah M. Abu Sbeit","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"AAbuSbeit@Hikma.com"},{"Name":"Eyad H. Alshobaky","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"EAlshobaky@Hikma.com"},{"Name":"Mustafa J. Mahmoud","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"MJMahmoud@Hikma.com"},{"Name":"Leen Y. Al Alami","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"LAlalami@Hikma.com"},{"Name":"Laith N. ALMousa","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"lalmousa@Hikma.com"},{"Name":"Amin A. Owis","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"aowis@Hikma.com"},{"Name":"Ibrahim  H. Swiess","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"ISwiess@Hikma.com"},{"Name":"Lina M. Alawneh","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"LAlawneh@Hikma.com"},{"Name":"Omar Y. Abughazzah","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"OAbughazzah@Hikma.com"},{"Name":"Iman S. Hdairis","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"IHdairis@Hikma.com"},{"Name":"Mohammed A. Abutouk","Subscription Model ID":"14","Course ID":"8","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"MAbutouk@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Mohammed Abu-Dayyeh","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"MAbuDayyeh@Hikma.com"},{"Name":"Bilal Altwaisi\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0\u00a0","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"BAltwaisi@Hikma.com"},{"Name":"Wisam F. Al Khatib","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"WAlKhatib@Hikma.com"},{"Name":"Salma M. Alghoul","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"salghoul@Hikma.com"},{"Name":"Lana Y. Alshaikh","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"LAlshaikh@Hikma.com"},{"Name":"Fidaa F. Odetallah","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"FOdetallah@Hikma.com"},{"Name":"Samer N. Abuzaineh","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"sabuzaineh@Hikma.com"},{"Name":"Amal T. Said","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"ASaid@Hikma.com"},{"Name":"Rasha A. Alshaqarin","Subscription Model ID":"14","Course ID":"16","$":"0","JD":"0","Method":"1","Country":"jordan","Date":"6\/14\/16","Email":"RAlshaqarin@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Rami A. Arafat","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"RArafat@Hikma.com"},{"Name":"Rasha A. Alshaqarin","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"RAlshaqarin@Hikma.com"},{"Name":"Samah H. Natour","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"SNatour@Hikma.com"},{"Name":"Tamara A. Abu-Khader","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"TAbuKhader@Hikma.com"},{"Name":"Feda S. Haddad","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"FHaddad@Hikma.com"},{"Name":"Lara A. Masad","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"lmasad@Hikma.com"},{"Name":"Deema S. Al Taji","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"DAlTaji@Hikma.com"},{"Name":"Dana N. Maraqa","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"DMaraqa@Hikma.com"},{"Name":"Dana Shekem","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"dshekem@hikma.com"},{"Name":"Saif S. Al Jaghbeer","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"SAlJaghbeer@Hikma.com"},{"Name":"Waseem  B. Awad","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"WbAwad@Hikma.com"},{"Name":"Saman I. Mazahreh","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"SMazahreh@Hikma.com"},{"Name":"Shereen M. Marei","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"SMarei@Hikma.com"},{"Name":"Medhat A. Alarjani","Subscription Model ID":"14","Course ID":"22","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"maalarjani@Hikma.com"},{"Name":"","Subscription Model ID":"","Course ID":"","$":"","JD":"","Method":"","Country":"","Date":"","Email":""},{"Name":"Noor Hamam","Subscription Model ID":"14","Course ID":"11","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"nwhamam@Hikma.com"},{"Name":"Emad M. Abu Dhaim","Subscription Model ID":"14","Course ID":"11","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"EAbuDhaim@Hikma.com"},{"Name":"Safa\' H. Abu Gharbiah","Subscription Model ID":"14","Course ID":"11","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"SAbuGharbiah@Hikma.com"},{"Name":"Haneen I. Hassan","Subscription Model ID":"14","Course ID":"11","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"HiHassan@Hikma.com"},{"Name":"Reem S. Abu Suleiman","Subscription Model ID":"14","Course ID":"11","$":"0","JD":"0","Method":"1","Country":"jordan ","Date":"6\/14\/16","Email":"Rabusuleiman@Hikma.com"}]';


        $users = json_decode($json, true);


        $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;


        foreach ($users as $key => $value) {
            //if (!$value["Email"] == "") {
                $subscription_model_id = $value["Subscription Model ID"] == "" ? "14" : $value["Subscription Model ID"];
                $name = $value["Name"];

                // Handle dubbule Emails
                $email = $value["Email"];
                if (strpos($email, ',') !== false) {
                    $email = explode(',', $email)[0];
                }


                $Subscription_Model_ID = $subscription_model_id;
                $Course_ID = $value["Course ID"];
                $Country = $value["Country"];
                $method = $value["Method"] == "" ? '1' : $value["Method"];

                $user = User::where('email', $email)->first();


                if (count($user) > 0) {


                    $user_id = $user->id;

                    $has_same_subscription = subscription::where('user_id', $user_id)->where('subscription_model_id', $Subscription_Model_ID)->count();
                    if ($has_same_subscription == 0) {

                        // Create Payment
                        //d($method);
                        $payment = new Payments;
                        $payment->invoice_id = 'old_website';
                        $payment->user_id = $user_id;
                        $payment->amount = $value["$"];
                        $payment->payment_method_id = $method;
                        $payment->save();
                        //End of payment


                        //Create Subscription
                        // Get Subscription Model

                        $subscription_model = SubscriptionModel::find($Subscription_Model_ID);

                        // Create A new Subscription
                        $subscription = new subscription;
                        $subscription->user_id = $user_id;
                        $subscription->subscription_model_id = $Subscription_Model_ID;
                        $subscription->payment_id = $payment->id;
                        $subscription->start_date = Carbon::now();
                        $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);


                        $subscription->save();
                        // End of Create Subscription
                    }

                } else {

                    if ($email != "") {
                        if ($name == "") {
                            $name = explode('@', $email)[0];
                        }

                        $password = bcrypt("$name-izif");
                        $user = User::create([
                            'name' => $name,
                            'email' => $email,
                            'password' => $password
                        ]);

                        $user_id = $user->id;

                        // Create Payment
                        //dd($method);
                        $payment = new Payments;
                        $payment->invoice_id = 'old_website';
                        $payment->user_id = $user_id;
                        $payment->amount = $value["$"];
                        $payment->payment_method_id = $method;
                        $payment->save();
                        // End of payment


                        //Create Subscription
                        // Get Subscription Model
                        $subscription_model = SubscriptionModel::find($Subscription_Model_ID);

                        // Create A new Subscription
                        $subscription = new subscription;
                        $subscription->user_id = $user_id;
                        $subscription->subscription_model_id = $Subscription_Model_ID;
                        $subscription->payment_id = $payment->id;
                        $subscription->start_date = Carbon::now();
                        $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);


                        $subscription->save();
                        // End of Create Subscription


                    }
                }
            //}
        }

    }


    public function imports()
    {

        $json = file_get_contents('http://izif.com/static/migrations/11.json');

        $users = json_decode($json, true);


        $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;


        foreach ($users as $key => $value) {
            $name = $value["Name"];
            $email = $value["Email"];
            $Subscription_Model_ID = $value["Subscription Model ID"];
            $Course_ID = $value["Course ID"];
            $Country = $value["Country"];
            $method = $value["Method"];
            $user = User::where('email', $email)->first();
            //var_dump(count($user));
            if (count($user) > 0) {


                $user_id = $user->id;

                // Create Payment
                $payment = new Payments;
                $payment->invoice_id = 'old_website';
                $payment->user_id = $user_id;
                $payment->amount = $value["$"];
                $payment->payment_method_id = $method;
                $payment->save();
                //End of payment

                if ($Subscription_Model_ID != $limitedAccessSubscriptionId) {


                    //Create Subscription
                    // Get Subscription Model
                    $subscription_model = SubscriptionModel::find($Subscription_Model_ID);

                    // Create A new Subscription
                    $subscription = new subscription;
                    $subscription->user_id = $user_id;
                    $subscription->subscription_model_id = $Subscription_Model_ID;
                    $subscription->payment_id = $payment->id;
                    $subscription->start_date = Carbon::now();
                    $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);


                    $subscription->save();
                    // End of Create Subscription

                } else {
                    // Create Subscription
                    // Get Subscription Model
                    $subscription_model = SubscriptionModel::find($Subscription_Model_ID);

                    // Create A new Subscription
                    $subscription = new subscription;
                    $subscription->user_id = $user_id;
                    $subscription->subscription_model_id = $Subscription_Model_ID;
                    $subscription->payment_id = $payment->id;
                    $subscription->start_date = Carbon::now();
                    $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);
                    $subscription->course_id = $Course_ID;


                    $subscription->save();
                    // End of Create Subscription
                }
            } else {

                if ($email != "") {
                    if ($name == "") {
                        $name = explode('@', $email)[0];
                    }

                    $password = bcrypt("$name-izif");
                    $user = User::create([
                        'name' => $name,
                        'email' => $email,
                        'password' => $password
                    ]);

                    $user_id = $user->id;

                    // Create Payment
                    $payment = new Payments;
                    $payment->invoice_id = 'old_website';
                    $payment->user_id = $user_id;
                    $payment->amount = $value["$"];
                    $payment->payment_method_id = $method;
                    $payment->save();
                    // End of payment

                    if ($Subscription_Model_ID != $limitedAccessSubscriptionId) {


                        //Create Subscription
                        // Get Subscription Model
                        $subscription_model = SubscriptionModel::find($Subscription_Model_ID);

                        // Create A new Subscription
                        $subscription = new subscription;
                        $subscription->user_id = $user_id;
                        $subscription->subscription_model_id = $Subscription_Model_ID;
                        $subscription->payment_id = $payment->id;
                        $subscription->start_date = Carbon::now();
                        $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);


                        $subscription->save();
                        // End of Create Subscription

                    } else {
                        // Create Subscription
                        // Get Subscription Model
                        $subscription_model = SubscriptionModel::find($Subscription_Model_ID);

                        // Create A new Subscription
                        $subscription = new subscription;
                        $subscription->user_id = $user_id;
                        $subscription->subscription_model_id = $Subscription_Model_ID;
                        $subscription->payment_id = $payment->id;
                        $subscription->start_date = Carbon::now();
                        $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);
                        $subscription->course_id = $Course_ID;


                        $subscription->save();
                        // End of Create Subscription
                    }
                }
            }
        }

    }
}
