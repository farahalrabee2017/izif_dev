<?php

namespace App\Http\Controllers;

use App\Marketing;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MarketingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $marketing = Marketing::get();
        return view('admin.marketing.index', compact('marketing'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($lang, Request $request)
    {
        
        $from = $request->input('from');
        $to = $request->input('to');

        if(!empty($request->input()))
        {


            $from = date_create($from );
            $from =  date_format($from, 'Y-m-d');

            $to = date_create($to );
            $to =  date_format($to, 'Y-m-d');
        }

        $marketing = new Marketing();
        $marketing->amount = $request->get('amount');
        $marketing->start_date = $from;
        $marketing->end_date = $to;
       

        if ($request->has('save')) {
            //$blog->active = 0;
            $message = 'Amount saved successfully';
        } else {
            //$blog->active = 1;
            $message = 'Amount not saved successfully';
        }
        $marketing ->save();
        return redirect($lang . '/admin/marketing')->withErrors($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    
    // this function is written to track the users who supscriped for the first time in a certain month and tracking
    //them if they still have active subscription on  izif or not.
    public  function  marketing_tracking ($lang ,Request $request)
    {
        if($_POST)
        {

            if ($request->input('date'))
            {
                $from = $request->input('date');
                $from1 = date("Y-m-d", strtotime($from));// return the month of the first subscription.
                $to1 = date("Y-m-d", strtotime($from1 ."+1 month"));//add one month to return "to" interval.

            }
            $values = $_POST['month']; // return the month/s we want to compare with.
            foreach ($values as $month) {

                $from2 = date("Y-m-d", strtotime($month));
                $to2 = date("Y-m-d", strtotime($from2 ."+1 month")); //add 1 month to return "to" interval.
                
                //this query return count of users that still active in certain months.
                $tracking[] = DB::select(DB::raw("SELECT count(DISTINCT y.user_id) as num from (SELECT DISTINCT t2.user_id ,min , t3.amount from (SELECT * from (select user_id ,min(created_at) as min from payments WHERE amount != '' and amount != '0' and created_at > '2016-06-01'   group by user_id)  as t1 WHERE min BETWEEN '$from1' and '$to1') as t2
                                                  join  
                                                  payments as t3 on t2.user_id = t3.user_id WHERE t3.created_at BETWEEN '$from2' and '$to2'  and (t3.amount = '19.00' or t3.amount = '19')  ) as y JOIN
                                                  (SELECT * from payments WHERE created_at BETWEEN '$from2' and '$to2') as t on y.user_id = t.user_id WHERE (t.amount = '19' or t.amount = '19.00')"));

                //return number of users that subscriped "monthly" in certain month.
           $new = DB::select(DB::raw(" SELECT count(DISTINCT t2.user_id) as new  from (SELECT * from (select user_id ,min(created_at) as min from payments WHERE amount != '' and amount != '0' and created_at > '2016-06-01'   group by user_id)  as t1 WHERE min BETWEEN '$from1' and '$to1') as t2
                                      join 
                                      payments as t3 on t2.user_id = t3.user_id WHERE t3.created_at BETWEEN '$from1' and '$to1'  and (t3.amount = '19' or t3.amount = '19.00') "));
                
            }
        }
        else
        {
            $tracking = "";
            $values = "";
        }
        return view('admin.marketing.tracking' , compact('tracking','values','new'));

    }
}
