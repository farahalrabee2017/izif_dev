<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Instrument;
use App\Country;
use App\instrument_request;
use Mail;
use DB;
use Lang;
use App;

class InstrumentController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(request $request) {
        if ($request->segment(1) == 'en') {
            $instruments = Instrument::where('id', '!=', '5')->lists('english_name', 'id')->prepend('Select', '');
            $countries = Country::lists('country_name', 'id')->prepend('Select', '');
        } else {
            $instruments = Instrument::where('id', '!=', '5')->lists('arabic_name', 'id')->prepend('اختيار', '');
            $countries = Country::lists('country_name', 'id')->prepend('اختيار', '');
        }

        return view('instruments.try_instrument', compact('instruments', 'countries'));
    }

    public function save(request $request) {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'city' => 'required',
            'country' => 'required',
            'Instrument' => 'required',
            'level' => 'required'
        ]);
        $instrumentInsert = instrument_request::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'instrument_id' => $request->input('Instrument'),
                    'country_id' => $request->input('country'),
                    'city' => $request->input('city'),
                    'level' => $request->input('level')
        ]);

        $requestedInstruments = DB::select(DB::raw
                                ("SELECT ri.id , ri.name , ri.email , ri.level , ri.city , c.country_name as country ,
                  i.english_name as instrument , ri.is_active
                  FROM 
                  instrument_request ri left join instruments i on (ri.instrument_id = i.id)
                  left join countries c on (ri.country_id = c.id)
                  where ri.id=$instrumentInsert->id;"));

        $data = array(
            'name' => $requestedInstruments[0]->name,
            'email' => $requestedInstruments[0]->email,
            'instrument_id' => $requestedInstruments[0]->instrument,
            'country_id' => $requestedInstruments[0]->country,
            'city' => $requestedInstruments[0]->city,
            'level' => $requestedInstruments[0]->level
        );
        Mail::send('emails.requestInstrument', $data, function ($message) {
            $message->from('info@izif.com', 'Instrument Request');
            $message->to('i3zif.com@gmail.com')->subject('Instrument Request');
        });
        $success = 1;
//        return back()->withErrors([trans('course.sucessInstrumentRequest')]);
//        return back()>with('successs', ['1']);
        $lang = App::getLocale() . '/' . trans('course.courses_list_link');
        return redirect()->back()->with('success', [trans('course.sucessInstrumentRequest'), trans('course.thank_you'), $lang]);
    }

}
