<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\subscription;
use App\SubscriptionModel;
use Carbon\Carbon;
use App\Payments;
use App\PaymentMethod;
use App\abandoned_carts;
//use Mockery\CountValidator\Exception;

class PaypalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected $mailchimp;
    protected $listId = 'cf7f32922e';

    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function addEmailToList($email)
    {
        try {
            $this->mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $email]
                );
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            // do something
            //dd($e);
        } catch (\Mailchimp_Error $e) {
            // do something
            //dd($e);
        }
    }

    public function index()
    {
        //
    }


    public function ipn()
    {


        define("DEBUG", 0);
        define("USE_SANDBOX", 1);
        define("LOG_FILE", "./ipn.log");

        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }

        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }

        if (USE_SANDBOX == false) {
            $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        } else {
            $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
        }
        $ch = curl_init($paypal_url);
        if ($ch == FALSE) {
            return FALSE;
        }
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

        if (DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) // cURL error
        {
            if (DEBUG == true) {
                error_log(date('[Y-m-d H:i e] ') . "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
            exit;
        } else {
            // Log the entire HTTP response if debug is switched on.
            if (DEBUG == true) {
                error_log(date('[Y-m-d H:i e] ') . "HTTP request of validation request:" . curl_getinfo($ch, CURLINFO_HEADER_OUT) . " for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
                error_log(date('[Y-m-d H:i e] ') . "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
        }

        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));

        $payment_method_id = PaymentMethod::where('name', 'paypal')->first()->id;

        // json POST Params to Save in the database
        $request = json_encode($_POST);





        if (strcmp($res, "VERIFIED") == 0) {

            if (isset($_POST['payment_status'])) {
                if ($_POST['payment_status'] == "Completed") {

                    // Provider Parameters
                    $item_name = $_POST['item_name'];
                    $item_number = $_POST['item_number'];


                    $payer_email = $_POST['payer_email'];


                    // Add to mailchimp subscribers
                    try {
                        $this->addEmailToList($payer_email);
                    }catch (\Exception $e){}

                    // Custom Fields
                    $item_id = explode(',', $item_number);
                    $user_id = $item_id[0];
                    $subscription_id = $item_id[1];
                    $course_id = $item_id[2];
                    $token = $item_id[3];

                    try{
                        $abandedId = $item_id[4];
                    }catch(\Exception $e){}

                    $invoice_id = $_POST['txn_id'];
                    $amount = $_POST['mc_gross'];

                    //Make a payment
                    $payment = new Payments;
                    $payment->invoice_id = $invoice_id;
                    $payment->user_id = $user_id;
                    $payment->amount = $amount;
                    $payment->payment_method_id = $payment_method_id;
                    $payment->save();

                    // Get Payment Id
                    $payment_id = Payments::where('invoice_id', $invoice_id)->first()->id;

                    // Get Subscription Model
                    $subscription_model = SubscriptionModel::find($subscription_id);

                    // Create A new Subscription
                    $subscription = new subscription;
                    $subscription->user_id = $user_id;
                    $subscription->subscription_model_id = $subscription_id;
                    $subscription->payment_id = $payment_id;
                    $subscription->start_date = Carbon::now();
                    $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);

                    if ($course_id != '0') {
                        $subscription->course_id = $course_id;
                    }

                    $subscription->save();

                    // Log
                    Log::info(date('[Y-m-d H:i e] ') . "Done | Verified IPN: $req | Payer Email : $payer_email");

                    // Remove from abandend
                    try{
                        if(isset($abandedId)){
                            abandoned_carts::where('id',$abandedId)->delete();
                        }
                    }catch(\Exception $e){}

                }
            }

        } else if (strcmp($res, "INVALID") == 0) {

            //Log::info(date('[Y-m-d H:i e] ') . "Verified IPN: $req | Payer Email : $payer_email");
        } else {
            //Log::info(date('[Y-m-d H:i e] ') . "Verified IPN: $req | Payer Email : $payer_email");
        }
    }


}
