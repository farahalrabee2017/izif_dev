<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Lang;
use Auth;

class PasswordController extends Controller
{

    public function reset()
    {
        return view('auth.reset');
    }

    public function makeReset(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', $email)->get();

        if (count($user) > 0) {

            User::where('email', $email)->update(['password' => bcrypt($password)]);

            $authDate = ['email' => $email, 'password' => $password];
            if (Auth::attempt($authDate)) {
                return redirect()->intended(Lang::getLocale() . '/profile');
            }else{
                return redirect()->intended(Lang::getLocale() . '/reset');
            }

        } else {
            return redirect()->intended(Lang::getLocale() . '/auth/login');
        }

    }
}
