<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Course;
use App\Instrument;
use Lang;
use Cache;
use App\course_type;
use App\Program_types;


class CoursesGridController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($lang , Request $request){

        $lang = $lang == 'ar' ? 1 : 2;

           $courses = Cache::remember("Course-list-$lang", 60, function() use ($lang) {
               return Course::orderBy('course_order')->where('is_active','1')->where('language',$lang)->get();
           });



        $instruments = Cache::remember('Instruments', 60, function(){
            return Instrument::orderBy('id', 'desc')->get();
        });

        $course_type = course_type::with('course')->get();
        $program_type = Program_types::all();

        //$courses = Course::orderBy('course_order')->where('language',$lang)->get();
        //$instruments = Instrument::orderBy('id', 'desc')->get();
        return view('courses.courseslist',compact('courses','instruments','course_type','program_type'));
    }
}
