<?php

namespace App\Http\Controllers;

use App\Blog;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::where('published_at', '<=', Carbon::now())
            ->where('active', '=', '1')->orderBy('created_at', 'desc')->paginate(6);
        if (request()->has('filter')) {
            if ($_GET['filter'] == 'latest') {
                $blogs = Blog::where('published_at', '<=', Carbon::now())
                    ->where('active', '=', '1')->orderBy('created_at', 'desc')
                    ->paginate(6);

            }
            if ($_GET['filter'] == 'oldest') {
                $blogs = Blog::where('published_at', '<=', Carbon::now())
                    ->where('active', '=', '1')->orderBy('created_at', 'Asc')
                    ->paginate(6);

            }
            if ($_GET['filter'] == 'most_viewed') {
                $blogs = Blog::where('published_at', '<=', Carbon::now())
                    ->where('active', '=', '1')->OrderBy('view_count', 'DESC')
                    ->paginate(6);

            }
            $filter = $_GET['filter'];

        }
        if (!empty($_GET['query'])) {
            $q = $_GET['query'];
            $blogs = Blog::where('published_at', '<=', Carbon::now())->
            where('title', 'LIKE', '%' . $q . '%')->where('active', '=', '1')->orderBy('created_at', 'desc')->paginate(6);

        }

        $random = Blog::where('published_at', '<=', Carbon::now())
            ->where('active', '=', '1')->orderByRaw('RAND()')->take(5)->get(array('title', 'slug'));

        $tags = Blog::where('active', '=', '1')->distinct()->orderBy('created_at', 'desc')->take(10)->get(array('category'));

        return view('blog.index', compact('blogs', 'filter', 'random', 'tags'));
    }

    public function showBlog($lang, $slug)
    {
        $blog = Blog::whereSlug($slug)->firstOrFail();
        $random = Blog::where('published_at', '<=', Carbon::now())
            ->where('active', '=', '1')->orderByRaw('RAND()')->take(5)->get(array('title', 'slug'));
        $tags = Blog::where('active', '=', '1')->distinct()->orderBy('created_at', 'desc')->take(10)->get(array('category'));
        $blog->view_count = $blog->view_count + 1;
        $blog->save();

        return view('blog.post', compact('blog', 'random','tags'));

    }

    public function create()
    {


        return view('blog.create');
    }

    public function store($lang, Request $request)
    {
        $blog = new Blog();
        $blog->title = $request->get('title');
        $blog->content = $request->get('content');
        $blog->slug = $request->get('slug');
        $blog->meta = $request->get('meta');
        $blog->user_id = $request->user()->id;
        $blog->page_title = $request->get('page_title');
        $blog->category = $request->get('category');
        $blog->created_at = $request->get('date');

        if ($request->has('save')) {
            //$blog->active = 0;
            $message = 'Post saved successfully';
        } else {
            //$blog->active = 1;
            $message = 'Post published successfully';
        }
        $blog->save();
        return redirect($lang . '/blog/' . $blog->slug)->withMessage($message);
    }

    public function edit($lang, Request $request, $slug)
    {
        $blog = Blog::where('slug', $slug)->first();
        $message = "";
        if (request()->has('active')) {
            if ($_GET['active'] == 'Unpublish') {
                $blog->active = '0';
                $blog->save();
                $message = "The blog is unpublished now";

            } else {
                $blog->active = '1';
                $blog->save();
                $message = "The blog is published now";

            }
        }
        return view('blog.edit')->with('blog', $blog)->withErrors($message);
        return redirect('/')->withErrors('you have not sufficient permissions');
    }

    public function update($lang, Request $request, $slug)
    {
        $blog = Blog::where('slug', $slug)->first();
        $blog_id = $blog->id;

        if ($blog && ($blog->user_id == $request->user()->id || $request->user()->is_admin())) {

            $title = $request->input('title');
            $new_slug = $request->input('slug');
            $content = $request->input('content');
            $meta = $request->input('meta');
            $category = $request->input('category');
            $date = $request->input('date');
            $page_title = $request->input('page_title');
            $duplicate = Blog::where('slug', $new_slug)->first();
            if ($duplicate) {
                if ($duplicate->id != $blog_id) {
                    return redirect('ar/edit-blog/' . $blog->slug)->withErrors('Slug already exists.')->withInput();
                } else {
                    $blog->slug = $new_slug;
                }
            }
            $blog->meta = $meta;
            $blog->title = $title;
            $blog->content = $request->input('content');
            $blog->slug = $new_slug;
            $blog->category = $category;
            $blog->page_title = $page_title;
            $blog->created_at = $date;
            if ($request->has('save')) {
                $message = 'Post updated successfully';
                $landing = 'ar/edit-blog/' . $blog->slug;

            } else {
                $message = 'Post updated successfully';
                $landing = ar / blog / $blog->slug;
            }
            $blog->save();

            return redirect($landing)->with('status', $message);
        } else {
            return redirect('/')->withErrors('you have not sufficient permissions');
        }
    }

    public function destroy($lang, $slug)

    {

        Blog::where('slug', $slug)->delete();

        return redirect($lang . '/blog')
            ->with('success', 'Item deleted successfully');

    }

    public function unpublished($lang, Request $request )
    {
//        if($id)
//        {
//            $case= Blog::find(explode(',', $id));
//            $case->active = '1';
//            $case->save();
//            $message = "The blog is unpublished now";
//
        $message ="";
        if (!empty($_GET['checkbox']))
        {

       foreach ($_GET['checkbox'] as $id) {

               $case= Blog::find(explode(',', $id))->first();
               $case->active = '1';
                $case->save();
                $message = "The blog is published now";
           }
        }
        $blogs = Blog::where('published_at', '<=', Carbon::now())
            ->where('active', '=', '0')->orderBy('created_at', 'desc')->paginate(5);

        return view('blog.unpublished', compact('blogs'))->withErrors($message);

    }

    public function category($lang, $category)

    {

        $tagposts = Blog::whereCategory($category)->where('active', '=', '1')->paginate(5);
        if (!empty($_GET['query'])) {
            $q = $_GET['query'];
            $tagposts = Blog::whereCategory($category)->
            where('title', 'LIKE', '%' . $q . '%')->where('active', '=', '1')->orderBy('created_at', 'desc')->paginate(5);

        }
        $random = Blog::where('published_at', '<=', Carbon::now())
            ->where('active', '=', '1')->orderByRaw('RAND()')->take(5)->get(array('title', 'slug'));
        $tags = Blog::where('active', '=', '1')->orderBy('created_at', 'desc')->distinct()->take(10)->get(array('category'));

        return view('blog.category', compact('tagposts', 'category', 'random', 'tags'));
    }
    
}

?>
