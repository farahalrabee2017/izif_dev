<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use App\subscription;
use App\SubscriptionModel;
use Carbon\Carbon;
use App\Payments;
use App\PaymentMethod;
use App\abandoned_carts;


class TwoCheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $mailchimp;
    protected $listId = 'cf7f32922e';

    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function addEmailToList($email)
    {
        try {
            $this->mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $email]
                );
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
            // do something
            //dd($e);
        } catch (\Mailchimp_Error $e) {
            // do something
            //dd($e);
        }
    }

    public function ipn()
    {
        // Account Id
        $account_id = "202356302";

        $payment_method_id = PaymentMethod::where('name', '2checkout')->first()->id;

        // Collect Post Params
        $insMessage = array();
        foreach ($_POST as $k => $v) {
            $insMessage[$k] = $v;
        }

        // json POST Params to Save in the database
        $request = json_encode($_POST);
        Log::info("New 2checkout transaction Request => $request");

        // Provider Params
        $vendor_id = $insMessage['vendor_id'];
        $invoice_id = $insMessage['invoice_id'];
        $invoice_status = isset($insMessage['invoice_status']) ? $insMessage['invoice_status']: ""; // no
        $fraud_status = isset($insMessage['fraud_status']) ? $insMessage['fraud_status'] : ""; //pass //no
        $message_type = $insMessage['message_type'];  //ORDER_CREATED //RECURRING_INSTALLMENT_SUCCESS
        $customer_email = $insMessage['customer_email'];

        // Add to mailchimp subscribers
        try {
            $this->addEmailToList($customer_email);
        }catch (\Exception $e){}

        $item_id = explode(',', $insMessage['item_id_1']);

        $user_id = $item_id[0];
        $subscription_id = $item_id[1];
        $course_id = $item_id[2];
        $token = $item_id[3];

        try{
            $abandedId = $item_id[4];
        }catch(\Exception $e){}


        if ($vendor_id == $account_id) {
            if ($message_type == "ORDER_CREATED" OR $message_type == "RECURRING_INSTALLMENT_SUCCESS") {


                Log::info("New 2checkout transaction => invoice_id : " . $invoice_id . " | $user_id");


                //Make a payment
                $payment = new Payments;
                $payment->invoice_id = $invoice_id;
                $payment->user_id = $user_id;
                $payment->amount = $insMessage['item_usd_amount_1'];
                $payment->payment_method_id = $payment_method_id;
                $payment->save();

                // Get Payment Id
                $payment_id = Payments::where('invoice_id', $invoice_id)->first()->id;

                // Get Subscription Model
                $subscription_model = SubscriptionModel::find($subscription_id);

                // Create A new Subscription
                $subscription = new subscription;
                $subscription->user_id = $user_id;
                $subscription->subscription_model_id = $subscription_id;
                $subscription->payment_id = $payment_id;
                $subscription->start_date = Carbon::now();
                $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);
                $subscription->status = $fraud_status;

                if ($course_id != '0') {
                    $subscription->course_id = $course_id;
                }

                $subscription->save();

                // Remove from abandend
                try{
                    if(isset($abandedId)){
                        abandoned_carts::where('id',$abandedId)->delete();
                    }
                }catch(\Exception $e){}

            }
        }


    }


    public function add2checkout()
    {
        // Account Id
        $account_id = "202356302";

        $payment_method_id = PaymentMethod::where('name', '2checkout')->first()->id;

        // Collect Post Params
        $insMessage = array();
        $pay = ["message_type" => "ORDER_CREATED", "message_description" => "New order created", "timestamp" => "2016-05-23 12=>55=>01", "message_id" => "5", "vendor_id" => "202356302", "vendor_order_id" => "", "invoice_id" => "105923622828", "recurring" => "1", "invoice_status" => "approved", "invoice_list_amount" => "19.00", "invoice_usd_amount" => "19.00", "invoice_cust_amount" => "19.00", "auth_exp" => "2016-05-30", "fraud_status" => "pass", "list_currency" => "USD", "cust_currency" => "USD", "payment_type" => "credit card", "sale_id" => "105923622819", "sale_date_placed" => "2016-05-23 12=>55=>01", "customer_ip" => "147.147.73.156", "customer_ip_country" => "United Kingdom", "customer_first_name" => "Bisher Abu", "customer_last_name" => "Taleb", "customer_name" => "Bisher Abu Taleb", "customer_email" => "tareq.kwaik@gmail.com", "customer_phone" => "", "ship_status" => "", "ship_tracking_number" => "", "ship_name" => "", "ship_street_address" => "", "ship_street_address2" => "", "ship_city" => "", "ship_state" => "", "ship_postal_code" => "", "ship_country" => "", "bill_street_address" => "123 araar street", "bill_street_address2" => "", "bill_city" => "amman", "bill_state" => "", "bill_postal_code" => "", "bill_country" => "JOR", "item_count" => "1", "item_name_1" => "Monthly Subscription", "item_id_1" => "84,13,0,RzyYXrWBK1Psn4w7vYOrQ7H9ZttPGW0kw5lqSnWQ", "item_list_amount_1" => "19.00", "item_usd_amount_1" => "19.00", "item_cust_amount_1" => "19.00", "item_type_1" => "bill", "item_duration_1" => "Forever", "item_recurrence_1" => "1 Month", "item_rec_list_amount_1" => "19.00", "item_rec_status_1" => "live", "item_rec_date_next_1" => "2016-06-23", "item_rec_install_billed_1" => "1", "md5_hash" => "67CE0B562E8FA225743FEAA32120CE82", "key_count" => "56"];

        foreach ($pay as $k => $v) {
            $insMessage[$k] = $v;
        }

        // json POST Params to Save in the database
        $request = json_encode($_POST);
        Log::info("New 2checkout transaction Request => $request");

        // Provider Params
        $vendor_id = $insMessage['vendor_id'];
        $invoice_id = $insMessage['invoice_id'];
        $invoice_status = $insMessage['invoice_status'];
        $fraud_status = $insMessage['fraud_status']; //pass
        $message_type = $insMessage['message_type'];  //ORDER_CREATED //RECURRING_INSTALLMENT_SUCCESS
        $customer_email = $insMessage['customer_email'];

        // Custom Fields
        /*$subscription_model_id = $insMessage['subscription_id'];
        $course_id = $insMessage['custom_courseid'];
        $user_id = $insMessage['custom_userid'];
        $token = $insMessage['custom_token'];*/

        $item_id = explode(',', $insMessage['item_id_1']);

        $user_id = $item_id[0];
        $subscription_id = $item_id[1];
        $course_id = $item_id[2];
        $token = $item_id[3];


        if ($vendor_id == $account_id && $fraud_status == "pass") {
            if ($message_type == "ORDER_CREATED") {


                Log::info("New 2checkout transaction => invoice_id : " . $invoice_id . " | $user_id");


                //Make a payment
                $payment = new Payments;
                $payment->invoice_id = $invoice_id;
                $payment->user_id = $user_id;
                $payment->amount = $insMessage['item_usd_amount_1'];
                $payment->payment_method_id = $payment_method_id;
                $payment->save();

                // Get Payment Id
                $payment_id = Payments::where('invoice_id', $invoice_id)->first()->id;

                // Get Subscription Model
                $subscription_model = SubscriptionModel::find($subscription_id);

                // Create A new Subscription
                $subscription = new subscription;
                $subscription->user_id = $user_id;
                $subscription->subscription_model_id = $subscription_id;
                $subscription->payment_id = $payment_id;
                $subscription->start_date = Carbon::now();
                $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);

                if ($course_id != '0') {
                    $subscription->course_id = $course_id;
                }

                $subscription->save();

            }
        }


    }


}
