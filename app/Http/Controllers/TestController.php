<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Imagick;
use App\Note;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $sheets = Note::all();
        foreach ($sheets as $sheet) {
            $path = explode('/', $sheet->note_path);
            $path = $path[count($path) - 1];
            $path = str_replace(' ', '+', $path);
            $url = str_replace(' ', '+', $sheet->note_path);
            $url = "https://dt5x436ix2iss.cloudfront.net/$url";
            echo $url;
            if (!file_exists("/var/www/html/izif/public/sheets/$path")) {

                file_put_contents(
                    "/tmp/$path",
                    file_get_contents($url)
                );
                $pdf = "/tmp/" . $path . "[0]";
                $im = new imagick($pdf);
                $im->setImageFormat('jpg');
                $savepath = str_replace('pdf', 'jpg', $path);
                $im->writeImage("/var/www/html/izif/public/sheets/$savepath");

                unlink("/tmp/$path");
            }
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
