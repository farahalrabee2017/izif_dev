<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PagesContainer;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $containers = PagesContainer::orderBy('section_order')->get();
        return view('pages.pages', compact('containers'));
    }

    public function addSection(Request $request)
    {
        $input = $request->all();
        PagesContainer::create($input);
    }

    public function deleteSection(Request $request,$lang, $id)
    {
        $container = PagesContainer::find($id);
        $container->delete();

    }

    public function updateSection(Request $request,$lang, $id)
    {

        $input = $request->all();
        PagesContainer::find($id)->update($input);

    }

    public function thankYou(){
        return view('pages.thankyou');
    }


}
