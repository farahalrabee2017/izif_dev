<?php

namespace App\Http\Controllers;

use App\SubscriptionModel;
use Faker\Provider\de_AT\Payment;
use Illuminate\Http\Request;
use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Group;
use App\Country;
use App\Video;
use App\Course;
use App\subscription;
use App\Instrument;
use App\Testimonial;
use App\Downloadable;
use App\Coupon;
use App\Note;
use App\PaymentMethod;
use App\Payments;
use DB;
use App\VideoStatus;
use App\course_type;
use App\pendingTransactions;
use Carbon\Carbon;
use DateTime;
use App\Tracking;
use App\abandoned_carts;
use Excel;
use Yajra\Datatables\Facades\Datatables;
use App\Program_types;

//use App\SubscriptionModel;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('isadmin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
       //exit(var_dump($to));
        $date1 = GETDATE();
        $date1['mday'];
        $date1['mon'];
        $date1['year'];
        $date2 =  $date1['year'] . "-". $date1['mon'] . "-". $date1['mday'];
       // exit(var_dump($date2));
        if ($from) {
            //convert date to Y-m-d format .

            $from_before = date("Y-m-d", strtotime($from ."-1 month"));
            $to_before = date("Y-m-d", strtotime($to ."-1 month"));
            $total_payments = DB::select(DB::raw("select  count(p.user_id) as total   from payments p join subscriptions s on (s.payment_id = p.id)where p.created_at between '$from' and '$to' and amount  != '0' and amount  != ''"));
            $total_payments_unique = DB::select(DB::raw("select  count(DISTINCT p.user_id) as total   from payments p join subscriptions s on (s.payment_id = p.id)where p.created_at between '$from' and '$to' and amount  != '0' and amount  != ''"));

            //total number of users that renewed their subscriptions.
            $renew = DB::select(DB::raw("SELECT  count(y.user_id) as Renew  from (SELECT DISTINCT t2.user_id  , t3.amount from (SELECT * from (select user_id , created_at from payments WHERE amount != '' and amount != '0' and created_at > '2016-06-01'  )  as t1
WHERE created_at BETWEEN '$from_before' and '$to_before') as t2
join
payments as t3 on t2.user_id = t3.user_id WHERE t3.created_at BETWEEN '$from_before' and '$to_before' and (t3.amount = '19.00' or t3.amount = '19')  ) as y JOIN
(SELECT * from payments WHERE created_at BETWEEN '$from' and '$to') as t on y.user_id = t.user_id WHERE (t.amount = '19' or t.amount = '19.00') "));

            $new = DB::select(DB::raw("SELECT count(DISTINCT t2.user_id) as new from (SELECT * from (select user_id ,min(created_at) as min from payments WHERE amount != '' and amount != '0' and created_at > '2016-06-01'   group by user_id)  as t1
WHERE min BETWEEN '$from' and '$to') as t2
join
payments as t3 on t2.user_id = t3.user_id WHERE t3.created_at BETWEEN '$from' and '$to'  and t3.amount != '0' and t3.amount != ''"));
            //total number of subscripers who watch videos.
            $totalUniqueActive = DB::select(DB::raw("SELECT COUNT( distinct t1.user_id ) as uniqeCount from (SELECT  user_id FROM izif.video_status WHERE created_at  between '$from' and '$to') as t1 join payments as t2 on t1.user_id = t2.user_id "));
            //total number of subscripers whom their subscription was finished and not renewed.
            $canceled = DB::select(DB::raw("SELECT  count(dd.user_id) as canceled from (SELECT DISTINCT t2.user_id  , t3.amount from (SELECT * from (select user_id , created_at from payments WHERE amount != '' and amount != '0' and created_at > '2016-06-01'  )  as t1
WHERE created_at BETWEEN '$from_before' and '$to_before') as t2
left join
payments as t3 on t2.user_id = t3.user_id WHERE t3.created_at BETWEEN '$from_before' and '$to_before'  and t3.amount = '19' or t3.amount = '19.00') as dd left outer JOIN
(SELECT * from payments WHERE   (amount = '19.00' or amount = '19') and created_at  BETWEEN '$from' and '$to'  ) as ss on dd.user_id = ss.user_id WHERE ss.id is null"));


            //total avg of dollars that users paid .
            //the sum of money that izif paid in a period.
            $marketing =  DB::select(DB::raw("SELECT sum(amount) as mar FROM marketing WHERE  start_date >= '$from'  AND end_date <= '$to'"));
            //total number of users that watch videos without subscription.
            $no_sub = DB::select(DB::raw("SELECT count(t1.user_id) as num from (SELECT DISTINCT user_id , u.created_at FROM `video_status` JOIN users as u on video_status.user_id = u.id) as t1 left JOIN subscriptions as t2 on t1.user_id = t2.user_id WHERE t2.user_id is null AND t1.created_at BETWEEN '$from' AND '$to' "));

            $total_subscriptions1 = DB::select(DB::raw("select
                                                    sum(p.amount) as sumation, avg(p.amount) as avg
                                                    from payments p join subscriptions s on (s.payment_id = p.id)
                                                    where p.created_at between '$from' and '$to';"));


            $unique =  $unique = DB::select(DB::raw("SELECT  count( DISTINCT y.user_id) as unique1  from (SELECT DISTINCT t2.user_id  , t3.amount from (SELECT * from (select user_id , created_at from payments WHERE amount != '' and amount != '0' and created_at > '2016-06-01'  )  as t1
WHERE created_at BETWEEN '$from_before' and '$to_before') as t2
join
payments as t3 on t2.user_id = t3.user_id WHERE t3.created_at BETWEEN '$from_before' and '$to_before'   and (t3.amount = '19.00' or t3.amount = '19')  ) as y JOIN
(SELECT * from payments WHERE created_at BETWEEN '$from' and '$to') as t on y.user_id = t.user_id WHERE (t.amount = '19' or t.amount = '19.00') "));


        } else {

            $unique = DB::select(DB::raw("select count(DISTINCT ee.user_id) as unique1 from (select t3.user_id,t3.min,num_of_subscriptions from (select MIN(s.created_at) as min,(select count(user_id) from payments t1 where t1.user_id = s.user_id and t1.created_at BETWEEN MIN(s.created_at) AND '$date2' )as num_of_subscriptions, s.user_id,u.created_at from users u join payments s on u.id = s.user_id WHERE u.created_at > '2016-06-01' GROUP by s.user_id  ) as t3 WHERE t3.num_of_subscriptions > 1) as ee join payments as ww on ee.user_id = ww.user_id WHERE  ww.amount !=0  "));

            $from_date1 = "$from first day of last month";
            $from_date2 =  date_create($from_date1);
            $from_before = $from_date2 -> format('Y-m-d') ;
            $to_date1 = "$to first day of last month";
            $to_date2 =  date_create($to_date1);
            $to_before = $to_date2 -> format('Y-m-d') ;
            $total_payments = DB::select(DB::raw("select  count(p.user_id) as total   from payments p join subscriptions s on (s.payment_id = p.id)where  amount  != '0' and amount  != '' and p.created_at > '2016-06-01'"));
            $total_payments_unique = DB::select(DB::raw("select  count(DISTINCT user_id) as total   from payments where  amount  != '0' and amount  != '' and created_at > '2016-06-01'"));

            $total_subscriptions1 = DB::select(DB::raw("select
                                                    sum(p.amount) as sumation  , avg(p.amount) as avg
                                                    from payments p join subscriptions s on (s.payment_id = p.id)
                                                    "));
            $from_date2 = "'$from.' first day of last month";
            $from_before =  date_create($from_date2);
            $to_date2 = "'$to.' last day of last month";
            $to_before =  date_create($to_date2);
            $renew = DB::select(DB::raw("select count(ww.user_id) as Renew from (select t3.user_id,t3.min,num_of_subscriptions from (select MIN(s.created_at) as min,(select count(user_id) from payments t1 where t1.user_id = s.user_id and t1.created_at BETWEEN MIN(s.created_at) AND '$date2' and t1.amount = '19.00' )as num_of_subscriptions, s.user_id,u.created_at from users u join payments s on u.id = s.user_id WHERE u.created_at > '2016-06-01' GROUP by s.user_id  ) as t3 WHERE t3.num_of_subscriptions > 1) as ee join payments as ww on ee.user_id = ww.user_id WHERE  ww.amount !=0  and ww.amount = '19.00'  "));
            $new = DB::select(DB::raw("SELECT count(DISTINCT user_id) as new FROM `payments` WHERE amount != '' and amount != '0' and created_at > '2016-06-01'"));
            $totalUniqueActive = DB::select(DB::raw("SELECT COUNT( distinct t1.user_id ) as uniqeCount from (SELECT  * FROM izif.video_status ) as t1 JOIN payments as t2 on t1.user_id = t2.user_id "));
            $canceled = DB::select(DB::raw("SELECT COUNT(s.user_id) as canceled from ( SELECT COUNT(user_id) as count,user_id, max(end_date) as MaxDate FROM `subscriptions` GROUP by user_id ) as s JOIN subscriptions as t on s.user_id = t.user_id WHERE s.MaxDate = t.end_date AND s.MaxDate  < '$date2'  and t.created_at > '2016-06-01' "));
            $avg =  DB::select(DB::raw("select sum(sum) as income , AVG(sum) as avg  from (SELECT user_id , sum(subscription_models.display_price) as sum FROM `subscriptions` join subscription_models on subscriptions.subscription_model_id = subscription_models.id GROUP BY subscriptions.user_id) as t"));
            $marketing =  DB::select(DB::raw("SELECT sum(amount) as mar FROM marketing "));
            $no_sub = DB::select(DB::raw("SELECT count(t1.user_id) as num from (SELECT DISTINCT user_id FROM `video_status` JOIN users on video_status.user_id = users.id) as t1 left JOIN subscriptions as t2 on t1.user_id = t2.user_id WHERE t2.user_id is null"));

        }
        //marketing dashboard

        $total1 = 0;
        foreach ($total_subscriptions1 as $sub) {
            $total1 = $total1 + $sub->sumation;
        }
        $users = User::where('user_group_id', 1)->count();
        $videos = Video::all()->count();
        $subscription = subscription::all()->count();

        $subscription_models = SubscriptionModel::with('SubscriptionUsers')->get();
        //exit(var_dump($subscription_models));

        $courses = Course::all()->count();

        $activeusers = DB::table('video_status')
            ->select('*')
            ->whereRaw('Date(created_at) = CURDATE()')
            ->groupBy('user_id')
            ->get();


        $total_subscriptions = DB::select(DB::raw("select
                                                   sum(p.amount) as sumation
                                                   from payments p join subscriptions s on (s.payment_id = p.id);"));


        $total = 0;
        foreach ($total_subscriptions as $sub) {
            $total = $total + $sub->sumation;
        };

        return view('admin.main',  compact('total_payments_unique','unique','sales','total_payments','new' ,'renew' ,'totalUniqueActive','canceled','avg','marketing' , 'no_sub','total1','total_subscriptions1','subscription_models', 'users', 'videos', 'subscription', 'courses', 'activeusers', 'total'));    }

    /* ------------------------ Users ------------------------------------- */

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function users()
    {
        DB::connection()->disableQueryLog();
        $users = User::all();
        return view('admin.user.users', compact('users'));
    }

    public function usersAjaxView()
    {
        return view('admin.user.users-ajax');
    }


    public function usersAjax()
    {

        $users = DB::table('users')->select(['id', 'name', 'email', 'created_at', 'updated_at']);
        return Datatables::of($users)->addColumn('Edit', function ($users) {
            return "<a href='/ar/admin/user/edit/$users->id' class='btn btn-block btn-info btn-xs'>Edit</a>";
        })->addColumn('Delete', function ($users) {
            return "<a href='/ar/admin/user/delete/$users->id' class='btn btn-block btn-danger btn-xs'>Delete</a>";
        })->make(true);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUser($lang, $id)
    {
        $user = User::find($id);
        $country = Country::lists('country_name', 'id');
        $group = Group::lists('name', 'id');
        return view('admin.user.edit', compact('user', 'country', 'group'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editUserInfo(Request $request, $lang, $id)
    {
        $input = $request->all();

        $profile_image = $request->file('profile_image');
        $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));

        if ($profile_image) {
            if ($profile_image->isValid()) {
                $move = $profile_image->move($path, $profile_image->getClientOriginalName());
                $input['profile_image'] = '/uploads/' . $profile_image->getClientOriginalName();
            }
        }


        User::find($id)->update($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/usersAjaxView');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteUser($lang, $id)
    {

        // Delete Any Tracking
        $tracking = Tracking::where('user_id', $id);
        $tracking->delete();

        // Delete Video Status
        $video_status = VideoStatus::where('user_id', $id);
        $video_status->delete();

        $abandoned_carts = App\abandoned_carts::where('user_id', $id);
        $abandoned_carts->delete();

        // Delete Subscriptions
        $subscriptions = subscription::where('user_id', $id);
        $subscriptions->delete();

        // delete user
        $user = User::find($id);
        $user->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/users');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createUser()
    {
        $country = Country::lists('country_name', 'id');
        $group = Group::lists('name', 'id');
        return view('admin.user.create', compact('country', 'group'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createUserInfo(Request $request)
    {
        $input = $request->all();
        $profile_image = $request->file('profile_image');
        $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));

        if ($profile_image) {
            if ($profile_image->isValid()) {
                $move = $profile_image->move($path, $profile_image->getClientOriginalName());
                $input['profile_image'] = '/uploads/' . $profile_image->getClientOriginalName();
            }
        }

        User::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/users');
    }

    /* ------------------------ Groups ------------------------------------- */

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function groups()
    {
        $groups = Group::all();
        return view('admin.group.groups', compact('groups'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createGroup()
    {
        return view('admin.group.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createGroupInfo(Request $request)
    {
        $input = $request->all();
        Group::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/groups');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editGroup(Request $request, $lang, $id)
    {
        $input = $request->all();
        Group::find($id)->update($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/groups');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteGroup($lang, $id)
    {
        $group = Group::find($id);
        $group->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/groups');
    }

    /* ------------------------ Courses ------------------------------------- */

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function courses()
    {
        $courses = Course::all();
        return view('admin.courses.list', compact('courses'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createCourse()
    {
        $group_id = Group::where('name', 'teacher')->first();
        $groups = User::where('user_group_id', $group_id->id)->lists('name', 'id');
        $instruments = Instrument::lists('english_name', 'id');
        $all_types = course_type::all();
        $program_types = Program_types::all();
        return view('admin.courses.create', compact('groups','program_types', 'instruments','all_types'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createCourseInfo(Request $request)
    {
        $input = $request->all();
        $file = $request->file('cover_image');
        if ($file) {
            if ($file->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file->move($path, $file->getClientOriginalName());
                $input['cover_image'] = $file->getClientOriginalName();
            }
        }

        Course::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/courses');
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editCourse(Request $request, $lang, $id)
    {

        $course = Course::find($id);
        $group_id = Group::where('name', 'teacher')->first();
        $groups = User::where('user_group_id', $group_id->id)->lists('name', 'id');
        $instruments = Instrument::lists('english_name', 'id');
        $all_types = course_type::all();
        $program_types = Program_types::all();
        return view('admin.courses.edit', compact('course','program_types', 'groups', 'instruments','all_types'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editCourseInfo(Request $request, $lang, $id)
    {
      //$shopId = $_Request['course_type'];

        $input = $request->all();
        $course_type = implode(',',$input['course_type']);
        $input['course_type'] = $course_type;
        $file = $request->file('cover_image');
        if ($file) {
            if ($file->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file->move($path, $file->getClientOriginalName());
                $input['cover_image'] = $file->getClientOriginalName();
                Course::find($id)->update($input);
                return redirect('/' . App::getLocale() . '/admin/courses');
            }
        } else {
            Course::find($id)->update($input);
            $lang = App::getLocale();
            return redirect('/' . $lang . '/admin/courses');
        }
    }

    public function usercourses()
    {

        $user = User::get();

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteCourse($lang, $id)
    {
        $course = Course::find($id);
        $course->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/courses');
    }

    /* ------------------------ Videos ------------------------------------- */

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function videos()
    {
        $videos = Video::with('teacher', 'course')->get();
        return view('admin.videos.list', compact('videos'));
    }

    public function courseVideos($lang, $id)
    {

        $videos = Video::where('course_id', $id)->with('teacher', 'course')->get();
        return view('admin.videos.list', compact('videos'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createVideo()
    {
        $group_id = Group::where('name', 'teacher')->first();
        $groups = User::where('user_group_id', $group_id->id)->lists('name', 'id');
        $instruments = Instrument::lists('english_name', 'id');
        $courses = Course::lists('title_arabic', 'id');
        return view('admin.videos.create', compact('groups', 'instruments', 'courses'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createVideoInfo(Request $request)
    {
        $input = $request->all();

        $file = $request->file('cover_image');
        if ($file) {
            if ($file->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file->move($path, $file->getClientOriginalName());
                $input['cover_image'] = $file->getClientOriginalName();
            }
        }

        Video::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/videos');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editVideo(Request $request, $lang, $id)
    {

        $video = Video::where('id', $id)->first();
        //dd($video);
        $group_id = Group::where('name', 'teacher')->first();
        $groups = User::where('user_group_id', $group_id->id)->lists('name', 'id');
        $instruments = Instrument::lists('english_name', 'id');
        $courses = Course::lists('title_arabic', 'id');
        return view('admin.videos.edit', compact('video', 'groups', 'instruments', 'courses'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editVideoInfo(Request $request, $lang, $id)
    {
        $input = $request->all();
        $file = $request->file('cover_image');
        if ($file) {
            if ($file->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file->move($path, $file->getClientOriginalName());
                $input['cover_image'] = $file->getClientOriginalName();
                Video::find($id)->update($input);

                $lang = App::getLocale();
                return redirect('/' . $lang . '/admin/videos');
            }
        } else {
            Video::find($id)->update($input);

            $lang = App::getLocale();
            return redirect('/' . $lang . '/admin/videos');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteVideo($lang, $id)
    {
        $course = Video::find($id);
        $course->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/videos');
    }

    /* ------------------------ Add Files To Video ------------------------------------- */

    public function AddFiles($lang, $id)
    {

        $files = Downloadable::where('video_id', $id)->get();
        $video = Video::find($id)->first();

        return view('admin.downloadable.create', compact('files', 'id', 'video'));
    }

    public function ListFiles($lang, $id)
    {

        $files = Downloadable::where('video_id', $id)->get();
        $video = Video::find($id)->first();

        return view('admin.downloadable.list', compact('files', 'id', 'video'));
    }

    public function AddFilesInfo(Request $request, $lang)
    {
        $input = $request->except('_token');
        $id = $request->input('video_id');
        $file_path_ara = $request->file('file_path_ara');
        $file_path_enu = $request->file('file_path_enu');

        if ($file_path_ara) {
            if ($file_path_ara->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file_path_ara->move($path, $file_path_ara->getClientOriginalName());
                $input['file_path_ara'] = '/uploads/' . $file_path_ara->getClientOriginalName();
            }
        }


        if ($file_path_enu) {
            if ($file_path_enu->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file_path_enu->move($path, $file_path_enu->getClientOriginalName());
                $input['file_path_enu'] = '/uploads/' . $file_path_enu->getClientOriginalName();
            }
        }


        Downloadable::create($input);

        $lang = App::getLocale();
        return redirect("/$lang/admin/downloadables/list/$id");
    }

    public function editDownloadable($lang, $id, $video_id)
    {

        $file = Downloadable::where('id', $id)->first();
        $video = Video::where('id', $video_id)->first();

        return view('admin.downloadable.edit', compact('file', 'video'));
    }

    public function editDownloadableInfo(Request $request, $lang, $file_id)
    {
        $input = $request->except('_token');
        $id = $request->input('video_id');
        $file_path_ara = $request->file('file_path_ara');
        $file_path_enu = $request->file('file_path_enu');

        if ($file_path_ara) {
            if ($file_path_ara->isValid()) {
                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file_path_ara->move($path, $file_path_ara->getClientOriginalName());
                $input['file_path_ara'] = '/uploads/' . $file_path_ara->getClientOriginalName();
            }
        }


        if ($file_path_enu) {

            if ($file_path_enu->isValid()) {

                $path = str_replace('app/Http/Controllers', 'public/uploads', dirname(__FILE__));
                $move = $file_path_enu->move($path, $file_path_enu->getClientOriginalName());
                $input['file_path_enu'] = '/uploads/' . $file_path_enu->getClientOriginalName();
            } else {
                dd($file_path_enu->getErrorMessage());
            }
        }


        Downloadable::where('id', $file_id)->update($input);


        $lang = App::getLocale();

        return redirect("/$lang/admin/downloadables/list/$id");
    }

    public function deleteDownloadable($lang, $id, $video_id)
    {
        $file = Downloadable::find($id);
        $file->delete();

        $lang = App::getLocale();
        return redirect("/$lang/admin/downloadables/list/$video_id");
    }

    /* ------------------------ Instruments ------------------------------------- */

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function instruments()
    {
        $instruments = Instrument::all();
        return view('admin.instruments.list', compact('instruments'));
    }

    public function courseInstruments($id)
    {

        $instruments = Instrument::where('course_id', $id)->with('teacher', 'course')->get();
        return view('admin.instruments.list', compact('instruments'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createInstrument()
    {

        return view('admin.instruments.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createInstrumentInfo(Request $request)
    {
        $input = $request->all();
        $icon = $request->file('icon');
        $big_image = $request->file('big_image');
        $path = str_replace('app/Http/Controllers', 'public/static/img', dirname(__FILE__));

        if ($icon) {
            if ($icon->isValid()) {
                $move = $icon->move($path, $icon->getClientOriginalName());
                $input['icon'] = '/static/img/' . $icon->getClientOriginalName();
            }
        }

        if ($big_image) {
            if ($big_image->isValid()) {
                $move = $big_image->move($path, $big_image->getClientOriginalName());
                $input['big_image'] = '/static/img/' . $big_image->getClientOriginalName();
            }
        }


        Instrument::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/instruments');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editInstrument(Request $request, $lang, $id)
    {
        $instrument = Instrument::find($id);


        return view('admin.instruments.edit', compact('instrument'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editInstrumentInfo(Request $request, $lang, $id)
    {
        $input = $request->all();
        $icon = $request->file('icon');
        $big_image = $request->file('big_image');
        $path = str_replace('app/Http/Controllers', 'public/static/img', dirname(__FILE__));

        if ($icon) {
            if ($icon->isValid()) {
                $move = $icon->move($path, $icon->getClientOriginalName());
                $input['icon'] = '/static/img/' . $icon->getClientOriginalName();
            }
        }

        if ($big_image) {
            if ($big_image->isValid()) {
                $move = $big_image->move($path, $big_image->getClientOriginalName());
                $input['big_image'] = '/static/img/' . $big_image->getClientOriginalName();
            }
        }

        Instrument::find($id)->update($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/instruments');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteInstrument($lang, $id)
    {
        $course = Instrument::find($id);
        $course->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/instruments');
    }

    /* ------------------------ testimonials ------------------------------------- */

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function testimonials()
    {
        $testimonials = Testimonial::all();
        return view('admin.testimonials.list', compact('testimonials'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createTestimonial()
    {

        return view('admin.testimonials.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createTestimonialInfo(Request $request)
    {
        $input = $request->all();
        $photo = $request->file('photo');
        $path = str_replace('app/Http/Controllers', 'public/static/img', dirname(__FILE__));

        if ($photo->isValid()) {
            $move = $photo->move($path, $photo->getClientOriginalName());
            $input['photo'] = '/static/img/' . $photo->getClientOriginalName();
        }

        Testimonial::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/testimonials');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editTestimonial(Request $request, $lang, $id)
    {
        $testimonial = Testimonial::find($id);


        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editTestimonialInfo(Request $request, $lang, $id)
    {
        $input = $request->all();
        $photo = $request->file('photo');
        $path = str_replace('app/Http/Controllers', 'public/static/img', dirname(__FILE__));

        if ($photo) {
            if ($photo->isValid()) {
                $move = $photo->move($path, $photo->getClientOriginalName());
                $input['photo'] = '/static/img/' . $photo->getClientOriginalName();
            }
        }

        Testimonial::find($id)->update($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/testimonials');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteTestimonial($lang, $id)
    {
        $course = Testimonial::find($id);
        $course->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/testimonials');
    }

    public function createCoupon()
    {
        $paymnet_methods = PaymentMethod::lists('name', 'id');
        $subscriptions = SubscriptionModel::lists('title_ara', 'id');
        return view('admin.coupons.create', compact('paymnet_methods', 'subscriptions'));
    }

    public function createCouponInfo(Request $request)
    {
        $input = $request->all();


        // Convert array to comma saparated value payment_methods
        $payment_methods = $input["payment_methods"];
        $prefix = '';
        $payment_methodsList = "";
        foreach ($payment_methods as $payment_method) {
            $payment_methodsList .= $prefix . '' . $payment_method . '';
            $prefix = ',';
        }
        $input["payment_methods"] = $payment_methodsList;


        // Convert array to comma saparated value subscription_types
        $subscription_types = $input["subscription_types"];
        $prefix = '';
        $subscription_typesList = "";
        foreach ($subscription_types as $subscription_type) {
            $subscription_typesList .= $prefix . '' . $subscription_type . '';
            $prefix = ',';
        }
        $input["subscription_types"] = $subscription_typesList;

        // Save
        Coupon::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/coupons');
    }

    public function editCoupon(Request $request, $lang, $id)
    {
        $paymnet_methods = PaymentMethod::lists('name', 'id');
        $subscriptions = SubscriptionModel::lists('title_ara', 'id');

        $coupon = Coupon::find($id);
        return view('admin.coupons.edit', compact('coupon', 'paymnet_methods', 'subscriptions'));
    }

    public function editCouponInfo(Request $request, $lang, $id)
    {
        $input = $request->all();


        // Convert array to comma saparated value payment_methods
        $payment_methods = $input["payment_methods"];
        $prefix = '';
        $payment_methodsList = "";
        foreach ($payment_methods as $payment_method) {
            $payment_methodsList .= $prefix . '' . $payment_method . '';
            $prefix = ',';
        }
        $input["payment_methods"] = $payment_methodsList;


        // Convert array to comma saparated value subscription_types
        $subscription_types = $input["subscription_types"];
        $prefix = '';
        $subscription_typesList = "";
        foreach ($subscription_types as $subscription_type) {
            $subscription_typesList .= $prefix . '' . $subscription_type . '';
            $prefix = ',';
        }
        $input["subscription_types"] = $subscription_typesList;

        // Save
        Coupon::find($id)->update($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/coupons');
    }

    public function deleteCoupon($lang, $id)
    {
        $coupon = Coupon::find($id);
        $coupon->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/coupons');
    }

    public function coupons()
    {
        $coupons = Coupon::all();
        return view('admin.coupons.list', compact('coupons'));
    }

    /** Notes * */
    public function createNotes()
    {

        $courses = Course::lists('title_arabic', 'id');
        $courses['0'] = 'no course';
        return view('admin.notes.create', compact('courses'));
    }

    public function createNotesInfo(Request $request)
    {
        $input = $request->all();
        Note::create($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/notes');
    }

    public function editNotes(Request $request, $lang, $id)
    {
        $note = Note::find($id);
        $courses = Course::lists('title_arabic', 'id');
        $courses['0'] = 'no course';
        return view('admin.notes.edit', compact('note', 'courses'));
    }

    public function editNotesInfo(Request $request, $lang, $id)
    {
        $input = $request->all();
        Note::find($id)->update($input);

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/notes');
    }

    public function deleteNotes($lang, $id)
    {
        $note = Note::find($id);
        $note->delete();

        $lang = App::getLocale();
        return redirect('/' . $lang . '/admin/notes');
    }

    public function notes()
    {
        $sheets = Note::all();
        return view('admin.notes.list', compact('sheets'));
    }

    /* create subscription */

    public function subscriptions(Request $request)
    {
        $subscriptions = subscription::where('id', '>', '0')->with('subscriptions_model', 'course', 'user')->get();
        return view('admin.subscriptions.list', compact('subscriptions'));
    }

    public function fraud(Request $request)
    {
        $subscriptions = subscription::where('status', '!=', 'pass')->with('subscriptions_model', 'course', 'user')->get();
        return view('admin.subscriptions.fraud', compact('subscriptions'));
    }

    public function activate(Request $request, $lang, $id)
    {
        $subscription = subscription::where('id', $id)->first();
        $subscription->status = 'pass';
        $subscription->save();

        return redirect('/ar/admin/subscriptions/fraud');
    }

    public function createSubscriptions(Request $request)
    {
        $subscriptions = subscription::all();
        $users = User::lists('email', 'id');
        $subscription_model = SubscriptionModel::lists('title_ara', 'id');
        $courses = Course::lists('title_arabic', 'id');
        $courses['0'] = 'no course';
        $payment_method = App\PaymentMethod::lists('name', 'id');
        return view('admin.subscriptions.create', compact('subscriptions', 'users', 'subscription_model', 'courses', 'payment_method'));
    }

    public function createSubscriptionsInfo(Request $request)
    {
        //Make a payment
        $payment = new Payments;
        $payment->invoice_id = $request->input('invoice_id');
        $payment->user_id = $request->input('user_id');
        $payment->amount = $request->input('amount');
        $payment->payment_method_id = $request->input('payment_method_id');
        $payment->save();

        // Get Payment Id
        $payment_id = $payment->id;


        // Create A new Subscription
        $subscription = new subscription;
        $subscription->user_id = $request->input('user_id');
        $subscription->subscription_model_id = $request->input('subscription_model_id');
        $subscription->payment_id = $payment_id;
        $subscription->start_date = $request->input('start_date');
        $subscription->end_date = $request->input('end_date');

        if ($request->input('course_id') != '0') {
            $subscription->course_id = $request->input('course_id');
        }

        $subscription->save();

        return redirect('/ar/admin/subscriptions');
    }

    function deleteSubscriptions($lang, $id)
    {

        $subscription = subscription::find($id);
        $subscription->delete();
        return redirect('/ar/admin/subscriptions');
    }

    /**
     *
     * return interactions view
     */
    public function interactions()
    {
        return view('admin.interactions.list');
    }

    public function getUserInteractions(Request $request)
    {
        $from = $request->input('fromMonth');
        $to = $request->input('toMonth');

//        $users = VideoStatus::groupBy('crea_month','crea_monthh')
//                ->whereBetween('created_at', array(new DateTime($from), new DateTime($to)))
//                ->orderBy('created_at')
//                ->get(['user_id', 'course_id', DB::raw('DAYNAME(created_at) as crea_month'),DB::raw('MONTHNAME(created_at) as crea_monthh'), DB::raw('count(distinct user_id) as count'), DB::raw('year(created_at) as year')]);

        $users = User::groupBy('crea_month', 'crea_monthh', 'daynumber','year')
            ->where('referal_account', '!=', '')
            ->whereBetween('created_at', array(new DateTime($from), new DateTime($to)))
            ->orderBy('created_at')
            ->get(['id', DB::raw('DAYNAME(created_at) as crea_month'), DB::raw('DAY(created_at) as daynumber'), DB::raw('MONTHNAME(created_at) as crea_monthh'), DB::raw('count(id) as count'), DB::raw('year(created_at) as year')]);

        // subscripers
//        $subscripers = VideoStatus::groupBy('crea_month', 'crea_monthh','daynumber')
//                ->whereBetween('created_at', array(new DateTime($from), new DateTime($to)))
//                ->orderBy('created_at')
//                ->get(['id', 'created_at', DB::raw('DAYNAME(created_at) as crea_month'),DB::raw('DAY(created_at) as daynumber'), DB::raw('MONTHNAME(created_at) as crea_monthh'), DB::raw('count(distinct user_id ) as countt'), DB::raw('year(created_at) as started_year')]);
//        $subscripers = VideoStatus::with('video')
//        $subscripers = Video::with('videostatus')
//                ->groupBy('crea_month', 'crea_monthh', 'daynumber')
//                ->whereBetween('created_at', array(new DateTime($from), new DateTime($to)))
//                ->orderBy('created_at')
//                ->get(['id', 'created_at', DB::raw('DAYNAME(created_at) as crea_month'), DB::raw('DAY(created_at) as daynumber'), DB::raw('MONTHNAME(created_at) as crea_monthh'), DB::raw('count(distinct user_id ) as countt'), DB::raw('year(created_at) as started_year')]);
//
//        $subscripers = Video::with('videostatus')
//                ->whereHas('videostatus', function ($query) {
//                    $query->whereBetween('created_at', array(new DateTime('2016-08-21 00:00:00'), new DateTime('2016-08-24 00:00:00')));
//                    $query->where('status', 'started');
//                    $query->where('is_open', '1');
//                    $query->groupBy('crea_month', 'crea_monthh', 'daynumber');
//                    $query->orderBy('created_at');
//                })
//                ->get(['id', 'created_at', DB::raw('DAYNAME(created_at) as crea_month'), DB::raw('DAY(created_at) as daynumber'), DB::raw('MONTHNAME(created_at) as crea_monthh'), DB::raw('count(distinct user_id ) as countt'), DB::raw('year(created_at) as started_year')]);
//        $subscripers = DB::table('video_status')
//                ->join('videos', 'videos.id', '=', 'video_status.video_id')
//                ->select('video_status.created_at', DB::raw('DAYNAME(video_status.created_at) as crea_month'), DB::raw('DAY(video_status.created_at) as daynumber'), DB::raw('MONTHNAME(video_status.created_at) as crea_monthh'), DB::raw('count(distinct video_status.user_id ) as countt'), DB::raw('year(video_status.created_at) as started_year'))
//                ->where('videos.is_open',0)
//                ->where('video_status.status', '=', 'started')
//                ->whereBetween('video_status.created_at', array(new DateTime($from), new DateTime($to)))
//                ->groupBy('crea_month', 'crea_monthh', 'daynumber')
//                ->orderBy('created_at')
//                ->get(['video_status.created_at', DB::raw('DAYNAME(video_status.created_at) as crea_month'), DB::raw('DAY(video_status.created_at) as daynumber'), DB::raw('MONTHNAME(video_status.created_at) as crea_monthh'), DB::raw('count(distinct video_status.user_id ) as countt'), DB::raw('year(video_status.created_at) as started_year')]);

//        $subscripers = VideoStatus::select(DB::raw("count(user_id) as countt , daynow , crea_month , daynumber , crea_monthh , started_year"))
//            ->from(DB::raw("(select distinct user_id , date_format(created_at, '%m-%d-%Y') as daynow ,DAYNAME(video_status.created_at) as crea_month,DAY(video_status.created_at) as daynumber,MONTHNAME(video_status.created_at) as crea_monthh ,year(video_status.created_at) as started_year
//                    from izif.video_status
//                    where
//                    date_format(created_at, '%m-%d-%Y') between date_format('" . $from . "', '%m-%d-%Y') and date_format('" . $to . "', '%m-%d-%Y')) tbl "))
//            ->groupBy("daynow")
//            ->get();
        $subscripers = DB::select(DB::raw(" select count(user_id) as countt , daynow , daynumber , crea_monthh , started_year from (select distinct user_id , date_format(created_at, '%Y-%m-%d') as daynow ,DAYNAME(video_status.created_at) as crea_month,DAY(video_status.created_at) as daynumber,MONTHNAME(video_status.created_at) as crea_monthh ,year(video_status.created_at) as started_year from izif.video_status ) tbl WHERE daynow BETWEEN '$from' and '$to' GROUP by daynow,daynumber,crea_monthh,started_year"));


//        $totalUniqueActive = DB::select(DB::raw
//        ("SELECT  COUNT( distinct user_id ) as uniqeCount
//                          FROM izif.video_status
//                          WHERE
//                          date_format(created_at, '%m-%d-%Y') between date_format('" . $from . "', '%m-%d-%Y')
//                          and date_format('" . $to . "', '%m-%d-%Y');"));
        $totalUniqueActive = DB::select(DB::raw
        ("SELECT  COUNT( distinct user_id ) as uniqeCount
                          FROM izif.video_status
                          WHERE
                          created_at between '$from'  and
                           '$to'"));
        return json_encode(array('users' => $users, 'subscripers' => $subscripers, 'totalUniqueActive' => $totalUniqueActive , 'from' => $from));
    }

    public function pendingTransactions()
    {
        $pendingTransactionData = pendingTransactions::with('SubscriptionModel')->get();
        return view('admin.pendingTransactions.list', compact('pendingTransactionData'));
    }

    // to make a subscription for user
    public function activatePendingTransaction(Request $request, $lang, $id)
    {
        pendingTransactions::find($id)->update(['is_active' => 1]);
        $pendingData = pendingTransactions::where('id', $id)->get();
        $paymentMethod = PaymentMethod::where('name', $pendingData[0]->transaction_type)->get();
        if (isset($pendingData[0]->user_id)) {
            $payment = new Payments;
            $payment->invoice_id = 'offline payment'; // offline payment
            $payment->user_id = $pendingData[0]->user_id; // user id
            $payment->amount = $pendingData[0]->amount; // amount
            $payment->payment_method_id = $paymentMethod[0]->id; // id for payment method westenunion
            $payment->save();

            $subscription_model = SubscriptionModel::find($pendingData[0]->subscription_model_id);

            // Create A new Subscription
            $subscription = new subscription;
            $subscription->user_id = $pendingData[0]->user_id;
            $subscription->subscription_model_id = $pendingData[0]->subscription_model_id;
            $subscription->payment_id = $payment->id;
            $subscription->start_date = Carbon::now();
            $subscription->end_date = Carbon::now()->addDays($subscription_model->time_period_in_days);

            $subscription->save();
        }
        return back();
    }

    // deleting user from pending tranaction
    public function deletePendingTransaction(Request $request, $lang, $id)
    {
        $pendingTransaction = pendingTransactions::find($id);
        $pendingTransaction->delete();
        return back();
    }

    /*     * **************************** Tracking **************************** */

    public function tracking()
    {
        $tracking = Tracking::distinct()->with('user', 'subscription')->where("user_id", "!=", "anonymous")->groupBy('user_id')->get();
        return view('admin.tracking.list', compact('tracking'));
    }

    // get total subscriptions filtering by date
    public function getTotalSubscriptions(Request $request)
    {

        $from = $request->input('fromDate');
        $to = $request->input('toDate');
        $from = date("Y-m-d", strtotime($from));
        $to = date("Y-m-d", strtotime($to));
        $total_subscriptions = DB::select(DB::raw("select
                                                    sum(p.amount) as sumation, avg(p.amount) as avg
                                                    from payments p join subscriptions s on (s.payment_id = p.id)
                                                    where p.created_at between '$from' and '$to';"));
        $total = 0;
        foreach ($total_subscriptions as $sub) {
            $total = $total + $sub->sumation;
        }


        //return view('admin.main',  compact('unique','sales','from','to','total_payments','new' ,'renew' ,'totalUniqueActive','canceled','avg','marketing' , 'no_sub','total1','total_subscriptions1','total'));

       return json_encode($total);
    }

    /* sales Dashboard */

    public function sales(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');


        if ($from) {
//            $sales = DB::select(DB::raw("select
// s.*,t.*,u.*,p.*,c.country_name, count(s.user_id) as num_of_subscriptions
//FROM
//subscriptions s join users u on (s.user_id= u.id )
//left join payments p on (p.id = s.payment_id)
//left join countries c on (c.id = u.country_id)
//left join traking t on (s.user_id = t.real_user_id)
//WHERE (s.created_at BETWEEN '$from' AND '$to')
//group by s.user_id ;"));
//
            // removed group by user_id
            $sales = DB::select(DB::raw("select s.*,u.*,t.*,c.country_name,p.*,courses.title_arabic as course_title,sm.title_ara as subtypename,
                                        (select count(user_id) from subscriptions t1 where t1.user_id = s.user_id) as num_of_subscriptions
                                        from users u join subscriptions s on (u.id = s.user_id)
                                        join payments p on (p.id = s.payment_id)
                                        left join traking t on (s.user_id = t.real_user_id)
                                        left join countries c on (c.id = u.country_id)
                                        left join courses courses on (courses.id = s.course_id)
                                        left join subscription_models sm on (sm.id = s.subscription_model_id)
                                        WHERE (s.created_at BETWEEN '$from' AND '$to')
                                        order by s.user_id;"));
        } else {
            // removed group by user_id
            $sales = DB::select(DB::raw("select s.*,u.*,t.*,c.country_name,p.*,courses.title_arabic as course_title,sm.title_ara as subtypename,
                                        (select count(user_id) from subscriptions t1 where t1.user_id = s.user_id) as num_of_subscriptions
                                         from users u join subscriptions s on (u.id = s.user_id)
                                        join payments p on (p.id = s.payment_id)
                                        left join traking t on (s.user_id = t.real_user_id)
                                        left join countries c on (c.id = u.country_id)
                                        left join courses courses on (courses.id = s.course_id)
                                        left join subscription_models sm on (sm.id = s.subscription_model_id)
                                        order by s.user_id;"));
        }

        return view('admin.sales.list', compact('sales'));
    }

    public function getRequestedInstruments()
    {
        $requestedInstruments = DB::select(DB::raw
        ("SELECT ri.id , ri.name , ri.email , ri.level , ri.city , c.country_name as country ,
                  i.english_name as instrument , ri.is_active
                  FROM
                  instrument_request ri left join instruments i on (ri.instrument_id = i.id)
                  left join countries c on (ri.country_id = c.id);"));

        return view('admin.requestedInstrument.list', compact('requestedInstruments'));
    }

    public function instrumentsDelivered(Request $request)
    {
        DB::table('instrument_request')
            ->where('id', $request->input('id'))
            ->update(array('is_active' => 1));
        return json_decode(1);
    }

    public function instrumentsNotDelivered(Request $request)
    {
        DB::table('instrument_request')
            ->where('id', $request->input('id'))
            ->update(array('is_active' => 0));
        return json_decode(1);
    }

    public function getabandonedCardInfo()
    {
        $abandoneCardInfo = App\abandoned_carts::with('user', 'subscriptions_model', 'course')->get();


        /* DB::select(DB::raw
          ("SELECT
          u.name  , ab.payment_method  , sm.title_enu , c.title_english
          FROM
          abandoned_carts ab left join users u on (ab.user_id = u.id)
          left join subscription_models sm on (ab.subscription_model_id = sm.id)
          left join courses c on (ab.course_id = c.id);"));
         */

        return view('admin.abandoneCard.list', compact('abandoneCardInfo'));
    }

    public function getSubscriptionByDate(Request $request)
    {
        $from = $request->input('fromDate');
        $to = $request->input('toDate');
        $from = date("Y-m-d", strtotime($from));
        $to = date("Y-m-d", strtotime($to));

        // get users between dates
//        $subscriptionModel = SubscriptionModel::with(['SubscriptionUsers' => function($q) use ($from, $to) {
//                        $q->whereBetween('created_at', array(new DateTime($from), new DateTime($to)));
//                        $q->groupBy('user_id');
//                    }])->get();

        $subscriptionModel = DB::select(DB::raw
        ("SELECT
                                          s.user_id, s.subscription_model_id
                                          FROM
                                          subscriptions s join payments p
                                          on (p.id = s.payment_id)
                                          WHERE p.amount !=0 and p.amount != '' and
                                          p.created_at between '$from' and '$to'
                                          GROUP BY p.user_id
                                          ORDER BY s.subscription_model_id;"));

        return json_encode($subscriptionModel);
    }

    public function user_completed_videos($lang, $id)
    {
        $user = User::find($id);
        $result = DB::select(DB::raw
        ("SELECT
                  *
                  FROM
                  (SELECT a.title_english,videot,b.url_identifier AS Cid,Vid, a.user_id , b.title_arabic AS ctit, a.instrument_id ,video_id , a.updated_at AS ViewD, status , course_id
                  FROM
                  (SELECT s.instrument_id,s.url_identifier AS Vid ,s.title_arabic AS videot ,s.title_english, p.user_id, p.status ,p.updated_at, p.video_id , s.id ,s.course_id
                  FROM videos s
                  JOIN video_status p
                  ON (p.video_id = s.id)
                  WHERE p.user_id = $id) a
                  JOIN courses b
                  ON (a.course_id = b.id))ff
                  JOIN instruments gg
                  ON (ff.instrument_id = gg.id)
                  ORDER BY status"));
        $total = count($result);
        $payment = Payments::with('payment_method', 'user')->where('user_id', '=', $id)->first();
        $subscription = Subscription::with('user', 'subscriptions_model')->where('user_id', '=', $id)->get();
        $totalw = Course::with('videoStatusCount')->take(3)->get();
        if ($total > 0) {
            return view('admin.user.All-user_videos', compact('session', 'totalw', 'result', 'id', 'total', 'user', 'payment', 'subscription'));

        } else {
            echo "This user didn't watch any video !";

        }
    }

    public function in_array_r($item, $array)
    {
        return preg_match('/"' . $item . '"/i', json_encode($array));
    }

    public function get_users_email(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');

        if ($from) {
            $emails = VideoStatus::with('user')->whereBetween('created_at', array($from, $to))->groupBy('user_id')->get();
        } else {
            $emails = VideoStatus::with('user')->groupBy('user_id')->get();

        }
        return view('admin.interactions.emailslist', compact('users_emails', 'emails'));

    }




    // this function is to export all emails for every result in admin/test
    public function getExport($lang ,Request $request){
        $type = $_GET['type'];


        Excel::create("Export $type", function($excel) {

            $excel->sheet('Sheet 1', function($sheet) {
               // exit(var_dump($_GET['from']));

                if(!empty($_GET['from']))
                {
                    $from = $_GET['from'];
                    $to = $_GET['to'];
                    $from = date("Y-m-d", strtotime($from));
                    $to = date("Y-m-d", strtotime($to));
                }
                else {
                    $from = "";
                    $to = "";

                }
                $date1 = GETDATE();
                $date1['mday'];
                $date1['wday'];
                $date1['year'];
                $date2 =  $date1['year'] . "-". $date1['wday'] . "-". $date1['mday'];
                $from_date1 = "$from first day of last month";
                $from_date2 =  date_create($from_date1);
                $from_before = $from_date2 -> format('Y-m-d') ;
                $to_date1 = "$to first day of last month";
                $to_date2 =  date_create($to_date1);
                $to_before = $to_date2 -> format('Y-m-d') ;


                    if ($_GET['type'] == 'renew') {

                        if ($from)
                        {

                            $new_emails = DB::select(DB::raw("  SELECT DISTINCT email from (select DISTINCT t2.user_id  from (select   p.user_id    from payments p join subscriptions s on (s.payment_id = p.id)
where p.created_at between '$from' and '$to' and amount  != '179.00' and amount  != '99.00' and amount  != '59.00' and amount  != '89.00' and amount  != '38' and amount  != '0' and amount  != '' and amount  != '79.00' and  p.created_at  >= '2016-06-01' ) as t1 join
(select   p.user_id   from payments p join subscriptions s on (s.payment_id = p.id)
where p.created_at between '$from_before' and '$to_before' and amount  != '179.00' and amount  != '99.00' and amount  != '59.00' and amount  != '89.00' and amount  != '38' and amount  != '0' and amount  != '' and amount  != '79.00' and  p.created_at  >= '2016-06-01' ) as t2  WHERE t1.user_id = t2.user_id) as mm join users on mm.user_id = users.id"));


                        }

                        else
                        {
                            $new_emails = DB::select(DB::raw("select DISTINCT email from (select ee.user_id from (select t3.user_id,t3.min,num_of_subscriptions from (select MIN(s.created_at) as min,(select count(user_id) from payments t1 where t1.user_id = s.user_id and t1.created_at BETWEEN MIN(s.created_at) AND '$date2' )as num_of_subscriptions, s.user_id,u.created_at from users u join payments s on u.id = s.user_id WHERE u.created_at > '2016-06-01' GROUP by s.user_id  ) as t3 WHERE t3.num_of_subscriptions > 1) as ee join payments as ww on ee.user_id = ww.user_id WHERE  ww.amount !=0) as l2 join users on l2.user_id = users.id"));

                        }

                }

                if ($_GET['type'] == 'total') {

                    if ($from)
                    {
                        $new_emails = DB::select(DB::raw("select t2.email from (select p.user_id from payments p join subscriptions s on (s.payment_id = p.id) where p.created_at between '$from' and '$to'  and amount != '0' and amount != '') as t1 join users as t2 on t1.user_id = t2.id"));


                    }

                    else
                    {
                        $new_emails =  DB::select(DB::raw("select t2.email from (select p.user_id from payments p join subscriptions s on (s.payment_id = p.id) where amount != '0' and amount != '') as t1 join users as t2 on t1.user_id = t2.id"));


                    }


                }

                if ($_GET['type'] == 'new') {
                    if ($from) {
                        $new_emails = DB::select(DB::raw("select email from (select (select count(user_id) from payments t1 where t1.user_id = s.user_id) as num_of_subscriptions,u.email, s.created_at, s.user_id from users u join payments s on u.id = s.user_id WHERE (date_format(s.created_at, '%Y-%m-%d') BETWEEN '$from' AND '$to') and s.amount !=0 ) as t3 WHERE t3.num_of_subscriptions =1 "));

                    }
                    else
                    {

                        $new_emails = DB::select(DB::raw("select email from (select (select count(user_id) from payments t1 where t1.user_id = s.user_id) as num_of_subscriptions,u.email, s.created_at, s.user_id from users u join payments s on u.id = s.user_id WHERE  s.amount !=0 ) as t3 WHERE t3.num_of_subscriptions =1 "));

                    }

                }
                if ($_GET['type'] == 'students') {
                    if ($from)
                    {

                        $new_emails = DB::select(DB::raw("SELECT DISTINCT users.email from (SELECT * FROM video_status WHERE created_at between '$from' and '$to') as t1 join users on t1.user_id=users.id "));

                    }
                    else
                    {
                        $new_emails = DB::select(DB::raw("SELECT DISTINCT users.email FROM izif.video_status join users on video_status.user_id=users.id "));


                    }


                }
                if ($_GET['type'] == 'canceled') {

                    if ($from)
                    {

                        $new_emails = DB::select(DB::raw("SELECT DISTINCT email from (SELECT s.user_id as canceled from ( SELECT user_id, max(end_date) as MaxDate FROM `subscriptions` GROUP by user_id ) as s JOIN subscriptions as t on s.user_id = t.user_id WHERE s.MaxDate = t.end_date AND s.MaxDate BETWEEN '$from' AND '$to') as t1 join users as u on canceled = u.id "));

                    }
                    else
                    {

                        $new_emails = DB::select(DB::raw("SELECT DISTINCT email from (SELECT s.user_id as canceled from ( SELECT user_id, max(end_date) as MaxDate FROM `subscriptions` GROUP by user_id ) as s JOIN subscriptions as t on s.user_id = t.user_id WHERE s.MaxDate = t.end_date AND s.MaxDate  < '$date2') as t1 join users as u on canceled = u.id "));

                    }

                }
                if ($_GET['type'] == 'income') {

                    if ($from)
                    {
                        $new_emails = DB::select(DB::raw("select d.email,user_id from (SELECT user_id , sum(subscription_models.display_price) as sum FROM `subscriptions` join subscription_models on subscriptions.subscription_model_id = subscription_models.id WHERE subscriptions.start_date  BETWEEN '$from' AND '$to' GROUP BY subscriptions.user_id) as t JOIN users as d on t.user_id = d.id "));

                    }
                    else
                    {
                        $new_emails = DB::select(DB::raw("select d.email,user_id from (SELECT user_id , sum(subscription_models.display_price) as sum FROM `subscriptions` join subscription_models on subscriptions.subscription_model_id = subscription_models.id GROUP BY subscriptions.user_id) as t JOIN users as d on t.user_id = d.id "));


                    }

                }

                if ($_GET['type'] == 'no_sub') {

                    if ($from)
                    {
                        $new_emails = DB::select(DB::raw("SELECT DISTINCT email  from (SELECT DISTINCT user_id , users.email,users.created_at FROM `video_status` JOIN users on video_status.user_id = users.id) as t1 left JOIN subscriptions as t2 on t1.user_id = t2.user_id WHERE t2.user_id is null  AND t1.created_at BETWEEN '$from' AND '$to'   "));


                    }
                    else
                    {
                        $new_emails = DB::select(DB::raw("SELECT DISTINCT email  from (SELECT DISTINCT user_id , users.email,users.created_at FROM `video_status` JOIN users on video_status.user_id = users.id) as t1 left JOIN subscriptions as t2 on t1.user_id = t2.user_id WHERE t2.user_id is null"));

                    }

                }
                if ($_GET['type'] == 'unique') {

                    if ($from)
                    {

                        $new_emails = DB::select(DB::raw("  SELECT DISTINCT email from (select DISTINCT t2.user_id  from (select   p.user_id    from payments p join subscriptions s on (s.payment_id = p.id)
where p.created_at between '$from' and '$to' and amount  != '179.00' and amount  != '99.00' and amount  != '59.00' and amount  != '89.00' and amount  != '38' and amount  != '0' and amount  != '' and amount  != '79.00' and  p.created_at  >= '2016-06-01' ) as t1 join
(select   p.user_id   from payments p join subscriptions s on (s.payment_id = p.id)
where p.created_at between '$from_before' and '$to_before' and amount  != '179.00' and amount  != '99.00' and amount  != '59.00' and amount  != '89.00' and amount  != '38' and amount  != '0' and amount  != '' and amount  != '79.00' and  p.created_at  >= '2016-06-01' ) as t2  WHERE t1.user_id = t2.user_id) as mm join users on mm.user_id = users.id"));


                    }

                    else
                    {
                        $new_emails = DB::select(DB::raw("select DISTINCT email from (select ee.user_id from (select t3.user_id,t3.min,num_of_subscriptions from (select MIN(s.created_at) as min,(select count(user_id) from payments t1 where t1.user_id = s.user_id and t1.created_at BETWEEN MIN(s.created_at) AND '$date2' )as num_of_subscriptions, s.user_id,u.created_at from users u join payments s on u.id = s.user_id WHERE u.created_at > '2016-06-01' GROUP by s.user_id  ) as t3 WHERE t3.num_of_subscriptions > 1) as ee join payments as ww on ee.user_id = ww.user_id WHERE  ww.amount !=0) as l2 join users on l2.user_id = users.id"));

                    }


                }
                foreach ($new_emails as $emails) {
                    $data[] = array(
                        $emails->email,

                    );
                }
                $sheet->fromArray($data);
            });
        })->export('xls');
    }

    public function test ()
    {
      return view('admin.courses.test');


    }
}
