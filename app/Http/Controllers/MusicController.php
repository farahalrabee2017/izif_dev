<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Note;
use App\SubscriptionModel;
use App\subscription;
use Auth;
use Cache;


class MusicController extends Controller
{
    public function sheets(){


        $access = false;
        // Get subscriptions
        $subscriptions = [];
        $courses_bought = [];
        if (Auth::Check()) {
            $limitedAccessSubscriptionId = SubscriptionModel::where('is_full_access', '0')->first()->id;
            $courses_bought = subscription::where(['user_id' => Auth::user()->id])->where('course_id','!=','')->with('subscriptions_model')->get();
            $subscriptions = subscription::where('user_id', Auth::user()->id)->where('subscription_model_id', '!=', $limitedAccessSubscriptionId)->with('subscriptions_model')->get();
        }

        if(!Auth::Guest()) {
            if (count($subscriptions) > 0) {
                $notes = Cache::remember('SubscribedNotes', 60, function(){
                    return Note::orderBy('order_number', 'desc')->get();
                });
                //$notes = Note::orderBy('order_number', 'desc')->get();
            } else {

                $courses = array();

                foreach ($courses_bought as $id) {
                    $courses[] = $id->course_id;
                }

                //$notes = Note::whereIn('course_id',$courses)->orderBy('order_number', 'desc')->get();
                //$notes = Note::orderBy('order_number', 'desc')->get();
                $notes = Cache::remember('SubscribedNotes', 60, function(){
                    return Note::orderBy('order_number', 'desc')->get();
                });
            }
            if (count($subscriptions) > 0 OR count($courses_bought) > 0) {
                $access = true;
            }
        }else{
            //$notes = Note::orderBy('order_number', 'desc')->get();
            $notes = Cache::remember('UnsubscribedNotes', 60, function(){
                return Note::orderBy('order_number', 'desc')->get();
            });

        }

        return view('pages.notes',compact('notes','access'));
    }

    public function sheet($lang,$id){

        $note = Note::where('id',$id)->first();
        return view('pages.note',compact('note'));
    }
}
