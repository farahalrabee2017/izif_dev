<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class Test extends Controller
{

    public function index(Request $request){
        return view('pages.test');
    }
    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function send(Request $request)
    {
        $user = User::find(1);

        Mail::send('emails.test', ['user' => $user], function ($m) use ($user) {
            $m->from('info@VentureHive.com', 'Venture Hive');

            $m->to('hassanalnator@gmail.com', 'Hasan Alnatour')->subject('Venture Hive U Program');
        });
    }
}