<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SubscriptionModel;
use App\SubscriptionModelFeatures;
use Lang;

class EditableController extends Controller
{

    public function __construct()
    {
       // $this->middleware('isadmin');
    }

    public function addSubscription(Request $request){
        $inputs = $request->all();
        //SubscriptionModel::create($inputs);
        return redirect('subscribe');
    }

    public function EditSubscription(Request $request, $id){

    }

    public function DeleteSubscription(Request $request){
        $id = $request->input('id');
        //SubscriptionModelFeatures::where('subscription_model_id',$id)->delete();
        //SubscriptionModel::find($id)->delete();


    }

    public function AddSubscriptionFeature(Request $request){
        $inputs = $request->all();
        //SubscriptionModelFeatures::create($inputs);
        return redirect('subscribe');
    }

    public function EditSubscriptionFeature(Request $request ){
        $id = $request->input('name');
        $value = $request->input('value');

        if(Lang::getLocale() == "ar") {
            //SubscriptionModelFeatures::find($id)->update(['title_ara' => $value]);
        }else{
            //SubscriptionModelFeatures::find($id)->update(['title_enu' => $value]);
        }
        echo $value;
    }

    public function DeleteSubscriptionFeature(Request $request){
        $id = $request->input('id');
        //SubscriptionModelFeatures::find($id)->delete();

    }
}
