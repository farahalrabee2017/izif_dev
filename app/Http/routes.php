<?php
use App\Course;
use App\CourseStatus;
use App\User;
use App\Video;
use App\VideoStatus;

/* * ***************************************************************************************
 * IZIF APPLICATION MAIN ROUTES
 * ALL ROUTES ARE INSIDE A ROUTE GROUP TO HANDLE LOCALIZATION
 * VIA A MIDDLEWARE CALLED LOCALIZATION MIDDLEWARE
 * *************************************************************************************** */

// Main Home Controller
    Route::get('/', 'HomeController@index');
// Languge Route Group to handle localization ['ar','en'] as prefix
    Route::group(['prefix' => '{lang}'], function () {
    Route::get('/import', 'HomeController@import');



    /*     * ***************************************************************************************
   * Blog Controllers
   * *************************************************************************************** */
    Route::group(['middleware' => 'isadmin'], function () {
        Route::any('update/{slug}', 'BlogController@update');
    });
    Route::get('blog', 'BlogController@index');
        Route::get('blog/category/{category}', 'BlogController@category');
//        Route::group(['middleware' => 'isadmin'], function () {
//        Route::get('blog/unpublished', 'BlogController@unpublished');
//    });
    Route::get('/blog/{slug}', 'BlogController@showBlog');

    Route::group(['middleware' => 'isadmin'], function () {
        Route::get('edit-blog/{slug}', 'BlogController@edit');
    });
    Route::group(['middleware' => 'isadmin'], function () {
        Route::post('new-blog', 'BlogController@store');
    });
    Route::group(['middleware' => 'isadmin'], function () {
        Route::get('delete/{slug}', 'BlogController@destroy');
    });


    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    Route::get('حساب-مجاني', 'IntroController@index');
    Route::get('free-account', 'IntroController@index');

    Route::get('/intro', 'IntroController@index');
    Route::post('/start', 'AutoStart@index');
    Route::get('/thank-you', 'PagesController@thankYou');

    // cashU
//    Route::post('/cashu', 'CheckoutController@cashu');
//    Route::post('/payment/cashu-success', 'PagesController@thankYou');
//    Route::post('/payment/cashu-notification', 'CashuController@notificationResponse');

    Route::post('/cashu', 'CheckoutController@cashu');
    Route::post('/payment/cashu-success', 'CashuController@returnSucess');
    Route::post('/payment/cashu-sorry', 'CashuController@cashuSorry');
    Route::post('/payment/cashu-notification', 'CashuController@notificationResponse');
    //Route::get('/thank_you', 'PagesController@thankYou');


    /*     * ***************************************************************************************
     * COURSES LISTS AND DETAILS PAGE
     * *************************************************************************************** */

    Route::get('الدورات-الموسيقية', 'CoursesGridController@index');
        Route::post('الدورات-الموسيقية', 'CoursesGridController@index');
    Route::get('Music-Courses', 'CoursesGridController@index');

    Route::get('/courses', 'CoursesGridController@index');

    Route::get('/course/{instrument}/{slug}', 'CourseController@index');
    Route::post('/course/start', 'CourseController@start');

    /*     * ***************************************************************************************
     * ABOUT PAGE AND TEXT PAGES
     * *************************************************************************************** */

    Route::get('/about', 'PagesController@index');
    Route::post('/pages/add', 'PagesController@addSection');
    Route::post('/pages/delete/{id}', 'PagesController@deleteSection');
    Route::post('/pages/update/{id}', 'PagesController@updateSection');
    Route::get('/terms', function () {
        if (Request::segment(1) == 'en')
            return view('pages.terms_en');
        else
            return view('pages.terms');
    });
    Route::get('/madrasaty', function () {
        return view('pages.madrasaty');
    });


    /*     * ***************************************************************************************
     * VIDEOS
     * *************************************************************************************** */

    Route::get('/videos/{instrument}/{courseslug}/{videoslug}', 'VideoController@index');
    Route::post('/videos/{instrument}/{courseslug}/{videoslug}', 'VideoController@videoCompleted');
    Route::get('/video/{id}', 'VideoController@geturl');
    Route::get('/free/videos', 'VideoController@freevideos');
    Route::post('/ask/teacher', 'VideoController@ask');

    //Comments
    Route::post('/video/comment', 'CommentsController@video');


    /*     * ***************************************************************************************
     * USER PROFILE
     * *************************************************************************************** */

    // my profile
        Route::group(['middleware' => 'session'], function () {
            Route::any('/profile', 'ProfileController@index');
        });
    Route::get('حسابي', 'ProfileController@index');
    Route::get('My-Profile', 'ProfileController@index');
//    Route::get('/profile', 'ProfileController@index');


    Route::post('/user/edit', 'ProfileController@editUserInfo');

    //Profile Image Plupload Upload
    Route::post('/plupload', function () {
        /* return Plupload::receive('file', function ($file) {
          $path = str_replace('app/Http', 'public/uploads', dirname(__FILE__));
          $name = Auth::user()->id . '_' . $file->getClientOriginalName();
          $file->move($path, $name);

          $user = App\User::find(Auth::user()->id);
          $user->profile_image = $name;
          $user->save();

          return 'ready';
          }); */
    });


    /*     * ***************************************************************************************
     * SUBSCRIPTIONS
     * *************************************************************************************** */

    Route::get("الإشتراكات", 'SubscriptionController@index');
    Route::get("subscriptions", 'SubscriptionController@index');

    Route::get('/subscribe', 'SubscriptionController@index');
    Route::post('/subscribe', 'SubscriptionController@subscribe');
    Route::get('/fulldiscount', 'SubscriptionController@fulldiscount');


    /*     * ***************************************************************************************
     * MUSIC SHEETS
     * *************************************************************************************** */

    Route::get('/music/sheets', 'MusicController@sheets');
    Route::get('/music/sheets/{id}', 'MusicController@sheet');


    /*     * ***************************************************************************************
     * CHECKOUT ROUTE
     * *************************************************************************************** */

    Route::post('طريقة-الدفع', 'CheckoutController@index');
    Route::post('payment-method', 'CheckoutController@index');
    Route::post('/checkout', 'CheckoutController@index');

    Route::post('/addPendingTrans', 'CheckoutController@pendingTransactionSave');


    /*     * ***************************************************************************************
     * Authentication & Reset Password
     * *************************************************************************************** */

    // login

//        Route::group(['middleware' => 'session'], function () {
//            Route::get('auth/login', 'Auth\AuthController@getLogin');
//        });
//        Route::group(['middleware' => 'session'], function () {
//            Route::post('auth/login', 'Auth\AuthController@postLogin');
//

 Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
//    Route::get('auth/logout', 'Auth\AuthController@getLogout');
        Route::group(['middleware' => 'logout'], function () {
            Route::get('auth/logout', 'Auth\AuthController@getLogout');
        });

    // login with facebook
    Route::post('facebook/login', 'FacebookController@login');

    // user varification
    Route::get('verify/{token}', 'UserController@verify');

    // create free account
    Route::get('إنشاء-حساب', 'Auth\AuthController@getRegister');
    Route::get('create-account', 'Auth\AuthController@getRegister');

    // register
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

    // change password
    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');


    /*     * ***************************************************************************************
     * ADMINISTRATION PANEL
     * *************************************************************************************** */

    Route::get('/admin', 'AdminController@index');
        Route::post('/admin', 'AdminController@index');
        Route::get('/testt', 'AdminController@test');



    // Users
    Route::get('/admin/users', 'AdminController@users');
    Route::get('/admin/usersAjaxView', 'AdminController@usersAjaxView');
    Route::get('/admin/usersAjax', 'AdminController@usersAjax');



    Route::get('/admin/user/create', 'AdminController@createUser');
    Route::post('/admin/user/create', ['as' => 'user.create', 'uses' => 'AdminController@createUserInfo']);
    Route::get('/admin/user/edit/{id}', 'AdminController@editUser');
    Route::post('/admin/user/edit/{id}', ['as' => 'user.edit', 'uses' => 'AdminController@editUserInfo']);
    Route::get('/admin/user/delete/{id}', 'AdminController@deleteUser');

    //Blog Administration
     Route::get('/admin/blog/new-blog', 'BlogController@create');
     Route::get('/admin/blog/upublished','BlogController@unpublished');

    // Groups
    Route::get('/admin/groups', 'AdminController@groups');
    Route::get('/admin/group/create', 'AdminController@createGroup');
    Route::post('/admin/group/create', ['as' => 'group.create', 'uses' => 'AdminController@createGroupInfo']);
    Route::get('/admin/group/edit/{id}', 'AdminController@editGroup');
    Route::post('/admin/group/edit/{id}', ['as' => 'group.edit', 'uses' => 'AdminController@editGroupInfo']);
    Route::get('/admin/group/delete/{id}', 'AdminController@deleteGroup');


    // Courses
    Route::get('/admin/course/create', 'AdminController@createCourse');
    Route::post('/admin/course/create', ['as' => 'course.create', 'uses' => 'AdminController@createCourseInfo']);
    Route::get('/admin/course/edit/{id}', 'AdminController@editCourse');
    Route::post('/admin/course/edit/{id}', ['as' => 'course.edit', 'uses' => 'AdminController@editCourseInfo']);
    Route::get('/admin/course/delete/{id}', 'AdminController@deleteCourse');
    Route::get('/admin/courses', 'AdminController@courses');
    Route::get('/admin/courses/types', 'CourseTypeController@index');
    Route::post('/admin/courses/types/create', 'CourseTypeController@store');
    Route::get('/admin/courses/types/delete/{id}', 'CourseTypeController@destroy');
    Route::get('/admin/courses/createTypes', 'CourseTypeController@create');
    Route::get('/admin/courses/program_types', 'program_typeController@index');
    Route::get('/admin/courses/createprogram', 'program_typeController@create');
    Route::post('/admin/courses/program/create', 'program_typeController@store');
    Route::get('/admin/courses/program/delete/{id}', 'program_typeController@destroy');




    // Videos
    Route::get('/admin/video/create', 'AdminController@createVideo');
    Route::post('/admin/video/create', ['as' => 'video.create', 'uses' => 'AdminController@createVideoInfo']);
    Route::get('/admin/video/edit/{id}', 'AdminController@editVideo');
    Route::post('/admin/video/edit/{id}', ['as' => 'video.edit', 'uses' => 'AdminController@editVideoInfo']);
    Route::get('/admin/video/delete/{id}', 'AdminController@deleteVideo');
    Route::get('/admin/videos/{id}', 'AdminController@courseVideos');
    Route::get('/admin/videos', 'AdminController@videos');

    // Video Downloadables
    Route::get('/admin/downloadables/list/{id}', 'AdminController@ListFiles');
    Route::get('/admin/downloadables/create/{id}', 'AdminController@AddFiles');
    Route::post('/admin/downloadables/create', ['as' => 'downloadable.create', 'uses' => 'AdminController@AddFilesInfo']);
    Route::get('/admin/downloadables/edit/{id}/{video_id}', 'AdminController@editDownloadable');
    Route::post('/admin/downloadables/edit/{id}', ['as' => 'downloadable.edit', 'uses' => 'AdminController@editDownloadableInfo']);
    Route::get('/admin/downloadables/delete/{id}/{video_id}', 'AdminController@deleteDownloadable');


    // Instruments
    Route::get('/admin/instrument/create', 'AdminController@createInstrument');
    Route::post('/admin/instrument/create', ['as' => 'instrument.create', 'uses' => 'AdminController@createInstrumentInfo']);
    Route::get('/admin/instrument/edit/{id}', 'AdminController@editInstrument');
    Route::post('/admin/instrument/edit/{id}', ['as' => 'instrument.edit', 'uses' => 'AdminController@editInstrumentInfo']);
    Route::get('/admin/instrument/delete/{id}/{video_id}', 'AdminController@deleteInstrument');
    Route::get('/admin/instruments', 'AdminController@instruments');


    // Testimonial
    Route::get('/admin/testimonial/create', 'AdminController@createTestimonial');
    Route::post('/admin/testimonial/create', ['as' => 'testimonial.create', 'uses' => 'AdminController@createTestimonialInfo']);
    Route::get('/admin/testimonial/edit/{id}', 'AdminController@editTestimonial');
    Route::post('/admin/testimonial/edit/{id}', ['as' => 'testimonial.edit', 'uses' => 'AdminController@editTestimonialInfo']);
    Route::get('/admin/testimonial/delete/{id}', 'AdminController@deleteTestimonial');
    Route::get('/admin/testimonials', 'AdminController@testimonials');


    // Coupon
    Route::get('/admin/coupon/create', 'AdminController@createCoupon');
    Route::post('/admin/coupon/create', ['as' => 'coupon.create', 'uses' => 'AdminController@createCouponInfo']);
    Route::get('/admin/coupon/edit/{id}', 'AdminController@editCoupon');
    Route::post('/admin/coupon/edit/{id}', ['as' => 'coupon.edit', 'uses' => 'AdminController@editCouponInfo']);
    Route::get('/admin/coupon/delete/{id}', 'AdminController@deleteCoupon');
    Route::get('/admin/coupons', 'AdminController@coupons');


    // Users Interactions
    Route::get('/admin/interactions', 'AdminController@interactions');
    Route::post('/admin/getUserInteractions', 'AdminController@getUserInteractions');


    // music sheets
    Route::get('/admin/notes/create', 'AdminController@createNotes');
    Route::post('/admin/notes/create', ['as' => 'notes.create', 'uses' => 'AdminController@createNotesInfo']);
    Route::get('/admin/notes/edit/{id}', 'AdminController@editNotes');
    Route::post('/admin/notes/edit/{id}', ['as' => 'note.edit', 'uses' => 'AdminController@editNotesInfo']);
    Route::get('/admin/notes/delete/{id}', 'AdminController@deleteNotes');
    Route::get('/admin/notes', 'AdminController@notes');


    // subscriptions & fraud
    Route::get('/admin/subscriptions/fraud', 'AdminController@fraud');
    Route::get('/admin/subscriptions/activate/{id}', 'AdminController@activate');
    Route::get('/admin/subscriptions/create', 'AdminController@createSubscriptions');
    Route::post('/admin/subscriptions/create', ['as' => 'subscriptions.create', 'uses' => 'AdminController@createSubscriptionsInfo']);
    Route::get('/admin/subscriptions/edit/{id}', 'AdminController@editSubscriptions');
    Route::post('/admin/subscriptions/edit/{id}', ['as' => 'subscriptions.edit', 'uses' => 'AdminController@editSubscriptionsInfo']);
    Route::get('/admin/subscriptions/delete/{id}', 'AdminController@deleteSubscriptions');
    Route::get('/admin/subscriptions', 'AdminController@subscriptions');

    //pending transactions
    Route::get('/admin/subscriptions/pendingTransactions', 'AdminController@pendingTransactions');
    Route::get('/admin/pendingTransactions/activate/{id}', 'AdminController@activatePendingTransaction');
    Route::get('/admin/pendingTransactions/delete/{id}', 'AdminController@deletePendingTransaction');

    // Views
    Route::get('/admin/views/courses', 'ViewsController@courses');
    Route::get('/admin/views/users', 'ViewsController@all_users');

    Route::get('/admin/views/course/{id}', 'ViewsController@course');
    Route::post('/admin/views/course/{id}', 'ViewsController@getCourseViews');
    Route::post('/admin/views/video/{id}', 'ViewsController@getVideoViews');
    //user completed videos

    Route::get('/admin/user/{id}', 'AdminController@user_completed_videos');
    Route::get('/admin/interactions/emails', 'AdminController@get_users_email');

    Route::get('/admin/views/video/{id}', 'ViewsController@video');

    // Teacher Formela
    Route::post('/admin/views/teachers/getTeachers', 'ViewsController@getTeachers');
    Route::post('/admin/views/teachers/getViewsForTeacher/{id}', 'ViewsController@getViewsForTeacher');
    Route::get('/admin/views/teachers/getSubscriptionForTeacher/{id}', 'ViewsController@getSubscriptionForTeacher');
    Route::get('/admin/views/teachers/TeachersBilling', 'ViewsController@viewBillings');
    Route::post('/admin/views/teachers/TeachersBilling', 'ViewsController@viewBillings');


    Route::get('/admin/views/teachers/getEndedSubscriptions', 'ViewsController@getEndedSubscriptions');

    //Tracking
    Route::get('/admin/tracking', 'AdminController@tracking');

    Route::post('/getTotalSubscriptions', 'AdminController@getTotalSubscriptions');
    Route::post('/getSubscriptionByDate', 'AdminController@getSubscriptionByDate');


    // Sales Dashboard
    Route::get('/admin/sales', 'AdminController@sales');

    //Marketing Dashboard
    Route::get('/admin/marketing_dashboard', 'AdminController@marketing_dashboard');
    Route::get('/admin/marketing', 'MarketingController@index');
    Route::post('/admin/marketing', 'MarketingController@store');
    Route::get('/admin/marketing_tracking', 'MarketingController@marketing_tracking');
    Route::post('/admin/marketing_tracking', 'MarketingController@marketing_tracking');


        //export marketing dashboard results.
    Route::get('/admin/export', 'AdminController@getExport');


    // Instrument Requests
    Route::get("شراء-آله-موسيقية", 'InstrumentController@index');
    Route::get("buy-musical-instrument", 'InstrumentController@index');
    Route::post('instrument_req/save', 'InstrumentController@save');

    Route::get('/admin/requestedInstruments', 'AdminController@getRequestedInstruments');
    Route::post('/deliver', 'AdminController@instrumentsDelivered');
    Route::post('/dontDeliver', 'AdminController@instrumentsNotDelivered');

    // Abandoned Card admin
    Route::get('/admin/abandonedCard', 'AdminController@getabandonedCardInfo');


    // Editables
    Route::post('/add', ['as' => 'subscription.create', 'uses' => 'EditableController@addSubscription']);
    Route::post('/subscription/delete', 'EditableController@DeleteSubscription');
    Route::post('/feature/delete', 'EditableController@DeleteSubscriptionFeature');
    Route::post('/addFeature', ['as' => 'subscriptionFeature.create', 'uses' => 'EditableController@AddSubscriptionFeature']);
    Route::post('/feature/edit', 'EditableController@EditSubscriptionFeature');

    // Edit Video Description by admin
    Route::post('/edit/video/description', 'VideoController@EditDescription');


    /*     * ***************************************************************************************
     * PAYMENT GATWAYS ROUTS
     * *************************************************************************************** */

    // Paypal
    Route::post('/paypal_ipn', 'PaypalController@ipn');

    // 2checkout
    Route::post('/2checkout', 'TwoCheckoutController@ipn');
    Route::get('add2checkout', 'TwoCheckoutController@add2checkout');

    //Payment Responses
    Route::get('/success/{gatway}', function ($gatway) {
        return redirect()->action('CoursesGridController@index');
    });

    Route::get('/cancel/{gatway}', function ($gatway) {
        return redirect()->action('HomeController@index');
    });


    /*     * ***************************************************************************************
     * payments form validation
     * *************************************************************************************** */

    Route::post('/paypal', 'CheckoutController@paypal');
    Route::post('/twocheckout', 'CheckoutController@twocheckout');


    /*     * ***************************************************************************************
     * GET EXCHANGE RATES FROM YAHOO FINANCE FOR SUBSCRIPTIONS PAGE
     * *************************************************************************************** */

    Route::post('/changerate/{from}/{to}', function ($lang, $from, $to) {

        $httpAdapter = new \Ivory\HttpAdapter\FileGetContentsHttpAdapter();
        $yahooProvider = new \Swap\Provider\YahooFinanceProvider($httpAdapter);
        $swap = new \Swap\Swap($yahooProvider);
        $rate = $swap->quote($from . '/' . $to);

        session(["rate-$from-$to" => $rate]);
        return $rate;
    });


    /*     * ***************************************************************************************
     * FIND OLD COURSES FROM THE OLD SITE VIA OLD URLS
     * *************************************************************************************** */

    Route::get('/course/{context}/{param1?}/{param2?}', ['as' => 'old_courses', 'uses' => 'CourseController@find']);
});


Route::get('/create/copune', function () {
    $code = \uniqid();
    $coupone = new App\Coupon;
    $coupone->code = $code;
    $coupone->payment_methods = "1,2,3,4,5";
    $coupone->subscription_types = "11,12,13,14";
    $coupone->starts_at = Carbon\Carbon::now();
    $coupone->ends_at = Carbon\Carbon::now()->addDays(3);
    $coupone->discount_type = "percentage";
    $coupone->amount = "20";
    $coupone->save();
});

Route::get('/sheets/generate', 'TestController@index');

// login
//
//Route::group(['middleware' => 'session'], function () {
//    Route::post('auth/login', 'Auth\AuthController@postLogin');
//});
//Route::group(['middleware' => 'session'], function () {
//    Route::get('auth/login', 'Auth\AuthController@getLogin');
//});
//Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
/* * ***************************************************************************************
 * AUTOMATED INSTALLATION
 * *************************************************************************************** */

Route::get('/admin/run/{key?}', array('as' => 'install', function ($key = null) {
    if ($key == "install-v.4.2.1") {
        try {
            echo '<br>Started Migrating Tables ...';
            Artisan::call('migrate');
            echo '<br>Tables Migration is completed';


            echo '<br>Started seedars ...';
            Artisan::call('db:seed', ['--class' => 'CountriesTableSeeder']);
            Artisan::call('db:seed', ['--class' => 'UserGroupsSeeder']);
            Artisan::call('db:seed', ['--class' => 'CreateMainInstuments']);
            Artisan::call('db:seed', ['--class' => 'CreateMainUsers']);
            echo '<br> Finished Base Data';

            echo '<br> Starting Sample Data ...';
            Artisan::call('db:seed', ['--class' => 'AddSampleCourse']);
            Artisan::call('db:seed', ['--class' => 'AddSampleVideo']);


            echo '<br>Finished ALL Seeding';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
    } else {
        App::abort(404);
    }
}));
