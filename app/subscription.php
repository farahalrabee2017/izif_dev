<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscription extends Model
{
    protected $table = 'subscriptions';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Payments', 'payment_id');
    }


    public function subscriptions_model()
    {
        return $this->belongsTo('App\SubscriptionModel', 'subscription_model_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id', 'id');
    }


}
