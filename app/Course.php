<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * @var string
     */
    protected $table = 'courses';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'title_arabic',
        'title_english',
        'overview_arabic',
        'overview_english',
        'seo_meta_title',
        'before_price',
        'price',
        'user_id',
        'seo_description_arabic',
        'seo_description_english',
        'seo_keywords_arabic',
        'seo_keywords_english',
        'url_identifier',
        'cover_image',
        'instrument_id',
        'course_estimated_time',
        'course_intro_video_path',
        'course_order',
        'level',
        'name',
        'language',
        'is_active',
        'old_website_prefix',
        'course_type',
        'course_category',
        'programType',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teacher()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function videos()
    {
        $query = "CAST(video_order AS DECIMAL(10,2)) ASC";
        return $this->hasMany('App\Video', 'course_id')->orderByRaw($query);
    }

    public function videoStatus(){
        return $this->hasMany('App\VideoStatus', 'course_id');
    }

    public function videoStatusCount(){
        return $this->hasMany('App\VideoStatus', 'course_id');
    }

    public function completedVideos(){
        return $this->hasMany('App\VideoStatus', 'course_id','id')->where('status','Done');
    }


    public function type()
    {
        return $this->belongsTo('App\course_type', 'course_type','id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instrument()
    {
        return $this->belongsTo('App\Instrument');
    }


}
