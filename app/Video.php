<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = "videos";
    public $timestamps = true;

    protected $fillable = [
        'title_arabic',
        'title_english',
        'description_arabic',
        'description_english',
        'course_id',
        'user_id',
        'seo_description_arabic',
        'seo_description_english',
        'seo_keywords_arabic',
        'seo_keywords_english',
        'url_identifier',
        'cover_image',
        'instrument_id',
        'estimated_time',
        'video_path',
        'video_order',
        'is_open',
        'is_youtube',
        'seo_meta_title',
        'name',
        'video_number',
        'free_order_number'
    ];

    public function teacher(){
        return $this->belongsTo('App\User','user_id');
    }

    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function instrument(){
        return $this->belongsTo('App\Instrument','instrument_id');
    }

    public function downloadables(){
        $this->hasMany('App\Downloadable','video_id');
    }
    
    public function videostatus(){
        return $this->hasMany('App\VideoStatus','video_id','id');
    }
    public function user()
    { return $this->belongsTo('User' , 'user_id') ;
    }
        public function instrument1(){
        return $this->hasOne('App\Instrument', 'id' ,'instrument_id');
    }
        

  


}
