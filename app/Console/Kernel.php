<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\weekSubscriber::class,
        Commands\monthSubscriber::class,
        Commands\welcomeSubscriber::class,
        Commands\AbandonedCard::class,
        Commands\UserAds::class
           
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command('inspire')->hourly();
        $schedule->command('week:subscriber')->weekly();
//      in this next one , you will handle two tasks inside it , with different queries :)
        $schedule->command('month:subscriber')->monthly();
        $schedule->command('welcome:subscriber')->cron('0 0 */3 * *');
        $schedule->command('abandoned:card')->cron('0 */2 * * *'); // check it or use ->hourly()
//        $schedule->command('users:ads')->cron('0 0 */3 * *');
        $schedule->command('users:ads')->daily();
    }

}
