<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\email_temp;
use Mail;
use DB;
use App\Coupon;
use Carbon\Carbon;

class UserAds extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:ads';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if (gethostname() == 'ip-172-31-44-62') {
//            $users = DB::select(DB::raw("SELECT DISTINCT
//                                    t.* , u.name as NAME , u.email EMAIL , u.lang
//                                    FROM 
//                                    traking t join users u on(t.user_id = u.id)
//                                    WHERE
//                                    t.user_id != 'anonymous' AND t.real_user_id !='' and
//                                    t.created_at >= DATE_ADD(CURDATE(), INTERVAL -3 DAY);"));
//        $users = DB::select(DB::raw("SELECT DISTINCT
//                                    t.* , u.name as NAME , u.email EMAIL , u.lang
//                                    FROM 
//                                    traking t join users u on(t.user_id = u.id)
//                                    WHERE
//                                    t.user_id != 'anonymous' AND t.real_user_id !='' and
//                                    u.email like '%abucake@yahoo.com%';"));


            $users = DB::select(DB::raw(" SELECT DISTINCT
                                    t.* , u.name as NAME , u.email EMAIL , u.lang ,
                                    DATE_FORMAT(t.created_at,'%d-%m-%y'),
                                    DATE_FORMAT(CURDATE(),'%d-%m-%y'),
                                    datediff(DATE_FORMAT(CURDATE(),'%y-%m-%d'),DATE_FORMAT(t.created_at,'%y-%m-%d'))
                                    FROM 
                                    traking t join users u on(t.user_id = u.id)
                                    WHERE
                                    t.user_id != 'anonymous' AND t.real_user_id !='' and
                                    datediff(DATE_FORMAT(CURDATE(),'%y-%m-%d'),DATE_FORMAT(t.created_at,'%y-%m-%d'))=3;"));
            $emailTemp = email_temp::where('id', 5)->get();
            foreach ($users as $user) {
                if ($user->lang == 'en') {
                    $textEmail = $emailTemp[0]->html_code_english;
                    $title = $emailTemp[0]->english_title;
                } else {
                    $textEmail = $emailTemp[0]->html_code;
//                $title =  rawurldecode('اشترك اليوم بنصف السعر و لمدة ثلاث أيام فقط !');
                    $title = "لمدة ثلاثة أيام فقط اشترك بنصف السعر";
                }
//            var_dump($title); die();
                $input_line = $textEmail;
                $output_array = array();
                preg_match("/(?<=\*\|)CODE.*(?=\|\*)/", $input_line, $output_array);

                $code_values = explode('%2F', $output_array[0]);
//                 var_dump($code_values[1]); die();
                $code = \uniqid();
                $coupone = new Coupon();
                $coupone->code = $code;
                $coupone->payment_methods = "1,2,3,4,5";
                $coupone->subscription_types = "12";
                $coupone->starts_at = Carbon::now();
                $coupone->ends_at = Carbon::now()->addDays($code_values[2]);
                $coupone->discount_type = "percentage";
                $coupone->amount = $code_values[1];
                $coupone->save();

//                dd($code);
                $userData = array(
                    '*|NAME|*' => $user->NAME,
                    "*|$output_array[0]|*" => $code
                );
                $result = str_replace(array_keys($userData), array_values($userData), $textEmail);

                Mail::raw($result, function ($message) use ($user, $result, $title) {
                    $message->from('info@izif.com', 'izif.com');

                    $message->to($user->EMAIL)->subject($title)->setBody($result, 'text/html');
                });
            }
        }
    }

}
