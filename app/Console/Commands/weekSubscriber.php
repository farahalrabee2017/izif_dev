<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\email_temp;
use App\User;
use DB;

class weekSubscriber extends Command {

    /**
     * The name and signature of the console command.
     * 
     * this command has been written by a geek , be careful when you edit 
     *
     * @var string
     */
    protected $signature = 'week:subscriber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will send email to unsubscribed users after one week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     * this function returns all users that have registered 
     * but not subscribed in first week
     */
    public function getUnsubFirstWeek() {
        if (gethostname() == 'ip-172-31-44-62') {
            $emailTemp = email_temp::where('id', 1)->get();

            $users = DB::select(DB::raw("SELECT 
                                     u.id as USERID, u.name as NAME, u.email as EMAIL, u.created_at as ACCDATE,
                                     u.telephone as TELEPHONE,c.country_name as COUNTRY , u.lang
                                     FROM users u LEFT JOIN countries c ON (u.country_id = c.id)
                                     where yearweek(u.created_at) = yearweek(now()- INTERVAL 7 DAY)
                                     and  u.id NOT IN (select user_id from subscriptions);"));

            foreach ($users as $user) {
                if ($user->lang == 'en') {
                    $textEmail = $emailTemp[0]->html_code_english;
                    $title = $emailTemp[0]->english_title;
                } else {
                    $textEmail = $emailTemp[0]->html_code;
                    $title = $emailTemp[0]->arabic_title;
                }
                $userData = array(
                    '*|NAME|*' => $user->NAME,
                );
                $result = str_replace(array_keys($userData), array_values($userData), $textEmail);

                Mail::raw($result, function ($message) use ($user, $result, $title) {
                    $message->from('info@izif.com', 'izif.com');

                    $message->to($user->EMAIL)->subject($title)->setBody($result, 'text/html');
                });
            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->getUnsubFirstWeek();
    }

}
