<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\email_temp;
use App\User;
use DB;

class welcomeSubscriber extends Command {

    /**
     * The name and signature of the console command.
     * 
     * this command has been written by a geek , be careful when you edit 
     *
     * @var string
     */
    protected $signature = 'welcome:subscriber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will send email to subscriped users after 3 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function subscripedUsers() {
        if (gethostname() == 'ip-172-31-44-62') {
            $emailTemp = email_temp::where('id', 3)->get();
            $users = DB::select(DB::raw("SELECT
                                     distinct u.name as NAME , u.email as EMAIL , u.lang, s.created_at as sub_date
                                     FROM
                                     subscriptions s join users u on (u.id = s.user_id)
                                     WHERE s.created_at >= DATE_ADD(CURDATE(), INTERVAL -3 DAY);"));


            foreach ($users as $user) {
                if ($user->lang == 'en') {
                    $textEmail = $emailTemp[0]->html_code_english;
                    $title = $emailTemp[0]->english_title;
                } else {
                    $textEmail = $emailTemp[0]->html_code;
                    $title = $emailTemp[0]->arabic_title;
                }

                $userData = array(
                    '*|NAME|*' => $user->NAME,
                );
                $result = str_replace(array_keys($userData), array_values($userData), $textEmail);
                Mail::raw($result, function ($message) use ($user, $result, $title) {
                    $message->from('info@izif.com', 'izif.com');

                    $message->to($user->EMAIL)->subject($title)->setBody($result, 'text/html');
                });
            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->subscripedUsers();
    }

}
