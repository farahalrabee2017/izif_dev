<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\email_temp;
use App\User;
use DB;
use App\Coupon;
use Carbon\Carbon;

class monthSubscriber extends Command {

    /**
     * The name and signature of the console command.
     * 
     * this command has been written by a geek , be careful when you edit 
     *
     * @var string
     */
    protected $signature = 'month:subscriber';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command will send email to unsubscribed users after one month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     */
    public function getUnsubAfterMonth() {
        if (gethostname() == 'ip-172-31-44-62') {
            $emailTemp = email_temp::where('id', 2)->get();
            $users = DB::select(DB::raw("SELECT 
                                     u.id as USERID, u.name NAME , u.email, u.created_at as ACCDATE,
                                     u.telephone as TELEPHONE,c.country_name as COUNTRY ,u.lang
                                     FROM users u LEFT JOIN countries c ON (u.country_id = c.id)
                                     WHERE month(u.created_at) = month(now() - INTERVAL 1 MONTH) 
                                     and  u.id NOT IN (select user_id from subscriptions)
                                     order by u.created_at;"));

            foreach ($users as $user) {
                if ($user->lang == 'en') {
                    $textEmail = $emailTemp[0]->html_code_english;
                    $title = $emailTemp[0]->english_title;
                } else {
                    $textEmail = $emailTemp[0]->html_code;
                    $title = $emailTemp[0]->arabic_title;
                }
                $input_line = $textEmail;
                $output_array = array();
                preg_match("/\*\|CODE.*\|\*/", $input_line, $output_array);

                $code_values = explode('%2F', $output_array[0]);

                $code = \uniqid();
                $coupone = new Coupon();
                $coupone->code = $code;
                $coupone->payment_methods = "1,2,3,4,5";
                $coupone->subscription_types = "11,12,13,14";
                $coupone->starts_at = Carbon::now();
                $coupone->ends_at = Carbon::now()->addDays($code_values[1]);
                $coupone->discount_type = "percentage";
                $coupone->amount = $code_values[2];
                $coupone->save();

                $userData = array(
                    '*|NAME|*' => $user->NAME,
                    "$output_array[0]" => $code
                );
                $result = str_replace(array_keys($userData), array_values($userData), $textEmail);

                Mail::raw($result, function ($message) use ($user, $result, $title) {
                    $message->from('info@izif.com', 'izif.com');

                    $message->to($user->EMAIL)->subject($title)->setBody($result, 'text/html');
                });
            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->getUnsubAfterMonth();
    }

}
