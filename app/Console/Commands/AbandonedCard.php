<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\email_temp;
use App\User;
use DB;
use App\abandoned_carts;
use App\Coupon;
use Carbon\Carbon;

class AbandonedCard extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abandoned:card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if (gethostname() == 'ip-172-31-44-62') {
            $users = DB::select(DB::raw("SELECT
                                        ab.* , u.name as NAME , u.email as EMAIL, u.lang
                                        FROM abandoned_carts ab join users u on (ab.user_id = u.id)
                                        WHERE ab.created_at >= NOW() - INTERVAL 3 HOUR and ab.created_at <= NOW() - INTERVAL 1 HOUR;"));
            
        
        
            $emailTemp = email_temp::where('id', 4)->get();

            foreach ($users as $user) {
                if ($user->lang == 'en') {
                    $textEmail = $emailTemp[0]->html_code_english;
                    $title = $emailTemp[0]->english_title;
                } else {
                    $textEmail = $emailTemp[0]->html_code;
                    $title = $emailTemp[0]->arabic_title;
                }

                $userData = array(
                    '*|NAME|*' => $user->NAME,
                );
                $result = str_replace(array_keys($userData), array_values($userData), $textEmail);
                Mail::raw($result, function ($message) use ($user, $result, $title) {
                    $message->from('info@izif.com', 'izif.com');

                    $message->to($user->EMAIL)->subject($title)->setBody($result, 'text/html');
                });
            }
        }
    }

}
