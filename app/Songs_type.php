<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Songs_type extends Model
{
  protected $table = "songs_type";

  protected $fillable = [
      'id',
      'english_name',
      'arabic_name',
  ];

  public function video(){
      return $this->hasMany('App\Video', 'id','course_category');
  }
}
