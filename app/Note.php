<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = "notes";

    protected $fillable = [
        'title_arabic',
        'title_english',
        'description_english',
        'note_path',
        'description',
        'author',
        'type',
        'year',
        'cover',
        'course_id',
        'is_open',
        'order_number'
    ];


}
