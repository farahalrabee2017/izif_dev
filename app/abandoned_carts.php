<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class abandoned_carts extends Model {

    protected $table = 'abandoned_carts';
    protected $fillable = [];


    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }


    public function subscriptions_model(){
        return $this->belongsTo('App\SubscriptionModel','subscription_model_id','id');
    }

    public function course(){
        return $this->belongsTo('App\Course','course_id','id');
    }

}
