<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $table = "testimonial";

    public $timestamps = true;

    protected $fillable = [
        'name',
        'name_enu',
        'testimonial',
        'photo',
        'testimonial_english'
    ];
}
