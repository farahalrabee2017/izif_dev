<?php

namespace App\Services;

use Auth;
use App\CourseStatus;

class VideoListData{

    public function getData(){
        $this->status();
        $this->complation();

    }

    public function status(){

        global $course,$course_status;

        $user_id = Auth::Guest() ? 0 : Auth::user()->id;
        $course_status = CourseStatus::where(['course_id' => $course->id, 'user_id' => $user_id])->get();
        $course_status = count($course_status) > 0 ? true : false;
    }

    public function complation(){

        global $course;

        $completed_videos = count($course->completedVideos);
        $not_completed_videos = abs($completed_videos - count($course->videoStatus));
        $not_completed_precent = 0.5 * $not_completed_videos;
        $completed = floor(($completed_videos + $not_completed_precent) / count($course->videos) * 100);
    }
}