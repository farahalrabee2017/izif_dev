<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherBilling extends Model
{
    protected $table = "teachers_billing";

    protected $fillable = [
        'amount',
        'number_of_views',
        'views_percentage',
        'views_list',
        'teacher_id',
        'user_id',
        'subscription_id',
        'subscription_model_id'
    ];

    public function teacher()
    {
        return $this->belongsTo('App\User', 'teacher_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subscription()
    {
        return $this->belongsTo('App\subscription', 'subscription_id');
    }

    public function subscription_model()
    {
        return $this->belongsTo('App\SubscriptionModel', 'subscription_model_id');
    }
}
