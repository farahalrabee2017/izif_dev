<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagesContainer extends Model
{
    //
    protected $table = "pages_container";

    protected $fillable = [
        'title_arabic',
        'title_english',
        'description_arabic',
        'description_english',
        'section_order'
    ];
}
