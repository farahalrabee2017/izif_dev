<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $dates = ['published_at'];

    protected $fillable = [
        'title',
        'content',
        'slug'
    ];
    
}
