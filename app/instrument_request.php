<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instrument_request extends Model {

    protected $table = 'instrument_request';
    protected $fillable = ['name', 'email', 'instrument_id', 'level', 'city','country_id'];

}