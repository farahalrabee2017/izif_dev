<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pendingTransactions extends Model {

    // table name
    protected $table = 'pending_transactions';
    // fillable colos
    protected $fillable = ['name', 'transfer_number', 'country', 'transaction_type', 'course_id', 'subscription_model_id','is_active','amount','user_id'];

    public function SubscriptionModel() {
        return $this->belongsTo('App\SubscriptionModel', 'subscription_model_id');
    }

}
