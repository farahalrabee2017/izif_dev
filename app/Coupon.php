<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = "coupon";

    protected $fillable = [
        'code',
        'payment_methods',
        'subscription_types',
        'starts_at',
        'ends_at',
        'discount_type',
        'amount'
    ];

    public $timestamps = true;
}
