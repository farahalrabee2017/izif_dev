<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStatus extends Model
{
    protected $table = "course_status";

    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'user_id',
        'status'
    ];
}
