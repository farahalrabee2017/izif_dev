<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoStatus extends Model {

    protected $table = "video_status";
    public $timestamps = true;
    protected $fillable = [
        'video_id',
        'course_id',
        'user_id',
        'status'
    ];

    public function video() {
        return $this->belongsTo('App\Video', 'video_id');
    }
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function course() {
        return $this->hasOne('App\Course', 'id','course_id');
    }


}
