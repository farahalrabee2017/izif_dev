<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = "payments";

    protected $fillable = [
        'user_id',
        'amount',
        'payment_method_id',
        'invoice_id'
    ];
    public function payment_method() {
        return $this->hasMany('App\PaymentMethod', 'id' , 'payment_method_id')->select('name' , 'id');
    }
    public function user() {
        return $this->hasOne('App\User', 'id' , 'user_id');

    }
}
