<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = "comments";
    protected $fillable = [
        'user_id',
        'video_id',
        'comment'

    ];
}
