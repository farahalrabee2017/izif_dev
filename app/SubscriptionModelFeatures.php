<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionModelFeatures extends Model
{
    protected $table = "subscription_model_features";

    protected $fillable = [
        'title_ara',
        'title_enu',
        'subscription_model_id'
    ];
}
