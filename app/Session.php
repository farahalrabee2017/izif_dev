<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = "session";

    protected $fillable = [
        'id',
        'user_id',
        'created_at',
        'logged_in',
    ];




}
