<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Mail;
use Mockery\Exception;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'name_enu',
        'email',
        'password',
        'user_group_id',
        'referal_account',
        'gender',
        'age',
        'country_id',
        'telephone',
        'profile_image',
        'about_ara',
        'about_enu',
        'lang'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];



    public static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        static::creating(function($user){
            $user->token = str_random(30);

            try {
                $authDate = ['email' => $user->email, 'password' => "$user->name-izif", 'name' => $user->name , 'token'=>$user->token];
                Mail::send('emails.welcome', $authDate, function ($message) use ($user) {

                    $message->from('info@izif.com', 'Welcome To Izif');
                    $message->to($user->email)->subject('Welcome to izif! أهلاُ و سهلاُ بك في إعزف');

                });
            }catch (Exception $e){}
        });


    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscription()
    {
        return $this->hasMany('App\Subscription', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('App\Group', 'user_group_id');
    }

    public function country() {
        return $this->hasMany('App\Country', 'country_id');
    }

    public function payment() {
        return $this->hasMany('App\Payments', 'user_id' , 'id');
    }
    public function session() {
        
        return $this->hasMany('App\Sessions' , 'user_id' , 'id');
    }



}
