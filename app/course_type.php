<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course_type extends Model
{

    protected $table = "course_type";
public $timestamps = false;
    protected $fillable = [
        'name',
        'arabic_name'
    ];

    public function course(){
        return $this->hasMany('App\Course', 'id','course_type');
    }
}
