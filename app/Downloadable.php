<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Downloadable extends Model
{
    protected $table = "downloadables";

    public $timestamps = true;

    public $fillable = [
        'title_ara',
        'title_enu',
        'file_path_ara',
        'file_path_enu',
        'data_added',
        'course_id',
        'video_id'
    ];

}
