<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionModel extends Model
{
    protected $table = "subscription_models";

    protected $fillable = [
        'title_ara',
        'title_enu',
        'is_premium',
        'location_order',
        'display_price',
        'price',
        'time_period_in_days',
        'is_full_access',
        'is_open_time_period'
    ];

    public function SubscriptionModelFeatures(){
        return $this->hasMany('App\SubscriptionModelFeatures','subscription_model_id','id');
    }

    public function SubscriptionUsers(){
        return $this->hasMany('App\subscription','subscription_model_id','id');
    }


}
