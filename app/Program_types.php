<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program_types extends Model
{
  protected $table = "program_type";
  public $timestamps = false;

  protected $fillable = [
      'id',
      'name',

  ];


  public function course(){
      return $this->hasMany('App\Course', 'id','programType');
  }

}
