<?php

return array(
    'title' => 'Users',
    'single' => 'user',
    'model' => 'App\User',
    'columns' => array(
        'email' => array(
            'title' => 'Email',
        )
    ),
    'edit_fields' => array(
        'email' => array(
            'title' => 'email',
            'type' => 'text'
        ),
    )
);