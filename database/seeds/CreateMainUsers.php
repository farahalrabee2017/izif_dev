<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateMainUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['name'=>"Izif Admin", 'email'=>"admin@izif.com", 'password'=>Hash::make('izif@12345'), 'user_group_id'=>"2"]
        ]);
    }
}
