<?php

use Illuminate\Database\Seeder;

class CreateMainInstuments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instruments')->insert(array(
            array('id' => '1','english_name' => 'Oud','arabic_name' => '&#1575;&#1604;&#1593;&#1608;&#1583;','english_name' => 'Oud','icon' => 'static/img/icon-music-1.png'),
            array('id' => '2','english_name' => 'Guitar','arabic_name' => '&#1575;&#1604;&#1580;&#1610;&#1578;&#1575;&#1585;','english_name' => 'Guitar','icon' => 'static/img/icon-music-2.png'),
            array('id' => '3','english_name' => 'Darbuka','arabic_name' => '&#1591;&#1576;&#1604;&#1577;','english_name' => 'Darbuka','icon' => 'static/img/icon-music-4.png'),
            array('id' => '5','english_name' => 'Vocals','arabic_name' => '&#1575;&#1604;&#1594;&#1606;&#1575;&#1569;','english_name' => 'Vocals','icon' => 'static/img/icon-music-5.png'),
            array('id' => '6','english_name' => 'Ney','arabic_name' => '&#1575;&#1604;&#1606;&#1575;&#1610;','english_name' => 'Ney','icon' => 'static/img/icon-music-6.png'),
            array('id' => '7','english_name' => 'Piano','arabic_name' => '&#1575;&#1604;&#1576;&#1610;&#1575;&#1606;&#1608;','english_name' => 'Piano','icon' => 'static/img/icon-music-3.png')
        ));
    }
}
