<?php

use Illuminate\Database\Seeder;

class AddSampleVideo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            ['title_arabic' => 'test','title_english' => 'test','description_arabic' => 'test','description_english' => 'test','cover_image' => 'test','course_id' => '1','user_id' => '1','estimated_time' => '1','seo_description_english' => '1','seo_description_arabic' => '1','seo_keywords_english' => '1','seo_keywords_arabic' => '1','instrument_id' => '1','datetime' => '2','video_order' => '1','video_path' => '1','url_identifier' => '1','is_open' => '1','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00']
        ]);
    }
}
