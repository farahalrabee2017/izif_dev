<?php

use Illuminate\Database\Seeder;

class AddSampleCourse extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('courses')->insert([
            ['title_arabic' => 'سلسلة فيديوهات تمارين ريشة العود','title_english' => '','overview_arabic' => 'دورة تمارين الريشة على يد الاستاذ طارق الجندي هي ج','overview_english' => '','price' => '25','user_id' => '1','seo_description_arabic' => 'دورة تمارين الريشة هي جزء من دورة تمارين متقدمة في','seo_description_english' => '','seo_keywords_arabic' => 'ريشة العود','seo_keywords_english' => '','url_identifier' => 'عود/تمارين_الريشه','cover_image' => '','instrument_id' => '1','course_estimated_time' => '5','course_intro_video_path' => 'https://www.youtube.com/watch?v=oQszW2bX6Mk','course_order' => '2','name' => 'سلسلة فيديوهات تمارين ريشة العود','created_at' => '0000-00-00 00:00:00','updated_at' => '0000-00-00 00:00:00']
        ]);


    }
}
