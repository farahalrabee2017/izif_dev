<?php

use Illuminate\Database\Seeder;

class UserGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            ['name'=>'user'],
            ['name'=>'admin'],
            ['name'=>'teacher']
        ]);
    }
}
