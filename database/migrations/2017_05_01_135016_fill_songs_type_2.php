<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillSongsType2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::table('songs_type')->insert(
   array(
       'english_name' => 'pop',
       'arabic_name' =>  'الأغاني الحديثة',
   )
);
    DB::table('songs_type')->insert(
    array(
     'english_name' => 'folklore',
     'arabic_name' => 'مكتبة التراث العربي',
    )
    );
    DB::table('songs_type')->insert(
    array(
     'english_name' => 'all',
     'arabic_name' => 'كل الأغاني',
    )
    );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
