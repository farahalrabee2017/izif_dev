<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ara');
            $table->string('title_enu');
            $table->tinyInteger('is_premium');
            $table->integer('location_order');
            $table->string('display_price');
            $table->string('price');
            $table->integer('time_period_in_days');
            $table->tinyInteger('is_full_access');
            $table->tinyInteger('is_open_time_period');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscription_models');
    }
}
