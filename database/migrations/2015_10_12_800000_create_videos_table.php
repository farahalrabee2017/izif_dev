<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_arabic');
            $table->string('title_english');
            $table->string('description_arabic');
            $table->string('description_english');
            $table->string('cover_image');
            $table->integer('course_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('estimated_time');
            $table->string('seo_meta_title');
            $table->string('seo_description_english');
            $table->string('seo_description_arabic');
            $table->string('seo_keywords_english');
            $table->string('seo_keywords_arabic');
            $table->integer('instrument_id')->unsigned();
            $table->string('datetime');
            $table->string('video_order');
            $table->string('video_path');
            $table->string('url_identifier');
            $table->tinyInteger('is_open');
            $table->tinyInteger('is_youtube');
            $table->tinyInteger('is_active');
            $table->timestamps();


        });

        Schema::table('videos', function($table) {

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('course_id')
                ->references('id')
                ->on('courses');

            $table->foreign('instrument_id')
                ->references('id')
                ->on('instruments');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
