<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionContainerPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('pages_container', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_arabic');
            $table->string('title_english');
            $table->string('description_arabic');
            $table->string('description_english');
            $table->integer('section_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pages_container');
    }
}
