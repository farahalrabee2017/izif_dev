<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('courses', function (Blueprint $table) {

            $table->increments('id');
            $table->string('title_arabic');
            $table->string('title_english');
            $table->text('overview_arabic');
            $table->text('overview_english');
            $table->integer('price');
            $table->integer('before_price');
            $table->integer('user_id')->unsigned();
            $table->string('seo_meta_title');
            $table->text('seo_description_arabic');
            $table->text('seo_description_english');
            $table->string('seo_keywords_arabic');
            $table->string('seo_keywords_english');
            $table->string('url_identifier');
            $table->string('cover_image');
            $table->integer('instrument_id')->unsigned();
            $table->string('course_estimated_time');
            $table->string('course_intro_video_path');
            $table->string('course_order');
            $table->string('name');
            $table->tinyInteger('is_active');

            $table->timestamps();

        });


        Schema::table('courses', function($table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('instrument_id')
                ->references('id')
                ->on('instruments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::drop('courses');
    }
}