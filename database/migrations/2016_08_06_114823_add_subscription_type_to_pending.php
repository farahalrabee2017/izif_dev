<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionTypeToPending extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('pending_transactions', function (Blueprint $table) {
            $table->string('course_id')->nullable();
            $table->string('subscription_model_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('pending_transactions', function (Blueprint $table) {
            //
        });
    }

}
