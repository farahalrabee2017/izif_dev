<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->integer('user_group_id')->unsigned()->default(1);
            $table->string('referal_account')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->string('telephone')->nullable();
            $table->text('profile_image')->nullable();
            $table->string('about_ara')->nullable();
            $table->string('about_enu')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });

        Schema::table('users',function($table){
            $table->foreign('user_group_id')
                ->references('id')
                ->on('user_groups');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
