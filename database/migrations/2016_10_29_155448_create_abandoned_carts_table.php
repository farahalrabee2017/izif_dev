<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbandonedCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abandoned_carts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("user_id")->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->integer("subscription_model_id")->unsigned()->nullable();
            $table->foreign('subscription_model_id')
                ->references('id')
                ->on('subscription_models');

            $table->integer('course_id')->unsigned()->nullable();

            $table->string('payment_method')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('abandoned_carts');
    }
}
