<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillCourseTypeWithData2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      DB::table('course_type')->insert(
      array(
       'name' => 'izif_programs',
       'arabic_name' => 'برامج إعزف التعليمية',
      )
      );
      DB::table('course_type')->insert(
      array(
       'name' => 'izif_music',
       'arabic_name' => 'دورات تعليم الموسيقى مع مدرّسي فريق إعزف',
      )
      );

      DB::table('course_type')->insert(
      array(
       'name' => 'izif_friends_courses',
       'arabic_name' => 'دورات أصدقاء إعزف',
      )
      );
      }
  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
