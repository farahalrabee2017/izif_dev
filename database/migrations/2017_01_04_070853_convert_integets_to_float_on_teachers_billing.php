<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertIntegetsToFloatOnTeachersBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers_billing', function (Blueprint $table) {
            $table->float("amount")->change();
            $table->float("views_percentage")->change();
        });
    }

    /**
     * Reverse the migrations.
     *d
     * @return void
     */
    public function down()
    {
        Schema::table('teachers_billing', function (Blueprint $table) {
            //
        });
    }
}
