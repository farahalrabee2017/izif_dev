<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_status', function (Blueprint $table) {

            $table->increments('id');
            $table->integer("course_id")->unsigned();
            $table->string("status");
            $table->integer("user_id")->unsigned();
            $table->timestamps();

        });


        Schema::table('course_status', function($table) {

            $table->foreign('course_id')
                ->references('id')
                ->on('courses');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_status');
    }
}
