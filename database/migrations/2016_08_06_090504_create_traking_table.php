<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrakingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traking', function (Blueprint $table) {
            $table->increments('id');
            $table->string("utm_source");
            $table->string("utm_medium");
            $table->string("utm_campaign");
            $table->string("url");
            $table->string("ip");
            $table->string("user_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('traking');
    }
}
