<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers_billing', function (Blueprint $table) {

            $table->increments('id');

            $table->integer("amount");


            $table->integer("number_of_views");

            $table->integer("views_percentage");

            $table->text("views_list");

            $table->integer("teacher_id")->unsigned()->nullable();
            $table->foreign('teacher_id')
                ->references('id')
                ->on('users');

            $table->integer("user_id")->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->integer("subscription_id")->unsigned()->nullable();
            $table->foreign('subscription_id')
                ->references('id')
                ->on('subscriptions');

            $table->integer("subscription_model_id")->unsigned()->nullable();
            $table->foreign('subscription_model_id')
                ->references('id')
                ->on('subscription_models');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teachers_billing');
    }
}
