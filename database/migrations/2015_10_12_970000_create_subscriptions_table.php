<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('subscription_model_id')->unsigned();
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->integer('payment_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->timestamps();

        });

        Schema::table('subscriptions',function($table){

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('payment_id')
                ->references('id')
                ->on('payments');

            $table->foreign('course_id')
                ->references('id')
                ->on('courses');

            $table->foreign('subscription_model_id')
                ->references('id')
                ->on('subscription_models');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscriptions');
    }
}
