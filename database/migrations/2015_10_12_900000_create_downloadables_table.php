<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('downloadables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ara');
            $table->string('title_enu');
            $table->string('file_path_ara');
            $table->string('file_path_enu');
            $table->string('data_added');
            $table->integer('course_id')->unsigned();
            $table->integer('video_id')->unsigned();
            $table->timestamps();


        });

        Schema::table('downloadables',function($table){

            $table->foreign('course_id')
                ->references('id')
                ->on('courses');

            $table->foreign('video_id')
                ->references('id')
                ->on('videos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('downloadables');
    }
}
