<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataToCourseTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::table('course_type')->insert( array(
                                array('name' => 'izif_songs'),
                                array('name' => 'izif_programs'),
                                array('name' => 'izif_music'),
                                array('name' => 'izif_friends_courses'),
                        ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('course_type')->delete();
    }
}
