<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionModelFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_model_features', function (Blueprint $table) {

            $table->increments('id');
            $table->string('title_ara');
            $table->string('title_enu');
            $table->integer('subscription_model_id')->unsigned();
            $table->timestamps();

        });

        Schema::table('subscription_model_features',function($table){

            $table->foreign('subscription_model_id')
                ->references('id')
                ->on('subscription_models');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscription_model_features');
    }
}
